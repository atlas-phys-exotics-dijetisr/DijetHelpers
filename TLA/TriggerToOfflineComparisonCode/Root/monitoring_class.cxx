//
//  monitoring_class.cpp
//
//
//  Created by Artur Hahn on 19/09/14.
//  Contact: artur_hahn@icloud.com
//

#include "TriggerToOfflineComparisonCode/monitoring_class.h"

// Implemenation of constructor
monitoring_class::monitoring_class(TString out_fn)
: output_fn(out_fn), out_file(0) {
  
  double BinDx=(log(ptLimit)-log(pt_Cut))/NptBins;
  for (int i=0;i<=NptBins;++i){ PtBinEdges.push_back(exp(log(pt_Cut)+i*BinDx));}
  //Eta histogram binning:
  //vector<double>
  EtaBinEdges.resize(NetaBins+1);
  EtaBinEdges[0]=-etaCutoff;
  EtaBinEdges[1]=-3.2;
  EtaBinEdges[2]=-2.8;
  EtaBinEdges[3]=-2.1;
  EtaBinEdges[4]=-1.2;
  EtaBinEdges[5]=-0.8;
  EtaBinEdges[6]=0.0;
  EtaBinEdges[7]=0.8;
  EtaBinEdges[8]=1.2;
  EtaBinEdges[9]=2.1;
  EtaBinEdges[10]=2.8;
  EtaBinEdges[11]=3.2;
  EtaBinEdges[12]=etaCutoff;
  
  ptAxis = new TAxis(NptBins,&PtBinEdges[0]);
  etaAxis = new TAxis(NetaBins,&EtaBinEdges[0]);
}

int monitoring_class::findMatchIndex(TLorentzVector aJet,vector<TLorentzVector> bJets){
  double dRmin = 99;
  int best_match_jet=-1;
  
  for(unsigned int imjet=0; imjet<bJets.size(); ++imjet){
    TLorentzVector mJet = bJets[imjet];
    if(mJet.Pt()<=pt_Cut) continue;
    
    double dR = mJet.DeltaR(aJet);
    if(dR < dRmin){
      dRmin = dR;
      best_match_jet = imjet;
    }
  }
  
  if(dRmin < 0.4) return best_match_jet;
  return -1;
}

void monitoring_class::MakeTheHistos() {
  //Creating log pt bins for x-axis and y-axis:
  //---------------------
  double LowPtEdge = ptLowLimit;
  if(ptLowLimit <= 0.0){
    LowPtEdge = pt_Cut;
    std::cout << "Used bad value (ptLowLimit = " << ptLowLimit << " GeV) as starting point for log binning, so the pt-cut (" << pt_Cut << " GeV) was used!" << std::endl;
  }
  vector<double> xbins;
  double dx=(log(ptLimit)-log(LowPtEdge))/numLogPtXbins;
  for (int i=0;i<=numLogPtXbins;++i) xbins.push_back(exp(log(LowPtEdge)+i*dx));
  vector<double> ybins;
  double dy=(log(ptLimit)-log(LowPtEdge))/numLogPtYbins;
  for (int i=0;i<=numLogPtYbins;++i) ybins.push_back(exp(log(LowPtEdge)+i*dy));
  vector<double> xcoursebins;
  double dxcourse=(log(ptLimit)-log(LowPtEdge))/numCoursePtBins;
  for (int i=0;i<=numCoursePtBins;++i) xcoursebins.push_back(exp(log(LowPtEdge)+i*dxcourse));
  //---------------------
  
  out_file = new TFile(output_fn,"recreate");
  
  TH1::SetDefaultSumw2();
  TH2::SetDefaultSumw2();
  
  //Unbinned histograms
  h_inclusive_detector_unbinned_trig = new TH2D("inclusive_detector_unbinned_trig","Inclusive distribution of trigger jets in detector;#eta;#phi;total number of jets",numEtaBins,-5,5,numPhiBins,-TMath::Pi(),TMath::Pi());
  h_inclusive_detector_unbinned_reco = new TH2D("inclusive_detector_unbinned_reco","Inclusive distribution of offline jets in detector;#eta;#phi;total number of jets",numEtaBins,-5,5,numPhiBins,-TMath::Pi(),TMath::Pi());
  
  //Binned histograms
  //Spatial family:
  for (int iHistNum=0; iHistNum<NptBins; ++iHistNum){
    h_inclusive_detector_trig[iHistNum] = makeEtaPhiHisto(PtBinEdges[iHistNum],PtBinEdges[iHistNum+1],Form("inclusive_detector_trig_%i",iHistNum+1),"Inclusive spatial distribution of trigger jets");
    h_inclusive_detector_reco[iHistNum] = makeEtaPhiHisto(PtBinEdges[iHistNum],PtBinEdges[iHistNum+1],Form("inclusive_detector_reco_%i",iHistNum+1),"Inclusive spatial distribution of offline jets");
    unmatched_reco_jets_eta_phi_bins[iHistNum] = makeEtaPhiProfile(PtBinEdges[iHistNum],PtBinEdges[iHistNum+1],Form("unmatched_reco_jets_eta_phi_bins_%i",iHistNum+1),"Unmatched offline jet distribution");
    unmatched_trig_jets_eta_phi_bins[iHistNum] = makeEtaPhiProfile(PtBinEdges[iHistNum],PtBinEdges[iHistNum+1],Form("unmatched_trig_jets_eta_phi_bins_%i",iHistNum+1),"Unmatched trigger jet distribution");
    trig_minus_reco_eta_2d_bins[iHistNum] = makeResponse2DHisto(PtBinEdges[iHistNum],PtBinEdges[iHistNum+1],Form("trig_minus_reco_eta_2d_bins_%i",iHistNum+1),"Distribution of #eta deviation of trigger jets","eta");
    trig_minus_reco_eta_avrg[iHistNum] = makeMeanResponseProfile(PtBinEdges[iHistNum],PtBinEdges[iHistNum+1],Form("trig_minus_reco_eta_avrg_%i",iHistNum+1),"Mean #eta deviation of trigger jets","eta");
    trig_minus_reco_phi_2d_bins[iHistNum] = makeResponse2DHisto(PtBinEdges[iHistNum],PtBinEdges[iHistNum+1],Form("trig_minus_reco_phi_2d_bins_%i",iHistNum+1),"Distribution of #phi deviation of trigger jets","phi");
    trig_minus_reco_phi_avrg[iHistNum] = makeMeanResponseProfile(PtBinEdges[iHistNum],PtBinEdges[iHistNum+1],Form("trig_minus_reco_phi_avrg_%i",iHistNum+1),"Mean #phi deviation of trigger jets","phi");
  }
  //Pt family:
  for (int iHistNum=0; iHistNum<NetaBins; ++iHistNum){
    h_inclusive_pt_trig_binned[iHistNum] = make1DptLogBinnedHistogram(EtaBinEdges[iHistNum],EtaBinEdges[iHistNum+1],Form("h_inclusive_pt_trig_binned_%i",iHistNum+1),"Inclusive distribution of jet - p_{T}","p_{T} [GeV];total number of jets",&xbins[0]);
    h_inclusive_pt_reco_binned[iHistNum] = make1DptLogBinnedHistogram(EtaBinEdges[iHistNum],EtaBinEdges[iHistNum+1],Form("h_inclusive_pt_reco_binned_%i",iHistNum+1),"Inclusive distribution of jet - p_{T}","p_{T} [GeV];total number of jets",&xbins[0]);
    unmatched_reco_jets_1d_pt_binned[iHistNum] = make1DlogPtBinnedProfile(EtaBinEdges[iHistNum],EtaBinEdges[iHistNum+1],Form("unmatched_reco_jets_1d_pt_binned_%i",iHistNum+1),"Unmatched jet percentage",numLogPtXbins,&xbins[0],"Offline jet p_{T} [GeV];unmatched jet percentage");
    unmatched_trig_jets_1d_pt_binned[iHistNum] = make1DlogPtBinnedProfile(EtaBinEdges[iHistNum],EtaBinEdges[iHistNum+1],Form("unmatched_trig_jets_1d_pt_binned_%i",iHistNum+1),"Unmatched jet percentage",numLogPtXbins,&xbins[0],"Trigger jet p_{T} [GeV];unmatched jet percentage");
    matched_trig_jets_pt_2d_binned[iHistNum] = makeTrigRecoHisto(EtaBinEdges[iHistNum],EtaBinEdges[iHistNum+1],Form("matched_trig_jets_pt_2d_binned_%i",iHistNum+1),"Matched jet p_{T} distribution",&xbins[0],&ybins[0]); //for log binning, pass ,&xbins[0],&ybins[0] after title
    trig_over_reco_2d_pt_binned[iHistNum] = makeResponse2DHisto(EtaBinEdges[iHistNum],EtaBinEdges[iHistNum+1],Form("trig_over_reco_2d_pt_binned_%i",iHistNum+1),"p_{T} response of matched jets","pt",&xcoursebins[0]);
    trig_over_reco_pt_mean_binned[iHistNum] = makeMeanResponseProfile(EtaBinEdges[iHistNum],EtaBinEdges[iHistNum+1],Form("trig_over_reco_pt_mean_binned_%i",iHistNum+1),"Mean p_{T} response of matched jets","pt",&xcoursebins[0]);
  }
  gROOT->cd();
}//histograms created.
//----------------------------------------------------------------------------------

void monitoring_class::FillTheHistos(const vector<TLorentzVector> &trigJets, const vector<TLorentzVector> &recoJets, double weight){
  //Offline jet matching:
  
  for (unsigned int irjet=0; irjet<recoJets.size();++irjet){
    TLorentzVector recoJet = recoJets[irjet];
    int ptBin = ptAxis->FindBin(recoJet.Pt()) - 1; //gives 0 for first bin
    int etaBin = etaAxis->FindBin(recoJet.Eta()) - 1;
    if(ptBin >= NptBins || ptBin < 0){
      fatal(Form("Landed in a pT-bin outside the range! pT = %.3f, bin = %d",recoJet.Pt(),ptBin)); abort();
    }
    if(etaBin >= NetaBins || etaBin < 0){
      fatal("Landed in an eta-bin outside the range!"); abort();
    }
    
    //Fill inclusive distribution (reconstructing the detector):
    //----------------------------------------------------------
    h_inclusive_detector_unbinned_reco->Fill(recoJet.Eta(),recoJet.Phi(),weight);
    h_inclusive_detector_reco[ptBin]->Fill(recoJet.Eta(),recoJet.Phi(),weight);
    h_inclusive_pt_reco_binned[etaBin]->Fill(recoJet.Pt(),weight);
    //inclusive distributions filled.
    //----------------------------------------------------------
    
    int imatch = monitoring_class::findMatchIndex(recoJet,trigJets); //Find closest trigger jet
    bool unmatched = (imatch == -1);
    
    //Fill histograms with unmatched offline jets:
    //--------------------------------------------
    unmatched_reco_jets_eta_phi_bins[ptBin]->Fill(recoJet.Eta(),recoJet.Phi(),100.0*unmatched,weight);
    unmatched_reco_jets_1d_pt_binned[etaBin]->Fill(recoJet.Pt(),100.0*unmatched,weight);
    //--------------------------------------------
    
    if (unmatched) continue;
    
    //If a matching trigger jet was found, the match quality histograms are filled:
    TLorentzVector mtrigJet = trigJets[imatch];
    //If this offline jet was matched to a trigger jet, fill the quality histograms:
    //------------------------------------------------------------------------------
    //Eta-response histograms:
    trig_minus_reco_eta_avrg[ptBin]->Fill(recoJet.Eta(),(mtrigJet.Eta()-recoJet.Eta()),weight);
    trig_minus_reco_eta_2d_bins[ptBin]->Fill(recoJet.Eta(),(mtrigJet.Eta()-recoJet.Eta()),weight);
    //Phi-response histograms:
    trig_minus_reco_phi_avrg[ptBin]->Fill(recoJet.Phi(),(mtrigJet.DeltaPhi(recoJet)),weight);
    trig_minus_reco_phi_2d_bins[ptBin]->Fill(recoJet.Phi(),(mtrigJet.DeltaPhi(recoJet)),weight);
    //Pt-response histograms:
    trig_over_reco_pt_mean_binned[etaBin]->Fill(recoJet.Pt(),(mtrigJet.Pt()/recoJet.Pt()),weight);
    trig_over_reco_2d_pt_binned[etaBin]->Fill(recoJet.Pt(),(mtrigJet.Pt()/recoJet.Pt()),weight);
    //------------------------------------------------------------------------------
  }//offline jets matched and histograms filled.
  
  //Trigger jet matching:
  for (unsigned int itjet=0; itjet<trigJets.size();++itjet){
    TLorentzVector trigJet = trigJets[itjet];
    int ptBin = ptAxis->FindBin(trigJet.Pt()) - 1; // gives 0 for first bin
    int etaBin = etaAxis->FindBin(trigJet.Eta()) - 1;
    
    //Fill inclusive distribution (reconstructing the detector):
    //----------------------------------------------------------
    h_inclusive_detector_unbinned_trig->Fill(trigJet.Eta(),trigJet.Phi(),weight);
    h_inclusive_detector_trig[ptBin]->Fill(trigJet.Eta(),trigJet.Phi(),weight);
    h_inclusive_pt_trig_binned[etaBin]->Fill(trigJet.Pt(),weight);
    //inclusive distributions filled.
    //----------------------------------------------------------
    
    int imatch = monitoring_class::findMatchIndex(trigJet,recoJets); //Find closest offline jet
    bool unmatched = (imatch == -1);
    
    //Fill histograms with unmatched trigger jets:
    //--------------------------------------------
    unmatched_trig_jets_eta_phi_bins[ptBin]->Fill(trigJet.Eta(),trigJet.Phi(),100.0*unmatched,weight);
    unmatched_trig_jets_1d_pt_binned[etaBin]->Fill(trigJet.Pt(),100.0*unmatched,weight);
    //--------------------------------------------
    
    if (unmatched) continue;
    
    //If a matching trigger jet was found, the following is done:
    TLorentzVector mrecoJet = recoJets[imatch];
    //If this offline jet was matched to a trigger jet, fill the quality histograms:
    //------------------------------------------------------------------------------
    //Pt-matching 2D histograms:
    matched_trig_jets_pt_2d_binned[etaBin]->Fill(trigJet.Pt(),mrecoJet.Pt(),weight);
  }
}//Jet Matching and filling matched distribution histograms done.
//-----------------------------------------------------------------------------------

//POST PROCESSING
//Correct the ranges of the pt-histograms:
//----------------------------------------
void monitoring_class::FinalizeAndSaveOutput(){
  for (int iHistNum=0; iHistNum<6; ++iHistNum){
    double pTmax = ptLimit/cosh(EtaBinEdges[6+iHistNum]);
    
    h_inclusive_pt_trig_binned[6+iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    h_inclusive_pt_trig_binned[5-iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    h_inclusive_pt_reco_binned[6+iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    h_inclusive_pt_reco_binned[5-iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    matched_trig_jets_pt_2d_binned[6+iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    matched_trig_jets_pt_2d_binned[5-iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    matched_trig_jets_pt_2d_binned[6+iHistNum]->GetYaxis()->SetRangeUser(pt_Cut,pTmax);
    matched_trig_jets_pt_2d_binned[5-iHistNum]->GetYaxis()->SetRangeUser(pt_Cut,pTmax);
    unmatched_trig_jets_1d_pt_binned[6+iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    unmatched_trig_jets_1d_pt_binned[5-iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    unmatched_reco_jets_1d_pt_binned[6+iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    unmatched_reco_jets_1d_pt_binned[5-iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    trig_over_reco_2d_pt_binned[6+iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    trig_over_reco_2d_pt_binned[5-iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    trig_over_reco_pt_mean_binned[6+iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
    trig_over_reco_pt_mean_binned[5-iHistNum]->GetXaxis()->SetRangeUser(pt_Cut,pTmax);
  }
  printf("\n Will write output to \n     %s \n\n", out_file->GetName());
  out_file->Write();
  out_file->Close();
  delete out_file;
}
//----------------------------------------

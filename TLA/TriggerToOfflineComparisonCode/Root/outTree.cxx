#define outTree_cxx
//#include "/afs/cern.ch/user/a/adi/TLA/plotTrigger/TriggerToOfflineComparisonCode/TriggerToOfflineComparisonCode/monitoring_class.h"
//#include "/afs/cern.ch/user/a/adi/TLA/plotTrigger/TriggerToOfflineComparisonCode/TriggerToOfflineComparisonCode/outTree.h"
#include "TriggerToOfflineComparisonCode/monitoring_class.h"
#include "TriggerToOfflineComparisonCode/outTree.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TString.h>
#include <TProfile.h>
#include <TLatex.h>
#include <iostream>
#include <TLorentzVector.h>
#include <math.h>
#include <TMath.h>
#include <cmath>
#include <vector>
using std::vector;

void outTree::Loop(TString ofn, int DataEnergyTeV = 0)
{
//   In a ROOT session, you can do:
//      Root > .L outTree.C
//      Root > outTree t
//      Root > t.GetEntry(12); // Fill t data members with entry number 12
//      Root > t.Show();       // Show values of entry 12
//      Root > t.Show(16);     // Read and show values of entry 16
//      Root > t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
  if (fChain == 0) return;
  monitoring_class Monitor(ofn);
  //Calibration tool for offline jets
  bool use_calib_tool = false; //*******************************************
  //Monte Carlo weights (true for MC, false for data)
  bool use_weights = false;  //*********************************************  //-------------------------------------------
  // Turn off all but the branches we want to use:
  fChain->SetBranchStatus("*",0);
  fChain->SetBranchStatus("njets",1);
  fChain->SetBranchStatus("jet_E",1);
  fChain->SetBranchStatus("jet_pt",1);
  fChain->SetBranchStatus("jet_phi",1);
  fChain->SetBranchStatus("jet_eta",1);
  fChain->SetBranchStatus("jet_rapidity",1);
  fChain->SetBranchStatus("jet_Timing",1);
  if(use_calib_tool){ //missing "Eventshape_rhoKt4EM" "vxp_nTracks"
    fChain->SetBranchStatus("jet_emScalePt",1);
    fChain->SetBranchStatus("jet_emScaleE",1);
    fChain->SetBranchStatus("jet_emScaleEta",1);
    fChain->SetBranchStatus("jet_emScalePhi",1);
    fChain->SetBranchStatus("averageInteractionsPerCrossing",1);
    fChain->SetBranchStatus("jet_ActiveArea4vec_pt",1);
    fChain->SetBranchStatus("jet_ActiveArea4vec_eta",1);
    fChain->SetBranchStatus("jet_ActiveArea4vec_phi",1);
    fChain->SetBranchStatus("jet_ActiveArea4vec_m",1);
    fChain->SetBranchStatus("rhoEM",1);
  }
  //missing branches: "trig_EF_jet_calibtags" "trig_EF_jet_n"
  fChain->SetBranchStatus("trigjet_pt",1);
  fChain->SetBranchStatus("trigjet_phi",1);
  fChain->SetBranchStatus("trigjet_eta",1);
  fChain->SetBranchStatus("trigjet_m",1);
  fChain->SetBranchStatus("mcEventWeight",1);
  fChain->SetBranchStatus("jet_truth_pt",1);


  //Offline jet calibration setup:
  //  //---------------------------------
  //    // create the calibration object:
  //JetAnalysisCalib::JetCalibrationTool *m_JESTool;

  TString jetAlgo="AntiKt4TopoEM";  //Should be set to one of "AntiKt4TopoEM", "AntiKt4LCTopo", "AntiKt6TopoEM", "AntiKt6LCTopo"
  TString JES_config_file = "";
  if(DataEnergyTeV == 14){
    JES_config_file="RootCore/data/ApplyJetCalibration/CalibrationConfigs/muScan2013/Rel17.2_JES_muScan_25ns_80_80.config";
  }
  else if(DataEnergyTeV == 8){
    JES_config_file="RootCore/data/ApplyJetCalibration/CalibrationConfigs/JES_Full2012dataset_Preliminary_Jan13.config";
  }
  else{
    printf("Don't know which jet calibration to use (8 or 14 TeV), so now running without calibration!\n");
    use_calib_tool = false;
  }

  bool isData = true;
  if(use_calib_tool){
    //m_JESTool = new JetAnalysisCalib::JetCalibrationTool(jetAlgo,JES_config_file,isData);
  }
  //---------------------------------
  Monitor.MakeTheHistos();

  Long64_t nentries = fChain->GetEntries();

  std::cout << "Will run over " << nentries << " events" << std::endl;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;

    if (jentry && jentry%10000==0) {
      printf("Processed %.2fM / %.2fM, %6.2f%%\n",
             1e-6*jentry,1e-6*nentries,1e2*jentry/nentries);
      //printf("Eventshape rho is %6.2f\n",Eventshape_rhoKt4EM);
      printf("Eventshape rho is %6.2f\n",rhoEM);
      std::cout << std::flush;
    }
    //Load all turned on branches into memory
    fChain->GetEntry(jentry);
    //Setting weights for Monte Carlo simulation events
    double w = 1.0;
    //if (mcEventWeight->size()==0) fatal("Current event has no MC evt weight!");
    //if(use_weights) w = mcEventWeight->at(0).at(0);
    if(use_weights) w = mcEventWeight;


    //Load jets into vectors:
    //    //-----------------------------------------------------------------------
    vector<TLorentzVector> recoJets, trigJets;

    for (unsigned int irjet=0; irjet<jet_pt->size(); ++irjet)    {
      if (use_calib_tool) {
        double Eraw    = jet_emScaleE->at(irjet);
        double eta     = jet_emScaleEta->at(irjet);
        double phi     = jet_emScalePhi->at(irjet);
        double pt      = jet_emScalePt->at(irjet);
        double Apt     = jet_ActiveArea4vec_pt->at(irjet);
        double Aeta    = jet_ActiveArea4vec_eta->at(irjet);
        double Aphi    = jet_ActiveArea4vec_phi->at(irjet);
        double Am      = jet_ActiveArea4vec_m->at(irjet);
        double rho     = rhoEM;

        double mu = averageInteractionsPerCrossing;
        int npv = NPV; // count the number of vertices with 2 or more tracks

        //for (unsigned int ii=0; ii<vxp_nTracks->size(); ii++){
        //  if (vxp_nTracks->at(ii)>=2) NPV++;
        //}

	TLorentzVector activeArea4vec;
	activeArea4vec.SetPtEtaPhiM(Apt,Aeta,Aphi,Am);
	double Ax = activeArea4vec.Px();
	double Ay = activeArea4vec.Py();
	double Az = activeArea4vec.Pz();
	double Ae = activeArea4vec.E();
        //TLorentzVector jet = m_JESTool->ApplyJetAreaOffsetEtaJES(Eraw,eta,phi,m,Ax,Ay,Az,Ae,rho,mu,npv);
        //jet *= 0.001;

        //if(jet.Pt()<=monitoring_class::pt_Cut) continue;
        //recoJets.push_back(jet);
      } else {
        TLorentzVector jet;
        //jet.SetPtEtaPhiM(jet_AntiKt4TopoEM_pt->at(irjet)*1e-3,jet_AntiKt4TopoEM_eta->at(irjet),
        //                 jet_AntiKt4TopoEM_phi->at(irjet),jet_AntiKt4TopoEM_m->at(irjet)*1e-3);
        jet.SetPtEtaPhiE(jet_pt->at(irjet),jet_eta->at(irjet),
                         jet_phi->at(irjet),jet_E->at(irjet));
        if(jet.Pt()<=monitoring_class::pt_Cut) continue;
        recoJets.push_back(jet);
      }
    }

    double truth_pt = 999.0;
    //if (AntiKt4Truth_pt->size()) truth_pt = AntiKt4Truth_pt->at(0)/1000;
    if (jet_truth_pt->size()) {
	truth_pt = jet_truth_pt->at(0);
	cout<<"PT TRUTH "<<jet_truth_pt->at(0)<<endl;
	}

    double pTavg=0;
    if (recoJets.size()>1) pTavg=(recoJets[0].Pt()+recoJets[1].Pt())/2;
    else if (recoJets.size()==1) pTavg=recoJets[0].Pt(); //in rare cases...

    if ( pTavg / truth_pt > 1.4 ) continue;
    //for (unsigned int itjet=0; itjet<trig_EF_jet_pt->size(); ++itjet){
    for (unsigned int itjet=0; itjet<trigjet_pt->size(); ++itjet){
      //if (trig_EF_jet_calibtags->at(itjet) != "AntiKt4_topo_calib_EMJES")
      //  continue;
      TLorentzVector jet;
      //jet.SetPtEtaPhiM(trig_EF_jet_pt->at(itjet)*1e-3,trig_EF_jet_eta->at(itjet),
      //                 trig_EF_jet_phi->at(itjet),trig_EF_jet_m->at(itjet)*1e-3);
      jet.SetPtEtaPhiM(trigjet_pt->at(itjet),trigjet_eta->at(itjet),
                       trigjet_phi->at(itjet),trigjet_m->at(itjet));
      if(jet.Pt()<=monitoring_class::pt_Cut) continue;
      trigJets.push_back(jet);
    }//Loaded jets into vectors.
    //-----------------------------------------------------------------------
    Monitor.FillTheHistos(trigJets,recoJets,w);
  } //Looped over all events.

  Monitor.FinalizeAndSaveOutput();

  printf("   Finished.\n");

}

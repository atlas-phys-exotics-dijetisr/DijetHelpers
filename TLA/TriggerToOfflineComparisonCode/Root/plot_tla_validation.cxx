//
//
//  Created by Artur Hahn on 25/09/14.
//  Contact: artur_hahn@icloud.com
//

#include "TriggerToOfflineComparisonCode/make_tla_plots.h"

TH1D *Get1DHist(TFile *f, TString hn) {
  return (TH1D*)f->Get(hn);
}

TProfile *GetProfile(TFile *f, TString hn) {
  return (TProfile*)f->Get(hn);
}

TH2D *Get2DHist(TFile *f, TString hn) {
  return (TH2D*)f->Get(hn);
}

//Method: Make percentile 2D histogram out of normal 2D histogram:
//--------------------------------------------------------------------------
TH2D* makePercentileDistribution(TH2D* in_hist){
  TH2D *Hist = (TH2D*)in_hist->Clone();
  int lastYbin = Hist->GetNbinsY();
  for(int xbin=1;xbin<=Hist->FindLastBinAbove(0,1);++xbin){
    double integratedEntriesForXbin = 0;
    for(int ybin=0;ybin<=lastYbin+1;++ybin){
      integratedEntriesForXbin += Hist->GetBinContent((Hist->GetBin(xbin,ybin)));
    }
    for(int ybin=0;ybin<=lastYbin+1;++ybin){
      if(Hist->GetBinContent(Hist->GetBin(xbin,ybin)) == 0) continue;
      Hist->SetBinContent(xbin,ybin,100*((Hist->GetBinContent(Hist->GetBin(xbin,ybin)))/integratedEntriesForXbin));
    }
  }
  Hist->GetZaxis()->SetRangeUser(0.0,100.0);
  return Hist;
}
//--------------------------------------------------------------------------
void Draw1DHist(TFile *f, TString hn, int col=kBlack, TString opt = "", double xOffSet=1.3, double yOffSet=1.0, int LogOpt = 0, int UnmatchedOpt = 0) {
  //Get the histo
  TH1D *h = Get1DHist(f,hn);
  
  TAxis *xAx = h->GetXaxis();
  xAx->SetTitleOffset(xOffSet);
  if(LogOpt == 1){
    gPad->SetLogx(1);
    xAx->SetMoreLogLabels(1);
  }
  if(UnmatchedOpt == 1){
    h->GetYaxis()->SetRangeUser(0.0,100.0);
  }
  h->GetYaxis()->SetTitleOffset(yOffSet);
  h->SetLineColor(col);
  h->Draw(opt);
  h->Draw("e same");
}

void Draw1Dstack(TFile *f, TString hnReco, TString hnTrig, double xOffSet=1.3, double yOffSet=1.2) {
  THStack *stack = new THStack("stack","");
  TH1D *h1 = Get1DHist(f,hnReco);
  h1->SetLineColor(kBlue);
  int ptMaxBin = h1->GetXaxis()->GetLast();
  TH1D *h2 = Get1DHist(f,hnTrig);
  h2->SetLineColor(kRed);
  stack->Add(h1);
  stack->Draw("nostack e");
  TAxis *xAx = stack->GetXaxis();
  TAxis *yAx = stack->GetYaxis();
  xAx->SetRange(1,ptMaxBin);
  xAx->SetTitle(h1->GetXaxis()->GetTitle());
  xAx->SetTitleOffset(xOffSet);
  xAx->SetMoreLogLabels();
  yAx->SetTitle(h1->GetYaxis()->GetTitle());
  yAx->SetTitleOffset(yOffSet);
  stack->SetMinimum(1);
  stack->Draw("nostack hist same");
  stack->Add(h2);
  stack->SetTitle(h1->GetTitle());
  stack->Draw("nostack e same");
}

void Draw1DProfile(TFile *f, TString hn, int col=kBlack, TString opt = "", double yOffSet=1.0) {
  //Get the histo
  TProfile *h = GetProfile(f,hn);
  
  h->SetLineColor(col); h->SetMarkerColor(col); h->SetMarkerSize(1.0); h->SetMarkerStyle(1);
  h->SetLineWidth(2);
  h->GetYaxis()->SetTitleOffset(yOffSet);
  
  h->Draw(opt);
  h->Draw("e x0 same");
}

TH2D* Draw2DHist(TFile *f, TString hn, TString opt = "", int percentileOpt = 0, double xOffSet = 1.0, double yOffSet = 1.0, int MoreLogLabels = 0, int UnmatchedOpt = 0) {
  //Get the histo
  TH2D *h = Get2DHist(f,hn);
  if (h==NULL) { cout << hn << endl; abort(); }
  
  TAxis *xAx = h->GetXaxis();
  TAxis *yAx = h->GetYaxis();
  xAx->SetTitleOffset(xOffSet);
  yAx->SetTitleOffset(yOffSet);
  
  if(UnmatchedOpt == 1){
    h->GetZaxis()->SetRangeUser(0.0,100.0);
  }
  
  if(MoreLogLabels == 1){
    xAx->SetMoreLogLabels();
    yAx->SetMoreLogLabels();
  }
  
  if(percentileOpt == 1){
    h = makePercentileDistribution(h);
    h->SetTitle("Percentile match distribution (along y-axis)");
    //   h->GetXaxis()->SetTitleOffset(xOffSet);
    //   h->GetYaxis()->SetTitleOffset(yOffSet);
    h->Draw(opt);
    return h;
  }
  if(percentileOpt == 2){
    h = makePercentileDistribution(h);
    //   h->GetXaxis()->SetTitleOffset(xOffSet);
    //   h->GetYaxis()->SetTitleOffset(yOffSet);
    h->Draw(opt);
    return h;
  }
  else {
    h->Draw(opt);
    return h;
  }
}

void DrawEtaLinesOn2DHisto(int HistType = 1){
  //vector<double> RangeY(2,0.0);
  //RangeY[0] =
  double etaLines[] = {-4.5, -3.2, -2.8, -2.1, -1.2, -0.8, 0.0, 0.8, 1.2, 2.1, 2.8, 3.2, 4.5};
  unsigned int n = sizeof(etaLines)/sizeof(etaLines[0]);
  
  if(HistType == 1){ //Histogram type: eta-phi-2D
    for(unsigned int iline=0;iline<n;++iline){
      TLine *etaLine = new TLine(etaLines[iline],-TMath::Pi(),etaLines[iline],TMath::Pi());
      etaLine->SetLineStyle(10);
      etaLine->SetLineColor(kBlack);
      etaLine->Draw();
    }
  }
  else if(HistType == 2){ //Histogram type: eta-response-2D
    gPad->Update();
    double yLow = gPad->GetFrame()->GetY1();
    double yHigh = gPad->GetFrame()->GetY2();
    for(unsigned int iline=0;iline<n;++iline){
      TLine *etaLine = new TLine(etaLines[iline],yLow,etaLines[iline],yHigh);
      etaLine->SetLineStyle(5);
      etaLine->SetLineColor(kBlack);
      etaLine->Draw();
    }
  }
  else {printf("Don't know how to draw Eta-Lines on histogram type %i\n",HistType);}
}

void DrawDiagonalLineOn2DPlot(TH2D* h){
  TAxis* xAx = h->GetXaxis();
  TAxis* yAx = h->GetYaxis();
  TFrame* frame = gPad->GetFrame();
  TLine *diagLine = new TLine(xAx->GetXmin(),yAx->GetXmin(),xAx->GetXmax(),yAx->GetXmax());
  diagLine->SetLineStyle(7);
  diagLine->SetLineColor(kGray+3);
  diagLine->Draw();
}

void PutYOverFlowContentInRange(TH2D* h){
  int NxBins=h->GetNbinsX(), NyBins = h->GetNbinsY();
  for(int xbins=1;xbins<=NxBins;++xbins){
    h->AddBinContent(h->GetBin(xbins,NyBins),h->GetBinContent(xbins,NyBins+1));
    h->AddBinContent(h->GetBin(xbins,1),h->GetBinContent(xbins,0));
  }
  h->Draw("colz same");
}

TLatex *tex;

void DrawLabels(TString txt = "") {
  tex->SetTextColor(kBlack); tex->SetTextAlign(32);
  
  tex->DrawLatex(0.75,0.9,txt);
  
  tex->SetTextColor(kBlue);
  tex->DrawLatex(0.85,0.85,"Offline jets");
  
  tex->SetTextColor(kRed);
  tex->DrawLatex(0.85,0.8,"Trigger jets");
}

void DrawLabel2D(TString txt) {
  tex->SetTextColor(kBlack); tex->SetTextAlign(32);
  tex->SetTextSize(0.032);
  tex->DrawLatex(0.75,0.9,txt);
  tex->SetTextSize(0.05);
  
}

void plot_tla_validation(TString ifn, TString ofs) {
  //Number of bins for binned histograms:
  int NptBins = 10;//*******************************************************
  int NetaBins = 12;//******************************************************
  double etaCutoff = 4.5; //************************************************
  
  // create text object
  tex= new TLatex(); tex->SetNDC(); tex->SetTextFont(42); tex->SetTextSize(0.05);
  
  // turn off the histogram title!!
  //gStyle->SetOptTitle(0);
  //Make pretty palette:
  //--------------------
  gStyle->SetOptStat(0);
  gStyle->SetPalette(1);
  bool darkBkg=true;//******************************************************
  int avgColor = (darkBkg ? kMagenta : kBlack );
  if (darkBkg) {
    const Int_t NRGBs = 5;
    const Int_t NCont = 255;
    
    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.75 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
  }
  //--------------------
  
  TFile *f = TFile::Open(Form("%s",ifn.Data()));
  
  TCanvas *can = new TCanvas("can","can",1200,700);
  //   can->SetMargin(0.12,0.04,0.15,0.04);
  //  TString truth_pt_pdf=Form("plots_truth_pt_%s.pdf",ofs.Data());
  TString incl_eta_phi_pdf=Form("plots_incl_eta_phi_%s.pdf",ofs.Data());
  TString incl_pt_pdf=Form("plots_incl_pt_%s.pdf",ofs.Data());
  TString unmatched_jet_eta_phi_pdf=Form("plots_unmatched_jet_eta_phi_%s.pdf",ofs.Data());
  TString unmatched_jet_pt_pdf=Form("plots_unmatched_jet_pt_%s.pdf",ofs.Data());
  TString eta_response_pdf=Form("plots_response_eta_%s.pdf",ofs.Data());
  TString phi_response_pdf=Form("plots_response_phi_%s.pdf",ofs.Data());
  TString pt_response_pdf=Form("plots_response_pt_%s.pdf",ofs.Data());
  TCanvas *divcan = new TCanvas("divcan","divcan",1600,600);//?????????
  divcan->Divide(2,1);//?????????
  TString matched_jet_pt_pdf=Form("plots_matched_trigvsreco_pt_%s.pdf",ofs.Data());//?????????
  
  //Open the pdf files:
  can->cd();//?????????
  //  can->Print(truth_pt_pdf+"[");
  can->Print(incl_eta_phi_pdf+"[");
  can->Print(incl_pt_pdf+"[");
  can->Print(unmatched_jet_eta_phi_pdf+"[");
  can->Print(unmatched_jet_pt_pdf+"[");
  can->Print(eta_response_pdf+"[");
  can->Print(phi_response_pdf+"[");
  can->Print(pt_response_pdf+"[");
  divcan->cd();//?????????
  divcan->Print(matched_jet_pt_pdf+"[");//?????????
  
  //Adding pages to the spatial pdf:
  //---------------------------------------------------------------------
  //Unbinned Plots:
  //---------------
  can->cd();//?????????
  //Truth pt histograms:
  //  gPad->SetLogy(1);
  //  Draw1DHist(f,"h_truth_pt_logbins",kGreen+2,"hist",1.3,1.2,1);
  //  can->Print(truth_pt_pdf);
  //  Draw1DHist(f,"h_truth_pt_logbins_noweight",kGreen+2,"hist",1.3,1.2,1);
  //  can->Print(truth_pt_pdf);
  gPad->SetLogx(0);
  //  gPad->SetLogy(0);
  
  //Inclusive distributions:
  Draw2DHist(f,"inclusive_detector_unbinned_reco","colz");
  DrawEtaLinesOn2DHisto();
  can->Print(incl_eta_phi_pdf);
  Draw2DHist(f,"inclusive_detector_unbinned_trig","colz");
  DrawEtaLinesOn2DHisto();
  can->Print(incl_eta_phi_pdf);
  
  //Binned Plots:
  //-------------
  //Spatial family:
  for (int ipt=1;ipt<=NptBins;++ipt) {
    TString suffix="_"; suffix+=ipt;
    //Inclusive distributions
    Draw2DHist(f,"inclusive_detector_reco"+suffix,"colz",0,1.0,1.0);
    DrawEtaLinesOn2DHisto();
    can->Print(incl_eta_phi_pdf);
    Draw2DHist(f,"inclusive_detector_trig"+suffix,"colz",0,1.0,1.0);
    DrawEtaLinesOn2DHisto();
    can->Print(incl_eta_phi_pdf);
    //Unmatched distributions:
    Draw2DHist(f,"unmatched_reco_jets_eta_phi_bins"+suffix,"colz",0,1.0,1.0,0,1);
    DrawEtaLinesOn2DHisto();
    can->Print(unmatched_jet_eta_phi_pdf);
    Draw2DHist(f,"unmatched_trig_jets_eta_phi_bins"+suffix,"colz",0,1.0,1.0,0,1);
    DrawEtaLinesOn2DHisto();
    can->Print(unmatched_jet_eta_phi_pdf);
    //Response plots:
    gPad->SetGridy(1);
    //Eta
    TH2D* ResponseHisto = Draw2DHist(f,"trig_minus_reco_eta_2d_bins"+suffix,"colz",2,1.0,1.2);
    PutYOverFlowContentInRange(ResponseHisto);
    DrawEtaLinesOn2DHisto(2);
    Draw1DProfile(f,"trig_minus_reco_eta_avrg"+suffix,avgColor,"same x0",1.2);
    can->Print(eta_response_pdf);
    //Phi
    ResponseHisto = Draw2DHist(f,"trig_minus_reco_phi_2d_bins"+suffix,"colz",2,1.0,1.2);
    PutYOverFlowContentInRange(ResponseHisto);
    Draw1DProfile(f,"trig_minus_reco_phi_avrg"+suffix,avgColor,"same x0",1.2);
    can->Print(phi_response_pdf);
    
    gPad->SetGridy(0);
    //      DrawLabels("Jet #it{p}_{T} for |#eta|-bin "+suffix+Form(", %.1f #leq |#eta| < %.1f",ptBins[ieta],ptBins[ieta+1]));
  } //binned spatial family plotted.
  //Pt family:
  gPad->SetLogx(1);
  for (int ieta=1;ieta<=NetaBins;++ieta) {
    TString suffix="_"; suffix+=ieta;
    //Inclusive distributions:
    Draw1Dstack(f,"h_inclusive_pt_reco_binned"+suffix,"h_inclusive_pt_trig_binned"+suffix,1.3,1.4);
    DrawLabels();
    gPad->SetLogy(1);
    can->Print(incl_pt_pdf);
    gPad->SetLogy(0);
    //Unmatched distributions:
    Draw1DHist(f,"unmatched_reco_jets_1d_pt_binned"+suffix,kBlue,"hist",1.3,1.0,1,1);
    Draw1DHist(f,"unmatched_trig_jets_1d_pt_binned"+suffix,kRed,"hist same",1.3,1.0,1,1);
    DrawLabels();
    can->Print(unmatched_jet_pt_pdf);
    //Matched trig vs. reco plots:
    divcan->cd(1);//?????????v
    gPad->SetLogx(1);
    gPad->SetLogy(1);
    gPad->SetLogz(1);
    TH2D* tempHist1 = Draw2DHist(f,"matched_trig_jets_pt_2d_binned"+suffix,"colz",0,1.4,1.5,1);//absolute
    DrawDiagonalLineOn2DPlot(tempHist1);
    divcan->cd(2);
    gPad->SetLogx(1);
    gPad->SetLogy(1);
    TH2D* tempHist2 = Draw2DHist(f,"matched_trig_jets_pt_2d_binned"+suffix,"colz",1,1.4,1.5,1);//percentile
    DrawDiagonalLineOn2DPlot(tempHist2);
    divcan->Print(matched_jet_pt_pdf);//?????????
    //Response plots:
    can->cd();//?????????
    gPad->SetGridy(1);
    gPad->SetLogx(1);
    gPad->SetLogy(0);
    //    gPad->SetLogz(1);
    TH2D* ResponseHisto = Draw2DHist(f,"trig_over_reco_2d_pt_binned"+suffix,"colz",2,1.4,1.2,1);
    PutYOverFlowContentInRange(ResponseHisto);
    Draw1DProfile(f,"trig_over_reco_pt_mean_binned"+suffix,avgColor,"same x0");
    can->Print(pt_response_pdf);
    gPad->SetGridy(0);
  } //binned pt family plotted.
  //---------------
  //---------------------------------------------------------------------
  //Closing pdfs:
  //  can->Print(truth_pt_pdf+"]");
  can->Print(incl_eta_phi_pdf+"]");
  can->Print(incl_pt_pdf+"]");
  can->Print(unmatched_jet_eta_phi_pdf+"]");
  can->Print(unmatched_jet_pt_pdf+"]");
  can->Print(eta_response_pdf+"]");
  can->Print(phi_response_pdf+"]");
  can->Print(pt_response_pdf+"]");
  divcan->Print(matched_jet_pt_pdf+"]");//?????????
}
TH1D *Get1DHist(TFile *f, TString hn) {
   return (TH1D*)f->Get(hn);
}

TProfile *GetProfile(TFile *f, TString hn) {
   return (TProfile*)f->Get(hn);
}

TH2D *Get2DHist(TFile *f, TString hn) {
   return (TH2D*)f->Get(hn);
}

//Method: Make percentile 2D histogram out of normal 2D histogram:
//--------------------------------------------------------------------------
TH2D* makePercentileDistribution(TH2D* in_hist){
   TH2D *Hist = (TH2D*)in_hist->Clone();
	for(int xbin=1;xbin<=Hist->FindLastBinAbove(0,1);++xbin){
		int integratedEntriesForXbin = 0;
		for(int ybin=1;ybin<=Hist->FindLastBinAbove(0,2);++ybin){
			integratedEntriesForXbin += Hist->GetBinContent((Hist->GetBin(xbin,ybin)));
		}
		for(int ybin=1;ybin<=Hist->FindLastBinAbove(0,2);++ybin){
			if(Hist->GetBinContent(Hist->GetBin(xbin,ybin)) == 0) continue;
			Hist->SetBinContent(xbin,ybin,100*((Hist->GetBinContent(Hist->GetBin(xbin,ybin)))/integratedEntriesForXbin));
		}
	}
	Hist->GetZaxis()->SetRange(0.0,100.0);
	return Hist;
}
//--------------------------------------------------------------------------
void Draw1DHist(TFile *f, TString hn, int col=kBlack, TString opt = "") {
   //Get the histo
   TH1D *h = Get1DHist(f,hn);
   
//	h->GetYaxis()->SetTitleOffset(1.4);
	h->SetLineColor(col);
	h->Draw(opt);
   h->Draw("e same");
}

void Draw1DProfile(TFile *f, TString hn, int col=kBlack, TString opt = "") {
   //Get the histo
   TProfile *h = GetProfile(f,hn);
   
//	h->GetYaxis()->SetTitleOffset(1.4);
	h->SetLineColor(col); h->SetMarkerColor(col); h->SetMarkerSize(1.0); h->SetMarkerStyle(27);
	h->SetLineWidth(2);
   h->Draw(opt);
   h->Draw("e X0 same");
}

void Draw2DHist(TFile *f, TString hn, TString opt = "", int percentileOpt = 0, double yOffSet = 1.4) {
   //Get the histo
   cout<<"adi draw0"<<endl;
   TH2D *h = Get2DHist(f,hn);
   cout<<"adi draw1"<<endl;
   if (h==NULL) { cout << hn << endl; abort(); }
//	h->GetYaxis()->SetTitleOffset(yOffSet);
   
   cout<<"adi draw2"<<endl;
   if(percentileOpt == 1){
   	h = makePercentileDistribution(h);
   	h->SetTitle("Percentile distribution");
   	h->Draw(opt);
        cout<<"adi draw20"<<endl;
   }
   if(percentileOpt == 2){
   	h = makePercentileDistribution(h);
   	h->Draw(opt);
        cout<<"adi draw21"<<endl;
   }
   else h->Draw(opt);
   cout<<"adi draw3"<<endl;
}

TLatex *tex;

void DrawLabels(TString txt = "") {
   tex->SetTextColor(kBlack); tex->SetTextAlign(32);

   tex->DrawLatex(0.75,0.9,txt);

   tex->SetTextColor(kBlue);
   tex->DrawLatex(0.75,0.85,"Offline jets");

   tex->SetTextColor(kRed);
   tex->DrawLatex(0.75,0.8,"Trigger jets");
}

void DrawLabel2D(TString txt) {
   tex->SetTextColor(kBlack); tex->SetTextAlign(32);
   tex->SetTextSize(0.032);
   tex->DrawLatex(0.75,0.9,txt);
   tex->SetTextSize(0.05);

}

void plot_tla_validation() {
    cout<<"adi 1"<<endl;
	//Number of bins for binned histograms:
	int NptBins = 12;	//*******************************************************************************************************
	int NetaBins = 12;	//****************************************************************************************************
	double etaCutoff = 4.5; //**************************************************************************************************
	
//	double ptBins[] = {10.0, 40.0, 60.0, 1000.0};	//*************************************************************************
//	double etaBins[] = {-5.0, -3.2, -2.8, -2.1, -1.2, -0.8, 0.0, 0.8, 1.2, 2.1, 2.8, 3.2, 5.0};	//change 5 to etaCufoff*******
	
   // create text object
   tex= new TLatex(); tex->SetNDC(); tex->SetTextFont(42); tex->SetTextSize(0.05);

    cout<<"adi 2"<<endl;
   // turn off the histogram title!!
//	gStyle->SetOptTitle(0);
   //Make pretty palette:
   //--------------------
   gStyle->SetOptStat(0);
   gStyle->SetPalette(1);
   bool darkBkg=false;	//***************************************************************************************************
   int avgColor = (darkBkg ? kWhite : kBlack );
   if (darkBkg) {
    const Int_t NRGBs = 5;
    const Int_t NCont = 255;

    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
   }   
   //--------------------
   
    cout<<"adi 3"<<endl;
   //TFile *f = TFile::Open("tla_val_14TeV_JZ1W_1.root");
   TFile *f = TFile::Open("data15_13TeV.00271744.physics_Main.Nominal.TrigJet_monitoringPlots.root");
   
   TCanvas *can = new TCanvas("can","can",800,600);
//   can->SetMargin(0.12,0.04,0.12,0.04);
   TString incl_eta_phi_pdf="plots_incl_eta_phi.pdf";
   TString incl_pt_pdf="plots_incl_pt.pdf";
   TString unmatched_jet_eta_phi_pdf="plots_unmatched_jet_eta_phi.pdf";
   TString unmatched_jet_pt_pdf="plots_unmatched_jet_pt.pdf";
   TString eta_response_pdf="plots_response_eta.pdf";
   TString phi_response_pdf="plots_response_phi.pdf";
   TString pt_response_pdf="plots_response_pt.pdf";
   TCanvas *divcan = new TCanvas("divcan","divcan",1600,600);														//?????????
   divcan->Divide(2,1);																							//?????????
   TString matched_jet_eta_pdf="plots_matched_trigvsreco_eta.pdf";//??
   TString matched_jet_phi_pdf="plots_matched_trigvsreco_phi.pdf";//??
   TString matched_jet_pt_pdf="plots_matched_trigvsreco_pt.pdf";//??
   
    cout<<"adi 4"<<endl;
   //Open the pdf files:
   can->cd();																									//?????????
   can->Print(incl_eta_phi_pdf+"[");
	can->Print(incl_pt_pdf+"[");
	can->Print(unmatched_jet_eta_phi_pdf+"[");
	can->Print(unmatched_jet_pt_pdf+"[");
	can->Print(eta_response_pdf+"[");
	can->Print(phi_response_pdf+"[");
	can->Print(pt_response_pdf+"[");
	divcan->cd();																									//?????????
	divcan->Print(matched_jet_eta_pdf+"[");																							//?????????
	divcan->Print(matched_jet_phi_pdf+"[");																							//?????????
	divcan->Print(matched_jet_pt_pdf+"[");																							//?????????
	
	//Adding pages to the spatial pdf:
	//---------------------------------------------------------------------
	//Unbinned Plots:
	//---------------
	//Inclusive distributions:
	can->cd();																							//?????????
	Draw2DHist(f,"inclusive_detector_unbinned_reco","colz");
   can->Print(incl_eta_phi_pdf);
   Draw2DHist(f,"inclusive_detector_unbinned_trig","colz");
   can->Print(incl_eta_phi_pdf);
//   Draw1DHist(f,"h_inclusive_pt_unbinned_reco",kBlue,"hist");
//   Draw1DHist(f,"h_inclusive_pt_unbinned_trig",kRed,"hist same");
//   DrawLabels();
//   can->Print(incl_pt_pdf);
   //Unmatched distributions:
//   Draw2DHist(f,"unmatched_reco_jets_eta_phi_2d","colz");
//   can->Print(unmatched_jet_eta_phi_pdf);
//   Draw2DHist(f,"unmatched_trig_jets_eta_phi_2d","colz");
//   can->Print(unmatched_jet_eta_phi_pdf);
   //Matched trig vs. reco plots:
   //Eta
/*   divcan->cd(1);
   Draw2DHist(f,"matched_trig_jets_absolute_eta_2d_total","colz");	//absolute
   divcan->cd(2);
   Draw2DHist(f,"matched_trig_jets_absolute_eta_2d_total","colz",1);	//percentile
   divcan->Print(matched_jet_eta_pdf);
   //Phi:
   divcan->cd(1);
	Draw2DHist(f,"matched_trig_jets_absolute_phi_2d_total","colz");
   divcan->cd(2);
   Draw2DHist(f,"matched_trig_jets_absolute_phi_2d_total","colz",1);
   divcan->Print(matched_jet_phi_pdf);
*/
   //Response plots:
//   can->cd();
    cout<<"adi 5"<<endl;
   //gPad->SetGridy(1);
    cout<<"adi 6"<<endl;
	//Eta:
    //Draw2DHist(f,"trig_minus_reco_eta_2d","colz",2);
    cout<<"adi 7"<<endl;
    //Draw1DProfile(f,"trig_minus_reco_eta_mean",avgColor,"same x0 P");
    cout<<"adi 8"<<endl;
    //can->Print(eta_response_pdf);
    cout<<"adi 9"<<endl;
   //Phi
   //Draw2DHist(f,"trig_minus_reco_phi_2d","colz",2);
	//Draw1DProfile(f,"trig_minus_reco_phi_mean",avgColor,"same x0");
   //can->Print(phi_response_pdf);
   gPad->SetGridy(0);
   //---------------
	//Binned Plots:
	//-------------
	//Spatial family:
   for (int ipt=1;ipt<=NptBins;++ipt) {
      if (ipt >=11) continue;
      TString suffix="_"; suffix+=ipt;
      //Inclusive distributions
      Draw2DHist(f,"inclusive_detector_reco"+suffix,"colz");
   	can->Print(incl_eta_phi_pdf);
      Draw2DHist(f,"inclusive_detector_trig"+suffix,"colz");
      can->Print(incl_eta_phi_pdf);
   	//Unmatched distributions:
   	Draw2DHist(f,"unmatched_reco_jets_eta_phi_bins"+suffix,"colz");
      can->Print(unmatched_jet_eta_phi_pdf);
   	Draw2DHist(f,"unmatched_trig_jets_eta_phi_bins"+suffix,"colz");
   	can->Print(unmatched_jet_eta_phi_pdf);
      //Matched trig vs. reco plots:
   	//Eta
   	//divcan->cd(1);
   	//Draw2DHist(f,"matched_trig_jets_absolute_eta_2d"+suffix,"colz");		//absolute
   	//divcan->cd(2);
   	//Draw2DHist(f,"matched_trig_jets_absolute_eta_2d"+suffix,"colz",1);	//percentile
   	//divcan->Print(matched_jet_eta_pdf);
   	//Phi
   	//divcan->cd(1);
        //Draw2DHist(f,"matched_trig_jets_absolute_phi_2d"+suffix,"colz");
   	//divcan->cd(2);
   	//Draw2DHist(f,"matched_trig_jets_absolute_phi_2d"+suffix,"colz",1);
   	//divcan->Print(matched_jet_phi_pdf);																								//?????????
   	//Response plots:
   	can->cd();																											//?????????
   	gPad->SetGridy(1);
   	//Eta
      Draw2DHist(f,"trig_minus_reco_eta_2d_bins"+suffix,"colz",2);
		Draw1DProfile(f,"trig_minus_reco_eta_avrg"+suffix,avgColor,"same x0");
   	can->Print(eta_response_pdf);
   	//Phi
      Draw2DHist(f,"trig_minus_reco_phi_2d_bins"+suffix,"colz",2);
   	Draw1DProfile(f,"trig_minus_reco_phi_avrg"+suffix,avgColor,"same x0");
   	can->Print(phi_response_pdf);	
   	
   	gPad->SetGridy(0);
//      DrawLabels("Jet #it{p}_{T} for |#eta|-bin "+suffix+Form(", %.1f #leq |#eta| < %.1f",ptBins[ieta],ptBins[ieta+1]));
   } //binned spatial family plotted.
	//Pt family:
	for (int ieta=1;ieta<=NetaBins;++ieta) {
      TString suffix="_"; suffix+=ieta;
      //Inclusive distributions:
   	Draw1DHist(f,"h_inclusive_pt_reco_binned"+suffix,kBlue,"hist");
  		Draw1DHist(f,"h_inclusive_pt_trig_binned"+suffix,kRed,"hist same");
   	DrawLabels();
   	can->Print(incl_pt_pdf);
    	//Unmatched distributions:
   	Draw1DHist(f,"unmatched_reco_jets_1d_pt_binned"+suffix,kBlue,"hist");
      can->Print(unmatched_jet_pt_pdf);
   	Draw1DHist(f,"unmatched_trig_jets_1d_pt_binned"+suffix,kRed,"hist");
   	can->Print(unmatched_jet_pt_pdf);
    	//Matched trig vs. reco plots:
    	divcan->cd(1);																							//?????????v
      Draw2DHist(f,"matched_trig_jets_pt_2d_binned"+suffix,"colz");		//absolute
   	divcan->cd(2);
   	Draw2DHist(f,"matched_trig_jets_pt_2d_binned"+suffix,"colz",1);	//percentile
   	divcan->Print(matched_jet_pt_pdf);																							//?????????
     	//Response plots:
    	can->cd();																											//?????????
    	gPad->SetGridy(1);
    	Draw2DHist(f,"trig_over_reco_2d_pt_binned"+suffix,"colz",2);
   	Draw1DProfile(f,"trig_over_reco_pt_mean_binned"+suffix,avgColor,"same x0");
   	can->Print(pt_response_pdf);	
   	gPad->SetGridy(0);
   } //binned pt family plotted.
	//---------------
   //---------------------------------------------------------------------
   //Closing pdfs:
   can->Print(incl_eta_phi_pdf+"]"); //spatial pdf finished.
   can->Print(incl_pt_pdf+"]");
   can->Print(unmatched_jet_eta_phi_pdf+"]");
   can->Print(unmatched_jet_pt_pdf+"]");
   can->Print(eta_response_pdf+"]");
   can->Print(phi_response_pdf+"]");
   can->Print(pt_response_pdf+"]");
   divcan->Print(matched_jet_eta_pdf+"]");																							//?????????
	divcan->Print(matched_jet_phi_pdf+"]");																							//?????????
	divcan->Print(matched_jet_pt_pdf+"]");																							//?????????
   cout<<"end of plotting"<<endl;
}

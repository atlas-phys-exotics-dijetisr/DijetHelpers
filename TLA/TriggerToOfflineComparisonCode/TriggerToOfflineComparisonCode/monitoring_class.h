//
//  monitoring_class.h
//
//
//  Created by Artur Hahn on 19/09/14.
//  Contact: artur_hahn@icloud.com
//
//
//  INSTRUCTIONS:
//-------------------------------------------------------------
//  ********How to use the tool with xAOD (or any other EDM):********
//
//  During initialization
//  ---------------------
//  *Creation of the montioring object:*
//  m_trigMon = new monitoring_class(output_file_name);
//
//  *Initalization:*
//  m_trigMon->MakeTheHistos();
//  ---------------------
//
//  For each event
//  --------------
//  *Code to convert xAOD containers to vectors of TLorentzVector:*
//  vector<TLorentzVector> recoJets, trigJets;
//  for ( auto &jet : xAODofflineJets ) recoJets.push_back( jet.p4() );
//  for ( auto &tjet : xAODtrigJets ) trigJets.push_back( tjet.p4() );
//
//  *Event weight - if applicable, e.g. prescale dependent:*
//  double eventWeight = 1.0;
//
//  *Call the histogram filling:*
//  m_trigMon -> FillTheHistos( trigJets, recoJets, eventWeight );
//  --------------
//
//  *Post processing (adjusting axes) and writing to .root file:*
//  *File name is argument of constructor*
//  m_trigMon -> FinalizeAndSaveOutput();
//
//
//  ************How to use produce the plots in pdf files************
//
//  *Done by standalone program from the .root file saved as output*
//  *with the binary produced when compilng with RootCore.*
//  *To run the program to produce 8 pdf documents*
//  *(one for each plot type), call the following:*
//
//  make_tla_plots INPUTFILE [OUTPUT_PDF_SUFFIX]
//
//  *If last argument is skipped, the input file name is used for suffix.*
//
//  Good luck!
//
//-------------------------------------------------------------


#ifndef ____monitoring_class__
#define ____monitoring_class__

#include <iostream>
#include <TH2.h>
#include <TStyle.h>
#include <TFile.h>
#include <TString.h>
#include <TProfile.h>
#include <TLorentzVector.h>
#include <TMath.h>
#include <TH1D.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TError.h>
#include <TROOT.h>
#include <cmath>
#include <vector>

using std::vector;

class monitoring_class {
public:
  
  // constructor
  monitoring_class(TString outfile_name);
  
  // make histograms given a set of reco jets and a set of trigger jets
  void MakeTheHistos();
  
  // fill histograms given a set of reco jets and a set of trigger jets
  void FillTheHistos(const vector<TLorentzVector> &trigJets, const vector<TLorentzVector> &recoJets, double weight=1.0);
  
  // post processing on pt histograms
  void FinalizeAndSaveOutput();
  
  // Constants for plot making
public:
  
  //Histogram specifications (can be edited by the user):
  //---------------------------------
  static constexpr double pt_Cut = 20.0;       //Minimum pt to consider
  static constexpr double etaCutoff = 5.0;     //Maximum absolute eta to consider
  
  //Spatial family:
  static const int numEtaBins = 250;       //number of eta bins for detector reconstruction resolution
  static const int numPhiBins = 150;       //number of phi bins for detector reconstruction resolution
  static const int numCourseEtaBins = 50;  //number of big eta bins for response plots
  static const int numCoursePhiBins = 30;  //number of big phi bins for response plots
  static const int NetaDiffBins = 40;      //number of bins for response variables -> parametrizes resolution of the response plots
  static const int NphiDiffBins = 40;      //number of bins for response variables -> parametrizes resolution of the response plots
  static constexpr double EtaDiffLimit = 0.04;
  static constexpr double PhiDiffLimit = 0.04;
  //Pt family:
  static constexpr double ptLowLimit = 0.0;    //low bound on pt for axes [GeV]
  static constexpr double ptLimit = 6000.0;    //top bound on pt for axes [GeV]
  static const int numLogPtXbins = 100;    //number of bins for log pt x axis
  static const int numLogPtYbins = 100;    //number of bins for log pt y axis -> parametrizes
  //tolerance for the matching pt error
  static const int numCoursePtBins = 50;   //number of big pt bins for response plots
  static const int numPtQuotBins = 80;     //number of bins for response plot y-axis -> parametrizes
  //resolution of the response
  static constexpr double PtQuotLimit = 0.4;   //maximum deviation of the pt-response from 1
  //---------------------------------
  
  //Specifications for the binning into different histograms:
  //---------------------------------------------------------
  //Define the number of binned histograms here:
  static const int NptBins = 10;  //Number of pt binned histograms
  static const int NetaBins = 12; //Number of eta binned histograms
  
  void fatal(TString msg) { printf("\nFATAL:\n\n  %s\n\n",msg.Data()); abort();}
  
  // private methods (internal helper methods)
private:
  
  //Method for jet matching:
  int findMatchIndex(TLorentzVector aJet,vector<TLorentzVector> bJets);
  
  //Methods for creating binned histograms:
  //-------------------------------------------------------------
  //Binned 1D histograms:
  //---------------------
  //Linearly pt-binned:
  TH1D* make1DetaBinnedHistogram(double LowBinEdge, double HighBinEdge, TString Name, TString Title, TString Labels){
    TString HistTitle = Title+Form(" with %.1f #leq #eta < %.1f;",LowBinEdge,HighBinEdge);
    
    return new TH1D(Name,HistTitle+Labels,numLogPtXbins,ptLowLimit,ptLimit);
  }
  //Log-binned:
  TH1D* make1DptLogBinnedHistogram(double LowBinEdge, double HighBinEdge, TString Name, TString Title, TString Labels, double* firstbin = NULL){
    TString HistTitle = Title+Form(" with %.1f #leq #eta < %.1f;",LowBinEdge,HighBinEdge);
    
    return new TH1D(Name,HistTitle+Labels,numLogPtXbins,firstbin);
  }
  //---------------------
  //Binned 1D profiles:
  //-------------------
  //General:
  TProfile* make1DptBinnedProfile(double LowBinEdge, double HighBinEdge, TString Name, TString Title, int Nbins, double xLowEdge,double xHighEdge){
    TString HistTitle = Title+Form(" with %.0f #leq p_{T} [GeV] < %.0f;",LowBinEdge,HighBinEdge);
    
    return new TProfile(Name,HistTitle,Nbins,xLowEdge,xHighEdge);
  }
  TProfile* make1DetaBinnedProfile(double LowBinEdge, double HighBinEdge, TString Name, TString Title, int Nbins, double xLowEdge,double xHighEdge){
    TString HistTitle = Title+Form(" with %.1f #leq #eta < %.1f;",LowBinEdge,HighBinEdge);
    
    return new TProfile(Name,HistTitle,Nbins,xLowEdge,xHighEdge);
  }
  //Log binning:
  TProfile* make1DlogPtBinnedProfile(double LowBinEdge, double HighBinEdge, TString Name, TString Title, int Nbins, double* firstbin, TString Labels=""){
    TString HistTitle = Title+Form(" with %.1f #leq #eta < %.1f;",LowBinEdge,HighBinEdge);
    
    return new TProfile(Name,HistTitle+Labels,Nbins,firstbin);
  }
  //Specific:
  TProfile* makeMeanResponseProfile(double LowBinEdge, double HighBinEdge, TString Name, TString Title, TString HistType, double* firstbin = NULL){
    if(HistType.CompareTo("eta")==0)
      return make1DptBinnedProfile(LowBinEdge,HighBinEdge,Name,Title,numCourseEtaBins,-5,5);
    if(HistType.CompareTo("phi")==0)
      return make1DptBinnedProfile(LowBinEdge,HighBinEdge,Name,Title,numCoursePhiBins,-TMath::Pi(),TMath::Pi());
    if(HistType.CompareTo("pt")==0)
      return make1DlogPtBinnedProfile(LowBinEdge,HighBinEdge,Name,Title,numCoursePtBins,firstbin);
    fatal("Dont know what profile to make of type "+HistType);
    return 0;
  }
  //-------------------
  //Binned 2D histograms:
  //---------------------
  //General:
  TH2D* make2DptBinnedHistogram(double LowBinEdge, double HighBinEdge, TString Name, TString Title, int Nxbins, double xLowEdge,double xHighEdge, int Nybins, double yLowEdge, double yHighEdge, TString Labels){
    TString HistTitle = Title+Form(" with %.0f #leq p_{T} [GeV] < %.0f;",LowBinEdge,HighBinEdge);
    
    return new TH2D(Name,HistTitle+Labels,Nxbins,xLowEdge,xHighEdge,Nybins,yLowEdge,yHighEdge);
  }
  TH2D* make2DetaBinnedHistogram(double LowBinEdge, double HighBinEdge, TString Name, TString Title,
                                 int Nxbins, double xLowEdge,double xHighEdge, int Nybins,
                                 double yLowEdge, double yHighEdge, TString Labels) {
    TString HistTitle = Title+Form(" with %.1f #leq #eta < %.1f;",LowBinEdge,HighBinEdge);
    
    return new TH2D(Name,HistTitle+Labels,Nxbins,xLowEdge,xHighEdge,Nybins,yLowEdge,yHighEdge);
  }
  //Log-binned:
  TH2D* make2DptLogBinnedHistogram(double LowBinEdge, double HighBinEdge, TString Name, TString Title, double* firstXbin = NULL, double* firstYbin = NULL, TString Labels=""){
    TString HistTitle = Title+Form(" with %.1f #leq #eta < %.1f;",LowBinEdge,HighBinEdge);
    
    return new TH2D(Name,HistTitle+Labels,numLogPtXbins,firstXbin,numLogPtYbins,firstYbin);
  }
  //Half-Log-binned:
  TH2D* make2DptHalfLogBinnedHistogram(double LowBinEdge, double HighBinEdge, TString Name, TString Title, double* firstXbin = NULL, TString Labels=""){
    TString HistTitle = Title+Form(" with %.1f #leq #eta < %.1f;",LowBinEdge,HighBinEdge);
    
    return new TH2D(Name,HistTitle+Labels,numCoursePtBins,firstXbin,numPtQuotBins,1.0-PtQuotLimit,1.0+PtQuotLimit);
  }
  //Specific:
  TH2D* makeEtaPhiHisto(double LowBinEdge, double HighBinEdge, TString Name, TString Title){
    return make2DptBinnedHistogram(LowBinEdge,HighBinEdge,Name,Title,numEtaBins,-5,5,numPhiBins,-TMath::Pi(),TMath::Pi(),"#eta;#phi;total number of jets");
  }
  TH2D* makeTrigRecoHisto(double LowBinEdge, double HighBinEdge, TString Name, TString Title, double* firstXbin = NULL, double* firstYbin = NULL){
    return make2DptLogBinnedHistogram(LowBinEdge,HighBinEdge,Name,Title,firstXbin,firstYbin,"trigger jet p_{T} [GeV];offline jet p_{T} [GeV];");
  }
  TH2D* makeResponse2DHisto(double LowBinEdge, double HighBinEdge, TString Name, TString Title, TString HistType, double* firstXbin = NULL){
    if(HistType.CompareTo("eta")==0)
      return make2DptBinnedHistogram(LowBinEdge,HighBinEdge,Name,Title,numCourseEtaBins,-5,5,NetaDiffBins,-EtaDiffLimit,EtaDiffLimit,"offline jet #eta;#eta(trig)-#eta(reco);percentage of jet matches at #eta(reco)");
    if(HistType.CompareTo("phi")==0)
      return make2DptBinnedHistogram(LowBinEdge,HighBinEdge,Name,Title,numCoursePhiBins,-TMath::Pi(),TMath::Pi(),NphiDiffBins,-PhiDiffLimit,PhiDiffLimit,"offline jet #phi;#Delta #phi (trig - reco);percentage of jet matches at #phi(reco)");
    if(HistType.CompareTo("pt")==0)
      return make2DptHalfLogBinnedHistogram(LowBinEdge,HighBinEdge,Name,Title,firstXbin,"offline jet p_{T} [GeV];p_{T}(trig)/p_{T}(reco);percentage of jet matches at p_{T}(reco)");
    fatal("Dont know what profile to make of type "+HistType);
    return 0;
  }
  //---------------------
  //Binned 2D Profiles:
  //-------------------
  //General
  TProfile2D* make2DptBinnedProfile(double LowBinEdge, double HighBinEdge, TString Name, TString Title, int Nxbins, double xLowEdge,double xHighEdge, int Nybins, double yLowEdge, double yHighEdge, TString Labels){
    TString HistTitle = Title+Form(" with %.0f #leq p_{T} [GeV] < %.0f;",LowBinEdge,HighBinEdge);
    
    return new TProfile2D(Name,HistTitle+Labels,Nxbins,xLowEdge,xHighEdge,Nybins,yLowEdge,yHighEdge);
  }
  
  TProfile2D* makeEtaPhiProfile(double LowBinEdge, double HighBinEdge, TString Name, TString Title){
    return make2DptBinnedProfile(LowBinEdge,HighBinEdge,Name,Title,numEtaBins,-5,5,numPhiBins,-TMath::Pi(),TMath::Pi(),"#eta;#phi;percentage unmatched");
  }
  //-------------------
  //----------------------------------------------------------------------------------------------------------------
  
  // private members of the class
private:
  
  TFile *out_file;
  TString output_fn;
  
  vector<double> PtBinEdges;
  vector<double> EtaBinEdges;
  TAxis *ptAxis, *etaAxis;
  
  //Declaration of the histograms:
  //------------------------------
  //Unbinned histograms
  TH2D *h_inclusive_detector_unbinned_trig;
  TH2D *h_inclusive_detector_unbinned_reco;
  
  //Binned histograms
  //Spatial family:
  TH2D *h_inclusive_detector_trig[NptBins];
  TH2D *h_inclusive_detector_reco[NptBins];
  TProfile2D *unmatched_reco_jets_eta_phi_bins[NptBins];
  TProfile2D *unmatched_trig_jets_eta_phi_bins[NptBins];
  TH2D *trig_minus_reco_eta_2d_bins[NptBins];
  TProfile *trig_minus_reco_eta_avrg[NptBins];
  TH2D *trig_minus_reco_phi_2d_bins[NptBins];
  TProfile *trig_minus_reco_phi_avrg[NptBins];
  //Pt family:
  TH1D *h_inclusive_pt_trig_binned[NetaBins];
  TH1D *h_inclusive_pt_reco_binned[NetaBins];
  TProfile *unmatched_reco_jets_1d_pt_binned[NetaBins];
  TProfile *unmatched_trig_jets_1d_pt_binned[NetaBins];
  TH2D *matched_trig_jets_pt_2d_binned[NetaBins];
  TH2D *trig_over_reco_2d_pt_binned[NetaBins];
  TProfile *trig_over_reco_pt_mean_binned[NetaBins];
  //------------------------------
};


#endif /* defined(____monitoring_class__) */

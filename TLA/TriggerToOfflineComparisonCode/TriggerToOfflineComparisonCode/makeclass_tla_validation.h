//
//
//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Aug 20 11:11:11 2014 by ROOT version 5.34/19
// from TTree physics/physics
// found on file: ../data/user.doglioni.012532.EXT0._00001.root
//////////////////////////////////////////////////////////
//
//
// INSTRUCTIONS:
//-------------------------------------------------------------
// *Compile makeclass_tla_validation.cxx and monitoring_class.cxx*
// *using RootCore, it produces the binary to run:*
//
// run_tla_monitor INPUT_FILE_DIRECTORY OUTPUT_FILE_NAME.root COLLISION_ENERGY
//
// *The last argument (COLLISION_ENERGY), which should be 8 or 14, is used*
// *for the offline jet calibration. It decides which config file to use.*
// *If it is left out or an integer other than 8 or 14 is passed, the calibration*
// *tool is not used in the program.*
//
//
// Good luck!
//-------------------------------------------------------------

#ifndef makeclass_tla_validation_h
#define makeclass_tla_validation_h

#include <TROOT.h>
#include <TChain.h>
#include <TTree.h>
#include <TFile.h>
#include <TH1D.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TString.h>
#include <TError.h>
#include "ApplyJetCalibration/ApplyJetCalibration.h"
#include "Riostream.h"

// Header file for the classes stored in the TTree if any.
#include <vector>
using namespace std;

// add a fatal method (Dags favorite)
void fatal(TString msg) { printf("\nFATAL:\n\n  %s\n\n",msg.Data());}

// Fixed size dimensions of array or collections stored in the TTree if any.



class makeclass_tla_validation {
   public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   
   // Declaration of leaf types
   UInt_t          RunNumber;
   UInt_t          EventNumber;
   UInt_t          timestamp;
   UInt_t          timestamp_ns;
   UInt_t          lbn;
   UInt_t          bcid;
   UInt_t          detmask0;
   UInt_t          detmask1;
   Float_t         actualIntPerXing;
   Float_t         averageIntPerXing;
   UInt_t          mc_channel_number;
   UInt_t          larFlags;
   UInt_t          tileFlags;
   UInt_t          fwdFlags;
   UInt_t          coreFlags;
   UInt_t          larError;
   UInt_t          tileError;
   UInt_t          fwdError;
   UInt_t          coreError;
   Bool_t          isSimulation;
   Bool_t          isCalibration;
   Bool_t          isTestBeam;
   Int_t           lar_ncellA;
   Int_t           lar_ncellC;
   Float_t         lar_energyA;
   Float_t         lar_energyC;
   Float_t         lar_timeA;
   Float_t         lar_timeC;
   Float_t         lar_timeDiff;
   UInt_t          bunch_configID;
   Float_t         Eventshape_rhoKt4EM;
   Float_t         Eventshape_rhoKt4LC;
   Float_t         MET_RefFinal_phi;
   Float_t         MET_RefFinal_et;
   Float_t         MET_RefFinal_sumet;
   Float_t         MET_Topo_phi;
   Int_t           jet_AntiKt4TopoEM_n;
   vector<float>   *jet_AntiKt4TopoEM_E;
   vector<float>   *jet_AntiKt4TopoEM_pt;
   vector<float>   *jet_AntiKt4TopoEM_m;
   vector<float>   *jet_AntiKt4TopoEM_eta;
   vector<float>   *jet_AntiKt4TopoEM_phi;
   vector<float>   *jet_AntiKt4TopoEM_EtaOrigin;
   vector<float>   *jet_AntiKt4TopoEM_PhiOrigin;
   vector<float>   *jet_AntiKt4TopoEM_MOrigin;
   vector<float>   *jet_AntiKt4TopoEM_WIDTH;
   vector<float>   *jet_AntiKt4TopoEM_Timing;
   vector<float>   *jet_AntiKt4TopoEM_LArQuality;
   vector<float>   *jet_AntiKt4TopoEM_OriginIndex;
   vector<float>   *jet_AntiKt4TopoEM_HECQuality;
   vector<float>   *jet_AntiKt4TopoEM_NegativeE;
   vector<float>   *jet_AntiKt4TopoEM_AverageLArQF;
   vector<float>   *jet_AntiKt4TopoEM_BCH_CORR_CELL;
   vector<float>   *jet_AntiKt4TopoEM_BCH_CORR_JET;
   vector<int>     *jet_AntiKt4TopoEM_SamplingMax;
   vector<float>   *jet_AntiKt4TopoEM_fracSamplingMax;
   vector<float>   *jet_AntiKt4TopoEM_hecf;
   vector<float>   *jet_AntiKt4TopoEM_tgap3f;
   vector<int>     *jet_AntiKt4TopoEM_isUgly;
   vector<int>     *jet_AntiKt4TopoEM_isBadLooseMinus;
   vector<int>     *jet_AntiKt4TopoEM_isBadLoose;
   vector<int>     *jet_AntiKt4TopoEM_isBadMedium;
   vector<int>     *jet_AntiKt4TopoEM_isBadTight;
   vector<float>   *jet_AntiKt4TopoEM_emfrac;
   vector<float>   *jet_AntiKt4TopoEM_emscale_E;
   vector<float>   *jet_AntiKt4TopoEM_emscale_pt;
   vector<float>   *jet_AntiKt4TopoEM_emscale_m;
   vector<float>   *jet_AntiKt4TopoEM_emscale_eta;
   vector<float>   *jet_AntiKt4TopoEM_emscale_phi;
   vector<float>   *jet_AntiKt4TopoEM_ActiveAreaPx;
   vector<float>   *jet_AntiKt4TopoEM_ActiveAreaPy;
   vector<float>   *jet_AntiKt4TopoEM_ActiveAreaPz;
   vector<float>   *jet_AntiKt4TopoEM_ActiveAreaE;
   vector<float>   *jet_AntiKt4TopoEM_jvtxf;
   vector<float>   *jet_AntiKt4TopoEM_jvtx_x;
   vector<float>   *jet_AntiKt4TopoEM_jvtx_y;
   vector<float>   *jet_AntiKt4TopoEM_jvtx_z;
   vector<float>   *jet_AntiKt4TopoEM_TruthMFindex;
   vector<float>   *jet_AntiKt4TopoEM_TruthMF;
   vector<float>   *jet_AntiKt4TopoEM_e_EMB3;
   vector<float>   *jet_AntiKt4TopoEM_e_EME3;
   vector<float>   *jet_AntiKt4TopoEM_e_TileBar0;
   vector<float>   *jet_AntiKt4TopoEM_e_TileExt0;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_IP3D;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_MV1;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_SV1;
   vector<float>   *jet_AntiKt4TopoEM_nTrk_pv0_1GeV;
   vector<float>   *jet_AntiKt4TopoEM_sumPtTrk_allpv_1GeV;
   vector<float>   *jet_AntiKt4TopoEM_nTrk_pv0_500MeV;
   vector<float>   *jet_AntiKt4TopoEM_sumPtTrk_pv0_500MeV;
   vector<float>   *jet_AntiKt4TopoEM_trackWIDTH_pv0_1GeV;
   vector<float>   *AntiKt4Truth_pt;
   vector<float>   *AntiKt4Truth_m;
   vector<float>   *AntiKt4Truth_eta;
   vector<float>   *AntiKt4Truth_phi;
   Int_t           InTimeAntiKt4Truth_n;
   vector<float>   *InTimeAntiKt4Truth_E;
   vector<float>   *InTimeAntiKt4Truth_pt;
   vector<float>   *InTimeAntiKt4Truth_m;
   vector<float>   *InTimeAntiKt4Truth_eta;
   vector<float>   *InTimeAntiKt4Truth_phi;
   vector<int>     *InTimeAntiKt4Truth_flavor_truth_label;
   vector<float>   *InTimeAntiKt4Truth_flavor_truth_dRminToB;
   vector<float>   *InTimeAntiKt4Truth_flavor_truth_dRminToC;
   vector<float>   *InTimeAntiKt4Truth_flavor_truth_dRminToT;
   vector<int>     *InTimeAntiKt4Truth_flavor_truth_BHadronpdg;
   vector<float>   *InTimeAntiKt4Truth_flavor_truth_vx_x;
   vector<float>   *InTimeAntiKt4Truth_flavor_truth_vx_y;
   vector<float>   *InTimeAntiKt4Truth_flavor_truth_vx_z;
   vector<int>     *InTimeAntiKt4Truth_constit_n;
   vector<vector<int> > *InTimeAntiKt4Truth_constit_index;
   Int_t           OutOfTimeAntiKt4Truth_n;
   vector<float>   *OutOfTimeAntiKt4Truth_E;
   vector<float>   *OutOfTimeAntiKt4Truth_pt;
   vector<float>   *OutOfTimeAntiKt4Truth_m;
   vector<float>   *OutOfTimeAntiKt4Truth_eta;
   vector<float>   *OutOfTimeAntiKt4Truth_phi;
   vector<int>     *OutOfTimeAntiKt4Truth_flavor_truth_label;
   vector<float>   *OutOfTimeAntiKt4Truth_flavor_truth_dRminToB;
   vector<float>   *OutOfTimeAntiKt4Truth_flavor_truth_dRminToC;
   vector<float>   *OutOfTimeAntiKt4Truth_flavor_truth_dRminToT;
   vector<int>     *OutOfTimeAntiKt4Truth_flavor_truth_BHadronpdg;
   vector<float>   *OutOfTimeAntiKt4Truth_flavor_truth_vx_x;
   vector<float>   *OutOfTimeAntiKt4Truth_flavor_truth_vx_y;
   vector<float>   *OutOfTimeAntiKt4Truth_flavor_truth_vx_z;
   vector<int>     *OutOfTimeAntiKt4Truth_constit_n;
   vector<vector<int> > *OutOfTimeAntiKt4Truth_constit_index;
   Int_t           vxp_n;
   vector<float>   *vxp_z;
   vector<int>     *vxp_nTracks;
   Float_t         mbtime_timeDiff;
   vector<vector<double> > *mcevt_weight;
   vector<unsigned int> *trig_L1_TAV;
   vector<short>   *trig_L2_passedPhysics;
   vector<short>   *trig_EF_passedPhysics;
   vector<unsigned int> *trig_L1_TBP;
   vector<unsigned int> *trig_L1_TAP;
   vector<short>   *trig_L2_passedRaw;
   vector<short>   *trig_EF_passedRaw;
   Bool_t          trig_L2_truncated;
   Bool_t          trig_EF_truncated;
   vector<short>   *trig_L2_resurrected;
   vector<short>   *trig_EF_resurrected;
   vector<short>   *trig_L2_passedThrough;
   vector<short>   *trig_EF_passedThrough;
   vector<short>   *trig_L2_wasPrescaled;
   vector<short>   *trig_L2_wasResurrected;
   vector<short>   *trig_EF_wasPrescaled;
   vector<short>   *trig_EF_wasResurrected;
   UInt_t          trig_DB_SMK;
   UInt_t          trig_DB_L1PSK;
   UInt_t          trig_DB_HLTPSK;
   Int_t           trig_L1_jet_n;
   vector<float>   *trig_L1_jet_eta;
   vector<float>   *trig_L1_jet_phi;
   vector<vector<string> > *trig_L1_jet_thrNames;
   vector<vector<float> > *trig_L1_jet_thrValues;
   vector<unsigned int> *trig_L1_jet_thrPattern;
   vector<unsigned int> *trig_L1_jet_RoIWord;
   vector<float>   *trig_L1_jet_et4x4;
   vector<float>   *trig_L1_jet_et6x6;
   vector<float>   *trig_L1_jet_et8x8;
   vector<float>   *trig_L1_jet_myKin_pt;
   vector<float>   *trig_L1_jet_myKin_eta;
   vector<float>   *trig_L1_jet_myKin_phi;
   Int_t           trig_L2_jet_n;
   vector<float>   *trig_L2_jet_E;
   vector<float>   *trig_L2_jet_eta;
   vector<float>   *trig_L2_jet_phi;
   vector<unsigned int> *trig_L2_jet_RoIWord;
   vector<string>  *trig_L2_jet_InputType;
   vector<string>  *trig_L2_jet_OutputType;
   vector<int>     *trig_L2_jet_counter;
   vector<int>     *trig_L2_jet_c4cchad;
   vector<int>     *trig_L2_jet_c4ccem;
   vector<int>     *trig_L2_jet_c4uchad;
   vector<double>  *trig_L2_jet_ehad0;
   vector<double>  *trig_L2_jet_eem0;
   vector<int>     *trig_L2_jet_nLeadingCells;
   vector<float>   *trig_L2_jet_hecf;
   vector<float>   *trig_L2_jet_jetQuality;
   vector<float>   *trig_L2_jet_emf;
   vector<float>   *trig_L2_jet_jetTimeCells;
   Int_t           trig_EF_jet_n;
   vector<float>   *trig_EF_jet_E;
   vector<float>   *trig_EF_jet_pt;
   vector<float>   *trig_EF_jet_m;
   vector<float>   *trig_EF_jet_eta;
   vector<float>   *trig_EF_jet_phi;
   vector<float>   *trig_EF_jet_emscale_E;
   vector<float>   *trig_EF_jet_emscale_pt;
   vector<float>   *trig_EF_jet_emscale_m;
   vector<float>   *trig_EF_jet_emscale_eta;
   vector<float>   *trig_EF_jet_emscale_phi;
   vector<float>   *trig_EF_jet_constscale_E;
   vector<float>   *trig_EF_jet_constscale_pt;
   vector<float>   *trig_EF_jet_constscale_m;
   vector<float>   *trig_EF_jet_constscale_eta;
   vector<float>   *trig_EF_jet_constscale_phi;
   vector<string>  *trig_EF_jet_author;
   vector<string>  *trig_EF_jet_calibtags;
   vector<float>   *trig_EF_jet_n90;
   vector<float>   *trig_EF_jet_Timing;
   vector<float>   *trig_EF_jet_LArQuality;
   vector<float>   *trig_EF_jet_HECQuality;
   vector<float>   *trig_EF_jet_NegativeE;
   vector<float>   *trig_EF_jet_fracSamplingMax;
   vector<float>   *trig_EF_jet_SamplingMax;
   vector<float>   *trig_EF_jet_n90constituents;
   vector<float>   *trig_EF_jet_TileQuality;
   vector<float>   *trig_EF_jet_hecF;
   vector<float>   *trig_EF_jet_EMFraction;
   vector<float>   *trig_EF_jet_presamplerFraction;
   vector<float>   *trig_EF_jet_AverageLArQF;
   vector<float>   *trig_EF_jet_EMJES;
   vector<float>   *trig_EF_jet_EMJES_EtaCorr;
   vector<float>   *trig_EF_jet_Discriminant;
   vector<float>   *trig_EF_jet_MeanTowerEtUnsubtr;
   vector<float>   *trig_EF_jet_TrigJetFeaturesUnpacker_Status;
   vector<int>     *trig_EF_jet_a4tc;
   vector<int>     *trig_EF_jet_a10tc;
   vector<int>     *trig_EF_jet_a2hi;
   vector<unsigned int> *trig_EF_jet_RoIword;
   
   // List of branches
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_timestamp;   //!
   TBranch        *b_timestamp_ns;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_detmask0;   //!
   TBranch        *b_detmask1;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_mc_channel_number;   //!
   TBranch        *b_larFlags;   //!
   TBranch        *b_tileFlags;   //!
   TBranch        *b_fwdFlags;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_larError;   //!
   TBranch        *b_tileError;   //!
   TBranch        *b_fwdError;   //!
   TBranch        *b_coreError;   //!
   TBranch        *b_isSimulation;   //!
   TBranch        *b_isCalibration;   //!
   TBranch        *b_isTestBeam;   //!
   TBranch        *b_lar_ncellA;   //!
   TBranch        *b_lar_ncellC;   //!
   TBranch        *b_lar_energyA;   //!
   TBranch        *b_lar_energyC;   //!
   TBranch        *b_lar_timeA;   //!
   TBranch        *b_lar_timeC;   //!
   TBranch        *b_lar_timeDiff;   //!
   TBranch        *b_bunch_configID;   //!
   TBranch        *b_Eventshape_rhoKt4EM;   //!
   TBranch        *b_Eventshape_rhoKt4LC;   //!
   TBranch        *b_MET_RefFinal_phi;   //!
   TBranch        *b_MET_RefFinal_et;   //!
   TBranch        *b_MET_RefFinal_sumet;   //!
   TBranch        *b_MET_Topo_phi;   //!
   TBranch        *b_jet_AntiKt4TopoEM_n;   //!
   TBranch        *b_jet_AntiKt4TopoEM_E;   //!
   TBranch        *b_jet_AntiKt4TopoEM_pt;   //!
   TBranch        *b_jet_AntiKt4TopoEM_m;   //!
   TBranch        *b_jet_AntiKt4TopoEM_eta;   //!
   TBranch        *b_jet_AntiKt4TopoEM_phi;   //!
   TBranch        *b_jet_AntiKt4TopoEM_EtaOrigin;   //!
   TBranch        *b_jet_AntiKt4TopoEM_PhiOrigin;   //!
   TBranch        *b_jet_AntiKt4TopoEM_MOrigin;   //!
   TBranch        *b_jet_AntiKt4TopoEM_WIDTH;   //!
   TBranch        *b_jet_AntiKt4TopoEM_Timing;   //!
   TBranch        *b_jet_AntiKt4TopoEM_LArQuality;   //!
   TBranch        *b_jet_AntiKt4TopoEM_OriginIndex;   //!
   TBranch        *b_jet_AntiKt4TopoEM_HECQuality;   //!
   TBranch        *b_jet_AntiKt4TopoEM_NegativeE;   //!
   TBranch        *b_jet_AntiKt4TopoEM_AverageLArQF;   //!
   TBranch        *b_jet_AntiKt4TopoEM_BCH_CORR_CELL;   //!
   TBranch        *b_jet_AntiKt4TopoEM_BCH_CORR_JET;   //!
   TBranch        *b_jet_AntiKt4TopoEM_SamplingMax;   //!
   TBranch        *b_jet_AntiKt4TopoEM_fracSamplingMax;   //!
   TBranch        *b_jet_AntiKt4TopoEM_hecf;   //!
   TBranch        *b_jet_AntiKt4TopoEM_tgap3f;   //!
   TBranch        *b_jet_AntiKt4TopoEM_isUgly;   //!
   TBranch        *b_jet_AntiKt4TopoEM_isBadLooseMinus;   //!
   TBranch        *b_jet_AntiKt4TopoEM_isBadLoose;   //!
   TBranch        *b_jet_AntiKt4TopoEM_isBadMedium;   //!
   TBranch        *b_jet_AntiKt4TopoEM_isBadTight;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emfrac;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emscale_E;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emscale_pt;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emscale_m;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emscale_eta;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emscale_phi;   //!
   TBranch        *b_jet_AntiKt4TopoEM_ActiveAreaPx;   //!
   TBranch        *b_jet_AntiKt4TopoEM_ActiveAreaPy;   //!
   TBranch        *b_jet_AntiKt4TopoEM_ActiveAreaPz;   //!
   TBranch        *b_jet_AntiKt4TopoEM_ActiveAreaE;   //!
   TBranch        *b_jet_AntiKt4TopoEM_jvtxf;   //!
   TBranch        *b_jet_AntiKt4TopoEM_jvtx_x;   //!
   TBranch        *b_jet_AntiKt4TopoEM_jvtx_y;   //!
   TBranch        *b_jet_AntiKt4TopoEM_jvtx_z;   //!
   TBranch        *b_jet_AntiKt4TopoEM_TruthMFindex;   //!
   TBranch        *b_jet_AntiKt4TopoEM_TruthMF;   //!
   TBranch        *b_jet_AntiKt4TopoEM_e_EMB3;   //!
   TBranch        *b_jet_AntiKt4TopoEM_e_EME3;   //!
   TBranch        *b_jet_AntiKt4TopoEM_e_TileBar0;   //!
   TBranch        *b_jet_AntiKt4TopoEM_e_TileExt0;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_IP3D;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_MV1;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_SV1;   //!
   TBranch        *b_jet_AntiKt4TopoEM_nTrk_pv0_1GeV;   //!
   TBranch        *b_jet_AntiKt4TopoEM_sumPtTrk_allpv_1GeV;   //!
   TBranch        *b_jet_AntiKt4TopoEM_nTrk_pv0_500MeV;   //!
   TBranch        *b_jet_AntiKt4TopoEM_sumPtTrk_pv0_500MeV;   //!
   TBranch        *b_jet_AntiKt4TopoEM_trackWIDTH_pv0_1GeV;   //!
   TBranch        *b_AntiKt4Truth_pt;   //!
   TBranch        *b_AntiKt4Truth_m;   //!
   TBranch        *b_AntiKt4Truth_eta;   //!
   TBranch        *b_AntiKt4Truth_phi;   //!
   TBranch        *b_InTimeAntiKt4Truth_n;   //!
   TBranch        *b_InTimeAntiKt4Truth_E;   //!
   TBranch        *b_InTimeAntiKt4Truth_pt;   //!
   TBranch        *b_InTimeAntiKt4Truth_m;   //!
   TBranch        *b_InTimeAntiKt4Truth_eta;   //!
   TBranch        *b_InTimeAntiKt4Truth_phi;   //!
   TBranch        *b_InTimeAntiKt4Truth_flavor_truth_label;   //!
   TBranch        *b_InTimeAntiKt4Truth_flavor_truth_dRminToB;   //!
   TBranch        *b_InTimeAntiKt4Truth_flavor_truth_dRminToC;   //!
   TBranch        *b_InTimeAntiKt4Truth_flavor_truth_dRminToT;   //!
   TBranch        *b_InTimeAntiKt4Truth_flavor_truth_BHadronpdg;   //!
   TBranch        *b_InTimeAntiKt4Truth_flavor_truth_vx_x;   //!
   TBranch        *b_InTimeAntiKt4Truth_flavor_truth_vx_y;   //!
   TBranch        *b_InTimeAntiKt4Truth_flavor_truth_vx_z;   //!
   TBranch        *b_InTimeAntiKt4Truth_constit_n;   //!
   TBranch        *b_InTimeAntiKt4Truth_constit_index;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_n;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_E;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_pt;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_m;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_eta;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_phi;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_flavor_truth_label;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_flavor_truth_dRminToB;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_flavor_truth_dRminToC;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_flavor_truth_dRminToT;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_flavor_truth_BHadronpdg;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_flavor_truth_vx_x;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_flavor_truth_vx_y;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_flavor_truth_vx_z;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_constit_n;   //!
   TBranch        *b_OutOfTimeAntiKt4Truth_constit_index;   //!
   TBranch        *b_vxp_n;   //!
   TBranch        *b_vxp_z;   //!
   TBranch        *b_vxp_nTracks;   //!
   TBranch        *b_mbtime_timeDiff;   //!
   TBranch        *b_mcevt_weight;   //!
   TBranch        *b_trig_L1_TAV;   //!
   TBranch        *b_trig_L2_passedPhysics;   //!
   TBranch        *b_trig_EF_passedPhysics;   //!
   TBranch        *b_trig_L1_TBP;   //!
   TBranch        *b_trig_L1_TAP;   //!
   TBranch        *b_trig_L2_passedRaw;   //!
   TBranch        *b_trig_EF_passedRaw;   //!
   TBranch        *b_trig_L2_truncated;   //!
   TBranch        *b_trig_EF_truncated;   //!
   TBranch        *b_trig_L2_resurrected;   //!
   TBranch        *b_trig_EF_resurrected;   //!
   TBranch        *b_trig_L2_passedThrough;   //!
   TBranch        *b_trig_EF_passedThrough;   //!
   TBranch        *b_trig_L2_wasPrescaled;   //!
   TBranch        *b_trig_L2_wasResurrected;   //!
   TBranch        *b_trig_EF_wasPrescaled;   //!
   TBranch        *b_trig_EF_wasResurrected;   //!
   TBranch        *b_trig_DB_SMK;   //!
   TBranch        *b_trig_DB_L1PSK;   //!
   TBranch        *b_trig_DB_HLTPSK;   //!
   TBranch        *b_trig_L1_jet_n;   //!
   TBranch        *b_trig_L1_jet_eta;   //!
   TBranch        *b_trig_L1_jet_phi;   //!
   TBranch        *b_trig_L1_jet_thrNames;   //!
   TBranch        *b_trig_L1_jet_thrValues;   //!
   TBranch        *b_trig_L1_jet_thrPattern;   //!
   TBranch        *b_trig_L1_jet_RoIWord;   //!
   TBranch        *b_trig_L1_jet_et4x4;   //!
   TBranch        *b_trig_L1_jet_et6x6;   //!
   TBranch        *b_trig_L1_jet_et8x8;   //!
   TBranch        *b_trig_L1_jet_myKin_pt;   //!
   TBranch        *b_trig_L1_jet_myKin_eta;   //!
   TBranch        *b_trig_L1_jet_myKin_phi;   //!
   TBranch        *b_trig_L2_jet_n;   //!
   TBranch        *b_trig_L2_jet_E;   //!
   TBranch        *b_trig_L2_jet_eta;   //!
   TBranch        *b_trig_L2_jet_phi;   //!
   TBranch        *b_trig_L2_jet_RoIWord;   //!
   TBranch        *b_trig_L2_jet_InputType;   //!
   TBranch        *b_trig_L2_jet_OutputType;   //!
   TBranch        *b_trig_L2_jet_counter;   //!
   TBranch        *b_trig_L2_jet_c4cchad;   //!
   TBranch        *b_trig_L2_jet_c4ccem;   //!
   TBranch        *b_trig_L2_jet_c4uchad;   //!
   TBranch        *b_trig_L2_jet_ehad0;   //!
   TBranch        *b_trig_L2_jet_eem0;   //!
   TBranch        *b_trig_L2_jet_nLeadingCells;   //!
   TBranch        *b_trig_L2_jet_hecf;   //!
   TBranch        *b_trig_L2_jet_jetQuality;   //!
   TBranch        *b_trig_L2_jet_emf;   //!
   TBranch        *b_trig_L2_jet_jetTimeCells;   //!
   TBranch        *b_trig_EF_jet_n;   //!
   TBranch        *b_trig_EF_jet_E;   //!
   TBranch        *b_trig_EF_jet_pt;   //!
   TBranch        *b_trig_EF_jet_m;   //!
   TBranch        *b_trig_EF_jet_eta;   //!
   TBranch        *b_trig_EF_jet_phi;   //!
   TBranch        *b_trig_EF_jet_emscale_E;   //!
   TBranch        *b_trig_EF_jet_emscale_pt;   //!
   TBranch        *b_trig_EF_jet_emscale_m;   //!
   TBranch        *b_trig_EF_jet_emscale_eta;   //!
   TBranch        *b_trig_EF_jet_emscale_phi;   //!
   TBranch        *b_trig_EF_jet_constscale_E;   //!
   TBranch        *b_trig_EF_jet_constscale_pt;   //!
   TBranch        *b_trig_EF_jet_constscale_m;   //!
   TBranch        *b_trig_EF_jet_constscale_eta;   //!
   TBranch        *b_trig_EF_jet_constscale_phi;   //!
   TBranch        *b_trig_EF_jet_author;   //!
   TBranch        *b_trig_EF_jet_calibtags;   //!
   TBranch        *b_trig_EF_jet_n90;   //!
   TBranch        *b_trig_EF_jet_Timing;   //!
   TBranch        *b_trig_EF_jet_LArQuality;   //!
   TBranch        *b_trig_EF_jet_HECQuality;   //!
   TBranch        *b_trig_EF_jet_NegativeE;   //!
   TBranch        *b_trig_EF_jet_fracSamplingMax;   //!
   TBranch        *b_trig_EF_jet_SamplingMax;   //!
   TBranch        *b_trig_EF_jet_n90constituents;   //!
   TBranch        *b_trig_EF_jet_TileQuality;   //!
   TBranch        *b_trig_EF_jet_hecF;   //!
   TBranch        *b_trig_EF_jet_EMFraction;   //!
   TBranch        *b_trig_EF_jet_presamplerFraction;   //!
   TBranch        *b_trig_EF_jet_AverageLArQF;   //!
   TBranch        *b_trig_EF_jet_EMJES;   //!
   TBranch        *b_trig_EF_jet_EMJES_EtaCorr;   //!
   TBranch        *b_trig_EF_jet_Discriminant;   //!
   TBranch        *b_trig_EF_jet_MeanTowerEtUnsubtr;   //!
   TBranch        *b_trig_EF_jet_TrigJetFeaturesUnpacker_Status;   //!
   TBranch        *b_trig_EF_jet_a4tc;   //!
   TBranch        *b_trig_EF_jet_a10tc;   //!
   TBranch        *b_trig_EF_jet_a2hi;   //!
   TBranch        *b_trig_EF_jet_RoIword;   //!
   
   makeclass_tla_validation(TTree *tree=0);
   virtual ~makeclass_tla_validation();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(TString ofn,int DataEnergyTeV);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef makeclass_tla_validation_cxx
makeclass_tla_validation::makeclass_tla_validation(TTree *tree) : fChain(0)
{
   // if parameter tree is not specified (or zero), connect the file
   // used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../data/user.doglioni.012532.EXT0._00001.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../data/user.doglioni.012532.EXT0._00001.root");
      }
      f->GetObject("physics",tree);
      
   }
   Init(tree);
}

makeclass_tla_validation::~makeclass_tla_validation()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t makeclass_tla_validation::GetEntry(Long64_t entry)
{
   // Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t makeclass_tla_validation::LoadTree(Long64_t entry)
{
   // Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void makeclass_tla_validation::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).
   
   // Set object pointer
   jet_AntiKt4TopoEM_E = 0;
   jet_AntiKt4TopoEM_pt = 0;
   jet_AntiKt4TopoEM_m = 0;
   jet_AntiKt4TopoEM_eta = 0;
   jet_AntiKt4TopoEM_phi = 0;
   jet_AntiKt4TopoEM_EtaOrigin = 0;
   jet_AntiKt4TopoEM_PhiOrigin = 0;
   jet_AntiKt4TopoEM_MOrigin = 0;
   jet_AntiKt4TopoEM_WIDTH = 0;
   jet_AntiKt4TopoEM_Timing = 0;
   jet_AntiKt4TopoEM_LArQuality = 0;
   jet_AntiKt4TopoEM_OriginIndex = 0;
   jet_AntiKt4TopoEM_HECQuality = 0;
   jet_AntiKt4TopoEM_NegativeE = 0;
   jet_AntiKt4TopoEM_AverageLArQF = 0;
   jet_AntiKt4TopoEM_BCH_CORR_CELL = 0;
   jet_AntiKt4TopoEM_BCH_CORR_JET = 0;
   jet_AntiKt4TopoEM_SamplingMax = 0;
   jet_AntiKt4TopoEM_fracSamplingMax = 0;
   jet_AntiKt4TopoEM_hecf = 0;
   jet_AntiKt4TopoEM_tgap3f = 0;
   jet_AntiKt4TopoEM_isUgly = 0;
   jet_AntiKt4TopoEM_isBadLooseMinus = 0;
   jet_AntiKt4TopoEM_isBadLoose = 0;
   jet_AntiKt4TopoEM_isBadMedium = 0;
   jet_AntiKt4TopoEM_isBadTight = 0;
   jet_AntiKt4TopoEM_emfrac = 0;
   jet_AntiKt4TopoEM_emscale_E = 0;
   jet_AntiKt4TopoEM_emscale_pt = 0;
   jet_AntiKt4TopoEM_emscale_m = 0;
   jet_AntiKt4TopoEM_emscale_eta = 0;
   jet_AntiKt4TopoEM_emscale_phi = 0;
   jet_AntiKt4TopoEM_ActiveAreaPx = 0;
   jet_AntiKt4TopoEM_ActiveAreaPy = 0;
   jet_AntiKt4TopoEM_ActiveAreaPz = 0;
   jet_AntiKt4TopoEM_ActiveAreaE = 0;
   jet_AntiKt4TopoEM_jvtxf = 0;
   jet_AntiKt4TopoEM_jvtx_x = 0;
   jet_AntiKt4TopoEM_jvtx_y = 0;
   jet_AntiKt4TopoEM_jvtx_z = 0;
   jet_AntiKt4TopoEM_TruthMFindex = 0;
   jet_AntiKt4TopoEM_TruthMF = 0;
   jet_AntiKt4TopoEM_e_EMB3 = 0;
   jet_AntiKt4TopoEM_e_EME3 = 0;
   jet_AntiKt4TopoEM_e_TileBar0 = 0;
   jet_AntiKt4TopoEM_e_TileExt0 = 0;
   jet_AntiKt4TopoEM_flavor_weight_IP3D = 0;
   jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN = 0;
   jet_AntiKt4TopoEM_flavor_weight_MV1 = 0;
   jet_AntiKt4TopoEM_flavor_weight_SV1 = 0;
   jet_AntiKt4TopoEM_nTrk_pv0_1GeV = 0;
   jet_AntiKt4TopoEM_sumPtTrk_allpv_1GeV = 0;
   jet_AntiKt4TopoEM_nTrk_pv0_500MeV = 0;
   jet_AntiKt4TopoEM_sumPtTrk_pv0_500MeV = 0;
   jet_AntiKt4TopoEM_trackWIDTH_pv0_1GeV = 0;
   AntiKt4Truth_pt = 0;
   AntiKt4Truth_m = 0;
   AntiKt4Truth_eta = 0;
   AntiKt4Truth_phi = 0;
   InTimeAntiKt4Truth_E = 0;
   InTimeAntiKt4Truth_pt = 0;
   InTimeAntiKt4Truth_m = 0;
   InTimeAntiKt4Truth_eta = 0;
   InTimeAntiKt4Truth_phi = 0;
   InTimeAntiKt4Truth_flavor_truth_label = 0;
   InTimeAntiKt4Truth_flavor_truth_dRminToB = 0;
   InTimeAntiKt4Truth_flavor_truth_dRminToC = 0;
   InTimeAntiKt4Truth_flavor_truth_dRminToT = 0;
   InTimeAntiKt4Truth_flavor_truth_BHadronpdg = 0;
   InTimeAntiKt4Truth_flavor_truth_vx_x = 0;
   InTimeAntiKt4Truth_flavor_truth_vx_y = 0;
   InTimeAntiKt4Truth_flavor_truth_vx_z = 0;
   InTimeAntiKt4Truth_constit_n = 0;
   InTimeAntiKt4Truth_constit_index = 0;
   OutOfTimeAntiKt4Truth_E = 0;
   OutOfTimeAntiKt4Truth_pt = 0;
   OutOfTimeAntiKt4Truth_m = 0;
   OutOfTimeAntiKt4Truth_eta = 0;
   OutOfTimeAntiKt4Truth_phi = 0;
   OutOfTimeAntiKt4Truth_flavor_truth_label = 0;
   OutOfTimeAntiKt4Truth_flavor_truth_dRminToB = 0;
   OutOfTimeAntiKt4Truth_flavor_truth_dRminToC = 0;
   OutOfTimeAntiKt4Truth_flavor_truth_dRminToT = 0;
   OutOfTimeAntiKt4Truth_flavor_truth_BHadronpdg = 0;
   OutOfTimeAntiKt4Truth_flavor_truth_vx_x = 0;
   OutOfTimeAntiKt4Truth_flavor_truth_vx_y = 0;
   OutOfTimeAntiKt4Truth_flavor_truth_vx_z = 0;
   OutOfTimeAntiKt4Truth_constit_n = 0;
   OutOfTimeAntiKt4Truth_constit_index = 0;
   vxp_z = 0;
   vxp_nTracks = 0;
   mcevt_weight = 0;
   trig_L1_TAV = 0;
   trig_L2_passedPhysics = 0;
   trig_EF_passedPhysics = 0;
   trig_L1_TBP = 0;
   trig_L1_TAP = 0;
   trig_L2_passedRaw = 0;
   trig_EF_passedRaw = 0;
   trig_L2_resurrected = 0;
   trig_EF_resurrected = 0;
   trig_L2_passedThrough = 0;
   trig_EF_passedThrough = 0;
   trig_L2_wasPrescaled = 0;
   trig_L2_wasResurrected = 0;
   trig_EF_wasPrescaled = 0;
   trig_EF_wasResurrected = 0;
   trig_L1_jet_eta = 0;
   trig_L1_jet_phi = 0;
   trig_L1_jet_thrNames = 0;
   trig_L1_jet_thrValues = 0;
   trig_L1_jet_thrPattern = 0;
   trig_L1_jet_RoIWord = 0;
   trig_L1_jet_et4x4 = 0;
   trig_L1_jet_et6x6 = 0;
   trig_L1_jet_et8x8 = 0;
   trig_L1_jet_myKin_pt = 0;
   trig_L1_jet_myKin_eta = 0;
   trig_L1_jet_myKin_phi = 0;
   trig_L2_jet_E = 0;
   trig_L2_jet_eta = 0;
   trig_L2_jet_phi = 0;
   trig_L2_jet_RoIWord = 0;
   trig_L2_jet_InputType = 0;
   trig_L2_jet_OutputType = 0;
   trig_L2_jet_counter = 0;
   trig_L2_jet_c4cchad = 0;
   trig_L2_jet_c4ccem = 0;
   trig_L2_jet_c4uchad = 0;
   trig_L2_jet_ehad0 = 0;
   trig_L2_jet_eem0 = 0;
   trig_L2_jet_nLeadingCells = 0;
   trig_L2_jet_hecf = 0;
   trig_L2_jet_jetQuality = 0;
   trig_L2_jet_emf = 0;
   trig_L2_jet_jetTimeCells = 0;
   trig_EF_jet_E = 0;
   trig_EF_jet_pt = 0;
   trig_EF_jet_m = 0;
   trig_EF_jet_eta = 0;
   trig_EF_jet_phi = 0;
   trig_EF_jet_emscale_E = 0;
   trig_EF_jet_emscale_pt = 0;
   trig_EF_jet_emscale_m = 0;
   trig_EF_jet_emscale_eta = 0;
   trig_EF_jet_emscale_phi = 0;
   trig_EF_jet_constscale_E = 0;
   trig_EF_jet_constscale_pt = 0;
   trig_EF_jet_constscale_m = 0;
   trig_EF_jet_constscale_eta = 0;
   trig_EF_jet_constscale_phi = 0;
   trig_EF_jet_author = 0;
   trig_EF_jet_calibtags = 0;
   trig_EF_jet_n90 = 0;
   trig_EF_jet_Timing = 0;
   trig_EF_jet_LArQuality = 0;
   trig_EF_jet_HECQuality = 0;
   trig_EF_jet_NegativeE = 0;
   trig_EF_jet_fracSamplingMax = 0;
   trig_EF_jet_SamplingMax = 0;
   trig_EF_jet_n90constituents = 0;
   trig_EF_jet_TileQuality = 0;
   trig_EF_jet_hecF = 0;
   trig_EF_jet_EMFraction = 0;
   trig_EF_jet_presamplerFraction = 0;
   trig_EF_jet_AverageLArQF = 0;
   trig_EF_jet_EMJES = 0;
   trig_EF_jet_EMJES_EtaCorr = 0;
   trig_EF_jet_Discriminant = 0;
   trig_EF_jet_MeanTowerEtUnsubtr = 0;
   trig_EF_jet_TrigJetFeaturesUnpacker_Status = 0;
   trig_EF_jet_a4tc = 0;
   trig_EF_jet_a10tc = 0;
   trig_EF_jet_a2hi = 0;
   trig_EF_jet_RoIword = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);
   
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
   fChain->SetBranchAddress("timestamp_ns", &timestamp_ns, &b_timestamp_ns);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("detmask0", &detmask0, &b_detmask0);
   fChain->SetBranchAddress("detmask1", &detmask1, &b_detmask1);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
   fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
   fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
   fChain->SetBranchAddress("fwdFlags", &fwdFlags, &b_fwdFlags);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("larError", &larError, &b_larError);
   fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
   fChain->SetBranchAddress("fwdError", &fwdError, &b_fwdError);
   fChain->SetBranchAddress("coreError", &coreError, &b_coreError);
   fChain->SetBranchAddress("isSimulation", &isSimulation, &b_isSimulation);
   fChain->SetBranchAddress("isCalibration", &isCalibration, &b_isCalibration);
   fChain->SetBranchAddress("isTestBeam", &isTestBeam, &b_isTestBeam);
   fChain->SetBranchAddress("lar_ncellA", &lar_ncellA, &b_lar_ncellA);
   fChain->SetBranchAddress("lar_ncellC", &lar_ncellC, &b_lar_ncellC);
   fChain->SetBranchAddress("lar_energyA", &lar_energyA, &b_lar_energyA);
   fChain->SetBranchAddress("lar_energyC", &lar_energyC, &b_lar_energyC);
   fChain->SetBranchAddress("lar_timeA", &lar_timeA, &b_lar_timeA);
   fChain->SetBranchAddress("lar_timeC", &lar_timeC, &b_lar_timeC);
   fChain->SetBranchAddress("lar_timeDiff", &lar_timeDiff, &b_lar_timeDiff);
   fChain->SetBranchAddress("bunch_configID", &bunch_configID, &b_bunch_configID);
   fChain->SetBranchAddress("Eventshape_rhoKt4EM", &Eventshape_rhoKt4EM, &b_Eventshape_rhoKt4EM);
   fChain->SetBranchAddress("Eventshape_rhoKt4LC", &Eventshape_rhoKt4LC, &b_Eventshape_rhoKt4LC);
   fChain->SetBranchAddress("MET_RefFinal_phi", &MET_RefFinal_phi, &b_MET_RefFinal_phi);
   fChain->SetBranchAddress("MET_RefFinal_et", &MET_RefFinal_et, &b_MET_RefFinal_et);
   fChain->SetBranchAddress("MET_RefFinal_sumet", &MET_RefFinal_sumet, &b_MET_RefFinal_sumet);
   fChain->SetBranchAddress("MET_Topo_phi", &MET_Topo_phi, &b_MET_Topo_phi);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_n", &jet_AntiKt4TopoEM_n, &b_jet_AntiKt4TopoEM_n);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_E", &jet_AntiKt4TopoEM_E, &b_jet_AntiKt4TopoEM_E);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_pt", &jet_AntiKt4TopoEM_pt, &b_jet_AntiKt4TopoEM_pt);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_m", &jet_AntiKt4TopoEM_m, &b_jet_AntiKt4TopoEM_m);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_eta", &jet_AntiKt4TopoEM_eta, &b_jet_AntiKt4TopoEM_eta);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_phi", &jet_AntiKt4TopoEM_phi, &b_jet_AntiKt4TopoEM_phi);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_EtaOrigin", &jet_AntiKt4TopoEM_EtaOrigin, &b_jet_AntiKt4TopoEM_EtaOrigin);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_PhiOrigin", &jet_AntiKt4TopoEM_PhiOrigin, &b_jet_AntiKt4TopoEM_PhiOrigin);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_MOrigin", &jet_AntiKt4TopoEM_MOrigin, &b_jet_AntiKt4TopoEM_MOrigin);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_WIDTH", &jet_AntiKt4TopoEM_WIDTH, &b_jet_AntiKt4TopoEM_WIDTH);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_Timing", &jet_AntiKt4TopoEM_Timing, &b_jet_AntiKt4TopoEM_Timing);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_LArQuality", &jet_AntiKt4TopoEM_LArQuality, &b_jet_AntiKt4TopoEM_LArQuality);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_OriginIndex", &jet_AntiKt4TopoEM_OriginIndex, &b_jet_AntiKt4TopoEM_OriginIndex);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_HECQuality", &jet_AntiKt4TopoEM_HECQuality, &b_jet_AntiKt4TopoEM_HECQuality);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_NegativeE", &jet_AntiKt4TopoEM_NegativeE, &b_jet_AntiKt4TopoEM_NegativeE);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_AverageLArQF", &jet_AntiKt4TopoEM_AverageLArQF, &b_jet_AntiKt4TopoEM_AverageLArQF);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_BCH_CORR_CELL", &jet_AntiKt4TopoEM_BCH_CORR_CELL, &b_jet_AntiKt4TopoEM_BCH_CORR_CELL);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_BCH_CORR_JET", &jet_AntiKt4TopoEM_BCH_CORR_JET, &b_jet_AntiKt4TopoEM_BCH_CORR_JET);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_SamplingMax", &jet_AntiKt4TopoEM_SamplingMax, &b_jet_AntiKt4TopoEM_SamplingMax);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_fracSamplingMax", &jet_AntiKt4TopoEM_fracSamplingMax, &b_jet_AntiKt4TopoEM_fracSamplingMax);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_hecf", &jet_AntiKt4TopoEM_hecf, &b_jet_AntiKt4TopoEM_hecf);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_tgap3f", &jet_AntiKt4TopoEM_tgap3f, &b_jet_AntiKt4TopoEM_tgap3f);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_isUgly", &jet_AntiKt4TopoEM_isUgly, &b_jet_AntiKt4TopoEM_isUgly);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_isBadLooseMinus", &jet_AntiKt4TopoEM_isBadLooseMinus, &b_jet_AntiKt4TopoEM_isBadLooseMinus);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_isBadLoose", &jet_AntiKt4TopoEM_isBadLoose, &b_jet_AntiKt4TopoEM_isBadLoose);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_isBadMedium", &jet_AntiKt4TopoEM_isBadMedium, &b_jet_AntiKt4TopoEM_isBadMedium);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_isBadTight", &jet_AntiKt4TopoEM_isBadTight, &b_jet_AntiKt4TopoEM_isBadTight);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emfrac", &jet_AntiKt4TopoEM_emfrac, &b_jet_AntiKt4TopoEM_emfrac);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emscale_E", &jet_AntiKt4TopoEM_emscale_E, &b_jet_AntiKt4TopoEM_emscale_E);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emscale_pt", &jet_AntiKt4TopoEM_emscale_pt, &b_jet_AntiKt4TopoEM_emscale_pt);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emscale_m", &jet_AntiKt4TopoEM_emscale_m, &b_jet_AntiKt4TopoEM_emscale_m);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emscale_eta", &jet_AntiKt4TopoEM_emscale_eta, &b_jet_AntiKt4TopoEM_emscale_eta);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emscale_phi", &jet_AntiKt4TopoEM_emscale_phi, &b_jet_AntiKt4TopoEM_emscale_phi);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaPx", &jet_AntiKt4TopoEM_ActiveAreaPx, &b_jet_AntiKt4TopoEM_ActiveAreaPx);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaPy", &jet_AntiKt4TopoEM_ActiveAreaPy, &b_jet_AntiKt4TopoEM_ActiveAreaPy);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaPz", &jet_AntiKt4TopoEM_ActiveAreaPz, &b_jet_AntiKt4TopoEM_ActiveAreaPz);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaE", &jet_AntiKt4TopoEM_ActiveAreaE, &b_jet_AntiKt4TopoEM_ActiveAreaE);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_jvtxf", &jet_AntiKt4TopoEM_jvtxf, &b_jet_AntiKt4TopoEM_jvtxf);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_jvtx_x", &jet_AntiKt4TopoEM_jvtx_x, &b_jet_AntiKt4TopoEM_jvtx_x);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_jvtx_y", &jet_AntiKt4TopoEM_jvtx_y, &b_jet_AntiKt4TopoEM_jvtx_y);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_jvtx_z", &jet_AntiKt4TopoEM_jvtx_z, &b_jet_AntiKt4TopoEM_jvtx_z);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_TruthMFindex", &jet_AntiKt4TopoEM_TruthMFindex, &b_jet_AntiKt4TopoEM_TruthMFindex);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_TruthMF", &jet_AntiKt4TopoEM_TruthMF, &b_jet_AntiKt4TopoEM_TruthMF);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_e_EMB3", &jet_AntiKt4TopoEM_e_EMB3, &b_jet_AntiKt4TopoEM_e_EMB3);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_e_EME3", &jet_AntiKt4TopoEM_e_EME3, &b_jet_AntiKt4TopoEM_e_EME3);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_e_TileBar0", &jet_AntiKt4TopoEM_e_TileBar0, &b_jet_AntiKt4TopoEM_e_TileBar0);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_e_TileExt0", &jet_AntiKt4TopoEM_e_TileExt0, &b_jet_AntiKt4TopoEM_e_TileExt0);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_IP3D", &jet_AntiKt4TopoEM_flavor_weight_IP3D, &b_jet_AntiKt4TopoEM_flavor_weight_IP3D);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN", &jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN, &b_jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_MV1", &jet_AntiKt4TopoEM_flavor_weight_MV1, &b_jet_AntiKt4TopoEM_flavor_weight_MV1);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_SV1", &jet_AntiKt4TopoEM_flavor_weight_SV1, &b_jet_AntiKt4TopoEM_flavor_weight_SV1);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_nTrk_pv0_1GeV", &jet_AntiKt4TopoEM_nTrk_pv0_1GeV, &b_jet_AntiKt4TopoEM_nTrk_pv0_1GeV);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_sumPtTrk_allpv_1GeV", &jet_AntiKt4TopoEM_sumPtTrk_allpv_1GeV, &b_jet_AntiKt4TopoEM_sumPtTrk_allpv_1GeV);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_nTrk_pv0_500MeV", &jet_AntiKt4TopoEM_nTrk_pv0_500MeV, &b_jet_AntiKt4TopoEM_nTrk_pv0_500MeV);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_sumPtTrk_pv0_500MeV", &jet_AntiKt4TopoEM_sumPtTrk_pv0_500MeV, &b_jet_AntiKt4TopoEM_sumPtTrk_pv0_500MeV);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_trackWIDTH_pv0_1GeV", &jet_AntiKt4TopoEM_trackWIDTH_pv0_1GeV, &b_jet_AntiKt4TopoEM_trackWIDTH_pv0_1GeV);
   fChain->SetBranchAddress("AntiKt4Truth_pt", &AntiKt4Truth_pt, &b_AntiKt4Truth_pt);
   fChain->SetBranchAddress("AntiKt4Truth_m", &AntiKt4Truth_m, &b_AntiKt4Truth_m);
   fChain->SetBranchAddress("AntiKt4Truth_eta", &AntiKt4Truth_eta, &b_AntiKt4Truth_eta);
   fChain->SetBranchAddress("AntiKt4Truth_phi", &AntiKt4Truth_phi, &b_AntiKt4Truth_phi);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_n", &InTimeAntiKt4Truth_n, &b_InTimeAntiKt4Truth_n);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_E", &InTimeAntiKt4Truth_E, &b_InTimeAntiKt4Truth_E);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_pt", &InTimeAntiKt4Truth_pt, &b_InTimeAntiKt4Truth_pt);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_m", &InTimeAntiKt4Truth_m, &b_InTimeAntiKt4Truth_m);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_eta", &InTimeAntiKt4Truth_eta, &b_InTimeAntiKt4Truth_eta);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_phi", &InTimeAntiKt4Truth_phi, &b_InTimeAntiKt4Truth_phi);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_flavor_truth_label", &InTimeAntiKt4Truth_flavor_truth_label, &b_InTimeAntiKt4Truth_flavor_truth_label);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_flavor_truth_dRminToB", &InTimeAntiKt4Truth_flavor_truth_dRminToB, &b_InTimeAntiKt4Truth_flavor_truth_dRminToB);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_flavor_truth_dRminToC", &InTimeAntiKt4Truth_flavor_truth_dRminToC, &b_InTimeAntiKt4Truth_flavor_truth_dRminToC);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_flavor_truth_dRminToT", &InTimeAntiKt4Truth_flavor_truth_dRminToT, &b_InTimeAntiKt4Truth_flavor_truth_dRminToT);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_flavor_truth_BHadronpdg", &InTimeAntiKt4Truth_flavor_truth_BHadronpdg, &b_InTimeAntiKt4Truth_flavor_truth_BHadronpdg);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_flavor_truth_vx_x", &InTimeAntiKt4Truth_flavor_truth_vx_x, &b_InTimeAntiKt4Truth_flavor_truth_vx_x);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_flavor_truth_vx_y", &InTimeAntiKt4Truth_flavor_truth_vx_y, &b_InTimeAntiKt4Truth_flavor_truth_vx_y);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_flavor_truth_vx_z", &InTimeAntiKt4Truth_flavor_truth_vx_z, &b_InTimeAntiKt4Truth_flavor_truth_vx_z);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_constit_n", &InTimeAntiKt4Truth_constit_n, &b_InTimeAntiKt4Truth_constit_n);
   fChain->SetBranchAddress("InTimeAntiKt4Truth_constit_index", &InTimeAntiKt4Truth_constit_index, &b_InTimeAntiKt4Truth_constit_index);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_n", &OutOfTimeAntiKt4Truth_n, &b_OutOfTimeAntiKt4Truth_n);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_E", &OutOfTimeAntiKt4Truth_E, &b_OutOfTimeAntiKt4Truth_E);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_pt", &OutOfTimeAntiKt4Truth_pt, &b_OutOfTimeAntiKt4Truth_pt);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_m", &OutOfTimeAntiKt4Truth_m, &b_OutOfTimeAntiKt4Truth_m);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_eta", &OutOfTimeAntiKt4Truth_eta, &b_OutOfTimeAntiKt4Truth_eta);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_phi", &OutOfTimeAntiKt4Truth_phi, &b_OutOfTimeAntiKt4Truth_phi);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_flavor_truth_label", &OutOfTimeAntiKt4Truth_flavor_truth_label, &b_OutOfTimeAntiKt4Truth_flavor_truth_label);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_flavor_truth_dRminToB", &OutOfTimeAntiKt4Truth_flavor_truth_dRminToB, &b_OutOfTimeAntiKt4Truth_flavor_truth_dRminToB);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_flavor_truth_dRminToC", &OutOfTimeAntiKt4Truth_flavor_truth_dRminToC, &b_OutOfTimeAntiKt4Truth_flavor_truth_dRminToC);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_flavor_truth_dRminToT", &OutOfTimeAntiKt4Truth_flavor_truth_dRminToT, &b_OutOfTimeAntiKt4Truth_flavor_truth_dRminToT);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_flavor_truth_BHadronpdg", &OutOfTimeAntiKt4Truth_flavor_truth_BHadronpdg, &b_OutOfTimeAntiKt4Truth_flavor_truth_BHadronpdg);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_flavor_truth_vx_x", &OutOfTimeAntiKt4Truth_flavor_truth_vx_x, &b_OutOfTimeAntiKt4Truth_flavor_truth_vx_x);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_flavor_truth_vx_y", &OutOfTimeAntiKt4Truth_flavor_truth_vx_y, &b_OutOfTimeAntiKt4Truth_flavor_truth_vx_y);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_flavor_truth_vx_z", &OutOfTimeAntiKt4Truth_flavor_truth_vx_z, &b_OutOfTimeAntiKt4Truth_flavor_truth_vx_z);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_constit_n", &OutOfTimeAntiKt4Truth_constit_n, &b_OutOfTimeAntiKt4Truth_constit_n);
   fChain->SetBranchAddress("OutOfTimeAntiKt4Truth_constit_index", &OutOfTimeAntiKt4Truth_constit_index, &b_OutOfTimeAntiKt4Truth_constit_index);
   fChain->SetBranchAddress("vxp_n", &vxp_n, &b_vxp_n);
   fChain->SetBranchAddress("vxp_z", &vxp_z, &b_vxp_z);
   fChain->SetBranchAddress("vxp_nTracks", &vxp_nTracks, &b_vxp_nTracks);
   fChain->SetBranchAddress("mbtime_timeDiff", &mbtime_timeDiff, &b_mbtime_timeDiff);
   fChain->SetBranchAddress("mcevt_weight", &mcevt_weight, &b_mcevt_weight);
   fChain->SetBranchAddress("trig_L1_TAV", &trig_L1_TAV, &b_trig_L1_TAV);
   fChain->SetBranchAddress("trig_L2_passedPhysics", &trig_L2_passedPhysics, &b_trig_L2_passedPhysics);
   fChain->SetBranchAddress("trig_EF_passedPhysics", &trig_EF_passedPhysics, &b_trig_EF_passedPhysics);
   fChain->SetBranchAddress("trig_L1_TBP", &trig_L1_TBP, &b_trig_L1_TBP);
   fChain->SetBranchAddress("trig_L1_TAP", &trig_L1_TAP, &b_trig_L1_TAP);
   fChain->SetBranchAddress("trig_L2_passedRaw", &trig_L2_passedRaw, &b_trig_L2_passedRaw);
   fChain->SetBranchAddress("trig_EF_passedRaw", &trig_EF_passedRaw, &b_trig_EF_passedRaw);
   fChain->SetBranchAddress("trig_L2_truncated", &trig_L2_truncated, &b_trig_L2_truncated);
   fChain->SetBranchAddress("trig_EF_truncated", &trig_EF_truncated, &b_trig_EF_truncated);
   fChain->SetBranchAddress("trig_L2_resurrected", &trig_L2_resurrected, &b_trig_L2_resurrected);
   fChain->SetBranchAddress("trig_EF_resurrected", &trig_EF_resurrected, &b_trig_EF_resurrected);
   fChain->SetBranchAddress("trig_L2_passedThrough", &trig_L2_passedThrough, &b_trig_L2_passedThrough);
   fChain->SetBranchAddress("trig_EF_passedThrough", &trig_EF_passedThrough, &b_trig_EF_passedThrough);
   fChain->SetBranchAddress("trig_L2_wasPrescaled", &trig_L2_wasPrescaled, &b_trig_L2_wasPrescaled);
   fChain->SetBranchAddress("trig_L2_wasResurrected", &trig_L2_wasResurrected, &b_trig_L2_wasResurrected);
   fChain->SetBranchAddress("trig_EF_wasPrescaled", &trig_EF_wasPrescaled, &b_trig_EF_wasPrescaled);
   fChain->SetBranchAddress("trig_EF_wasResurrected", &trig_EF_wasResurrected, &b_trig_EF_wasResurrected);
   fChain->SetBranchAddress("trig_DB_SMK", &trig_DB_SMK, &b_trig_DB_SMK);
   fChain->SetBranchAddress("trig_DB_L1PSK", &trig_DB_L1PSK, &b_trig_DB_L1PSK);
   fChain->SetBranchAddress("trig_DB_HLTPSK", &trig_DB_HLTPSK, &b_trig_DB_HLTPSK);
   fChain->SetBranchAddress("trig_L1_jet_n", &trig_L1_jet_n, &b_trig_L1_jet_n);
   fChain->SetBranchAddress("trig_L1_jet_eta", &trig_L1_jet_eta, &b_trig_L1_jet_eta);
   fChain->SetBranchAddress("trig_L1_jet_phi", &trig_L1_jet_phi, &b_trig_L1_jet_phi);
   fChain->SetBranchAddress("trig_L1_jet_thrNames", &trig_L1_jet_thrNames, &b_trig_L1_jet_thrNames);
   fChain->SetBranchAddress("trig_L1_jet_thrValues", &trig_L1_jet_thrValues, &b_trig_L1_jet_thrValues);
   fChain->SetBranchAddress("trig_L1_jet_thrPattern", &trig_L1_jet_thrPattern, &b_trig_L1_jet_thrPattern);
   fChain->SetBranchAddress("trig_L1_jet_RoIWord", &trig_L1_jet_RoIWord, &b_trig_L1_jet_RoIWord);
   fChain->SetBranchAddress("trig_L1_jet_et4x4", &trig_L1_jet_et4x4, &b_trig_L1_jet_et4x4);
   fChain->SetBranchAddress("trig_L1_jet_et6x6", &trig_L1_jet_et6x6, &b_trig_L1_jet_et6x6);
   fChain->SetBranchAddress("trig_L1_jet_et8x8", &trig_L1_jet_et8x8, &b_trig_L1_jet_et8x8);
   fChain->SetBranchAddress("trig_L1_jet_myKin_pt", &trig_L1_jet_myKin_pt, &b_trig_L1_jet_myKin_pt);
   fChain->SetBranchAddress("trig_L1_jet_myKin_eta", &trig_L1_jet_myKin_eta, &b_trig_L1_jet_myKin_eta);
   fChain->SetBranchAddress("trig_L1_jet_myKin_phi", &trig_L1_jet_myKin_phi, &b_trig_L1_jet_myKin_phi);
   fChain->SetBranchAddress("trig_L2_jet_n", &trig_L2_jet_n, &b_trig_L2_jet_n);
   fChain->SetBranchAddress("trig_L2_jet_E", &trig_L2_jet_E, &b_trig_L2_jet_E);
   fChain->SetBranchAddress("trig_L2_jet_eta", &trig_L2_jet_eta, &b_trig_L2_jet_eta);
   fChain->SetBranchAddress("trig_L2_jet_phi", &trig_L2_jet_phi, &b_trig_L2_jet_phi);
   fChain->SetBranchAddress("trig_L2_jet_RoIWord", &trig_L2_jet_RoIWord, &b_trig_L2_jet_RoIWord);
   fChain->SetBranchAddress("trig_L2_jet_InputType", &trig_L2_jet_InputType, &b_trig_L2_jet_InputType);
   fChain->SetBranchAddress("trig_L2_jet_OutputType", &trig_L2_jet_OutputType, &b_trig_L2_jet_OutputType);
   fChain->SetBranchAddress("trig_L2_jet_counter", &trig_L2_jet_counter, &b_trig_L2_jet_counter);
   fChain->SetBranchAddress("trig_L2_jet_c4cchad", &trig_L2_jet_c4cchad, &b_trig_L2_jet_c4cchad);
   fChain->SetBranchAddress("trig_L2_jet_c4ccem", &trig_L2_jet_c4ccem, &b_trig_L2_jet_c4ccem);
   fChain->SetBranchAddress("trig_L2_jet_c4uchad", &trig_L2_jet_c4uchad, &b_trig_L2_jet_c4uchad);
   fChain->SetBranchAddress("trig_L2_jet_ehad0", &trig_L2_jet_ehad0, &b_trig_L2_jet_ehad0);
   fChain->SetBranchAddress("trig_L2_jet_eem0", &trig_L2_jet_eem0, &b_trig_L2_jet_eem0);
   fChain->SetBranchAddress("trig_L2_jet_nLeadingCells", &trig_L2_jet_nLeadingCells, &b_trig_L2_jet_nLeadingCells);
   fChain->SetBranchAddress("trig_L2_jet_hecf", &trig_L2_jet_hecf, &b_trig_L2_jet_hecf);
   fChain->SetBranchAddress("trig_L2_jet_jetQuality", &trig_L2_jet_jetQuality, &b_trig_L2_jet_jetQuality);
   fChain->SetBranchAddress("trig_L2_jet_emf", &trig_L2_jet_emf, &b_trig_L2_jet_emf);
   fChain->SetBranchAddress("trig_L2_jet_jetTimeCells", &trig_L2_jet_jetTimeCells, &b_trig_L2_jet_jetTimeCells);
   fChain->SetBranchAddress("trig_EF_jet_n", &trig_EF_jet_n, &b_trig_EF_jet_n);
   fChain->SetBranchAddress("trig_EF_jet_E", &trig_EF_jet_E, &b_trig_EF_jet_E);
   fChain->SetBranchAddress("trig_EF_jet_pt", &trig_EF_jet_pt, &b_trig_EF_jet_pt);
   fChain->SetBranchAddress("trig_EF_jet_m", &trig_EF_jet_m, &b_trig_EF_jet_m);
   fChain->SetBranchAddress("trig_EF_jet_eta", &trig_EF_jet_eta, &b_trig_EF_jet_eta);
   fChain->SetBranchAddress("trig_EF_jet_phi", &trig_EF_jet_phi, &b_trig_EF_jet_phi);
   fChain->SetBranchAddress("trig_EF_jet_emscale_E", &trig_EF_jet_emscale_E, &b_trig_EF_jet_emscale_E);
   fChain->SetBranchAddress("trig_EF_jet_emscale_pt", &trig_EF_jet_emscale_pt, &b_trig_EF_jet_emscale_pt);
   fChain->SetBranchAddress("trig_EF_jet_emscale_m", &trig_EF_jet_emscale_m, &b_trig_EF_jet_emscale_m);
   fChain->SetBranchAddress("trig_EF_jet_emscale_eta", &trig_EF_jet_emscale_eta, &b_trig_EF_jet_emscale_eta);
   fChain->SetBranchAddress("trig_EF_jet_emscale_phi", &trig_EF_jet_emscale_phi, &b_trig_EF_jet_emscale_phi);
   fChain->SetBranchAddress("trig_EF_jet_constscale_E", &trig_EF_jet_constscale_E, &b_trig_EF_jet_constscale_E);
   fChain->SetBranchAddress("trig_EF_jet_constscale_pt", &trig_EF_jet_constscale_pt, &b_trig_EF_jet_constscale_pt);
   fChain->SetBranchAddress("trig_EF_jet_constscale_m", &trig_EF_jet_constscale_m, &b_trig_EF_jet_constscale_m);
   fChain->SetBranchAddress("trig_EF_jet_constscale_eta", &trig_EF_jet_constscale_eta, &b_trig_EF_jet_constscale_eta);
   fChain->SetBranchAddress("trig_EF_jet_constscale_phi", &trig_EF_jet_constscale_phi, &b_trig_EF_jet_constscale_phi);
   fChain->SetBranchAddress("trig_EF_jet_author", &trig_EF_jet_author, &b_trig_EF_jet_author);
   fChain->SetBranchAddress("trig_EF_jet_calibtags", &trig_EF_jet_calibtags, &b_trig_EF_jet_calibtags);
   fChain->SetBranchAddress("trig_EF_jet_n90", &trig_EF_jet_n90, &b_trig_EF_jet_n90);
   fChain->SetBranchAddress("trig_EF_jet_Timing", &trig_EF_jet_Timing, &b_trig_EF_jet_Timing);
   fChain->SetBranchAddress("trig_EF_jet_LArQuality", &trig_EF_jet_LArQuality, &b_trig_EF_jet_LArQuality);
   fChain->SetBranchAddress("trig_EF_jet_HECQuality", &trig_EF_jet_HECQuality, &b_trig_EF_jet_HECQuality);
   fChain->SetBranchAddress("trig_EF_jet_NegativeE", &trig_EF_jet_NegativeE, &b_trig_EF_jet_NegativeE);
   fChain->SetBranchAddress("trig_EF_jet_fracSamplingMax", &trig_EF_jet_fracSamplingMax, &b_trig_EF_jet_fracSamplingMax);
   fChain->SetBranchAddress("trig_EF_jet_SamplingMax", &trig_EF_jet_SamplingMax, &b_trig_EF_jet_SamplingMax);
   fChain->SetBranchAddress("trig_EF_jet_n90constituents", &trig_EF_jet_n90constituents, &b_trig_EF_jet_n90constituents);
   fChain->SetBranchAddress("trig_EF_jet_TileQuality", &trig_EF_jet_TileQuality, &b_trig_EF_jet_TileQuality);
   fChain->SetBranchAddress("trig_EF_jet_hecF", &trig_EF_jet_hecF, &b_trig_EF_jet_hecF);
   fChain->SetBranchAddress("trig_EF_jet_EMFraction", &trig_EF_jet_EMFraction, &b_trig_EF_jet_EMFraction);
   fChain->SetBranchAddress("trig_EF_jet_presamplerFraction", &trig_EF_jet_presamplerFraction, &b_trig_EF_jet_presamplerFraction);
   fChain->SetBranchAddress("trig_EF_jet_AverageLArQF", &trig_EF_jet_AverageLArQF, &b_trig_EF_jet_AverageLArQF);
   fChain->SetBranchAddress("trig_EF_jet_EMJES", &trig_EF_jet_EMJES, &b_trig_EF_jet_EMJES);
   fChain->SetBranchAddress("trig_EF_jet_EMJES_EtaCorr", &trig_EF_jet_EMJES_EtaCorr, &b_trig_EF_jet_EMJES_EtaCorr);
   fChain->SetBranchAddress("trig_EF_jet_Discriminant", &trig_EF_jet_Discriminant, &b_trig_EF_jet_Discriminant);
   fChain->SetBranchAddress("trig_EF_jet_MeanTowerEtUnsubtr", &trig_EF_jet_MeanTowerEtUnsubtr, &b_trig_EF_jet_MeanTowerEtUnsubtr);
   fChain->SetBranchAddress("trig_EF_jet_TrigJetFeaturesUnpacker_Status", &trig_EF_jet_TrigJetFeaturesUnpacker_Status, &b_trig_EF_jet_TrigJetFeaturesUnpacker_Status);
   fChain->SetBranchAddress("trig_EF_jet_a4tc", &trig_EF_jet_a4tc, &b_trig_EF_jet_a4tc);
   fChain->SetBranchAddress("trig_EF_jet_a10tc", &trig_EF_jet_a10tc, &b_trig_EF_jet_a10tc);
   fChain->SetBranchAddress("trig_EF_jet_a2hi", &trig_EF_jet_a2hi, &b_trig_EF_jet_a2hi);
   fChain->SetBranchAddress("trig_EF_jet_RoIword", &trig_EF_jet_RoIword, &b_trig_EF_jet_RoIword);
   Notify();
}

Bool_t makeclass_tla_validation::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.
   
   return kTRUE;
}

void makeclass_tla_validation::Show(Long64_t entry)
{
   // Print contents of entry.
   // If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t makeclass_tla_validation::Cut(Long64_t entry)
{
   // This function may be called from Loop.
   // returns  1 if entry is accepted.
   // returns -1 otherwise.
   return 1;
}
#endif // #ifdef makeclass_tla_validation_cxx

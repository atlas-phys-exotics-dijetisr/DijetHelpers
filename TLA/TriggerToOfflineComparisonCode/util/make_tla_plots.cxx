#include<TriggerToOfflineComparisonCode/make_tla_plots.h>

int main(int argc, char **argv) {
  TString ifn, ofs;	//input file name and output file suffix

  if (argc<2) {printf("You have to give the input file name (output file suffix is optional)!\n\n"); return 0; }
  if (argc==2) {
   ifn=argv[1];
  	ofs=ifn; ofs.ReplaceAll(".root","");
   printf("The output files' suffix was set to %s\n\n",ofs.Data()); 
  }
  if (argc>2) {
  	ifn=argv[1]; ofs=argv[2];
	printf("The output files' suffix will be: %s\n",ofs.Data());
  }
  printf("Will now make the plots.\n\n");
  plot_tla_validation(ifn,ofs);

  printf("All done with plotting!\n");
}

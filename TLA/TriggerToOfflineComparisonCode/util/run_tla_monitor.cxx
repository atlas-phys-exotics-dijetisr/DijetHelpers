#include "TChain.h"
#include "TriggerToOfflineComparisonCode/makeclass_tla_validation.h"

int main(int argc, char **argv) {
  if (argc<3) {printf("You have to give the following as arguments:\n\n 1) the input file directory\n 2) the output file name\n 3) the energy scale in TeV *[8 or 14]* (if jet energy calibration is needed)\n\n"); return 0; }
  TString dir=argv[1];
  TString ofn=argv[2];
  int DataEnergyTeV = 0;
  if (argc>=4) DataEnergyTeV = atoi(argv[3]);

  printf("Will run over files in directory %s\n",dir.Data());
  printf("The output file name will be: %s\n",ofn.Data());

  TChain *chain = new TChain("physics");
  chain->Add(dir+"/*root*");

  makeclass_tla_validation tla(chain);
  printf("Will now loop.\n");
  tla.Loop(ofn,DataEnergyTeV);

  printf("All done with running!\n");
}

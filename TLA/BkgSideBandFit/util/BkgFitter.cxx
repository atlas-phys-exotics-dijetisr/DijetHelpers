//////////////////////////////////////////////////////////////////////
//
//Use: 
//BkgFitter file.root histo_name
//
//Where file.root contains a TH1 histogram called histo_name 
//
///////////////////////////////////////////////////////////////////////
#define COUT std::cout << "I'm here : " << __FILE__ << " : " << __LINE__ << std::endl;
#include "TSystem.h"
#include <TCanvas.h>
#include <TTree.h>
#include <TFile.h>
#include <TString.h>
#include <TGraph.h>
#include "THStack.h"
#include "TCut.h" 
#include "TLegend.h"
#include "TLatex.h"
#include "TAxis.h"
#include "TPaveText.h"
#include "TPaveStats.h"
#include "TMath.h"
#include "TInterpreter.h"
#include "TEnv.h"
#include "TEfficiency.h"
#include "TGraphAsymmErrors.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TMatrixDSym.h"
#include "TVirtualFitter.h"
#include "TRandom3.h"
#include <sstream>
#include <TMinuit.h>
#include "Math/ProbFuncMathCore.h"
#include "Math/MinimizerOptions.h"
#include "Minuit2/Minuit2Minimizer.h"
#include "Math/Functor.h"

#include <stdio.h>
#include <vector>
#include <fstream>
#include <iostream>

#include <TEventList.h>
#include <TList.h>

#include <math.h>
#include <string>

#include <stdlib.h>
#include <time.h>

#include "BkgSideBandFit/Utilities.h"
#include "BkgSideBandFit/AtlasStyle.C"

using namespace std;

//Global variables
double sqrt_s;
double start_b1;
double end_b1;
double start_b2;
double end_b2; 
string fit_type;
Double_t est_mjj_low;
Double_t est_mjj_high;
Double_t est_mjj_middle;
Double_t previous_est;
Double_t previous_bin_error;
int    left_side_nbins; 
int    right_side_nbins;
int    middle_nbins;
int    n_param_func;
Bool_t reject;
bool   do_global_fit_quality;
bool   do_smoothing;
int    n_pseudo_exp;
int    do_n_refits;
bool   do_scaled_hist_plot;
bool   do_window_pvalue;

int n_fit_attempts;
float min_chi_per_DoF; 
float variability_factor;

int smooth_step;//Needs to be an odd integer
double second_der_accuracy;

vector<double> previous_function_param;
int NDF;


const double GeV = 1000.0;


void InitializeGlobalVariables(TH1D * h, double mjj_start, int left_B1_size, int SR_size, int right_B2_size){

  int first_bin_b1 = h->FindBin(mjj_start);
  int last_bin_b1  = first_bin_b1 + left_B1_size; // Always look at left-side of bin for mass value delimitations
  start_b1 = h->GetXaxis()->GetBinLowEdge(first_bin_b1);
  end_b1 = h->GetXaxis()->GetBinLowEdge(last_bin_b1);
  
  int first_bin_b2 = last_bin_b1 + SR_size;
  int last_bin_b2 = first_bin_b2 + right_B2_size;
  start_b2 = h->GetXaxis()->GetBinLowEdge(first_bin_b2);
  end_b2   = h->GetXaxis()->GetBinLowEdge(last_bin_b2);

}

Double_t Function(Double_t *x, Double_t *par){
  
  if (reject && ( x[0] <= start_b1 || (x[0] >= end_b1 && x[0] <= start_b2) || x[0] >= end_b2 ) ){
    TF1::RejectPoint();
    return 0;
  }
  //Write 3 parameters function
  if(n_param_func == 3){ return par[0] * pow((1 - (x[0]/sqrt_s)),par[1]) * pow((x[0]/sqrt_s), par[2]); }
  if(n_param_func == 4){ return par[0] * pow((1 - (x[0]/sqrt_s)),par[1]) * pow((x[0]/sqrt_s),(par[2] + par[3] * log((x[0]/sqrt_s)) ) ) ; }
  if(n_param_func == 5){ return par[0] * pow((1 - (x[0]/sqrt_s)),par[1]) * pow((x[0]/sqrt_s),(par[2] + par[3] * log((x[0]/sqrt_s)) + par[4] * pow((log(x[0]/sqrt_s)), 2) ) ); }
  
  //if reading here
  return 0; 
  
}

Double_t Global_Function(Double_t *x, Double_t *par){
  
  //Write 3 parameters function
  if(n_param_func == 3){ return par[0] * pow((1 - (x[0]/sqrt_s)),par[1]) * pow((x[0]/sqrt_s), par[2]); }
  if(n_param_func == 4){ return par[0] * pow((1 - (x[0]/sqrt_s)),par[1]) * pow((x[0]/sqrt_s),(par[2] + par[3] * log((x[0]/sqrt_s)) ) ) ; }
  if(n_param_func == 5){ return par[0] * pow((1 - (x[0]/sqrt_s)),par[1]) * pow((x[0]/sqrt_s),(par[2] + par[3] * log((x[0]/sqrt_s)) + par[4] * pow((log(x[0]/sqrt_s)), 2) ) ); }
  
  //if reading here
  return 0; 
  
}


double PoissonPval(double d, double b){//Poisson p value calc, as done in StatisticalAnalyssis2012 package.
 
  double answer = 1.;

  if (d >= b) {
    answer = TMath::Gamma(d,b);
  }
  else {
    answer = 1. - TMath::Gamma(d+1,b);
  }
  return answer;
}

double PoissonProb(double d, double b){

  return TMath::PoissonI(d, b);

}


void InitializeFunctionParameters(TF1 * function){

  //Could make this iterative
  function->SetParameter(0, 500.);
  function->SetParameter(1, 76.0);
  function->SetParameter(2, -1.0);
  function->SetParameter(3, 2.0);
  function->SetParameter(4, 1.25);

}

void InitializeFunctionParameters(TF1 * function, vector<double> param ){

  //Could make this iterative
  for(int itr = 0; itr < (int) param.size(); itr++){
    function->SetParameter(itr, param[itr]);
  }

}

bool IsTooManyEmptyBins(TH1D * h_est, int empty_bin){
  
  int n_empty_bins_tail = 0;
  int n_empty_bins_head = 0;
  for(int itr = empty_bin; itr > empty_bin - right_side_nbins +1; itr--){
    if(h_est->GetBinContent(itr) == 0){ n_empty_bins_tail++; }
  }
  for(int itr = empty_bin-(left_side_nbins + middle_nbins + right_side_nbins); itr < empty_bin-(middle_nbins + right_side_nbins); itr++){
    if(h_est->GetBinContent(itr) == 0){ n_empty_bins_head++; }
  }
  if( (right_side_nbins - n_empty_bins_tail)  < 2 ){ return true; } //Require at least 2 non-empty bins 
  if( (n_empty_bins_tail + n_empty_bins_head) - (right_side_nbins + left_side_nbins) > n_param_func ) { return true; } //Making sure the fit has at least one degree of freedom

  return false;

}

void CreateHistogramGlobalFit(TH1D * h_est, TH1D * h_est_glob_fit, TF1 * fit_glob){
  
  for(int itr = h_est->FindFirstBinAbove(0) ; itr <= h_est->FindLastBinAbove(0); itr++){
    h_est_glob_fit->SetBinContent(itr, fit_glob->Integral(h_est->GetBinLowEdge(itr), h_est->GetBinLowEdge(itr+1))/h_est->GetBinWidth(itr));
  }

}

void Beautify(TH1D * h){

  h->SetTitle("");
  h->GetXaxis()->SetTitle("mjj [GeV]");
  h->GetXaxis()->SetLabelSize(0.1);
  h->GetXaxis()->SetTitleOffset(0.9);
  h->GetXaxis()->SetTitleSize(0.17);
  h->GetXaxis()->SetLabelSize(0.15);
  h->GetYaxis()->SetTitle("Significance");
  h->GetYaxis()->SetTitleOffset(0.4);
  h->GetYaxis()->SetTitleSize(0.15);
  h->GetYaxis()->SetLabelSize(0.07);

}


void ConstructSignificantHistogram(TH1D * significance, TH1D * h_est, TH1D * h_data){
  
  for ( int itr_bin = h_est->FindFirstBinAbove(0,1); itr_bin < (h_est->FindLastBinAbove(0,1) + 1); itr_bin++){
      Double_t sig = 0.;
      if (h_data->GetBinContent(itr_bin) > 0.){
	sig = ( h_data->GetBinContent(itr_bin) - h_est->GetBinContent(itr_bin) )/sqrt(h_data->GetBinContent(itr_bin)) ;
      }  
      significance->SetBinContent(itr_bin, sig);
    }

}
  
TF1 * ComputeGlobalFit(TH1D * histo, TF1 * fit_function, int start, int end){
  
  Info("BkgFitter()", "====================> Computing Global Fit <=========================" );
  
  Double_t fit_range_min = histo->GetBinLowEdge(histo->FindBin(start));
  Double_t fit_range_max = histo->GetBinLowEdge(histo->FindBin(end)+1);
  Info("BkgFitter()", "Global fit range : [%f, %f]", fit_range_min, fit_range_max);

  InitializeFunctionParameters(fit_function); 
  fit_function->SetLineColor(8);
  TFitResultPtr fit_result = histo->Fit(fit_function, "LSN","", fit_range_min, fit_range_max);
  int fit_status = fit_result->Status();
  while(fit_status > 5){ 
    fit_status = fit_status/10.0;
  }
  
  
  ///////////////////////////////////////////////
  //Make sure fit is OK:
  ///////////////////////////////////////////////
  
  int number_of_fit_attempts = 0;
  while((fit_status == 4 && number_of_fit_attempts < n_fit_attempts) ){

    if(number_of_fit_attempts%500 == 0){ Info("BkgFitter()", "Attempting %i fit", number_of_fit_attempts); }
    if(number_of_fit_attempts%10  == 0){ InitializeFunctionParameters(fit_function ); } //Go back to initial parameters
    
    for(int itr = 0; itr < (int) fit_function->GetNpar(); itr++){
      if(fit_function->GetParameter(itr) != fit_function->GetParameter(itr)){
	Info("BkgFitter()", "Parameter %i is NaN", itr);
	break;
      }
      TRandom3 * Random_Gauss = new TRandom3();
      Random_Gauss->SetSeed((2*(number_of_fit_attempts))+5); //Need odd number seed
      double random_variation = Random_Gauss->Gaus(0., 1.0);
      fit_function->SetParameter(itr, fit_function->GetParameter(itr)*(1.0 + variability_factor*random_variation) );
      delete Random_Gauss;
    }
    number_of_fit_attempts++;
    fit_result = histo->Fit(fit_function , "LSN", "", fit_range_min, fit_range_max);
    fit_status = fit_result->Status();
  }


  Info("BkgFitter()", "Found global fit solution after %i attempts", number_of_fit_attempts);
    
  return fit_function;

}

double ComputeWindowPvalue(TH1D * histo_main, TH1D * h_estimation, int window_first_bin, int window_last_bin, int n_pseudoexp){

  double data = histo_main->Integral(window_first_bin,window_last_bin);
  double estimation = h_estimation->Integral(window_first_bin,window_last_bin);
  double poisson_prob_data = PoissonPval(data,estimation);
  
  TRandom3 * random = new TRandom3();
  vector<double> probability_distribution; probability_distribution.reserve(n_pseudoexp);
  
  double random_poisson;
  for (int itr = 0;itr < n_pseudoexp; itr++){
    random_poisson = 0;
    for (int itr_bin = window_first_bin; itr_bin < window_last_bin; itr_bin++){
      random->SetSeed(2*(itr+time(0))+1);
      random_poisson += random->Poisson(h_estimation->GetBinContent(itr_bin));
    }
    
    probability_distribution.push_back(PoissonPval(random_poisson, estimation));
    std::sort(probability_distribution.begin(),probability_distribution.end());
  }
  
  double p_value = 0.;
  for (int itr = 0; itr < probability_distribution.size(); itr++){
    if (probability_distribution[itr] > poisson_prob_data){
      p_value = double(itr) / double(n_pseudoexp);
      break;
    }
  }
  if(p_value < 5.0/(n_pseudoexp) && n_pseudoexp < 2e4 ){ //do more pseudo-experiments when p-value is small
    probability_distribution.reserve(2*n_pseudoexp);
    for (int itr = n_pseudoexp; itr < 2*n_pseudoexp; itr++){
      random_poisson = 0;
      for (int itr_bin = window_first_bin; itr_bin < window_last_bin; itr_bin++){
        random->SetSeed(2*(itr+time(0))+11);
        random_poisson += random->Poisson(h_estimation->GetBinContent(itr_bin));
      }
      probability_distribution.push_back(PoissonPval(random_poisson, estimation));
      std::sort(probability_distribution.begin(),probability_distribution.end());
    }
    for (int itr = 0; itr < probability_distribution.size(); itr++){
      if (probability_distribution[itr] > poisson_prob_data){
        p_value = double(itr) / double(2*n_pseudoexp);
        break;
      }
    }
  }
  delete random;
  return p_value;
  
}

double ComputeWindowPvalue_NLL(TH1D * histo_main, TH1D * h_estimation, int window_first_bin, int window_last_bin, int n_pseudoexp){
  
  TRandom3 * random = new TRandom3();
  vector<double> likelihood_distribution; likelihood_distribution.reserve(n_pseudoexp);
  
  double poisson;
  double NLL;
  for (int itr = 0; itr < n_pseudoexp; itr++){
    poisson = 0.;
    NLL = 0.;
    for (int itr_bin = window_first_bin; itr_bin < window_last_bin; itr_bin++){
      random->SetSeed(2*(itr+time(0))+1);
      poisson = random->Poisson(h_estimation->GetBinContent(itr_bin));
      NLL += log(PoissonProb(poisson, h_estimation->GetBinContent(itr_bin)));
    }
    //if(itr%100 == 0){ Info("BkgFitter()", "Window pseudo-experiment %i has a negative-Log-Likelihood value = %f", itr, NLL); }
    likelihood_distribution.push_back(NLL);
    std::sort(likelihood_distribution.begin(), likelihood_distribution.end());
  }

  double NLL_data = 0.;
  for (int itr_bin = window_first_bin; itr_bin < window_last_bin; itr_bin++){
    NLL_data += log(PoissonProb(histo_main->GetBinContent(itr_bin), h_estimation->GetBinContent(itr_bin)));
  }
  //Info("BkgFitter()", "Window has a data negative-Log-Likelihood value = %f", NLL_data); 
  
  double p_value = 0;
  for (int itr = 0; itr < likelihood_distribution.size(); itr++){
    if (likelihood_distribution[itr] > NLL_data){
      p_value = double(itr) / double(n_pseudoexp);
      break;
    }
  }
  
  if(p_value < 5.0/(n_pseudoexp) && n_pseudoexp < 2e4 ){ //do more pseudo-experiments when p-value is small
    likelihood_distribution.reserve(2*n_pseudoexp);
    for (int itr = n_pseudoexp; itr < 2*n_pseudoexp; itr++){
      for (int itr_bin = window_first_bin; itr_bin < window_last_bin; itr_bin++){
        random->SetSeed(2*(itr+time(0))+11);
        poisson = random->Poisson(h_estimation->GetBinContent(itr_bin));
        NLL += log(PoissonProb(poisson, h_estimation->GetBinContent(itr_bin)));
      }
      likelihood_distribution.push_back(NLL);
      std::sort(likelihood_distribution.begin(), likelihood_distribution.end());
    }
    for (int itr = 0; itr < likelihood_distribution.size(); itr++){
      if (likelihood_distribution[itr] > NLL_data){
        p_value = double(itr) / double(2*n_pseudoexp);
        break;
      }
    }
  }
  
  delete random;
  return p_value;
}

double ComputeWindowChi2(TH1D * histo_main, TH1D * h_estimation, int window_first_bin, int window_last_bin, int NDF){
  
  double chi2 = 0;
  for (int itr = window_first_bin; itr <= window_last_bin; itr++){
    chi2+= pow((h_estimation->GetBinContent(itr) - histo_main->GetBinContent(itr)),2)/sqrt(pow(histo_main->GetBinError(itr),2)+pow(h_estimation->GetBinError(itr),2));
  }
  return chi2;
}
    


double ComputeGlobalPvalue(TH1D * histo_main, TH1D * h_estimation, int n_pseudoexp, string output_dir){

  Info("BkgFitter()", "---------> Computing global p-value");

  TH1D * h_neg_log_likelihood = new TH1D("h_neg_log_likelihood", "Negative Log-Likelihood", 400, -1000., 0.);
  TRandom3 * random = new TRandom3(); 
  int bin_start = h_estimation->FindFirstBinAbove(0);
  int bin_end   = h_estimation->FindLastBinAbove(0);

  //Construct negative log-likelihood distribution:
  vector<double> likelihood_distribution; likelihood_distribution.reserve(n_pseudoexp);
  
  for(int itr = 0; itr < n_pseudoexp; itr++){ //Iterate over pseudo-experiments 
    double n_log_likelihood = 0.;
    for(int itr_bin = bin_start; itr_bin < bin_end; itr_bin++){ //Bin iteration 
      random->SetSeed(2*(itr+itr_bin+time(0))+1);
      double estimation = h_estimation->GetBinContent(itr_bin);
      double poisson = random->Poisson(h_estimation->GetBinContent(itr_bin));
      double data = histo_main->GetBinContent(itr_bin);
      //n_log_likelihood += log( PoissonProb( random->Poisson(histo_main->GetBinContent(itr_bin)), histo_main->GetBinContent(itr_bin) ) );//Generate pseudo from data
      n_log_likelihood += log( PoissonProb( random->Poisson(h_estimation->GetBinContent(itr_bin)), h_estimation->GetBinContent(itr_bin) ) );//from est
      if(itr == 0){ h_neg_log_likelihood->SetBins(400, (n_log_likelihood-50.0), (n_log_likelihood+50)); }
    }
    if(itr%1000 == 0){ Info("BkgFitter()", "Pseudo-experiment %i has a negative-Log-Likelihood value = %f", itr, n_log_likelihood); }
    likelihood_distribution.push_back(n_log_likelihood);
    h_neg_log_likelihood->Fill(n_log_likelihood);
    std::sort(likelihood_distribution.begin(), likelihood_distribution.end());
  }
  
  //Repeat for data:
  double likelihood_distribution_data = 0.;
  double n_log_likelihood_data = 0.;
  for(int itr_bin = bin_start; itr_bin < bin_end; itr_bin++){
    //n_log_likelihood_data += log( PoissonProb( h_estimation->GetBinContent(itr_bin), histo_main->GetBinContent(itr_bin) ) );//Pseudo from data
    n_log_likelihood_data += log( PoissonProb( histo_main->GetBinContent(itr_bin), h_estimation->GetBinContent(itr_bin) ) );//Pseudo from est
  }
  Info("BkgFitter()", "Data has a negative-Log-Likelihood value = %f", n_log_likelihood_data);  
  
  //Compute Data's p-value:
  double p_value = 0.0;
  for(int itr = 0; itr < likelihood_distribution.size(); itr++){
    if(likelihood_distribution[itr] > n_log_likelihood_data){
      p_value = (double) (itr) / (double) n_pseudoexp ;
      break;
    }
  }
 
  //Draw likelihood distribution and save:
  TCanvas * canvas_NLL = new TCanvas("canvas_NLL", "Negative Log-Likelihood", 800, 600);
  TLine * data_line = new TLine(n_log_likelihood_data, 0., -1000., 1000.);
  data_line->SetVertical(kTRUE);
  data_line->SetLineColor(2);
  canvas_NLL->cd();
  h_neg_log_likelihood->SetFillColor(kYellow);
  h_neg_log_likelihood->Draw();
  data_line->Draw("SAME");
  string output_file = output_dir+"/neg_log_likelihood.root";
  canvas_NLL->SaveAs(output_file.c_str());
  
  delete h_neg_log_likelihood;
  delete canvas_NLL;
  delete data_line;
  delete random;
  return p_value;
  
}

void ComputeBkgEstimation(TH1D * histo, TH1D * h_bkg, TF1 * fit_function, double start, double end, int b1, int s, int b2, string output_dir){
  
  if (histo->GetNbinsX() != h_bkg->GetNbinsX() ) {
    Error("BkgFitter()", "Input histogram bin specs don't match output");
  }  

  int start_bin = histo->FindBin(start);
  int end_bin   = histo->FindBin(end);
  int est_bin;  //Bin to be calculated in each step
  
  //start estimation 4 bins after the maximum
  //  start_bin = histo->GetMaximumBin() + 4;

  //mjj region [low, high] for which the estimation will be computed
  est_mjj_low = -1; 
  est_mjj_middle = -1; 
  est_mjj_high = -1;

  Info("BkgFitter()", "Fitting the background from %.2f GeV to %.2f GeV", histo->GetXaxis()->GetBinLowEdge(start_bin), histo->GetXaxis()->GetBinLowEdge(end_bin));
  Info("BkgFitter()", "Estimating the background from %.2f GeV to %.2f GeV", histo->GetXaxis()->GetBinLowEdge(start_bin+b1+int((s-1)/2)), histo->GetXaxis()->GetBinLowEdge(end_bin+1-b2-int(s/2)+(s+1)%2));
  int n_fits = 0;
  int n_good_fits = 0;
  NDF = b1+b2-n_param_func; //Degrees of freedom 

  previous_est = -1.0; //For EVEN number of bins in signal region
  previous_bin_error = -1.0; //For EVEN number of bins in signal region
  vector<double> previous_function_param; 
  vector<double> saved_function_param; //This holds the function parameters where the lowest pval bin

  //file to store the fit output at each step of iteration
  string filename = output_dir+"/iteration_fits.root";
  TString file_name = filename;
  TFile *iter_fit   = new TFile(file_name, "RECREATE");
  TCanvas *canvas_iter_fit = new TCanvas("canvas_iter_fit","canvas_iter_fit",800,600);

  //file to store the chi over mjj plot (before refitting)

 
  SetAtlasStyle();
  
  Double_t min_pval = 1.;//To motinor bin pvalue    
  int min_pval_bin = -1.;
  int min_pval_fit = -1.;

  vector<double> mjj_values;
  vector<double> chi_values;

  ////////////////////////////Fit - Loop///////////////////////////////////

  
  double normalization = 0.;
  
  for (est_bin = start_bin + b1 + int((s-1)/2) ; est_bin < end_bin - b2 -int(s/2) + 1 ; est_bin++ )  {
    //
    if( IsTooManyEmptyBins(histo, est_bin + int(s/2) + b2) ) {

      //if( histo->GetBinContent(est_bin + int(s/2) + b2) == 0. ){ //Force stop as soon as one bin is empty
      Info("BkgFitter()", "Quitting fits since bin %i is empty", est_bin + int(s/2) + b2);
      //continue;
      break;
    }
    
    n_fits++;
    Info("BkgFitter()", "================================> Fit #%i <==============================================================================================================", n_fits ); 
    
    reject = kTRUE;
    
    //Bin at the center of the signal window for which the background is estimated
    //In case signal region is even, est bin is the first of the 2 mid bins.
    //est_bin   = ibin + b1 + int((s-1)/2); //Rounds off to lower value. 
    
    Info("BkgFitter()", "Start of B1 at %.1f GeV", start_b1);
    Info("BkgFitter()", "  End of B1 at %.1f GeV", end_b1);
    Info("BkgFitter()", "Start of B2 at %.1f GeV", start_b2);
    Info("BkgFitter()", "  End of B2 at %.1f GeV", end_b2);
    
    InitializeFunctionParameters(fit_function, previous_function_param); //Initializing with previous function parameters 

    ///////////////////////////////////////////////
    //Perform fit
    ///////////////////////////////////////////////
    TFitResultPtr fit_result = histo->Fit(fit_function , fit_type.c_str());
    int fit_status = fit_result->Status();
    while(fit_status > 5){ 
      fit_status = fit_status/10.0;
    }
    
    ///////////////////////////////////////////////
    //Make sure fit is OK:
    ///////////////////////////////////////////////
    int number_of_fit_attempts = 0;
    float best_chi_square = 1e10;
    while((fit_status == 4 && number_of_fit_attempts < n_fit_attempts) || //Bad Fit
	  (fit_status != 4 && number_of_fit_attempts < n_fit_attempts*10 && fit_function->GetChisquare()/NDF > min_chi_per_DoF ) || // ){ //Bad Chi2
	  ( normalization <= 0. && number_of_fit_attempts < n_fit_attempts ) ){
      if(number_of_fit_attempts%10 == 0){// Go back to original initialization to not divert to far away from it
	InitializeFunctionParameters(fit_function, previous_function_param);
      }
      bool found_nan = false;
      for(int itr = 0; itr < (int) fit_function->GetNpar(); itr++){
	if(fit_function->GetParameter(itr) != fit_function->GetParameter(itr)){
	  Info("BkgFitter()", "Parameter %i is NaN", itr);
	  found_nan = true;
	}
	TRandom3 * Random_Gauss = new TRandom3();
       	Random_Gauss->SetSeed((2*(n_fits+number_of_fit_attempts+itr+time(0)))+1); //Need odd number seed
	double random_variation = Random_Gauss->Gaus(0., 1.0);// 
	fit_function->SetParameter(itr, fit_function->GetParameter(itr)*(1.0 + variability_factor*random_variation) );
	delete Random_Gauss;
      }
      if(found_nan){ break;}
      fit_result = histo->Fit(fit_function , fit_type.c_str());
      fit_status = fit_result->Status();
      while(fit_status > 5){ 
	fit_status = fit_status/10.0;
      }
      number_of_fit_attempts++;
      if(best_chi_square > fit_function->GetChisquare()/NDF){ best_chi_square = fit_function->GetChisquare()/NDF; }
      
      normalization = fit_function->GetParameter(0);
      
    }
    if(number_of_fit_attempts == 0){
      Info("BkgFitter()", "Found good fit on first try."); n_good_fits++;
    }
    if(number_of_fit_attempts < n_fit_attempts && number_of_fit_attempts > 0){
      Info("BkgFitter()", "Found good fit after %i re-attempt(s).", number_of_fit_attempts); n_good_fits++;
    }
    if(number_of_fit_attempts >= n_fit_attempts && (fit_status != 4 && best_chi_square < min_chi_per_DoF) ){
      Info("BkgFitter()", "Found good fit after %i re-attempt(s).", number_of_fit_attempts); n_good_fits++;
    }
    if(number_of_fit_attempts >= n_fit_attempts && (fit_status == 4 || best_chi_square > min_chi_per_DoF) ){
      Info("BkgFitter()", "No good fits found after %i attempts.", number_of_fit_attempts);
      Info("BkgFitter()", "Best chi square per NDF found = %.2f.", best_chi_square);
    }

    previous_function_param.clear();
    //Save function parameters
    for(int itr = 0; itr < (int) fit_function->GetNpar(); itr++){
      previous_function_param.push_back(fit_function->GetParameter(itr));
    }
    
    if (s%2) { 
      //Signal region has ODD no. of bins
      //Estimation is done for 1 bin
      est_mjj_low  = h_bkg->GetXaxis()->GetBinLowEdge(est_bin);
      est_mjj_high = h_bkg->GetXaxis()->GetBinLowEdge(est_bin+1);
    }
    else {    
      est_mjj_low  = h_bkg->GetXaxis()->GetBinLowEdge(est_bin);
      est_mjj_middle = h_bkg->GetXaxis()->GetBinLowEdge(est_bin+1);
      est_mjj_high = h_bkg->GetXaxis()->GetBinLowEdge(est_bin+2); 
    } 
    
    Info("BkgFitter()", "Estimation region #%i: [%.2f, %.2f] GeV, for %i bin(s)", n_fits, est_mjj_low, est_mjj_high, (s+1)%2 + 1); 
    
    reject = kFALSE;

    //Retrieve fit function
    TF1 * est_function = new TF1("est_function", Function, start, end, fit_function->GetNpar());
    est_function->SetParameters(fit_function->GetParameters());
    est_function->SetParErrors(fit_function->GetParErrors());
    Double_t est_bin_pval;
    Double_t est_bin_pval_chi2;

    if(s%2){ //Single bin estimation 
      h_bkg->SetBinContent(est_bin, est_function->Integral(est_mjj_low, est_mjj_high)/histo->GetBinWidth(est_bin));
      h_bkg->SetBinError(est_bin, est_function->IntegralError(est_mjj_low,est_mjj_high, fit_result->GetParams(), fit_result->GetCovarianceMatrix().GetMatrixArray())/histo->GetBinWidth(est_bin));
      Info("BkgFitter()", "In bin %i, data has %.2f events, estimation has %.3f ± %.3f events, fit status is %d with a chi squared per DoF = %.2f ", est_bin, histo->GetBinContent(est_bin), est_function->Integral(est_mjj_low,est_mjj_high)/histo->GetBinWidth(est_bin), h_bkg->GetBinError(est_bin) , fit_status, fit_function->GetChisquare()/NDF); 
 
      est_bin_pval = PoissonPval(histo->GetBinContent(est_bin),h_bkg->GetBinContent(est_bin));//check bin p value (BumpHunter method) 
      est_bin_pval_chi2 = fit_function->GetProb();
      Info("BkgFitter()", "bin p value %f ",est_bin_pval);
      if (est_bin_pval <= min_pval){ 
        min_pval = est_bin_pval;
        min_pval_bin = est_bin;
        min_pval_fit = n_fits;
        saved_function_param.clear();
        for(int itr = 0; itr < (int) fit_function->GetNpar(); itr++){
          saved_function_param.push_back(previous_function_param[itr]);//Save the fit parameters of the lowest pval (will be used for re-fitting)
        }
      }
    }
    else { //Two bin estimation
      //Setting 1st bin
      if(h_bkg->GetBinContent(est_bin) > 0.){
	Double_t new_estimation = (previous_est + (est_function->Integral(est_mjj_low, est_mjj_middle)/histo->GetBinWidth(est_bin)) )/2.;
        Double_t new_bin_error = (previous_bin_error + (est_function->IntegralError(est_mjj_low,est_mjj_middle,fit_result->GetParams(),fit_result->GetCovarianceMatrix().GetMatrixArray())/histo->GetBinWidth(est_bin)))/2.;
	h_bkg->SetBinContent(est_bin, new_estimation);
        h_bkg->SetBinError(est_bin, new_bin_error);
	Info("BkgFitter()", "In bin %i, data has %.2f events, new estimation has %.3f ± %.3f events, fit status is %d with a chi squared per DoF = %.2f ", est_bin, histo->GetBinContent(est_bin), new_estimation, new_bin_error, fit_status, fit_function->GetChisquare()/NDF); 
        est_bin_pval = PoissonPval(histo->GetBinContent(est_bin),h_bkg->GetBinContent(est_bin));//check bin p value (BumoHunter method)
	est_bin_pval_chi2 = fit_function->GetProb();
        Info("BkgFitter()", "bin p value %f ", est_bin_pval);
        if (est_bin_pval <= min_pval){
          min_pval = est_bin_pval;
          min_pval_bin = est_bin;
          min_pval_fit = n_fits;
          saved_function_param.clear();
          for(int itr = 0; itr < (int) fit_function->GetNpar(); itr++){
            saved_function_param.push_back(previous_function_param[itr]);//Save the fit parameters of the lowest pval (will be used for re-fitting)
          }
        }
      } 
      if(h_bkg->GetBinContent(est_bin) <= 0.){
	h_bkg->SetBinContent(est_bin, est_function->Integral(est_mjj_low, est_mjj_middle)/histo->GetBinWidth(est_bin) );
        h_bkg->SetBinError(est_bin, est_function->IntegralError(est_mjj_low, est_mjj_middle,fit_result->GetParams(),fit_result->GetCovarianceMatrix().GetMatrixArray())/histo->GetBinWidth(est_bin));
	Info("BkgFitter()", "In bin %i, data has %.2f events, estimation has %.2f ± %.2f events, fit status is %d with a chi squared per DoF = %.2f", est_bin, histo->GetBinContent(est_bin), est_function->Integral(est_mjj_low, est_mjj_middle)/histo->GetBinWidth(est_bin),h_bkg->GetBinError(est_bin), fit_status, fit_function->GetChisquare()/NDF); 
      }
      //Setting 2nd bin (necessarily empty)
      h_bkg->SetBinContent(est_bin+1, est_function->Integral(est_mjj_middle, est_mjj_high)/histo->GetBinWidth(est_bin+1) );
      h_bkg->SetBinError(est_bin+1, est_function->IntegralError(est_mjj_middle, est_mjj_high,fit_result->GetParams(),fit_result->GetCovarianceMatrix().GetMatrixArray())/histo->GetBinWidth(est_bin+1)); 
      Info("BkgFitter()", "In bin %i, data has %.2f events,     estimation has %.2f ± %.2f, fit status is %d with a chi squared per DoF = %.2f", est_bin+1, histo->GetBinContent(est_bin+1), est_function->Integral(est_mjj_middle, est_mjj_high)/histo->GetBinWidth(est_bin+1),h_bkg->GetBinError(est_bin+1), fit_status, fit_function->GetChisquare()/NDF);

      previous_est = est_function->Integral(est_mjj_middle, est_mjj_high)/histo->GetBinWidth(est_bin+1);
      previous_bin_error = est_function->IntegralError(est_mjj_middle,est_mjj_high,fit_result->GetParams(),fit_result->GetCovarianceMatrix().GetMatrixArray())/histo->GetBinWidth(est_bin+1);
    }


    chi_values.push_back(fit_function->GetProb());
    mjj_values.push_back(est_mjj_low + h_bkg->GetBinWidth(est_bin)/2);
   
    //plotting fits:
    TLegend * legend = new TLegend(0.6,0.6,0.9,0.8);
    legend->SetFillColor(0);
    float chi2 = fit_function->GetChisquare()/NDF;
    string text1 = "#chi^{2}/NDF = ";
    text1 = text1+Form("%.2f", chi2 );
    string text2 = "Fit status : ";
    text2 = text2+Form("%i", int(fit_result));
    //string text2 = "Fit Status : "+Form("%i", fit_status).c_str();
    legend->AddEntry((TObject*) 0, text1.c_str(), "");
    legend->AddEntry((TObject*) 0, text2.c_str(), "");
    
    canvas_iter_fit->cd();
    
    //    gStyle->SetOptTitle(0);
    TH1D * histo_aux = (TH1D*) histo->Clone("histo_aux");

    //histo_aux->SetTitle("");
    int min_bin_for_plot = (int)TMath::Max(start_bin,est_bin - s - b1 - 1);
    int max_bin_for_plot = (int)TMath::Min(end_bin, est_bin + s + b2 + 1);
    histo_aux->GetXaxis()->SetRange( min_bin_for_plot,max_bin_for_plot);
    histo_aux->GetXaxis()->SetLabelOffset(histo->GetXaxis()->GetLabelOffset()/2.);
    histo_aux->GetXaxis()->SetTitle("mjj [GeV]");
    histo_aux->GetXaxis()->SetLabelSize(0.04);
    histo_aux->Draw();

    TF1 *fleft   = new TF1("fleft", fit_function, start_b1, end_b1, n_param_func);
    fleft->SetParameters(fit_function->GetParameters());
    TF1 *fmiddle = new TF1("fmiddle", fit_function, end_b1, start_b2, n_param_func);
    fmiddle->SetParameters(fit_function->GetParameters());
    TF1 *fright  = new TF1("fright",fit_function, start_b2, end_b2, n_param_func);
    fright->SetParameters(fit_function->GetParameters());
    
    fleft->SetLineColor(2);
    fright->SetLineColor(2);
    fmiddle->SetLineColor(3);

    fleft->Draw("SAME");
    fright->Draw("SAME");
    fmiddle->Draw("SAME");
    
    legend->Draw("SAME");

    TString n_str;
    n_str.Form("%d",n_fits);
    canvas_iter_fit->Write("fit_"+n_str+"_range"+Form("%.0f:%.0f",est_mjj_low,est_mjj_high));
    iter_fit->Write();

    delete histo_aux;
    canvas_iter_fit->Clear();

    //To define fit region:
   
    start_b1 = start_b1 + h_bkg->GetBinWidth(h_bkg->FindBin(start_b1));
    end_b1   = end_b1 + h_bkg->GetBinWidth(h_bkg->FindBin(end_b1));
    start_b2 = start_b2 + h_bkg->GetBinWidth(h_bkg->FindBin(start_b2));
    end_b2   = end_b2 + h_bkg->GetBinWidth(h_bkg->FindBin(end_b2));
     
    
    

  }// End of fit loop

 
  Info("BkgFitter()", "================================> End of fits <======================================="); 
  Info("BkgFitter()", "%i/%i fits succeeded", n_good_fits, n_fits);
  Info("BkgFitter()", "lowest p value %6.f at bin #%d (fit #%d) ", min_pval, min_pval_bin, min_pval_fit);
  canvas_iter_fit->Close();
  iter_fit->Close();
  
  //Create & plot chi2 over mjj graph (before re-fits)
  string chi_plot_filename = output_dir+"/chi_plot.root";
  TString chi_plot_file_name = chi_plot_filename;
  TFile * chi_plot = new TFile (chi_plot_file_name, "RECREATE");
  TCanvas * canvas_chi_plot = new TCanvas("chi_plot","chi_plot",800,600);

  canvas_chi_plot->cd();
  TGraph *g = new TGraph(n_fits);
  g->SetTitle("#chi^{2}  p value plot;m(jj) [GeV];#chi^{2} p-value;");
  //  g->GetYaxis()->SetTitle("#chi^{2} p-value");
  g->GetYaxis()->SetLabelSize(0.04);
  g->GetXaxis()->SetTitle("mjj [GeV]");
  g->GetXaxis()->SetLabelSize(0.025);
  g->SetMarkerColor(2);
  g->SetMarkerStyle(22);
  g->SetMarkerSize(1.2);
  
  for (int i = 1; i < n_fits+1 ;i++){
    g->SetPoint(i,mjj_values.at(i-1),chi_values.at(i-1));
  }
  g->Draw("AP");
  
  canvas_chi_plot->Update();
  canvas_chi_plot->Write("chi2_plot");
  canvas_chi_plot->Clear();
  
  chi_plot->Write();
  chi_plot->Close(); 

}

void SmoothBkgEstimation(TH1D * histo, TH1D * h_bkg, TF1 * fit_function, TF1 * global_fit_function, double start, double end, int b1, int s, int b2, string output_dir){

  ////////////////////////////////////////
  //Smoothing using second derivative
  ////////////////////////////////////////

  vector<double> der1; 
  vector<double> der2;
  vector<int>    bins_to_smooth;
  vector<int>    pos_der1;
  vector<double> previous_function_param; 
  
  int refits_loop = 0;//Counter for the # times the refitting loop is performed (until all 2nd derivatives are positive)
  int n_refits = 0;
  int n_good_refits = 0;   
  int first_bin_with_data = h_bkg->FindFirstBinAbove(0,1);
  int last_bin_with_data  = h_bkg->FindLastBinAbove(0,1);

  Info("BkgFitter()", "Bkg estimated between bins %i  -  %i  ", first_bin_with_data, last_bin_with_data);
    
  do { 
    refits_loop++;
    bins_to_smooth.clear();
    pos_der1.clear();
    der1.clear();
    der2.clear();

    Info("BkgFitter()","------------------------------------ REFITS ROUND #%i ---------------------------------------------------", refits_loop);
    
    for (int itr = first_bin_with_data; itr < last_bin_with_data ; itr+= smooth_step){
      if ( h_bkg->GetBinContent(itr) <= 0 ){ 
	Info("BkgFitter()", "Stopping smoothing since bin %i is empty", itr);
	break;
      }
      if (h_bkg->GetBinContent(itr + smooth_step) <= 0){
	Info("BkgFitter()", "Stopping smoothing since bin %i is empty", itr + smooth_step);
	break;
      }
      double bins_diff = (h_bkg->GetBinContent(itr + smooth_step) - h_bkg->GetBinContent(itr))/fabs(h_bkg->GetBinCenter(itr) - h_bkg->GetBinCenter(itr + smooth_step)); 
      der1.push_back(bins_diff);
      if (bins_diff > 0){ //Not used
	//bins_to_smooth.push_back(itr+smooth_step);
	pos_der1.push_back(itr+smooth_step);
	Info("BkgFitter()", "Positive 1st derivative:      At bin %i 1st derivative is %3.f", itr, bins_diff);
      }
    }
    //for (int itr = 1; itr < der1.size() ; itr++){
    for (int itr_der1 = 0; itr_der1 < ((int) der1.size())-1 ; itr_der1++){ 
      double der_diff = ( der1.at(itr_der1 + 1) - der1.at(itr_der1) ) / fabs(h_bkg->GetBinCenter(first_bin_with_data + itr_der1) - h_bkg->GetBinCenter(first_bin_with_data + itr_der1 + smooth_step)); //Second derivative
      der2.push_back(der_diff);
      //h_bkg->GetBinCenter(first_bin_with_data + itr_der1 + 1);
      double global_fit_second_der = global_fit_function->Derivative2(h_bkg->GetBinCenter(first_bin_with_data + itr_der1 + 1)); //
      //std::cout << "========================================================================================================>>>>>>>>>>>>>>>> " << std::endl;
      //std::cout << "Function 2nd derivative   = " << global_fit_second_der << " at x = " << h_bkg->GetBinCenter(first_bin_with_data + itr_der1 + 1) <<  std::endl;
      //std::cout << "Estimation 2nd derivative = " << der_diff <<  std::endl;
      if ( der_diff > global_fit_second_der*second_der_accuracy || der_diff < global_fit_second_der*(2 - second_der_accuracy) ){ //Bins that fail this condition must be refitted with background estimation:  
      //if( der_diff < 0 ){  
	bins_to_smooth.push_back(first_bin_with_data + itr_der1 + 1);
	Info("BkgFitter()", "Negative 2nd derivative:      At bin %i 2nd derivative is %f, whereas the global fit 2nd derivative is %f ", first_bin_with_data + itr_der1 + 1, der_diff, global_fit_second_der);
      }
    } 
    sort(bins_to_smooth.begin(),bins_to_smooth.end());
    for (int ibin = 0; ibin < bins_to_smooth.size() ; ibin++){
      n_refits++;
      int est_bin = bins_to_smooth.at(ibin);

      //Adjust b1,s,b2 regions
      start_b1 = h_bkg->GetBinLowEdge(est_bin - int((s-1)/2) - b1);
      end_b1 = h_bkg->GetBinLowEdge(est_bin - int((s-1)/2));
      start_b2 = h_bkg->GetBinLowEdge(est_bin + int(s/2));
      end_b2 = h_bkg->GetBinLowEdge(est_bin + int(s/2) + b2);
      
      int check_b1 = b1;//In case h_bkg at start_b1 (or end_b2) is underflow (or overflow) - adjust b1 & b2 while keeping same NDF
      int check_b2 = b2;
      while (h_bkg->GetBinContent(est_bin - int((s-1)/2) - check_b1) == 0){
	check_b1--;
	check_b2++;
	start_b1 = h_bkg->GetBinLowEdge(est_bin - int((s-1)/2) - check_b1);
	end_b2 = h_bkg->GetBinLowEdge(est_bin + int(s/2) + check_b2);
      }
      check_b1 = b1;
      check_b2 = b2;
      while (h_bkg->GetBinContent(est_bin + int(s/2) + check_b2) == 0){
	check_b2--;
	check_b1++;
	end_b2 = h_bkg->GetBinLowEdge(est_bin + int(s/2) + check_b2);
	start_b1 = h_bkg->GetBinLowEdge(est_bin - int((s-1)/2) - check_b1);
      }
      
      
      Info("BkgFitter()", "========================================= Re fit #%i ===================================", ibin + 1); 
      Info("BkgFitter()", "Start of B1 at %.1f GeV", start_b1);
      Info("BkgFitter()", "  End of B1 at %.1f GeV", end_b1);
      Info("BkgFitter()", "Start of B2 at %.1f GeV", start_b2);
      Info("BkgFitter()", "  End of B2 at %.1f GeV", end_b2);
    
      if (ibin == 0) { InitializeFunctionParameters(fit_function); } 
      else { InitializeFunctionParameters(fit_function, previous_function_param);}

      reject = kTRUE;
    
      TFitResultPtr fit_result = h_bkg->Fit(fit_function,fit_type.c_str());
      int fit_status = fit_result->Status();
      while(fit_status > 5){
	fit_status = fit_status/10.;
      }
      
      int number_of_fit_attempts = 0;
      float best_chi_square = 1e10;
      while((fit_status == 4 && number_of_fit_attempts < n_fit_attempts) ||
	    (fit_status != 4 && number_of_fit_attempts < n_fit_attempts*10 && fit_function->GetChisquare()/NDF > min_chi_per_DoF)){
	if(number_of_fit_attempts%10 == 0 && ibin == 0){
	  InitializeFunctionParameters(fit_function);
	} 
	else if(number_of_fit_attempts%10 ==0 && ibin != 0){
	  InitializeFunctionParameters(fit_function, previous_function_param);
	}

	bool found_nan = false;
	for(int itr = 0; itr < (int) fit_function->GetNpar(); itr++){
	  if(fit_function->GetParameter(itr) != fit_function->GetParameter(itr)){
	    Info("BkgFitter()", "Parameter %i is NaN", itr);
	    found_nan = true;
	  }
	  TRandom3 * Random_Gauss_refit = new TRandom3();
	  Random_Gauss_refit->SetSeed((2*(n_refits+number_of_fit_attempts+itr))+1);
	  double random_variation = Random_Gauss_refit->Gaus(0.,1.);
	  fit_function->SetParameter(itr, fit_function->GetParameter(itr)*(1.0 + variability_factor*random_variation));
	  delete Random_Gauss_refit;
	}
	if(found_nan){break;}
	fit_result = h_bkg->Fit(fit_function, fit_type.c_str());
	fit_status = fit_result->Status();
	while (fit_status > 5){
	  fit_status = fit_status/10.;
	}
	number_of_fit_attempts++;
	if (best_chi_square > fit_function->GetChisquare()/NDF){ best_chi_square = fit_function->GetChisquare()/NDF; }
      }  
      if(number_of_fit_attempts == 0){
	Info("BkgFitter()", "Found good fit on first try."); n_good_refits++;
      }
      if(number_of_fit_attempts < n_fit_attempts && number_of_fit_attempts > 0){
	Info("BkgFitter()", "Found good fit after %i re-attempt(s).", number_of_fit_attempts); n_good_refits++;
      }
      if(number_of_fit_attempts >= n_fit_attempts && fit_status != 4){
	Info("BkgFitter()", "Found good fit after %i re-attempt(s).", number_of_fit_attempts); n_good_refits++;
      }
      if(number_of_fit_attempts >= n_fit_attempts && fit_status == 4){
	Info("BkgFitter()", "No good fits found after %i attempts.", number_of_fit_attempts);
	Info("BkgFitter()", "Best chi square per NDF found = %.2f.", best_chi_square);
      }

      previous_function_param.clear();
      //Save function parameters
      for(int i = 0; i < (int) fit_function->GetNpar(); i++){
	previous_function_param.push_back(fit_function->GetParameter(i));
      }
      if (s%2) { 
	//Signal region has ODD no. of bins
	//Estimation is done for 1 bin
	est_mjj_low  = h_bkg->GetXaxis()->GetBinLowEdge(est_bin);
	est_mjj_high = h_bkg->GetXaxis()->GetBinLowEdge(est_bin+1);
      }
      else {    
	//Signal region has EVEN no. of bins
	//Estimation is done for 2 bins before averaging over 2 fit results
	est_mjj_low    = h_bkg->GetXaxis()->GetBinLowEdge(est_bin); 
	est_mjj_middle = h_bkg->GetXaxis()->GetBinLowEdge(est_bin+1); 
	est_mjj_high   = h_bkg->GetXaxis()->GetBinLowEdge(est_bin+2); 
      } 
      Info("BkgFitter()", " Estimation region: [%.2f, %.2f] GeV, for %i bin(s)",est_mjj_low, est_mjj_high, (s+1)%2 + 1);

      reject = kFALSE;

      //retrieve fit_function
      TF1 * est_function = new TF1("est_function", Function, start, end, fit_function->GetNpar());
      est_function->SetParameters(fit_function->GetParameters());
      est_function->SetParErrors(fit_function->GetParErrors());
    
      if(s%2){ //Single bin estimation 
	h_bkg->SetBinContent(est_bin, est_function->Integral(est_mjj_low,est_mjj_high)/histo->GetBinWidth(est_bin));
	h_bkg->SetBinError(est_bin, est_function->IntegralError(est_mjj_low,est_mjj_high, fit_result->GetParams(), fit_result->GetCovarianceMatrix().GetMatrixArray())/histo->GetBinWidth(est_bin));
	Info("BkgFitter()", "Re-fits: In bin %i, data has %.2f events, estimation has %.3f ± %.3f events, fit status is %d with a chi squared per DoF = %.2f ", est_bin, histo->GetBinContent(est_bin), est_function->Integral(est_mjj_low,est_mjj_high)/histo->GetBinWidth(est_bin), h_bkg->GetBinError(est_bin) , fit_status, fit_function->GetChisquare()/NDF); 
       
      }
      else { //Two bin estimation
	//Setting 1st bin
	if(h_bkg->GetBinContent(est_bin) > 0.){
	  Double_t new_estimation = (previous_est + (est_function->Integral(est_mjj_low, est_mjj_middle)/histo->GetBinWidth(est_bin)) )/2.;
	  Double_t new_bin_error = (previous_bin_error + (est_function->IntegralError(est_mjj_low,est_mjj_middle,fit_result->GetParams(),fit_result->GetCovarianceMatrix().GetMatrixArray())/histo->GetBinWidth(est_bin)))/2.;
	  h_bkg->SetBinContent(est_bin, new_estimation);
	  h_bkg->SetBinError(est_bin, new_bin_error);
	  Info("BkgFitter()", "Re-fits: In bin %i, data has %.2f events, new estimation has %.3f ± %.3f events, fit status is %d with a chi squared per DoF = %.2f ", est_bin, histo->GetBinContent(est_bin), new_estimation, new_bin_error, fit_status, fit_function->GetChisquare()/NDF); 
	}
	if(h_bkg->GetBinContent(est_bin) <= 0.){
	  h_bkg->SetBinContent(est_bin, est_function->Integral(est_mjj_low, est_mjj_middle)/histo->GetBinWidth(est_bin) );
	  h_bkg->SetBinError(est_bin, est_function->IntegralError(est_mjj_low, est_mjj_middle,fit_result->GetParams(),fit_result->GetCovarianceMatrix().GetMatrixArray())/histo->GetBinWidth(est_bin));
	  Info("BkgFitter()", "Re-fits: In bin %i, data has %.2f events, estimation has %.2f ± %.2f events, fit status is %d with a chi squared per DoF = %.2f", est_bin, histo->GetBinContent(est_bin), est_function->Integral(est_mjj_low, est_mjj_middle)/histo->GetBinWidth(est_bin),h_bkg->GetBinError(est_bin), fit_status, fit_function->GetChisquare()/NDF); 
	}
	//Setting 2nd bin (necessarily empty)
	h_bkg->SetBinContent(est_bin+1, est_function->Integral(est_mjj_middle, est_mjj_high)/histo->GetBinWidth(est_bin+1) );
	h_bkg->SetBinError(est_bin+1, est_function->IntegralError(est_mjj_middle, est_mjj_high,fit_result->GetParams(),fit_result->GetCovarianceMatrix().GetMatrixArray())/histo->GetBinWidth(est_bin+1)); 
	Info("BkgFitter()", "Re-fits: In bin %i, data has %.2f events,     estimation has %.2f ± %.2f, fit status is %d with a chi squared per DoF = %.2f", est_bin+1, histo->GetBinContent(est_bin+1), est_function->Integral(est_mjj_middle, est_mjj_high)/histo->GetBinWidth(est_bin+1),h_bkg->GetBinError(est_bin+1), fit_status, fit_function->GetChisquare()/NDF);
        
	previous_est = est_function->Integral(est_mjj_middle, est_mjj_high)/histo->GetBinWidth(est_bin+1);
	previous_bin_error = est_function->IntegralError(est_mjj_middle,est_mjj_high,fit_result->GetParams(),fit_result->GetCovarianceMatrix().GetMatrixArray())/histo->GetBinWidth(est_bin+1);
      }
      
      //chi_values.at(est_bin - start_bin - b1 - int((s-1)/2)) = fit_function->GetChisquare()/NDF;//conversion between vector's index to bin number
      
    }  
    //end of re-fits loop 
    Info("BkgFitter()", "================================> End of re-fits <======================================="); 
    Info("BkgFitter()", "%i/%i fits succeeded", n_good_refits, n_refits);
  }
  //  while (bins_to_smooth.size() > 2  || pos_der1.size() > 0);
  while (bins_to_smooth.size() > 2 && n_refits <= do_n_refits); // end of 'do'
  
} 

  
int main(int argc, char **argv){

  if(argc < 3){
    std::cout << "Need 2 arguments: " << std::endl;
    std::cout << "BkgFitter input_file histo_name " << std::endl;
    return 0;
  }

  SetAtlasStyle();

  gInterpreter->GenerateDictionary("vector<float>","vector");

  TString configFile = gSystem->ExpandPathName ("$ROOTCOREBIN/data/BkgSideBandFit/settings_fit.config");
  std::cout << "configFile " << configFile << std::endl;
  //  configFile = configFile+"setting.
  TEnv * settings = new TEnv();
  Info("BkgFitter()", "Config file = %s", configFile.Data());
  //  printf("Reading settings from: %s\n",configFile.Data());                                                         
  int status = settings->ReadFile(configFile.Data(),EEnvLevel(0));
  if (status != 0 ){
    Info("BkgFitter()", "cannot read config file %s", configFile.Data());
    return 0;
  }
  
  string inputfile = argv[1];
  TString input_file = inputfile; //full path to input file
  while((int) inputfile.find("/") > 0 ){
    std::size_t position = inputfile.find("/");
    inputfile.erase(0, position+1); 
  }
  TString histo_name = argv[2];
  string histoname = argv[2];
  TString scaled_histo_name;
  std::size_t position = histoname.find("DataLike");
  if ((int) position > 0){
    histoname.replace(position,8,"Scaled");
  }
  std::size_t isScaledHist = histoname.find("WithQStar");
  if ((int) isScaledHist > 0){
    histoname.erase(isScaledHist,13);
  }
  scaled_histo_name = histoname;
  
  
  double mjj_start_fit          = settings->GetValue("mJJStartPointGeV", 0.0);
  double mjj_end_fit            = settings->GetValue("mJJEndPointGeV", 0.0);
  Info("BkgFitter()", "Global fit range : [%.2f, %.2f]", mjj_start_fit, mjj_end_fit);
  left_side_nbins        = settings->GetValue("leftSideBandNBins", 3);
  right_side_nbins       = settings->GetValue("rightSideBandNBins", 3);
  middle_nbins           = settings->GetValue("middleNBins", 4);
  n_param_func                  = settings->GetValue("nParamFunction", 4);
  sqrt_s                        = settings->GetValue("SqrtS_inGeV", 13.0);
  fit_type                      = settings->GetValue("fitType", "LQ0S");
  n_fit_attempts                = settings->GetValue("nFitAttempts", 100);
  min_chi_per_DoF               = settings->GetValue("minChiSquare/NDF", 10000.0);
  variability_factor            = settings->GetValue("variabilityFactor", 0.1);
  smooth_step                   = settings->GetValue("smooth_step", 1);  
  do_global_fit_quality         = settings->GetValue("estGlobalFitQuality", false);
  n_pseudo_exp                  = settings->GetValue("nPseudoExperiments", 1000);
  do_smoothing                  = settings->GetValue("doSmoothing", false);
  second_der_accuracy           = settings->GetValue("secDerivativeAccurary", 1.5);
  do_n_refits                   = settings->GetValue("doNRefits", 10000);
  do_scaled_hist_plot           = settings->GetValue("doScaledHistPlot", false);
  do_window_pvalue              = settings->GetValue("doWindowPval",false);

  if (!smooth_step%2){//round smooth_step to 
    smooth_step-=1;
    Info("BkgFitter()","Input step in smoothing scheme must be a positive odd integer. using step = %i ", smooth_step);
  }
  if (smooth_step <= 0){
    smooth_step = 1;
    Info("BkgFitter()","Input step in smoothing scheme must be a positive odd integer. using step = 1 ");
  }
  
  //Prepare output directory
  string command = "mkdir BkgSideBandFit/results/"+inputfile;
  system(command.c_str()); 
  command = command+"/"+histo_name;
  system(command.c_str()); 
  string command_smooth = command+"/smoothing";
  system(command_smooth.c_str()); 
  string command_global_fit = command+"/global_fit";
  system(command_global_fit.c_str()); 
  string output_dir_path = command.erase(0,6);
  string output_dir_path_smooth = command_smooth.erase(0,6);
  string output_dir_path_global = command_global_fit.erase(0,6);
    
  //Open input file
  TFile * f = TFile::Open(input_file);
  if (f == NULL){
    Error("Cannot open "+input_file);
    return 0;
  }
  //Retrieve histogram 
  TDirectoryFile *d = (TDirectoryFile*) f->Get("Nominal");
  TH1D * histo_main = (TH1D*) d->Get(histo_name);

  if (histo_main == NULL){ 
    Error("Cannot access histogram "+histo_name+" in "+input_file);
    return 0;
  }
 
  //Open file which has no injected signal and retrieve from it the scaled histogram 
  string inputfile_noQStar = argv[1];
  std::size_t isSignalFile = inputfile_noQStar.find("WithQStar");
  if ((int) isSignalFile > 0){
    inputfile_noQStar.erase(isSignalFile,13);
  }
  TString input_file_noQStar = inputfile_noQStar;
  
  TFile * f_noQStar = TFile::Open(input_file_noQStar);
  TDirectoryFile *d_noQStar = (TDirectoryFile*) f_noQStar->Get("Nominal");
  TH1D * scaled_histo = (TH1D*) d_noQStar->Get(scaled_histo_name);
  if (scaled_histo == NULL){ 
    Error("Cannot access histogram "+scaled_histo_name+" in "+input_file);
    return 0;
  }    
  
  //Print out some info about histogram:
  Info("BkgFitter()", "Input histogram, general info: " );
  Info("BkgFitter()", "Histogram has %i bins.", histo_main->GetNbinsX());//Has underflow and overflow bin
  Info("BkgFitter()", "Histogram has %f events", histo_main->Integral());
  
  InitializeGlobalVariables(histo_main, mjj_start_fit, left_side_nbins, middle_nbins, right_side_nbins);
  
  //Clone histogram for bgk estimation
  TH1D * h_estimation        = (TH1D*) histo_main->Clone("h_estimation"); 
  h_estimation->Reset();
  h_estimation->GetSumw2()->Set(0);

  Info("BkgFitter()", "Cloned histogram has %f events", h_estimation->Integral());
  
  //Prepare output directory
  string    filename_pvalue   = output_dir_path+"/pvalues.root";
  TString   file_name_pvalue  = filename_pvalue; 
  string    filename   = output_dir_path+"/fit.root";
  TString   file_name  = filename; 
  TFile   * Pvalue_output = new TFile(file_name_pvalue, "RECREATE");
  TCanvas * canvas_pvalue = new TCanvas("canvas_pvalue", "p-values", 800,600);
  TFile   * Fit_output = new TFile(file_name, "RECREATE");
  TCanvas * canvas_fit = new TCanvas("canvas_fit", "fit", 800,600);

  

  //Define function with either 3, 4, or 5 parameters (see 'Function')
  TF1 * fit_function        = new TF1("fit_function",        Function,        mjj_start_fit, mjj_end_fit, n_param_func); 
  TF1 * global_fit_function = new TF1("global_fit_function", Global_Function, mjj_start_fit, mjj_end_fit, n_param_func); 
  
  //Do 'global' fit 
  TF1 * fit_global = ComputeGlobalFit(histo_main, global_fit_function, mjj_start_fit, mjj_end_fit);
  
  //Do iterative fits 
  Info("BkgFitter()", "Performing fit with %i parameter function", n_param_func);
  ComputeBkgEstimation(histo_main, h_estimation, fit_function, mjj_start_fit, mjj_end_fit, left_side_nbins,  middle_nbins, right_side_nbins, output_dir_path);
  Info("BkgFitter()", "Bkg histogram has %i bins.", h_estimation->GetNbinsX());
  Info("BkgFitter()", "Bkg histogram has %.2f events", h_estimation->Integral(mjj_start_fit, mjj_end_fit));
  Info("BkgFitter()", "Data histogram has %.2f events", histo_main->Integral(mjj_start_fit, mjj_end_fit));
  
  //compute global p value
  if(do_global_fit_quality){
    double p_value = ComputeGlobalPvalue(histo_main, h_estimation, n_pseudo_exp, output_dir_path);
    //double p_value = ComputeGlobalPvalue(scaled_histo, h_estimation, n_pseudo_exp, output_dir_path);
    Info("BkgFitter()", "Non-smoothed Global p-value is %f ", p_value);
  }
  
  //clone histogram to create smooth and global bkg estimations
  TH1D * h_estimation_smooth     = (TH1D*) h_estimation->Clone("h_estimation_smooth"); 
  TH1D * h_estimation_global_fit = (TH1D*) h_estimation->Clone("h_estimation_global_fit"); 
  h_estimation_global_fit->Reset();
  h_estimation_global_fit->GetSumw2()->Set(0);
  CreateHistogramGlobalFit(h_estimation, h_estimation_global_fit, fit_global);

  //compute global fit's global p-value
  double p_value_global_fit = ComputeGlobalPvalue(histo_main, h_estimation_global_fit, n_pseudo_exp, output_dir_path_global);
  //double p_value_global_fit = ComputeGlobalPvalue(scaled_histo, h_estimation_global_fit, n_pseudo_exp, output_dir_path_global);
  Info("BkgFitter()", "Global fit Global p-value is   %f", p_value_global_fit);
  //if p value < 0.1 insert here bump exclusion + re-fit
  
  if(do_smoothing){ 
    //For smoothing histogram:
    SmoothBkgEstimation(histo_main, h_estimation_smooth, fit_function, fit_global, mjj_start_fit, mjj_end_fit, left_side_nbins,  middle_nbins, right_side_nbins, output_dir_path);
    double p_value = ComputeGlobalPvalue(histo_main, h_estimation_smooth, n_pseudo_exp, output_dir_path_smooth);
    //double p_value = ComputeGlobalPvalue(scaled_histo, h_estimation_smooth, n_pseudo_exp, output_dir_path_smooth);
    Info("BkgFitter()", "Smoothed Global p-value is %f, computed with a smoothing step %i", p_value, smooth_step);
  }

  
  //computing the p-value for each specific signal window
  Info("BkgFitter()","==============================================================================");
  Info("BkgFitter()","=================== Computing Signal Windows p-values ========================");
  Info("BkgFitter()","==============================================================================");
  
  int first_s_bin = h_estimation->FindFirstBinAbove(0);
  int last_s_bin  = h_estimation->FindLastBinAbove(0) - middle_nbins; //check this

  

  if (do_window_pvalue){
    //loop runs over the first signal bin of every signal window. 

    //For TGraphs:
    const int n = last_s_bin + 1 - first_s_bin; double mjj_global[n]; double pvalues_global[n]; double mjj_smooth[n]; double pvalues_smooth[n]; double mjj_stand[n]; double pvalues_stand[n];
  
    for (int itr_window_first_bin = first_s_bin; itr_window_first_bin < last_s_bin + 1; itr_window_first_bin++){
      Info("BkgFitter()", "----------------->");
      int itr = itr_window_first_bin - first_s_bin; //set back to 0 for TGraphs
      double mjj = (h_estimation->GetBinLowEdge(itr_window_first_bin) + h_estimation->GetBinLowEdge(itr_window_first_bin + middle_nbins) + h_estimation->GetBinWidth(itr_window_first_bin + middle_nbins))/2.0; //mjj value for TGraph
      mjj_global[itr] = mjj; mjj_smooth[itr] = mjj; mjj_stand[itr] = mjj;
    
      ///////////////////////////
      //Global Fit: 
      double window_p_value = ComputeWindowPvalue_NLL(histo_main, h_estimation_global_fit, itr_window_first_bin, itr_window_first_bin + middle_nbins, 1000);
      Info("BkgFitter()"," window: %2.f - %2.f GeV , global fit p-value:    %f ", h_estimation->GetBinLowEdge(itr_window_first_bin), h_estimation->GetBinLowEdge(itr_window_first_bin + middle_nbins) + h_estimation->GetBinWidth(itr_window_first_bin + middle_nbins), window_p_value);
      pvalues_global[itr] = window_p_value;

      ///////////////////////////
      //Smoothed background: 
      if(do_smoothing){
	window_p_value = ComputeWindowPvalue_NLL(histo_main, h_estimation_smooth, itr_window_first_bin, itr_window_first_bin + middle_nbins, 1000);
	Info("BkgFitter()"," window: %2.f - %2.f GeV , smoothed bkg p-value:  %f ", h_estimation->GetBinLowEdge(itr_window_first_bin), h_estimation->GetBinLowEdge(itr_window_first_bin + middle_nbins) + h_estimation->GetBinWidth(itr_window_first_bin + middle_nbins), window_p_value);
	pvalues_smooth[itr] = window_p_value;
      }

      ///////////////////////////
      //Standard background: 
      window_p_value = ComputeWindowPvalue_NLL(histo_main, h_estimation, itr_window_first_bin, itr_window_first_bin + middle_nbins, 1000);
      Info("BkgFitter()"," window: %2.f - %2.f GeV , standard bkg p-value:  %f ", h_estimation->GetBinLowEdge(itr_window_first_bin), h_estimation->GetBinLowEdge(itr_window_first_bin + middle_nbins) + h_estimation->GetBinWidth(itr_window_first_bin + middle_nbins), window_p_value);
      pvalues_stand[itr] = window_p_value;
  
    }

    TGraph * g_pvalues_global = new TGraph(n, mjj_global, pvalues_global);
    TGraph * g_pvalues_smooth = new TGraph(n, mjj_smooth, pvalues_smooth);
    TGraph * g_pvalues_stand  = new TGraph(n, mjj_stand,  pvalues_stand);
  
    /////////////////////
    //Plotting
    /////////////////////
    Pvalue_output->cd();
    canvas_pvalue->cd();
    g_pvalues_global->SetTitle("Window p-values; m(jj) [GeV]; p-value");
    g_pvalues_global->SetLineColor(3);
    g_pvalues_smooth->SetLineColor(4);
    g_pvalues_stand->SetLineColor(2);
    g_pvalues_global->SetMarkerColor(3);
    g_pvalues_smooth->SetMarkerColor(4);
    g_pvalues_stand->SetMarkerColor(2);
    g_pvalues_global->SetMarkerSize(0.1);
    g_pvalues_smooth->SetMarkerSize(0.1);
    g_pvalues_stand->SetMarkerSize(0.1);
    g_pvalues_global->SetLineWidth(3);
    g_pvalues_smooth->SetLineWidth(3);
    g_pvalues_stand->SetLineWidth(3);
    g_pvalues_global->Draw();
    g_pvalues_smooth->Draw("SAME");
    g_pvalues_stand->Draw("SAME");

    TLegend * legend_pvalue = new TLegend(0.8,0.8,0.95,0.95);
    legend_pvalue->SetFillColor(0);
    legend_pvalue->AddEntry(g_pvalues_stand,"SideBands Fit", "l");
    legend_pvalue->AddEntry(g_pvalues_smooth,"Smoothed Fit", "l");  
    legend_pvalue->AddEntry(g_pvalues_global,"Global Fit", "l");

    legend_pvalue->Draw("SAME");

    canvas_pvalue->Write("p-values");
    Pvalue_output->Write();
    Pvalue_output->Close();
  }

  ////////////////////
  //Fit outputs:
  Fit_output->cd();
  canvas_fit->cd();
  
  Float_t PadH_begin_y;
  Float_t PadR_begin_y;
  Float_t PadR_end_y;

  PadH_begin_y = 0.12;
  PadR_begin_y = 0.0;
  PadR_end_y = 0.24;

  TPad *PadH = new TPad("PadH", "PadH", 0., PadH_begin_y, 1., 1.);
  PadH->SetTitle("");
  PadH->SetLeftMargin(0.16);
  PadH->SetRightMargin(0.05);
  PadH->SetTopMargin(0.05);
  PadH->SetBottomMargin(0.16);
  PadH->Range(-493.7974,-0.1067686,8504.937,3.826558);
  TPad *PadR = new TPad("PadR", "PadR", 0., 0., 1., 0.2586445);
  PadR->SetTitle("");
  PadR->SetTopMargin(0);
  PadR->Range(-485.0325,-5.236441,8505.083,2.537343);
  PadR->SetLeftMargin(0.1591784);
  PadR->SetRightMargin(0.05006418);
  PadR->SetTopMargin(0.01069519);
  PadR->SetBottomMargin(0.3582888);
  PadH->Draw();
  PadR->Draw();

  //TPad * pad = new TPad("Pad", "Pad", 0., 0., 1., 1.);
  //pad->Divide(2,1);

  ////////////////////////////////////////////////////
  //Draw estimations, data, and global fit:
  ///////////////////////////////////////////////////

  PadH->cd(); 
  PadH->SetLogy();
  
  //Draw background estimation:
  TH1D * h_estimation_line = (TH1D*) h_estimation->Clone("h_estimation_line"); 
  h_estimation_line->GetSumw2()->Set(0);
  h_estimation_line->SetLineColor(2);
  h_estimation->SetMarkerStyle(0);
  h_estimation->SetFillColor(kRed);
  h_estimation->SetFillStyle(3001);
  gStyle->SetOptTitle(0);
  h_estimation->GetXaxis()->SetLabelOffset(h_estimation->GetXaxis()->GetLabelOffset()/2.);
  h_estimation->GetYaxis()->SetTitle("Events");
  h_estimation->GetYaxis()->SetTitleOffset(1.0);
    
  //For x-axis range  
  int first_bin = histo_main->FindFirstBinAbove(0);
  int last_bin  = histo_main->FindLastBinAbove(0);
  float y_max = histo_main->GetBinContent(first_bin);
  h_estimation->GetXaxis()->SetRange(first_bin, last_bin+2);
  TH1D *ratio = (TH1D*) h_estimation->Clone();
  TH1D *ratio_smooth = (TH1D*) h_estimation_smooth->Clone();
  TH1D *ratio_global_fit = (TH1D*) h_estimation_global_fit->Clone();
  h_estimation->SetMinimum(0.100001);
  h_estimation->SetMaximum(y_max*2);

  
  
  h_estimation->Draw("L E2");
  h_estimation_line->Draw("HIST SAME");

  //Draw smoothed background estimation:
  TH1D * h_estimation_smooth_line = (TH1D*) h_estimation_smooth->Clone("h_estimation_smooth_line"); 
  h_estimation_smooth_line->GetSumw2()->Set(0);
  h_estimation_smooth_line->SetLineColor(4);
  h_estimation_smooth_line->Draw("HIST SAME");
  
  //Draw global fit: 
  //fit_global->SetLineColor(3);
  //fit_global->Draw();
  global_fit_function->SetLineColor(3);
  global_fit_function->Draw("SAME");

  //Draw scaled histogram
  if (do_scaled_hist_plot){
    scaled_histo->SetMarkerColor(1);
    scaled_histo->SetLineColor(1);
    scaled_histo->Draw("SAME E");
  }
  else {
    //Draw data:
    histo_main->SetMarkerColor(1);
    histo_main->SetLineColor(1);
    histo_main->Draw("SAME");
  }
  
  TLegend * legend_fits = new TLegend(0.6,0.6,0.9,0.8);
  legend_fits->SetFillColor(0);
  legend_fits->AddEntry(histo_main, "Data", "lep");
  legend_fits->AddEntry(h_estimation_line, "Sideband", "lf");
  legend_fits->AddEntry(h_estimation_smooth_line, "Smoothed Sideband", "l");
  legend_fits->AddEntry(global_fit_function, "Global Fit", "l");
  if (do_scaled_hist_plot){
    legend_fits->AddEntry(scaled_histo, "Scaled histogram", "lep");
  }
  legend_fits->Draw("SAME");

  
  reject = kFALSE;
  
  PadR->cd();
  //h_estimation->SetFillStyle(1001);
  ratio->SetFillStyle(1001);
  ratio->Reset("ICES");
  ratio_smooth->Reset("ICES");
  ratio_global_fit->Reset("ICES");  
  ConstructSignificantHistogram(ratio, h_estimation, histo_main);
  ConstructSignificantHistogram(ratio_smooth, h_estimation_smooth, histo_main);
  ConstructSignificantHistogram(ratio_global_fit, h_estimation_global_fit, histo_main);
  /*
  for (int itr_bin = h_estimation->FindFirstBinAbove(0,1); itr_bin < h_estimation->FindLastBinAbove(0,1) + 1; itr_bin++){
    Double_t sig = 0.;
    if (h_estimation->GetBinContent(itr_bin) >= 0.){
      sig =( histo_main->GetBinContent(itr_bin) - h_estimation->GetBinContent(itr_bin))/sqrt(histo_main->GetBinContent(itr_bin));
    }  
    ratio->SetBinContent(itr_bin,sig);
  }
  */
  Beautify(ratio);
  ratio->SetLineWidth(1);
  Beautify(ratio_smooth);
  ratio_smooth->SetLineColor(4);
  ratio_smooth->SetLineWidth(1);
  ratio_smooth->SetFillStyle(0);
  Beautify(ratio_global_fit);
  ratio_global_fit->SetLineColor(3);
  ratio_global_fit->SetLineWidth(1);
  ratio_global_fit->SetFillStyle(0);

  ratio->Draw(); 
  ratio_smooth->Draw("SAME"); 
  ratio_global_fit->Draw("SAME"); 
  
  //TF1 *fleft   = new TF1("fleft", fit_function, start_b1, end_b1, n_param_func);
  //fleft->SetParameters(fit_function->GetParameters());
  //TF1 *fmiddle = new TF1("fmiddle", fit_function, end_b1, start_b2, n_param_func);
  //fmiddle->SetParameters(fit_function->GetParameters());
  //TF1 *fright  = new TF1("fright",fit_function, start_b2, end_b2, n_param_func);
  //fright->SetParameters(fit_function->GetParameters());
  //fright->Draw("SAME");
  //fmiddle->Draw("SAME");

  //fit_function->SetLineColor(2);
  
  //  fit_function->Draw("SAME");
    

  //Set Legends
  TLegend * legend0 = new TLegend(0.6,0.9,1.0,0.99);
  legend0->SetFillColor(0);
  vector<TString> Legend; 
  Legend.push_back("a");

  canvas_fit->Update();
  canvas_fit->Write("Fit");
  Fit_output->Write();
  Fit_output->Close();

  return 0;
  
}
 

#include "TLatex.h"
#include "TLine.h"
#include "TPave.h"
#include "TMarker.h"
#include "TROOT.h"
#include "TH2.h"
#include "THStack.h"

#include <sstream>
#include <stdio.h>
#include <vector>
#include <fstream>
#include <iostream>

using namespace std;

void Error(TString msg) {
  printf("ERROR: %s\n", msg.Data()); 
}

static void setLogBins(Int_t nbins, Double_t min, Double_t max, Double_t* xpoints){
  Double_t logmin      = log10(min);
  Double_t logmax      = log10(max);
  Double_t logbinwidth = (Double_t)((logmax-logmin)/(Double_t)nbins);
  xpoints[0]           = min;
  for(Int_t i=1; i <= nbins; i++) xpoints[i] = TMath::Power(10,(logmin+i*logbinwidth));
}


TH1F * FromTreeMakeTH1F(TTree *t, TString var, TString hname, int Nbins, double min, double max, TCut cut, Double_t scale, bool isGeV, bool isData, bool unit_scale, Int_t fill_color, bool isLog_Xaxis){
  
  //if(isLog_Xaxis && min <= 0.0){ 
  //std::cout << "
  //TString var = Var;

  static const Int_t    nlogmassbins = 50;  // or any other number that gives reasonable spacing
  static const Double_t logmassmin   = 0.0000001; // cannot start from 0 and there’s no point in that anyway...
  //if(min <= 0.0){ logmassmin = 0.0000001; }
  static const Double_t logmassmax   = max;
  static Double_t       logmassbins[nlogmassbins+1];
  setLogBins(nlogmassbins, logmassmin, logmassmax, logmassbins); // set the binning
  //TCanvas* cnv = new TCanvas(“c”,”c”,600,400);                   // or whatever
  //cnv->SetLogx();                                                // bins will appear with equal size eventually
  

  

  Double_t xbins[Nbins+1];
  if(isLog_Xaxis){
    Nbins++;
    bool isPreDefinedBins = false;
    Double_t logxmin; 
    Double_t logxmax;
    if(min > 0.0){ logxmin = log10(min); 
    } else { logxmin = 0.00000001; }
    if(max > 0.0){ logxmax = log10(max);
    } else { logxmax = 100000.0; }

    Double_t logbinwidth = (logxmax-logxmin)/Nbins;
  
    if(Nbins < 0){ 
      isPreDefinedBins = true;
      Nbins = 65; 
      COUT;
      min = 253.;
      max = 5350.;
    }
  
    /*
      Double_t xbins[66]={253,272,294,316,339,364,390,
      417,445,474,504,535,566,599,633,668,
      705,743,782,822,864,907,952,999,
      1048,1098,1150,1203,1259,1316,1376,
      1437,1501,1567,1635,1706,1779,1854,
      1932,2012,2095,2181,2269,2360,2454,
      2551,2650,2753,2860,2970,3084,3202,
      3324,3450,3581,3716,3855,3999,
      4149,4303,4463,4629,4800,4977,
      5160,5350};
    */
    //xbins[0] = min;
    if(!isPreDefinedBins){
      for (Int_t i = 0; i <= Nbins; i++){
	xbins[i] = TMath::Power(10, logxmin+i*logbinwidth); //Setting bin boundaries
	//	cout << "xbins["<<i<<"] = "<< xbins[i] << endl; 
      }
    }
    if(isPreDefinedBins){
      xbins[0]=253.;
      xbins[1]=272.;
      xbins[2]=294.;
      xbins[3]=316.;
      xbins[4]=339.;
      xbins[5]=364.;
      xbins[6]=390.;
      xbins[7]=417.;
      xbins[8]=445.;
      xbins[9]=474.;
      xbins[10]=504.;
      xbins[11]=535.;
      xbins[12]=566.;
      xbins[13]=599.;
      xbins[14]=633.;
      xbins[15]=668.;
      xbins[16]=705.;
      xbins[17]=743.;
      xbins[18]=782.;
      xbins[19]=822.;
      xbins[20]=864.;
      xbins[21]=907.;
      xbins[22]=952.;
      xbins[23]=999.;
      xbins[24]=1048.;
      xbins[25]=1098.;
      xbins[26]=1150.;
      xbins[27]=1203.;
      xbins[28]=1259.;
      xbins[29]=1316.;
      xbins[30]=1376.;
      xbins[31]=1437.;
      xbins[32]=1501.;
      xbins[33]=1567.;
      xbins[34]=1635.;
      xbins[35]=1706.;
      xbins[36]=1779.;
      xbins[37]=1854.;
      xbins[38]=1932.;
      xbins[39]=2012.;
      xbins[40]=2095.;
      xbins[41]=2181.;
      xbins[42]=2269.;
      xbins[43]=2360.;
      xbins[44]=2454.;
      xbins[45]=2551.;
      xbins[46]=2650.;
      xbins[47]=2753.;
      xbins[48]=2860.;
      xbins[49]=2970.;
      xbins[50]=3084.;
      xbins[51]=3202.;
      xbins[52]=3324.;
      xbins[53]=3450.;
      xbins[54]=3581.;
      xbins[55]=3716.;
      xbins[56]=3855.;
      xbins[57]=3999.;
      xbins[58]=4149.;
      xbins[59]=4303.;
      xbins[60]=4463.;
      xbins[61]=4629.;
      xbins[62]=4800.;
      xbins[63]=4977.;
      xbins[64]=5160.;
      xbins[65]=5350.;
    }
  }
  
  //TH1F * hh = new TH1F("hh","hist with log x axis", Nbins, xbins);
  TH1F* hh = new TH1F("hh","h_logx", nlogmassbins, logmassbins);        // use the bins array to define the histogram
    
  //hh->Sumw2();
  
  TH1F * histo;
  //if( isData){ histo->Sumw2();}
  //TString hh;
  
  if(!isGeV && !isLog_Xaxis){ t->Draw(var+"/1000.0>>"+hname+Form("(%d,%.3f,%.3f)",Nbins,min,max),cut); }
  //if(!isGeV && isLog_Xaxis){ t->Draw("log10("+var+"/1000.0)>>hh"+Form("(%d,",Nbins)+"xbins)",cut); }
  if(!isGeV && isLog_Xaxis){ t->Draw(var+"/1000.0>>hh",cut); }
  //if(!isGeV && isLog_Xaxis){ t->Draw(var+"/1000.0>>"+hname+Form("%d,",Nbins)+"xbins",cut); }
  
  if( isGeV && !isLog_Xaxis){ t->Draw(var+">>"+hname+Form("(%d,%.3f,%.3f)",Nbins,min,max),cut); }
  //  if( isGeV &&  isLog_Xaxis){ t->Draw("log10("+var+")>>hh"+Form("(%d,",Nbins)+"xbins)",cut); }
  if( isGeV &&  isLog_Xaxis){ t->Draw(var+">>hh", cut); }
  
  if(!isLog_Xaxis){ histo = (TH1F*) gROOT->FindObject(hname);  }
  if( isLog_Xaxis){ histo = hh; }


  histo->GetXaxis()->SetLabelSize(0.04);
  histo->GetXaxis()->SetTitleSize(0.04);
  
  if(isData){
    histo->Sumw2();
    histo->SetLineColor(fill_color);
    histo->SetMarkerColor(fill_color);
    histo->SetMarkerSize(0.5);
    if(unit_scale && histo->Integral(0, Nbins) <= 0. ) { histo->Scale(1./(histo->Integral(0, Nbins))); } 
  }
  if(!isData){ 
    histo->SetFillColor(fill_color);
    histo->Scale(scale);
    if(unit_scale && histo->Integral(0, Nbins) <= 0.) { histo->Scale(1./(histo->Integral(0, Nbins))); }
  }


    
  TH1F * empty_histo = (TH1F*) histo->Clone("empty_histo");
  if (t == NULL){ Error("Could not draw histogram "+hname+" for variable "+var+". Returning empty histogram."); delete hh; return empty_histo; }     
  if (histo == NULL){ Error("Could not FindObject("+hname+") for histogram "+hname+". Returning empty histogram."); delete hh; return empty_histo; }
  if (histo->Integral(0, Nbins) <= 0.){ Error("Histogram "+hname+" is empty."); delete hh; return empty_histo;}


  delete hh;
  delete empty_histo;
  
  return histo;

}


TH2F * FromTreeMakeTH2F(TTree *t, TString var1 /*y-axis*/, TString var2 /*x-axis*/, TString hname, int Nbins_x, double min_x, double max_x, int Nbins_y, double min_y, double max_y, TCut cut, bool isGeV_x, bool isGeV_y, TString x_axis, TString y_axis, bool isAutoBin){
  
  TH2F * histo;
  
  if(!isGeV_x && !isGeV_y ){ 
    if( !isAutoBin ){ t->Draw(var1+"/1000.0:"+var2+"/1000.0>>"+hname+Form("(%d,%.3f,%.3f,%d,%.3f,%.3f)",Nbins_x,min_x,max_x,Nbins_y,min_y,max_y), cut, "colz"); }
    if( isAutoBin  ){ t->Draw(var1+"/1000.0:"+var2+"/1000.0>>"+hname, cut, "colz"); }
  }
  if(!isGeV_x && isGeV_y ){ 
    if( !isAutoBin ){ t->Draw(var1+"/1.0:"+var2+"/1000.0>>"+hname+Form("(%d,%.3f,%.3f,%d,%.3f,%.3f)",Nbins_x,min_x,max_x,Nbins_y,min_y,max_y), cut, "colz"); }
    if( isAutoBin  ){ t->Draw(var1+"/1.0:"+var2+"/1000.0>>"+hname, cut, "colz"); }
  }
  if(isGeV_x && !isGeV_y ){ 
    if( !isAutoBin ){ t->Draw(var1+"/1000.0:"+var2+"/1.0>>"+hname+Form("(%d,%.3f,%.3f,%d,%.3f,%.3f)",Nbins_x,min_x,max_x,Nbins_y,min_y,max_y), cut, "colz"); }
    if( isAutoBin  ){ t->Draw(var1+"/1000.0:"+var2+"/1.0>>"+hname, cut, "colz"); }
  }
  if( isGeV_x && isGeV_y ){ 
    if( !isAutoBin ){ t->Draw(var1+":"+var2+">>"+hname+Form("(%d,%.3f,%.3f,%d,%.3f,%.3f)",Nbins_x,min_x,max_x,Nbins_y,min_y,max_y),cut, "colz"); }
    if( isAutoBin  ){ t->Draw(var1+":"+var2+">>"+hname+Form("(%d,%.3f,%.3f,%d,%.3f,%.3f)",Nbins_x,min_x,max_x,Nbins_y,min_y,max_y),cut, "colz"); }
  }


  histo = (TH2F*) gROOT->FindObject(hname);  
  
  TH2F * empty_histo = (TH2F*) histo->Clone("empty_histo");

  //  TH1F * empty_histo = new TH1F("empty_histo", "", Nbins, min, max);
  if (t == NULL){ Error("Could not draw histogram "+hname+" for variable "+var1+":"+var2+". Returning empty histogram."); return empty_histo; }     
    
  if (histo == NULL){ Error("Could not FindObject("+hname+") for histogram "+hname+". Returning empty histogram."); return empty_histo; }
  //if (histo->Integral(0, Nbins) <= 0.){ Error("Histogram "+hname+" is empty."); return empty_histo;}

  histo->GetXaxis()->SetTitle(x_axis);
  histo->GetYaxis()->SetTitle(y_axis);
  histo->GetZaxis()->SetTitleFont(42);
  histo->SetTitle(0);
  histo->Draw("colz");
  /*
  if( isData){
    histo->Sumw2();
    histo->SetLineColor(fill_color);
    histo->SetMarkerColor(fill_color);
    histo->SetMarkerSize(0.7);
    if(unit_scale) { histo->Scale(1./(histo->Integral(0, Nbins))); } 
  }
  if(!isData){ 
    histo->SetFillColor(fill_color);
    histo->Scale(scale);
    if(unit_scale) { histo->Scale(1./(histo->Integral(0, Nbins))); }
  }
  */

  delete empty_histo;
  
  return histo;

}

TTree * GetTree(TString fn, TString tree_name){

  cout << "Opening File : "+fn << endl;
  TFile * f = TFile::Open(fn);
  int file_has_problem = 0;
  if (f==NULL && file_has_problem == 0){
    Error("Cannot open "+fn);
    file_has_problem = 1;
  }
  TTree * t = (TTree*) f->Get(tree_name);
  if (t==NULL && file_has_problem == 0){
    Error("Cannot access Tree in "+fn);
    file_has_problem = 1;
  }
  //--- Make sure Tree has at least one event ---
  TString met_histo; 
  if(t->GetEntries() < 1 && file_has_problem == 0){
   //File exists but has 0 events ==> Must use Default file
    Error("File exists but has 0 events...");//
    //  f = TFile::Open(Default);
    file_has_problem = 1;
  }
  
  return t;
}
 
pair<THStack *, TLegend *> FromTH1FMakeTHStack(vector<TH1F*> * histo, TString stack_name, TLegend * legend, vector<TString> * h_description, bool order_size, bool nostack, bool isSumw2, TString x_axis, TString y_axis, TH1F * data = NULL){
 
  if((int) histo->size() != (int) h_description->size()){ std::cout <<"Histogram number(s) "<<(int) histo->size()<<" and legend item(s) "<<(int) h_description->size()<<" do not match" << std::endl; }
  THStack * stack = new THStack("stack", stack_name);
  stack->SetTitle("");
  // TLegend * legend = new TLegend(0.7,0.5,0.9,0.9);
  legend->Clear();
  legend->SetFillColor(0);


  Int_t h_array_size = (int) histo->size();
  Int_t neworder[h_array_size];
  Int_t reorder[h_array_size];
  if(order_size){
    Float_t integral[h_array_size];
    for(int itr = 0; itr < h_array_size; itr++){
      integral[itr] = (*histo)[itr]->Integral();
      reorder[itr] = 0;
      //cout << "First histo has integral = " << integral[itr] << endl;
    }
    for(int itr1 = 0; itr1 < h_array_size; itr1++){
      for(int itr2 = 0; itr2 < h_array_size; itr2++){
	if(itr1 == itr2) continue;
	if(integral[itr1] < integral[itr2]){
	  reorder[itr1] += 1;
	}
      }
    }
    for(int itr = 0; itr < h_array_size; itr++){ neworder[reorder[itr]] = itr; }
  }
  for(int itr = 0; itr < h_array_size; itr++){
    //cout << "reorder["<<itr<<"] "<< reorder[itr] << endl;
    if( nostack && (*histo)[itr] != NULL ){ 
      //std::cout << "(*histo)["<<itr<<"]->Integral()" << (*histo)[itr]->Integral() << std::endl;
      //std::cout << "(*histo)[itr]->GetDimension()" << (*histo)[itr]->GetDimension() << std::endl;
      stack->Add((*histo)[itr], "nostack");
    }
    if(!nostack && (*histo)[itr] != NULL ){ stack->Add((*histo)[reorder[itr]]); }
    //if(!nostack && (*histo)[itr] != NULL ){ stack->Add((*histo)[itr]); }
    TString type = "f"; if(isSumw2){ type = "lep";}
    legend->AddEntry((*histo)[itr], (*h_description)[itr], type);
  }


  if(data != NULL){ 
    stack->Add(data, "nostack");
    legend->AddEntry(data, "Data", "lep");
  }
  
  //Draw Axis
  stack->Draw(); //Must be called before GetXaxis()
  stack->GetXaxis()->SetTitle(x_axis);
  stack->GetYaxis()->SetTitle(y_axis);
  
  pair<THStack*,TLegend*> stackLegend; 
  stackLegend.first = stack;
  stackLegend.second = legend;

  return stackLegend;

}

void FromTEfficiencyDrawCanvas(vector<TEfficiency*> *graph, TCanvas * canvas, TLegend * legend, vector<TString> * h_description, TString x_axis, TString y_axis, TString title){
 


  
}

void FromTHStackWriteToFile(THStack * stack, TString file_name, TString file_type, TString path_to_write, Int_t canvas_x_size, Int_t canvas_y_size) {

  TCanvas * canvas = new TCanvas(file_name, file_name, canvas_x_size, canvas_y_size);
  stack->Draw();
  canvas->SaveAs(path_to_write+file_name+"."+file_type);
  delete canvas;
  
}

THStack * DrawSame(vector<TH1F*> * histo, TString stack_name, vector<TString> * h_description, bool order_size, bool nostack, TString x_axis, TString y_axis, TH1F * data = NULL){
 
  if((int) histo->size() != (int) h_description->size()){ Error("Histogram numbers and legend items do not match");}

  THStack * stack = new THStack("stack", stack_name);
  stack->SetTitle("");
  TLegend * legend = new TLegend(0.7,0.5,0.9,0.9);
  legend->SetFillColor(0);

  Int_t h_array_size = (int) histo->size();
  Int_t reorder[h_array_size];
  if(order_size){
    Float_t integral[h_array_size];
    for(int itr = 0; itr < h_array_size; itr++){
      integral[itr] = (*histo)[itr]->Integral();
    }
    for(int itr1 = 0; itr1 < h_array_size; itr1++){
      for(int itr2 = 0; itr2 < h_array_size; itr2++){
	if(itr1 == itr2) continue;
	if(integral[itr1] < integral[itr2]){
	  reorder[itr1] += 1;
	}
      }
    }
  }
  for(int itr = 0; itr < h_array_size; itr++){
    if( nostack ){ stack->Add((*histo)[reorder[itr]], "nostack"); }
    if(!nostack ){ stack->Add((*histo)[reorder[itr]]); }
    legend->AddEntry((*histo)[itr], (*h_description)[itr], "f");
  }
  if(data != NULL){ 
    stack->Add(data, "nostack");
    legend->AddEntry(data, "Data", "p");
  }

  return stack;

}

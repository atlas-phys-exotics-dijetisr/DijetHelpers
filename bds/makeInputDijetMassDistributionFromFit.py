#!/usr/bin/env python

#******************************************
#import stuff
import ROOT
import math, os, sys

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

def makeInputDijetMassDistributionFromFit(inputFileName, inputHistName, nPar, tag):

    print '\n******************************************'
    print 'making input dijet mass ditribution from MC fit'

    #------------------------------------------
    #input parameters
    print '\nparameters:'
    print '  file:          %s'%inputFileName
    print '  hist:          %s'%inputHistName
    print '  nPar:          %s'%nPar
    print '  tag:           %s'%tag
    
    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    
    #******************************************
    #get input MC histogram
    #******************************************
    
    #------------------------------------------
    #check input file
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file')

    #open input file
    inputFile = ROOT.TFile.Open(inputFileName,'READ')
    if not inputFile:
        raise SystemExit('\n***ERROR*** couldn\'t open input file')

    #get input data-like QCD histogram
    inputMjj = inputFile.GetDirectory('Nominal').Get(inputHistName)
    #inputMjj = inputFile.Get(inputHistName)
    if not inputMjj:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram\n***NOTE*** check the how the histogram is retrieved in the code (path?)\n')
    inputMjj.SetName('mjj')
    inputMjj.SetTitle('mjj')

    #------------------------------------------
    #check output file
    outputFileName = localdir+'/results/inputMjjDL.'+tag+'.'+nPar+'.par.root'
    if os.path.isfile(outputFileName):
        raise SystemExit('\n***WARNING*** output file exists already: '+outputFileName)

    #------------------------------------------
    #save input mjj histogram
    outputFile = ROOT.TFile.Open(outputFileName, 'RECREATE')
    outputFile.cd()
    inputMjj.Write()
    outputFile.Close()
        
    #******************************************
    #fit input MC histogram
    #******************************************

    #------------------------------------------
    #prepare config file for fit
    configFileName    = localdir+'/data/searchPhase.config'
    newConfigFileName = localdir+'/configs/searchPhase.'+tag+'.'+nPar+'.par.config'
    searchPhaseOutputFileName = localdir+'/results/searchPhase.'+tag+'.'+nPar+'.par.root'

    if nPar == '3':
        print 'it\'s 3 parameters'
        pars = [0.180377, 8.1554, -5.25718]
    elif nPar == '5':
        pars = [0.426909, 9.45062, -5.49925, -0.759262, -0.240173]
    else:
        nPar = '4' #DEFAULT
        pars = [4.28171, 10.814, -2.72612, 0.577889]

    with open(configFileName,'r') as configFile:
        with open(newConfigFileName,'w') as newConfigFile:
            for line in configFile:
                newLine = line
                newLine = newLine.replace('dummyInputFileName', outputFileName)
                newLine = newLine.replace('dummyHistName', 'mjj')
                newLine = newLine.replace('dummyOutputFileName', searchPhaseOutputFileName)
                
                #3 parameters
                if nPar == '3':
                    newLine = newLine.replace('dummyFuncCode', str(9))
                    newLine = newLine.replace('dummyNPar', str(3))
                    newLine = newLine.replace('dummyP1', str(pars[0]))
                    newLine = newLine.replace('dummyP2', str(pars[1]))
                    newLine = newLine.replace('dummyP3', str(pars[2]))

                #4 parameters
                if nPar == '4':
                    newLine = newLine.replace('dummyFuncCode', str(4))
                    newLine = newLine.replace('dummyNPar', str(4))
                    newLine = newLine.replace('dummyP1', str(pars[0]))
                    newLine = newLine.replace('dummyP2', str(pars[1]))
                    newLine = newLine.replace('dummyP3', str(pars[2]))
                    newLine = newLine.replace('#parameter4', 'parameter4\t\t'+str(pars[3]))

                #5 parameters
                elif nPar == '5':
                    newLine = newLine.replace('dummyFuncCode', str(7))
                    newLine = newLine.replace('dummyNPar', str(5))
                    newLine = newLine.replace('dummyP1', str(pars[0]))
                    newLine = newLine.replace('dummyP2', str(pars[1]))
                    newLine = newLine.replace('dummyP3', str(pars[2]))
                    newLine = newLine.replace('#parameter4', 'parameter4\t\t'+str(pars[3]))
                    newLine = newLine.replace('#parameter5', 'parameter5\t\t'+str(pars[4]))

                newConfigFile.write(newLine)

    #------------------------------------------
    #search phase
    print '\n******************************************'
    print 'runnning SearchPhase'
    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------
    os.system('SearchPhase --config %s --noDE'%newConfigFileName)

    #------------------------------------------
    #get BH p-value
    #open SearchPhase results file
    if not os.path.isfile(searchPhaseOutputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file')
    searchPhaseOutputFile = ROOT.TFile(searchPhaseOutputFileName)

    #get SearchPhase results
    #histograms
    #basicData      = searchPhaseOutputFile.Get("basicData")
    basicBackground = searchPhaseOutputFile.Get("basicBkgFrom4ParamFit")
    #residualHist   = searchPhaseOutputFile.Get("residualHist")
    
    #vector
    bumpHunterStatOfFitToData = searchPhaseOutputFile.Get("bumpHunterStatOfFitToData")
    if not bumpHunterStatOfFitToData:
        raise SystemExit('\n***ERROR*** couldn\'t find bumpHunterStatOfFitToData vector')
    
    bumpHunterStatValue = bumpHunterStatOfFitToData[0]
    bumpHunterPValue    = bumpHunterStatOfFitToData[1]
    bumpHunterPValueErr = bumpHunterStatOfFitToData[2]

    #vector
    bumpHunterPLowHigh = searchPhaseOutputFile.Get('bumpHunterPLowHigh')
    #bumpHunterStatValue = bumpHunterPLowHigh[0]
    bumpLowEdge         = bumpHunterPLowHigh[1]
    bumpHighEdge        = bumpHunterPLowHigh[2]

    #print
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)
    
    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas

    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

    
    #******************************************
    #round and then save the fit histogram
    #******************************************
    print '\n******************************************'
    print 'saving background histogram obtained from fit'

    #------------------------------------------
    #round fit histogram bin content to integer numbers; NOTE fill histogram one entry at a time
    mjjFit = ROOT.TH1D('mjj','mjj',
                       basicBackground.GetXaxis().GetNbins(),
                       basicBackground.GetXaxis().GetXbins().GetArray())
    mjjFit.SetDefaultSumw2(ROOT.kTRUE)
    for ii in xrange(mjjFit.GetNbinsX()):
        bincontent = int( round( basicBackground.GetBinContent(ii) ) )
        for jj in xrange(bincontent):
            mjjFit.Fill(mjjFit.GetBinCenter(ii))

    #------------------------------------------
    #save mjj fit histogram
    outputMjjFitFileName = localdir+'/results/inputMjjFit.'+tag+'.'+nPar+'.par.root'
    if os.path.isfile(outputMjjFitFileName):
        raise SystemExit('\n***WARNING*** output file exists already: '+outputMjjFitFileName)

    outputMjjFitFile = ROOT.TFile.Open(outputMjjFitFileName, 'RECREATE')
    outputMjjFitFile.cd()
    mjjFit.Write()
    outputMjjFitFile.Close()

    #------------------------------------------
    return outputMjjFitFileName


#******************************************
if __name__ == '__main__':

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 5:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters (%s/%s) \
            \nHOW TO: time python -u makeInputDijetMassDistributionFromFit.py inputFileName inputHistName nPar tag\n\
EXAMPLE: time python -u makeInputDijetMassDistributionFromFit.py /atlas/data5/userdata/guescini/dijet/inputs/MC15_20150412/dataLikeHists/mc15_13TeV_Pythia8_v1/dataLikeHistograms.QCDDiJet.v1.root mjj_DataLike_QCDDiJet_1fb 4 test'\
            %(len(sys.argv),5))

    #------------------------------------------
    #get input parameters and run
    inputFileName = sys.argv[1].strip()
    inputHistName = sys.argv[2].strip()
    nPar = sys.argv[3].strip()
    tag = sys.argv[4].strip()

    out = makeInputDijetMassDistributionFromFit(inputFileName, inputHistName, nPar, tag)
    
    #------------------------------------------
    print '\ndone %s'%out

#!/bin/python

#******************************************
#import stuff
import ROOT
import re, sys, os, math, shutil, fileinput

#******************************************
#set ATLAS style #MOVED inside functions
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def plotFitsComparison(searchFileName1, searchFileName2, lumi, tag):

    print '\n******************************************'
    print 'plotting fits comparison'
    print '\nparameters:'
    print '  search file name 1: %s'%searchFileName1
    print '  search file name 2: %s'%searchFileName2
    print '  lumi [ifb]:         %s'%lumi
    print '  tag:                %s'%tag

    #------------------------------------------
    #open SearchPhase results files
    if not os.path.isfile(searchFileName1):
        raise SystemExit('\n***ERROR*** couldn\'t find input file 1')
    f1 = ROOT.TFile(searchFileName1)

    if not os.path.isfile(searchFileName2):
        raise SystemExit('\n***ERROR*** couldn\'t find input file 2')
    f2 = ROOT.TFile(searchFileName2)
    
    #------------------------------------------
    #first things first
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    #define necessary quantities
    lumi = float(lumi) #fb^-1
    slumi = ('%.1f'%float(str(lumi).replace('p','.'))).replace('.','p')
    nPar1 = f1.Get("fittedParameters").GetNoElements()
    nPar2 = f2.Get("fittedParameters").GetNoElements()
    stag = 'figures/fitsComparison.'+slumi+'.ifb.'+str(nPar1)+'.par1'+str(nPar2)+'.par2'
        
    #------------------------------------------
    #get SearchPhase results
    #histograms
    basicBackground1            = f1.Get("basicBkgFrom4ParamFit")
    residualHist1               = f1.Get("residualHist")

    basicBackground2            = f2.Get("basicBkgFrom4ParamFit")
    residualHist2               = f2.Get("residualHist")

    #signal bump
    bumpHunterPLowHigh1 = f1.Get('bumpHunterPLowHigh')
    #bumpHunterStatValue1 = bumpHunterPLowHigh1[0]
    bumpLowEdge1         = bumpHunterPLowHigh1[1]
    bumpHighEdge1        = bumpHunterPLowHigh1[2]

    bumpHunterPLowHigh2 = f2.Get('bumpHunterPLowHigh')
    #bumpHunterStatValue2 = bumpHunterPLowHigh2[0]
    bumpLowEdge2         = bumpHunterPLowHigh2[1]
    bumpHighEdge2        = bumpHunterPLowHigh2[2]

    #initial p-value
    bumpHunterStatOfFitToDataInitial1 = f1.Get("bumpHunterStatOfFitToDataInitial")
    bumpHunterStatValueInitial1 = bumpHunterStatOfFitToDataInitial1[0]
    bumpHunterPValueInitial1    = bumpHunterStatOfFitToDataInitial1[1]
    bumpHunterPValueErrInitial1 = bumpHunterStatOfFitToDataInitial1[2]

    bumpHunterStatOfFitToDataInitial2 = f2.Get("bumpHunterStatOfFitToDataInitial")
    bumpHunterStatValueInitial2 = bumpHunterStatOfFitToDataInitial2[0]
    bumpHunterPValueInitial2    = bumpHunterStatOfFitToDataInitial2[1]
    bumpHunterPValueErrInitial2 = bumpHunterStatOfFitToDataInitial2[2]
    
    #final p-value
    bumpHunterStatOfFitToData1 = f1.Get("bumpHunterStatOfFitToData")
    bumpHunterStatValue1 = bumpHunterStatOfFitToData1[0]
    bumpHunterPValue1    = bumpHunterStatOfFitToData1[1]
    bumpHunterPValueErr1 = bumpHunterStatOfFitToData1[2]

    bumpHunterStatOfFitToData2 = f2.Get("bumpHunterStatOfFitToData")
    bumpHunterStatValue2 = bumpHunterStatOfFitToData2[0]
    bumpHunterPValue2    = bumpHunterStatOfFitToData2[1]
    bumpHunterPValueErr2 = bumpHunterStatOfFitToData2[2]

    bumpFoundVector1 = f1.Get("bumpFound")
    bumpFound1 = bumpFoundVector1[0]
    
    bumpFoundVector2 = f2.Get("bumpFound")
    bumpFound2 = bumpFoundVector2[0]

    #chi2
    chi2OfFitToData1 = f1.Get('chi2OfFitToData')
    chi2OfFitToDataValue1 = chi2OfFitToData1[0]

    chi2OfFitToData2 = f2.Get('chi2OfFitToData')
    chi2OfFitToDataValue2 = chi2OfFitToData2[0]

    #ndf
    ndf1 = f1.Get('NDF')
    ndfValue1 = ndf1[0]

    ndf2 = f2.Get('NDF')
    ndfValue2 = ndf2[0]

    #chi2/ndf
    chi2ndf1 = float(chi2OfFitToDataValue1/ndfValue1)
    chi2ndf2 = float(chi2OfFitToDataValue2/ndfValue2)

    #log likelihood
    logL1 = f1.Get('logLOfFitToData')
    logLValue1 = logL1[0]

    logL2 = f2.Get('logLOfFitToData')
    logLValue2 = logL2[0]
    
    #print
    print '\n%s parameters fit'%nPar1
    print "bump range: %s GeV - %s GeV"%(bumpLowEdge1,bumpHighEdge1)
    print "bump window excluded? %s"%bumpFound1
    print "BumpHunter stat = %s"%bumpHunterStatValue1
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue1, bumpHunterPValueErr1)        
    print 'fit chi2/ndf = %.3f'%chi2ndf1
    print 'ndf = %s'%ndfValue1
    print 'logL = %s'%logLValue1
    
    print '\n%s parameters fit'%nPar2
    print "bump range: %s GeV - %s GeV"%(bumpLowEdge2,bumpHighEdge2)
    print "bump window excluded? %s"%bumpFound2
    print "BumpHunter stat = %s"%bumpHunterStatValue2
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue2, bumpHunterPValueErr2)        
    print 'fit chi2/ndf = %.3f'%chi2ndf2
    print 'ndf = %s'%ndfValue2
    print 'logL = %s'%logLValue2

    deltaLogL = float(logLValue1 - logLValue2)
    print '\ndelta log likelihood = %f'%deltaLogL
    deltaNdf = int(ndfValue1 - ndfValue2)
    print 'delta ndf = %s'%deltaNdf
    wilksPVal = ROOT.TMath.Prob( abs(deltaLogL), abs(deltaNdf) )
    print 'Wilks\' p-value = %f'%wilksPVal
    
    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------


    #------------------------------------------
    #plot

    #------------------------------------------
    #canvas and pads
    c1 = ROOT.TCanvas('c1', 'c1', 400, 50, 800, 600)
    pad1 = ROOT.TPad("pad1","pad1",0, 0.7, 1, 1.0)#top
    pad2 = ROOT.TPad("pad2","pad2",0, 0.4, 1, 0.7)#central
    pad3 = ROOT.TPad("pad3","pad3",0, 0.0, 1, 0.4)#bottom
    
    pad1.SetTopMargin(0.08)
    pad1.SetBottomMargin(0.0)
    pad1.Draw()

    pad2.SetTopMargin(0.0)
    pad2.SetBottomMargin(0.0)
    pad2.Draw()

    pad3.SetTopMargin(0.0)
    pad3.SetBottomMargin(0.25) #CHECK
    pad3.Draw()

    #------------------------------------------
    #common stuff
    intervalLine = ROOT.TLine()
    intervalLine.SetLineColor(ROOT.kBlue)

    m = ROOT.TLatex()
    m.SetNDC()
    m.SetTextFont(43)
    m.SetTextColor(1)
    m.SetTextSize(15)

    textx = 0.70

    #------------------------------------------
    #pad 1: functions ratio
    pad1.cd()
    pad1.Clear()
    pad1.SetLogx(0)
    pad1.SetLogy(0)

    #get ratio
    funcRatio = basicBackground1.Clone()
    funcRatio.SetName('funcRatio')
    funcRatio.SetName('funcRatio')
    funcRatio.Divide(basicBackground2)

    funcRatio.GetXaxis().SetRangeUser(1000., funcRatio.GetXaxis().GetXmax())

    funcRatio.GetYaxis().SetTitle('%s-par. fit / %s-par. fit'%(str(nPar1),str(nPar2)))
    funcRatio.GetYaxis().SetTitleFont(43)
    funcRatio.GetYaxis().SetTitleSize(15)
    funcRatio.GetYaxis().SetLabelFont(43)
    funcRatio.GetYaxis().SetLabelSize(15)
    funcRatio.Draw('hist ][')

    pad1.Update()
    
    #------------------------------------------
    #pad 2: default fit
    pad2.cd()
    pad2.Clear()
    pad2.SetLogx(0)
    pad2.SetLogy(0)

    residualHist1.GetXaxis().SetRangeUser(1000., funcRatio.GetXaxis().GetXmax())

    residualHist1.GetYaxis().SetTitle('%s-par. fit residuals'%str(nPar1))
    residualHist1.GetYaxis().SetTitleFont(43)
    residualHist1.GetYaxis().SetTitleSize(15)
    residualHist1.GetYaxis().SetLabelFont(43)
    residualHist1.GetYaxis().SetLabelSize(15)
    residualHist1.SetFillColor(ROOT.kRed)
    residualHist1.Draw()

    #bump range
    ROOT.gPad.Update()
    lowy = ROOT.gPad.GetFrame().GetY1()
    highy = ROOT.gPad.GetFrame().GetY2()
    if bumpFound1:
        intervalLine.SetLineColor(ROOT.kGreen+1)
    else:
        intervalLine.SetLineColor(ROOT.kBlue)
    intervalLine.DrawLine(bumpLowEdge1,lowy,bumpLowEdge1,highy)
    intervalLine.DrawLine(bumpHighEdge1,lowy,bumpHighEdge1,highy)
    m.DrawLatex(textx,0.50,'logL = %.1f'%logLValue1)
    m.DrawLatex(textx,0.40,'#chi^{2}/ndf = %.3f'%chi2ndf1)
    m.DrawLatex(textx,0.30,'BH range [GeV] = %.0f - %.0f'%(bumpLowEdge1,bumpHighEdge1))
    m.DrawLatex(textx,0.20,'initial BH p-value = %s'%bumpHunterPValueInitial1)
    m.DrawLatex(textx,0.10,'final BH p-value = %s'%bumpHunterPValue1)

    pad2.Update()

    #------------------------------------------
    #pad 3: QCD
    pad3.cd()
    pad3.Clear()
    pad3.SetLogx(0)
    pad3.SetLogy(0)

    residualHist2.GetXaxis().SetTitle("m_{jj} [GeV]")
    residualHist2.GetXaxis().SetTitleOffset(4)
    residualHist2.GetXaxis().SetTitleFont(43)
    residualHist2.GetXaxis().SetTitleSize(15)
    residualHist2.GetXaxis().SetLabelFont(43)
    residualHist2.GetXaxis().SetLabelSize(15)
    residualHist2.GetXaxis().SetRangeUser(1000., funcRatio.GetXaxis().GetXmax())
    #residualHist2.GetXaxis().SetNdivisions(50510) #TEST

    residualHist2.GetYaxis().SetTitle('%s-par. fit residuals'%str(nPar2))
    residualHist2.GetYaxis().SetTitleFont(43)
    residualHist2.GetYaxis().SetTitleSize(15)
    residualHist2.GetYaxis().SetLabelFont(43)
    residualHist2.GetYaxis().SetLabelSize(15)
    residualHist2.SetFillColor(ROOT.kRed)
    residualHist2.Draw()
    
    #bump range
    ROOT.gPad.Update()
    lowy = ROOT.gPad.GetFrame().GetY1()
    highy = ROOT.gPad.GetFrame().GetY2()
    if bumpFound2:
        intervalLine.SetLineColor(ROOT.kGreen+1)
    else:
        intervalLine.SetLineColor(ROOT.kBlue)
    intervalLine.DrawLine(bumpLowEdge2,lowy,bumpLowEdge2,highy)
    intervalLine.DrawLine(bumpHighEdge2,lowy,bumpHighEdge2,highy)
    m.DrawLatex(textx,0.62,'logL = %.1f'%logLValue2)
    m.DrawLatex(textx,0.54,'#chi^{2}/ndf = %.3f'%chi2ndf2)
    m.DrawLatex(textx,0.46,'BH range [GeV] = %.0f - %.0f'%(bumpLowEdge2,bumpHighEdge2))
    m.DrawLatex(textx,0.38,'initial BH p-value = %s'%bumpHunterPValueInitial2)
    m.DrawLatex(textx,0.30,'final BH p-value = %s'%bumpHunterPValue2)
    
    pad3.Update()
    
    #------------------------------------------
    #legend
    pad1.cd()
    lXmin = textx#0.70
    lYmax = 0.80#0.40

    #ATLAS
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(20)
    a.DrawLatex(lXmin,lYmax,'ATLAS')#0.34
    
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(20)
    p.DrawLatex(lXmin+0.09,lYmax,'internal') #0.09, 0.34
    #p.DrawLatex(lXmin+0.09,lYmax+0.34,'simulation') #0.1
    #p.DrawLatex(lXmin,lYmax+0.22,'internal')

    #notes
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(43)
    n.SetTextColor(1)
    n.SetTextSize(15)
    
    n.DrawLatex(lXmin,lYmax-0.10,'#sqrt{s} = 13 TeV')
    n.DrawLatex(lXmin,lYmax-0.20,'L_{int} = %.1f fb^{-1}'%lumi)
    n.DrawLatex(lXmin,lYmax-0.30,'Wilks\' p-value = %.5f'%wilksPVal)

    #------------------------------------------
    c1.cd()
    c1.Update()
    #c1.WaitPrimitive()
    c1.SaveAs(stag+'.'+tag+'.pdf')

    #------------------------------------------
    return chi2ndf1, chi2ndf2, wilksPVal

#******************************************
if __name__ == "__main__":
    
    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 5:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
            \nplot search phase results comparison \
            \nHOW TO: python -u plotFitsComparison.py searchFileName1 searchFileName2 lumi tag'
            %(len(sys.argv), 5))
    
    #------------------------------------------
    #input parameters
    searchFileName1 = sys.argv[1].strip()
    searchFileName2 = sys.argv[2].strip()
    lumi = sys.argv[3].strip()
    tag = sys.argv[4].strip()

    output = plotFitsComparison(searchFileName1, searchFileName2, lumi, tag)
    
    #------------------------------------------
    print '\ndone %s'%list(output)

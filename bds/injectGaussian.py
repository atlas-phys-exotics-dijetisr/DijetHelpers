#!/usr/bin/env python

#******************************************
#import stuff
import ROOT, math, os, sys

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def injectGaussian(inputFileName, inputHistDir, inputHistName, mass, width, norm, lumi, tag, debug=True, batch=False):
    
    #------------------------------------------
    #input parameters
    #norm = int(norm) #TEST
    slumi = ('%.1f'%float(lumi.replace('p','.'))).replace('.','p')
    stag = mass+'.GeV.'+str(width).replace('.','p')+'.width.'+slumi+'.ifb'

    if type(debug) is not bool:
        if debug == 'True' or debug == 'true':
            debug = True
        else:
            debug = False
        
    if type(batch) is not bool:
        if batch == 'True' or batch == 'true':
            batch = True
        else:
            batch = False

    if debug and not batch:
        print '\n******************************************'
        print 'injecting Gaussian signal'
        print '\nparameters:'
        print '  file:          %s'%inputFileName
        print '  dir:           %s'%inputHistDir
        print '  hist:          %s'%inputHistName
        print '  mass:          %s'%mass
        print '  width:         %s'%width
        print '  signal events: %s'%norm
        print '  lumi [ifb]:    %s'%lumi
        print '  tag:           %s'%tag
        print '  string tag:    %s'%stag
        print '  debug:         %s'%debug

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    
    #------------------------------------------
    #check input file
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file')

    #open input file
    f = ROOT.TFile.Open(inputFileName,'READ')
    if not f:
        raise SystemExit('\n***ERROR*** couldn\'t open input file')

    #get input data-like QCD histogram
    if len(inputHistDir) > 0:
        qcd = f.GetDirectory(inputHistDir).Get(inputHistName)
    else:
        qcd = f.Get(inputHistName)
    if not qcd:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram')
    qcd.SetName('mjj.QCD')
    qcd.SetTitle('mjj.QCD')
    
    #------------------------------------------
    #get signal shape

    #OPTION 1: gaussian #DEFAULT
    fgauss = ROOT.TF1('gauss','gaus(0)',qcd.GetXaxis().GetXmin(),qcd.GetXaxis().GetXmax())#gaussian
    fgauss.SetParameters(norm, float(mass), float(width)*float(mass))#NOTE parameters are: norm, mean, width
    #print '\nnorm  = %.2e'%fgauss.GetParameter(0)
    #print 'mean  = %s'%fgauss.GetParameter(1)
    #print 'sigma = %s'%fgauss.GetParameter(2)

    #OPTION 2: gaussian with suppressed tails #TEST
    #fgauss = ROOT.TF1("gauss","[0] * exp( -0.5*((x-[1])/[2])**2 ) * exp( -3*TMath::Erf((x-[1])) )",qcd.GetXaxis().GetXmin(),qcd.GetXaxis().GetXmax())
    #fgauss = ROOT.TF1("gauss","[0] * exp(-0.5*((x-[1])/[2])**2) * exp(-2.*TMath::ATan(x-[1])/TMath::Pi())",qcd.GetXaxis().GetXmin(),qcd.GetXaxis().GetXmax())
    #fgauss = ROOT.TF1("gauss",
    #                    '[0] * exp(-0.5*((x-[1])/[2])**2) * ( ((0.5*abs(x-[1])/([1]-x))+0.5) * exp(-1*TMath::Erf((x-[1]))) ) + \
    #                     [0] * exp(-0.5*((x-[1])/[2])**2) * ( ((0.5*abs(x-[1])/(x-[1]))+0.5) * exp(-1*x) )',
    #                     qcd.GetXaxis().GetXmin(),qcd.GetXaxis().GetXmax())
    #fgauss = ROOT.TF1("gauss",
    #                    '[0] * exp(-0.5*((x-[1])/[2])**2) * ((0.5*abs(x-[1])/([1]-x))+0.5) + \
    #                     [0] * exp(-0.5*((x-[1])/[2])**2) * ( ((0.5*abs(x-[1])/(x-[1]))+0.5) * exp(-1*x) )',
    #                     qcd.GetXaxis().GetXmin(),qcd.GetXaxis().GetXmax())
    #fgauss.SetParameters(norm, float(mass), float(width)*float(mass))#NOTE parameters are: norm, mean, width
    
    #------------------------------------------
    #fill histogram
    hgauss = ROOT.TH1D('mjj.gauss', 'mjj.gauss', qcd.GetXaxis().GetNbins(), qcd.GetXaxis().GetXbins().GetArray())

    #OPTION 1
    #use FillRandom to ge the the signal histogram
    #NOTE this will give important poisson fluctuations at low normalizations, affecting the search phase and the BH-value
    #NOTE normalization is different from the smooth gaussian signal case
    #hgauss.FillRandom('gauss',int(norm))

    #OPTION 2
    #gaussian signal
    #NOTE normalization od the signal is different from the FillRandom case
    hgauss.SetDefaultSumw2(ROOT.kTRUE)

    #random number generator
    rand3 = ROOT.TRandom3(1986) #1986

    for ii in xrange(hgauss.GetNbinsX()):

        #------------------------------------------
        #OPTION 1: smooth
        #bincontent = int( round( fgauss.Eval( hgauss.GetBinCenter(ii) ) ) )
        #for jj in xrange(bincontent):
        #    hgauss.Fill(hgauss.GetBinCenter(ii))

        #------------------------------------------
        #OPTION 2: Poisson noise

        #pretend there are 100 times more eff. entries than necessary
        #gaussValue = int( round( fgauss.Eval( hgauss.GetBinCenter(ii) ) ) )
        #nee = int( round( 100. * gaussValue ) )
        gaussValue = fgauss.Eval( hgauss.GetBinCenter(ii) )
        nee = 100. * gaussValue

        #set seed
        #NOTE the seed for each bin must be always the same
        binSeed = int( round( hgauss.GetBinCenter(ii) ) )
        if binSeed < 0:
            binSeed = 0
        #print 'bin %s\t seed = %i'%(ii, binSeed)
        rand3.SetSeed(binSeed)

        #get data-like bin content by drawing entries
        #NOTE weights are poissonian by construction
        for jj in xrange( int( round( nee ) ) ):
            if rand3.Uniform() < (gaussValue / nee) :
                hgauss.Fill(hgauss.GetBinCenter(ii))
        #------------------------------------------

    #------------------------------------------
    #print peak significance
    peakBin = hgauss.FindBin( fgauss.GetParameter(1) )
    peakSignificance = hgauss.GetBinContent(peakBin) / math.sqrt(qcd.GetBinContent(peakBin))
    if debug and not batch:
        print '\ngauss hist entries = %.2e'%hgauss.GetEntries()
        print 'peak bin = %s'%peakBin
        print 'peak entries = %s'%hgauss.GetBinContent(peakBin)
        print 'peak error =   %s'%hgauss.GetBinError(peakBin)
    print '\npeak significance = %.2f'%peakSignificance

    
    #------------------------------------------
    #QCD + signal
    total = qcd.Clone()
    total.SetName('mjj.QCD_gauss')
    total.SetTitle('mjj.QCD_gauss')
    total.Add(hgauss)

    #------------------------------------------
    #remove any signal entry if there are no QCD entries (low mass region)
    nbins = qcd.GetNbinsX()
    if total is not None:
        for ii in xrange(nbins):
            if qcd.GetBinContent(ii) == 0:
                total.SetBinContent(ii,0)
            else:
                break

    #------------------------------------------
    #save QCD+signal histogram
    outputFileName = localdir+'/results/injectGaussian.mjj.'+stag+'.'+tag+'.root'
    outputFile = ROOT.TFile.Open(outputFileName, 'RECREATE')
    outputFile.cd()
    total.Write()
    qcd.Write()
    hgauss.Write()
    fgauss.Write()
    outputFile.Write()
    outputFile.Close()
    
    #------------------------------------------
    #plot
    if debug:
        c1 = ROOT.TCanvas('c1', 'c1', 1050, 50, 800, 600)
        c1.Clear()
        c1.SetLogy(1)
        c1.SetLogx(0)

        #------------------------------------------
        #TEST
        #fgauss.Draw('l')
        #c1.WaitPrimitive()
        #------------------------------------------

        #------------------------------------------
        #add q* to plot as a reference
        #get input data-like QCD histogram
        qstarFileName = '/atlas/data5/userdata/guescini/dijet/inputs/MC15_20150412/dataLikeHists/mc15_13TeV_Pythia8_v1/dataLikeHistograms.QStar%s.v1.root'%mass
        qstarFile = ROOT.TFile.Open(qstarFileName,'READ')
        if not qstarFile:
            raise SystemExit('\n***ERROR*** couldn\'t find input q* file: %s'%qstarFileName)
        qstarHistName = 'mjj_Scaled_QStar%s_%.0ffb'%(mass, float(lumi))
        qstar = qstarFile.GetDirectory('Nominal').Get(qstarHistName)#scaled
        #qstar = qstarFile.GetDirectory('Nominal').Get('mjj_DataLike_QStar3000_1fb')#data-like
        if not qstar:
            raise SystemExit('\n***ERROR*** couldn\'t find input q* histogram: %s'%qstarHistName)
        qstar.SetMarkerColor(ROOT.kOrange+7)
        qstar.SetMarkerStyle(27)
        qstar.SetLineColor(ROOT.kOrange+7)
        #------------------------------------------

        qcd.SetMarkerColor(ROOT.kAzure+1)
        qcd.SetMarkerStyle(25)
        qcd.SetLineColor(ROOT.kAzure+1)

        hgauss.SetMarkerColor(ROOT.kRed+1)
        hgauss.SetMarkerStyle(24)
        hgauss.SetLineColor(ROOT.kRed+1)

        total.SetMarkerColor(ROOT.kGreen+1)
        total.SetMarkerStyle(26)
        total.SetLineColor(ROOT.kGreen+1)

        hs = ROOT.THStack('hs','hs')
        hs.Add(qcd)
        hs.Add(hgauss)
        hs.Add(total)
        hs.Add(qstar)

        hs.Draw('nostack')
        hs.GetXaxis().SetTitle('m_{jj} [GeV]')
        hs.GetYaxis().SetTitle('events')

        #ATLAS
        aX = 0.65
        aY = 0.88
        a = ROOT.TLatex()
        a.SetNDC()
        a.SetTextFont(73)
        a.SetTextColor(1)
        a.SetTextSize(30)
        a.DrawLatex(aX,aY,'ATLAS')
		
        #internal
        p = ROOT.TLatex()
        p.SetNDC()
        p.SetTextFont(43)
        p.SetTextColor(1)
        p.SetTextSize(30)
        p.DrawLatex(aX+0.13,aY,'internal')
    
        #note
        n = ROOT.TLatex()
        n.SetNDC()
        n.SetTextFont(42)
        n.SetTextColor(1)
        n.SetTextSize(0.04)
        n.DrawLatex(aX,aY-0.08,'#intL dt = %s fb^{-1}'%(float(lumi.replace("p","."))))#lYmax+0.16
        n.DrawLatex(aX,aY-0.14,'sig. mass = %.0f GeV'%fgauss.GetParameter(1))#lYmax+0.10
        #n.DrawLatex(aX,aY-0.18,'sigma = %s GeV'%fgauss.GetParameter(2))#lYmax+0.06
        n.DrawLatex(aX,aY-0.18,'sig. width = %.0f%%'%(float(width)*100.))#lYmax+0.06
        n.DrawLatex(aX,aY-0.22,'sig. norm. = %s'%norm)#lYmax+0.02
        n.DrawLatex(aX,aY-0.26,'peak sign. = %.2f'%peakSignificance)#lYmax+0.02

        #legend
        lXmin = aX#0.65
        lXmax = aX+0.15#0.80
        lYmin = aY-0.28#0.52
        lYmax = aY-0.40#0.62
        l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
        l.SetFillStyle(0)
        l.SetFillColor(0)
        l.SetLineColor(0)
        l.SetTextFont(43)
        l.SetBorderSize(0)
        l.SetTextSize(15)
        l.AddEntry(qcd,"data-like QCD","pl")
        l.AddEntry(hgauss,'gaussian signal','pl')
        l.AddEntry(total,'total','pl')
        l.AddEntry(qstar,'q*','pl')
        l.Draw()
        
        c1.Update()
        if not batch:
            c1.WaitPrimitive()
        c1.SaveAs('figures/injectGaussian.mjj.'+stag+'.'+tag+'.pdf')
        #del c1

    #------------------------------------------
    return outputFileName, peakSignificance #peak significance


#******************************************
if __name__ == '__main__':

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 10:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters (%s/%s) \
            \nHOW TO: time python -u injectGaussian.py inputFileName inputHistName mass width norm lumi tag debug batch\n\
EXAMPLE: time python -u injectGaussian.py /atlas/data5/userdata/guescini/dijet/inputs/MC15_20150412/dataLikeHists/mc15_13TeV_Pythia8_v1/dataLikeHistograms.QCDDiJet.v1.root mjj_DataLike_QCDDiJet_1fb 4000 0.25 1000 1 test True False'\
            %(len(sys.argv),10))

    #------------------------------------------
    #get input parameters and run
    inputFileName = sys.argv[1].strip()
    inputHistName = sys.argv[2].strip()
    mass = sys.argv[3].strip()
    width = sys.argv[4].strip()
    #norm = int(sys.argv[5].strip())#TEST
    norm = float(sys.argv[5].strip())
    lumi = sys.argv[6].strip()
    tag = sys.argv[7].strip()
    debug = sys.argv[8].strip()
    batch = sys.argv[9].strip()

    out = injectGaussian(inputFileName, inputHistName, mass, width, norm, lumi, tag, debug, batch)
    
    #------------------------------------------
    print '\ndone %s'%list(out) #peak significance

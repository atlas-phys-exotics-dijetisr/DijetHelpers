#!/usr/bin/env python

#******************************************
#import stuff
import ROOT
import math, os, sys
import runSearchPhase, plotSearchPhase, plotFitsComparison

#******************************************
#set ATLAS style #MOVED inside the functions
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def compareFits(inputFileName, inputHistDir, inputHistName, lumi, nPar1, nPar2, tag):

    print '\n******************************************'
    print 'fit distributions and compare'

    #------------------------------------------
    #input parameters
    nPar1 = int(nPar1)
    nPar2 = int(nPar2)
    slumi = ('%.1f'%float(lumi.replace('p','.'))).replace('.','p')
    stag = slumi+'.ifb.'+str(nPar1)+'.par1.'+str(nPar2)+'.par2'
    
    print '\nparameters:'
    print '  input file name: %s'%inputFileName
    print '  input hist dir:  %s'%inputHistDir
    print '  input hist name: %s'%inputHistName
    print '  lumi [ifb]:      %s'%lumi
    print '  n par. ref. fit: %s'%nPar1
    print '  n par. alt. fit: %s'%nPar2
    print '  tag:             %s'%tag
    print '  string tag:      %s'%stag

    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get input histogram
    f = ROOT.TFile.Open(inputFileName,'READ')
    if not f:
        raise SystemExit('\n***ERROR*** couldn\'t open input file')
    if len(inputHistDir) > 0:
        h = f.GetDirectory(inputHistDir).Get(inputHistName)
    else:
        h = f.Get(inputHistName)
    if not h:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram')
    
    #------------------------------------------
    #run search phase using reference function
    print '\n******************************************'
    print 'running search phase using reference function'
    tag1 = slumi+'.ifb.'+str(nPar1)+'.par'
    searchPhaseOutputFile1 = 'results/searchPhase.'+tag1+'.'+tag+'.root'
    if os.path.isfile(searchPhaseOutputFile1):
        print '\n***WARNING*** search phase results using reference function exist already'
        searchPhaseOutputFileName1 = localdir+'/results/searchPhase.'+tag1+'.'+tag+'.root'
        #get logL
        if not os.path.isfile(searchPhaseOutputFileName1):
            raise SystemExit('\n***ERROR*** couldn\'t find SearchPhase output file')
        searchPhaseOutputFile1 = ROOT.TFile(searchPhaseOutputFileName1)
        logL1 = searchPhaseOutputFile1.Get('logLOfFitToData')
        logLValue1 = logL1[0]
    else:
        searchPhaseOutputFileName1, bumpHunterPValueInitial1, bumpHunterPValue1, bumpFound1, chi2ndf1, logLValue1 = runSearchPhase.runSearchPhase(inputFileName, inputHistDir, inputHistName, lumi, nPar1, tag1+'.'+tag)

    #plot search phase
    plotSearchPhase.plotSearchPhase(searchPhaseOutputFileName1, ['logL = %.1f'%logLValue1])
    
    #------------------------------------------
    #run search phase using alternative function
    print '\n******************************************'
    print 'running search phase using alternative function'
    tag2 = slumi+'.ifb.'+str(nPar2)+'.par'
    searchPhaseOutputFile2 = 'results/searchPhase.'+tag2+'.'+tag+'.root'
    if os.path.isfile(searchPhaseOutputFile2):
        print '\n***WARNING*** search phase results using alternative function exist already'
        searchPhaseOutputFileName2 = localdir+'/results/searchPhase.'+tag2+'.'+tag+'.root'
        #get logL
        if not os.path.isfile(searchPhaseOutputFileName2):
            raise SystemExit('\n***ERROR*** couldn\'t find SearchPhase output file')
        searchPhaseOutputFile2 = ROOT.TFile(searchPhaseOutputFileName2)
        logL2 = searchPhaseOutputFile2.Get('logLOfFitToData')
        logLValue2 = logL2[0]
    else:
        searchPhaseOutputFileName2, bumpHunterPValueInitial2, bumpHunterPValue2, bumpFound2, chi2ndf2, logLValue2 = runSearchPhase.runSearchPhase(inputFileName, inputHistDir, inputHistName, lumi, nPar2, tag2+'.'+tag)

    #plot search phase
    plotSearchPhase.plotSearchPhase(searchPhaseOutputFileName2, ['logL = %.1f'%logLValue2])

    #------------------------------------------
    #TEST
    #raise SystemExit('\n***EXIT*** exit')
    #------------------------------------------

    #------------------------------------------
    #plot comparison with Wilks p-value
    chi2ndf1, chi2ndf2, wilksPVal = plotFitsComparison.plotFitsComparison(searchPhaseOutputFileName1, searchPhaseOutputFileName2, lumi, tag)
    
    #------------------------------------------
    return chi2ndf1, chi2ndf2, wilksPVal


#******************************************
if __name__ == '__main__':

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 8:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters (%s/%s) \
            \nHOW TO: time python -u compareFits.py inputFileName inputHistDir inputHistName lumi nPar1 nPar2 tag'\
            %(len(sys.argv),8))

    #------------------------------------------
    #get input parameters and run
    inputFileName = sys.argv[1].strip()
    inputHistDir = sys.argv[2].strip()
    inputHistName = sys.argv[3].strip()
    lumi = sys.argv[4].strip()
    nPar1 = sys.argv[5].strip()
    nPar2 = sys.argv[6].strip()
    tag = sys.argv[7].strip()

    output = compareFits(inputFileName, inputHistDir, inputHistName, lumi, nPar1, nPar2, tag)
    
    #------------------------------------------
    print '\ndone: %s'%list(output)

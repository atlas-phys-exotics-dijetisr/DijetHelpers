#!/usr/bin/env python

#******************************************
#import stuff
import ROOT, math, os, sys
import plotUtils

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def injectQStar(inputQCDFileName, inputQCDHistDir, inputQCDHistName, mass, norm, lumi, tag, smooth=True, debug=True, batch=False):
    
    #------------------------------------------
    #input parameters
    slumi = ('%.1f'%float(lumi.replace('p','.'))).replace('.','p')
    stag = mass+'.GeV.'+slumi+'.ifb'

    if type(smooth) is not bool:
        if smooth == 'True' or smooth == 'true':
            smooth = True
        else:
            smooth = False

    if type(debug) is not bool:
        if debug == 'True' or debug == 'true':
            debug = True
        else:
            debug = False
        
    if type(batch) is not bool:
        if batch == 'True' or batch == 'true':
            batch = True
        else:
            batch = False

    inputQStarFileName = '/atlas/data5/userdata/guescini/dijet/inputs/MC15a_20150707/dataLikeHists_v1/dataLikeHistograms.QStar'+str(mass)+'.root'#DEFAULT
    #inputQStarFileName = "/atlas/data5/userdata/guescini/dijet/inputs/MC15a_20150623/dataLikeHistograms/dataLikeHists_v1/dataLikeHistograms.QStar"+str(mass)+".root"#OLD
    #inputQStarFileName = '/atlas/data5/userdata/guescini/dijet/inputs/MC15_20150412/dataLikeHists/mc15_13TeV_Pythia8_v1/dataLikeHistograms.QStar%s.v1.root'%mass#OLD

    #NOTE just one scaled file, no matter what lumi (use 1/fb for sake of simplicity)
    #inputQStarHistName = "mjj_Scaled_QStar"+mass+"_"+lumi.replace('.','p')+"fb"#DEFAULT
    #NOTE just one scaled file, no matter what lumi (use 1/fb for sake of simplicity)

    inputQStarHistDir = inputQCDHistDir #TEST
    
    if smooth:
        inputQStarHistName = 'mjj_Smooth_QStar'+mass+'_1fb'
    else:
        inputQStarHistName = 'mjj_Scaled_QStar'+mass+'_1fb'
        
    if debug and not batch:
        print '\n******************************************'
        print 'injecting q* signal'
        print '\nparameters:'
        print '  QCD file:       %s'%inputQCDFileName
        print '  QCD dir:        %s'%inputQCDHistDir
        print '  QCD hist:       %s'%inputQCDHistName
        print '  q* file:        %s'%inputQStarFileName
        print '  q* dir:         %s'%inputQStarHistDir
        print '  q* hist:        %s'%inputQStarHistName
        print '  mass:           %s'%mass
        print '  signal stenght: %s'%norm
        print '  lumi [ifb]:     %s'%lumi
        print '  tag:            %s'%tag        
        print '  string tag:     %s'%stag
        print '  smooth:         %s'%smooth
        print '  debug:          %s'%debug
        print '  batch:          %s'%batch

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    
    #------------------------------------------
    #check input files
    if not os.path.isfile(inputQCDFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find QCD input file')

    if not os.path.isfile(inputQStarFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find q* input file')

    #open input files
    fqcd = ROOT.TFile.Open(inputQCDFileName,'READ')
    if not fqcd:
        raise SystemExit('\n***ERROR*** couldn\'t open input QCD file')

    fqstar = ROOT.TFile.Open(inputQStarFileName,'READ')
    if not fqstar:
        raise SystemExit('\n***ERROR*** couldn\'t open input q* file')

    #get input data-like QCD histogram
    if len(inputQCDHistDir) > 0:    
        qcd = fqcd.GetDirectory(inputQCDHistDir).Get(inputQCDHistName)
    else:
        qcd = fqcd.Get(inputQCDHistName)
        
    if not qcd:
        raise SystemExit('\n***ERROR*** couldn\'t find input QCD histogram')
    qcd.SetName('mjj.QCD')
    qcd.SetTitle('mjj.QCD')

    #get input SCALED q* histogram
    if len(inputQStarHistDir) > 0:    
        qstar = fqstar.GetDirectory(inputQStarHistDir).Get(inputQStarHistName)
    else:
        qstar = fqstar.Get(inputQStarHistName)
        
    if not qstar:
        print ''
        keys = fqstar.GetDirectory('Nominal').GetListOfKeys()
        for key in keys:
            print '  %s'%key.GetName()
        raise SystemExit('\n***ERROR*** couldn\'t find input q* histogram: %s'%inputQStarHistName)
    qstar.SetName('mjj.QStar')
    qstar.SetTitle('mjj.QStar')

    #------------------------------------------
    #scale signal
    #NOTE remember I am using 1/fb input q* mjj, hence the lumi factor here
    scaleFactor = float(lumi) * float(norm)
    print '\nscaleFactor = %s'%scaleFactor
    qstar.Scale(scaleFactor)
    
    #get data-like signal
    if smooth:
        qstarDL = plotUtils.getSmoothHistogram(qstar, 'mjj.QStar.smooth')
    else:
        qstarDL = plotUtils.getDataLikeHist( plotUtils.getEffectiveEntriesHistogram(qstar, 'qstarEffEnt'), qstar, 'mjj.QStar.DL', 1986)
    
    #------------------------------------------
    #QCD + q*
    total = qcd.Clone()
    total.SetName('mjj.QCD_QStar')
    total.SetTitle('mjj.QCD_QStar')
    total.Add(qstarDL)

    #------------------------------------------
    #remove any signal entry if there are no QCD entries (low mass region)
    nbins = qcd.GetNbinsX()
    if total is not None:
        for ii in xrange(nbins):
            if qcd.GetBinContent(ii) == 0:
                total.SetBinContent(ii,0)
            else:
                break

    #------------------------------------------
    #save QCD+q* histogram
    outputFileName = localdir+'/results/injectQStar.'+stag+'.'+tag+'.root'
    outputFile = ROOT.TFile.Open(outputFileName, 'RECREATE')
    outputFile.cd()
    total.Write()
    qcd.Write()
    qstarDL.Write()
    outputFile.Write()
    #outputFile.Close()
    
    #------------------------------------------
    #plot
    if debug:
        c1 = ROOT.TCanvas('c1', 'c1', 1050, 50, 800, 600)
        c1.Clear()
        c1.SetLogy(1)
        c1.SetLogx(0)

        qcd.SetMarkerColor(ROOT.kAzure+1)
        qcd.SetMarkerStyle(25)
        qcd.SetLineColor(ROOT.kAzure+1)

        qstarDL.SetMarkerColor(ROOT.kRed+1)
        qstarDL.SetMarkerStyle(24)
        qstarDL.SetLineColor(ROOT.kRed+1)

        total.SetMarkerColor(ROOT.kGreen+1)
        total.SetMarkerStyle(26)
        total.SetLineColor(ROOT.kGreen+1)

        hs = ROOT.THStack('hs','hs')
        hs.Add(qcd)
        hs.Add(qstarDL)
        hs.Add(total)

        hs.Draw('nostack')
        hs.GetXaxis().SetTitle('m_{jj} [GeV]')
        hs.GetYaxis().SetTitle('events')

        #ATLAS
        aX = 0.65
        aY = 0.88
        a = ROOT.TLatex()
        a.SetNDC()
        a.SetTextFont(73)
        a.SetTextColor(1)
        a.SetTextSize(30)
        a.DrawLatex(aX,aY,'ATLAS')
		
        #internal
        p = ROOT.TLatex()
        p.SetNDC()
        p.SetTextFont(43)
        p.SetTextColor(1)
        p.SetTextSize(30)
        p.DrawLatex(aX+0.13,aY,'internal')
    
        #note
        n = ROOT.TLatex()
        n.SetNDC()
        n.SetTextFont(42)
        n.SetTextColor(1)
        n.SetTextSize(0.04)
        n.DrawLatex(aX,aY-0.08,'#intL dt = %s fb^{-1}'%(float(lumi.replace("p","."))))#lYmax+0.16
        n.DrawLatex(aX,aY-0.14,'m_{q*} = %.0f GeV'%float(mass))#lYmax+0.10
        n.DrawLatex(aX,aY-0.22,'sig. strength = %.3f'%float(norm))#lYmax+0.02

        #legend
        lXmin = aX#0.65
        lXmax = aX+0.15#0.80
        lYmin = aY-0.28#0.52
        lYmax = aY-0.40#0.62
        l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
        l.SetFillStyle(0)
        l.SetFillColor(0)
        l.SetLineColor(0)
        l.SetTextFont(43)
        l.SetBorderSize(0)
        l.SetTextSize(15)
        if smooth:
            l.AddEntry(qcd,"smooth QCD","pl")
            l.AddEntry(qstarDL,'smooth q*','pl')
        else:
            l.AddEntry(qcd,"data-like QCD","pl")
            l.AddEntry(qstarDL,'data-like q*','pl')
        l.AddEntry(total,'total','pl')
        l.Draw()
        
        c1.Update()
        if not batch:
            c1.WaitPrimitive()
        c1.SaveAs('figures/injectQStar.'+stag+'.'+tag+'.pdf')
        #del c1

    #------------------------------------------
    return outputFileName


#******************************************
if __name__ == '__main__':

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 10:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters (%s/%s) \
            \nHOW TO: time python -u injectQStar.py inputFileName inputHistName mass norm lumi tag debug batch\n\
EXAMPLE: time python -u injectQStar.py /atlas/data5/userdata/guescini/dijet/inputs/MC15_20150412/dataLikeHists/mc15_13TeV_Pythia8_v1/dataLikeHistograms.QCDDiJet.v1.root mjj_DataLike_QCDDiJet_1fb 4000 2000 1 test True True False'\
            %(len(sys.argv),10))

    #------------------------------------------
    #get input parameters and run
    inputFileName = sys.argv[1].strip()
    inputHistName = sys.argv[2].strip()
    mass = sys.argv[3].strip()
    norm = int(sys.argv[4].strip())
    lumi = sys.argv[5].strip()
    tag = sys.argv[6].strip()
    smooth = sys.argv[7].strip()
    debug = sys.argv[8].strip()
    batch = sys.argv[9].strip()

    out = injectQStar(inputFileName, inputHistName, mass, norm, lumi, tag, smooth, debug, batch)
    
    #------------------------------------------
    print '\ndone %s'%out#list(out)

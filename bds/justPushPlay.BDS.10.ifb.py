#!/usr/bin/env python

#******************************************
#import stuff
from findBarelyDetectableSignal import findBarelyDetectableSignal

#******************************************
if __name__ == '__main__':

    print '\n******************************************'
    print 'looping over signal mass values and widths'

    tag = 'smooth'#poisson, smooth
    
    #combine input parameters
    smooth = [True]#,False]
    luminosity = ['10']#/fb #['1', '10']
    masses = ['3000', '4000', '5000']
    widths = ['0.10']#['0.15', '0.20', '0.30']#['0.10', '0.15', '0.20', '0.30', '0.40']
    nParameters = ['3', '4', '5']
    combinations = [(s, l, m, w, np) for s in smooth for l in luminosity for m in masses for w in widths for np in nParameters]
    #combinations = [(True, 1, 3000, 0.15, 4)]#TEST
    
    #loop over combinations
    for combination in combinations:
        
        print '\n******************************************'
        #print '  %s %s %s %s %s'%(combination[0], combination[1], combination[2], combination[3], combination[4])
        print 'smooth:       %s'%combination[0]
        print 'luminosity:   %s'%combination[1]
        print 'signal mass:  %s'%combination[2]
        print 'signal width: %s'%combination[3]
        print 'n parameters: %s'%combination[4]
        print 'tag:          %s'%tag        
        out = findBarelyDetectableSignal( str(combination[2]),
                                          str(combination[3]),
                                          str(combination[1]),
                                          str(combination[4]),
                                          tag,
                                          combination[0],
                                          True)
        print '\ndone %s'%(list(out)) #n steps, n signal events, BH p-value
        
    #------------------------------------------
    print '\n******************************************'
    print '\ndone looping over combinations'

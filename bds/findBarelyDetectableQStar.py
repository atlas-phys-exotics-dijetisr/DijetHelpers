#!/usr/bin/env python

#******************************************
#import stuff
import ROOT
import math, os, sys
import injectQStar
import plotSearchPhase, plotFuncRatio

#******************************************
#set ATLAS style #MOVED inside the functions
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()


#******************************************
def findBarelyDetectableQStar(mass, lumi, nPar, tag, smooth, debug):

    print '\n******************************************'
    print 'finding barely detectable q*'

    #------------------------------------------
    #input parameters
    nPar= int(nPar)
    slumi = ('%.1f'%float(lumi.replace('p','.'))).replace('.','p')
    stag = mass+'.GeV.'+slumi+'.ifb.'+str(nPar)+'.par'

    if type(smooth) is not bool:
        if smooth == 'True' or smooth == 'true':
            smooth = True
        else:
            smooth = False

    if type(debug) is not bool:
        if debug == 'True' or debug == 'true':
            debug = True
        else:
            debug = False
        
    if debug:
        print '\nparameters:'
        print '  mass:          %s'%mass
        print '  lumi [ifb]:    %s'%lumi
        print '  n parameters:  %s'%nPar
        print '  tag:           %s'%tag
        print '  string tag:    %s'%stag
        print '  smooth:        %s'%smooth

    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get input QCD histogram
    #NOTE this file and histogarms are created using the makeInputDijetMassDistributionFromFit.py script
    if smooth:
        inputFileName = '/atlas/users/guescini/bakerstreet/DijetHelpers/bds/results/inputMjjFit.MC15a.20150623.'+slumi+'.ifb.'+str(nPar)+'.par.root'#smooth
    else:
        inputFileName = '/atlas/users/guescini/bakerstreet/DijetHelpers/bds/results/inputMjjDL.MC15a.20150623.'+slumi+'.ifb.'+str(nPar)+'.par.root'#poisson

    inputHistName = 'mjj'

    if debug:
        print '\n  input file name: %s'%inputFileName
        print '  input hist name: %s'%inputHistName

    #get QCD histogram
    f = ROOT.TFile.Open(inputFileName,'READ')
    if not f:
        raise SystemExit('\n***ERROR*** couldn\'t open input file')
    #qcd = f.GetDirectory('Nominal').Get(inputHistName)
    qcd = f.Get(inputHistName)
    if not qcd:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram')
    
    #------------------------------------------
    #run search phase on QCD background only
    print '\n******************************************'
    print 'running search phase on QCD background only'
    #QCDFileName = inputFileName #CHCECK do I need this? apparently no...
    QCDtag = slumi+'.ifb.'+str(nPar)+'.par'
    searchPhaseQCDOutputFile = 'results/searchPhase.'+QCDtag+'.QCD.'+tag+'.root'
    if os.path.isfile(searchPhaseQCDOutputFile):
        print '\n***WARNING*** QCD search phase results exist already'
    else:    
        runSearchPhase(nPar, lumi, QCDtag, 'QCD.'+tag, inputFileName, inputHistName)

    #------------------------------------------
    #TEST
    #raise SystemExit('\n***EXIT*** exit')
    #------------------------------------------
        
    #------------------------------------------
    #starting signal strength
    #NOTE start from half of the nominal cross section (good for 3 TeV q*)
    norm = 0.5
    
    #------------------------------------------
    #LOOP 1
    #loop until: 0.010 < BH p-value < 0.015
    BHpvalHighMin = 0.010 #0.010
    BHpvalHighMax = 0.015 #0.015
    print '\n\n******************************************'
    print 'tuning q* injection INCLUDING signal range: %.3f < BH p-value < %.3f'%(BHpvalHighMin, BHpvalHighMax)
    print '******************************************'
    normHigh, bumpHunterPValueInitialHigh, bumpFoundHigh = tuneQStarInjection(inputFileName, inputHistName,
                                                                    mass, norm, lumi, nPar, stag, 'includeSig.'+tag,
                                                                    BHpvalHighMin, BHpvalHighMax, 100,
                                                                    1.10, 0.96, #1.2, 0.9
                                                                    False)
    
    if bumpHunterPValueInitialHigh < BHpvalHighMin or bumpHunterPValueInitialHigh > BHpvalHighMax:
        print '\n\n***WARNING*** q* injection tuning did NOT work'
        print '***WARNING*** initial BH p-value = %.4f'%bumpHunterPValueInitialHigh
        print '***WARNING*** BH range = %.4f - %.4f'%(BHpvalHighMin, BHpvalHighMax)
        print '  mass:          %s'%mass
        print '  lumi [ifb]:    %s'%lumi
        print '  n parameters:  %s'%nPar
        print '  tag:           %s'%tag
        print '  string tag:    %s'%stag
        print '  smooth:        %s'%smooth
        print '  norm:          %s'%normHigh
        print '  bump found:    %s'%bumpFoundHigh
        
        #------------------------------------------
        #TEST
        #raise SystemExit('\n***EXIT*** exit')
        #------------------------------------------

    #------------------------------------------
    #LOOP 2
    #loop until: 0.005 < BH p-value < 0.010
    BHpvalLowMin = 0.005 #0.005
    BHpvalLowMax = BHpvalHighMin #0.010
    norm = normHigh
    print '\n\n******************************************'
    print 'tuning q* injection EXCLUDING signal range: %.3f < BH p-value < %.3f'%(BHpvalLowMin, BHpvalLowMax)
    print '******************************************'
    normLow, bumpHunterPValueInitialLow, bumpFoundLow = tuneQStarInjection(inputFileName, inputHistName,
                                                                    mass, norm, lumi, nPar, stag, 'excludeSig.'+tag,
                                                                    BHpvalLowMin, BHpvalLowMax, 100,
                                                                    1.02, 0.99, #1.05, 0.98
                                                                    True)
    
    if bumpHunterPValueInitialLow < BHpvalLowMin or bumpHunterPValueInitialLow > BHpvalLowMax:
        print '\n\n***WARNING*** q* injection tuning did NOT work'
        print '***WARNING*** initial BH p-value = %.4f'%bumpHunterPValueInitialLow
        print '***WARNING*** BH range = %.4f - %.4f'%(BHpvalLowMin, BHpvalLowMax)
        print '  mass:          %s'%mass
        print '  lumi [ifb]:    %s'%lumi
        print '  n parameters:  %s'%nPar
        print '  tag:           %s'%tag
        print '  string tag:    %s'%stag
        print '  smooth:        %s'%smooth
        print '  norm:          %s'%normLow
        print '  bump found:    %s'%bumpFoundLow
        
        #------------------------------------------
        #TEST
        #raise SystemExit('\n***EXIT*** exit')
        #------------------------------------------

    #------------------------------------------
    #plot results
    
    #plot SearchPhase results - signal included
    if (bumpHunterPValueInitialHigh > BHpvalHighMin or bumpHunterPValueInitialHigh < BHpvalHighMax):
        searchPhaseSignalIncludedOutputFile = 'results/searchPhase.'+stag+'.includeSig.'+tag+'.root'
        plotSearchPhase.plotQStarSearchPhase(searchPhaseSignalIncludedOutputFile, normHigh)#NEW

    #plot SearchPhase results - signal excluded
    if (bumpHunterPValueInitialLow > BHpvalLowMin  or bumpHunterPValueInitialLow < BHpvalLowMax):
        searchPhaseSignalExcludedOutputFile = 'results/searchPhase.'+stag+'.excludeSig.'+tag+'.root'
        plotSearchPhase.plotQStarSearchPhase(searchPhaseSignalExcludedOutputFile, normLow)#NEW
        
    #plot functions ratios
    if (bumpHunterPValueInitialHigh > BHpvalHighMin or bumpHunterPValueInitialHigh < BHpvalHighMax) and (bumpHunterPValueInitialLow  > BHpvalLowMin  or bumpHunterPValueInitialLow  < BHpvalLowMax):
        plotFuncRatio.plotDoubleFuncRatio(searchPhaseQCDOutputFile, searchPhaseSignalIncludedOutputFile, searchPhaseSignalExcludedOutputFile, 'QStar', mass, 0., normHigh, normLow, lumi, nPar, tag)

    #------------------------------------------
    return (bumpHunterPValueInitialHigh, bumpHunterPValueInitialLow) #initial BH p-value signal included, initial BH p-value signal excluded
    

#******************************************
def runSearchPhase(nPar, lumi, stag, tag, inputFileName, histName):

    #------------------------------------------
    #prepare config file
    localdir = os.path.dirname(os.path.realpath(__file__))
    configFileName    = localdir+'/data/searchPhase.config'
    newConfigFileName = localdir+'/configs/searchPhase.'+stag+'.'+tag+'.config'
    #print configFileName
    #print newConfigFileName
    searchPhaseOutputFileName = localdir+'/results/searchPhase.'+stag+'.'+tag+'.root'

    #starting parameters
    #3 par, 1/fb:   0.180377, 8.1554,  -5.25718
    #4 par, 1/fb:   4.28171, 10.814,   -2.72612,  0.577889
    #4 par, 30/fb: 59.28,    12.85,    -0.5871,   1.069
    #5 par, 1/fb: 0.426909,   9.45062, -5.49925, -0.759262, -0.240173

    if nPar == 3:
        pars = [0.180377, 8.1554, -5.25718]
    elif nPar == 5:
        pars = [0.426909, 9.45062, -5.49925, -0.759262, -0.240173]
    elif nPar == 6:
        pars = [0.756961, 21.0028, -5.18678, -1.25474, -0.326143, 19998.6]
    else:
        nPar = 4 #DEFAULT
        pars = [4.28171, 10.814, -2.72612, 0.577889]
        if float(lumi.replace('p','.')) > 10.0:
            pars = [59.28,    12.85,  -0.5871,  1.069]

    with open(configFileName,'r') as configFile:
        with open(newConfigFileName,'w') as newConfigFile:
            for line in configFile:
                newLine = line
                newLine = newLine.replace('dummyInputFileName', inputFileName)
                newLine = newLine.replace('dummyHistName', histName)
                newLine = newLine.replace('dummyOutputFileName', searchPhaseOutputFileName)

                #3 parameters
                if nPar == 3:
                    newLine = newLine.replace('dummyFuncCode', str(9))
                    newLine = newLine.replace('dummyNPar', str(3))
                    newLine = newLine.replace('dummyP1', str(pars[0]))
                    newLine = newLine.replace('dummyP2', str(pars[1]))
                    newLine = newLine.replace('dummyP3', str(pars[2]))

                #4 parameters
                elif nPar == 4:
                    newLine = newLine.replace('dummyFuncCode', str(4))
                    newLine = newLine.replace('dummyNPar', str(4))
                    newLine = newLine.replace('dummyP1', str(pars[0]))
                    newLine = newLine.replace('dummyP2', str(pars[1]))
                    newLine = newLine.replace('dummyP3', str(pars[2]))
                    newLine = newLine.replace('#parameter4', 'parameter4\t\t'+str(pars[3]))

                #5 parameters
                elif nPar == 5:
                    newLine = newLine.replace('dummyFuncCode', str(7))
                    newLine = newLine.replace('dummyNPar', str(5))
                    newLine = newLine.replace('dummyP1', str(pars[0]))
                    newLine = newLine.replace('dummyP2', str(pars[1]))
                    newLine = newLine.replace('dummyP3', str(pars[2]))
                    newLine = newLine.replace('#parameter4', 'parameter4\t\t'+str(pars[3]))
                    newLine = newLine.replace('#parameter5', 'parameter5\t\t'+str(pars[4]))

                #6 parameters
                elif nPar == 6:
                    newLine = newLine.replace('dummyFuncCode', str(8))
                    newLine = newLine.replace('dummyNPar', str(6))
                    newLine = newLine.replace('dummyP1', str(pars[0]))
                    newLine = newLine.replace('dummyP2', str(pars[1]))
                    newLine = newLine.replace('dummyP3', str(pars[2]))
                    newLine = newLine.replace('#parameter4', 'parameter4\t\t'+str(pars[3]))
                    newLine = newLine.replace('#parameter5', 'parameter5\t\t'+str(1.000))
                    newLine = newLine.replace('#parameter6', 'parameter6\t\t'+str(1.000))

                newConfigFile.write(newLine)

    #------------------------------------------
    #search phase
    print '\n******************************************'
    print 'runnning SearchPhase'
    os.system('SearchPhase --config %s --noDE'%newConfigFileName)
    
    #------------------------------------------
    #get BH p-value
    #open SearchPhase results file
    if not os.path.isfile(searchPhaseOutputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find SearchPhase output file')
    searchPhaseOutputFile = ROOT.TFile(searchPhaseOutputFileName)

    #get SearchPhase results
    #histograms
    basicData                  = searchPhaseOutputFile.Get("basicData")
    basicBackground            = searchPhaseOutputFile.Get("basicBkgFrom4ParamFit")
    residualHist               = searchPhaseOutputFile.Get("residualHist")
    #bumpHunterStatHistNullCase = searchPhaseOutputFile.Get("bumpHunterStatHistNullCase")
    #print 'basicBackground entries = %s'%basicBackground.GetEntries()

    #initial BH p-value
    bumpHunterStatOfFitToDataInitial = searchPhaseOutputFile.Get("bumpHunterStatOfFitToDataInitial")#NEW
    #bumpHunterStatValueInitial = bumpHunterStatOfFitToDataInitial[0]
    bumpHunterPValueInitial    = bumpHunterStatOfFitToDataInitial[1]
    bumpHunterPValueErrInitial = bumpHunterStatOfFitToDataInitial[2]

    #vector
    bumpHunterStatOfFitToData = searchPhaseOutputFile.Get("bumpHunterStatOfFitToData")
    if not bumpHunterStatOfFitToData:
        raise SystemExit('\n***ERROR*** couldn\'t find bumpHunterStatOfFitToData vector')
    
    bumpHunterStatValue = bumpHunterStatOfFitToData[0]
    bumpHunterPValue    = bumpHunterStatOfFitToData[1]
    bumpHunterPValueErr = bumpHunterStatOfFitToData[2]

    #vector
    bumpHunterPLowHigh = searchPhaseOutputFile.Get('bumpHunterPLowHigh')
    #bumpHunterStatValue = bumpHunterPLowHigh[0]
    bumpLowEdge         = bumpHunterPLowHigh[1]
    bumpHighEdge        = bumpHunterPLowHigh[2]

    #vector
    bumpFoundVector = searchPhaseOutputFile.Get("bumpFound")
    bumpFound = bool(bumpFoundVector[0])

    #print
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "initial BumpHunter p-value = %s +/- %s"%(bumpHunterPValueInitial, bumpHunterPValueErrInitial)
    print "final BumpHunter p-value =   %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)
    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas
    print 'bump found? %s'%bumpFound
    
    #------------------------------------------
    return bumpHunterPValueInitial, bumpHunterPValue, bumpFound


#******************************************
def tuneQStarInjection(inputFileName, inputHistName,
                        mass, norm, lumi, nPar, stag, tag,
                        BHpvalMin=0.010, BHpvalMax=0.15, countMax=50,
                        increaseStep=1.6, decreaseStep=0.6, #1.75, 0.8 #1.5, 0.8 #1.6, 0.8
                        findBump=True):

    #------------------------------------------
    bumpHunterPValueInitial = 999.
    count = int(0)
    bumpFound = False
    
    #lists
    lBumpHunterPValue = []
    lNorm = []
    
    while (bumpHunterPValueInitial < BHpvalMin or bumpHunterPValueInitial > BHpvalMax) and count < countMax:

        print '\n\n******************************************'
        #print 'trial %s - signal strength = %s'%(count, norm)
        print 'trial %s'%count
        print 'q* mass = %s'%mass
        print 'q* signal strength = %s'%norm
        if findBump:
            print 'excluding signal region'
        else:
            print 'including signal region'
        print '******************************************'

        #------------------------------------------
        #inject q*
        injectedQStarFileName = injectQStar.injectQStar(inputFileName, inputHistName, mass, norm, lumi, str(nPar)+'.par.'+tag, False, False, True)#do NOT plot each step
        #injectedQStarFileName = injectQStar.injectQStar(inputFileName, inputHistName, mass, norm, lumi, str(nPar)+'.par.'+tag, False, True, True)#plot injected spectrum before moving on
        
        #------------------------------------------
        #run search phase
        bumpHunterPValueInitial, bumpHunterPValueFinal, bumpFound = runSearchPhase(nPar, lumi, stag, tag, injectedQStarFileName, 'mjj.QCD_QStar')
        print 'q* signal strength = %s'%norm

        #------------------------------------------
        #tune signal strength
        #NOTE based on ***INITIAL*** BH p-value
        lBumpHunterPValue.append(bumpHunterPValueInitial)
        lNorm.append(norm)
        oldNorm = norm

        print '\n------------------------------------------'
        print 'initial BH p-values         = %s'%lBumpHunterPValue
        print 'q* signal strength = %s'%lNorm
        print 'loops = %s'%len(lBumpHunterPValue)
        print '------------------------------------------\n'

        if bumpHunterPValueInitial > BHpvalMax:
            norm = norm*increaseStep

        elif bumpHunterPValueInitial < BHpvalMin:
            norm = norm*decreaseStep

        elif (findBump and bumpFound) or (not findBump and not bumpFound):
            injectedQStarFileName = injectQStar.injectQStar(inputFileName, inputHistName, mass, norm, lumi, str(nPar)+'.par.'+tag, False, True, True)#OK, now plot
            break

        count+=1

    #------------------------------------------
    #check if injection has converged to the desired BH p-value range
    if count >= countMax and (bumpHunterPValueInitial < BHpvalMin or bumpHunterPValueInitial > BHpvalMax):
        print '\n***WARNING*** reached maximum number of trials (%s)'%countMax

        #find best BH p-value
        lowDelta = 999.
        highDelta = 999.
        bestDelta = 999.
        bestii = -1
        for ii, BHpval in enumerate(lBumpHunterPValue):
            lowDelta = math.fabs(BHpval - BHpvalMin)
            highgDelta = math.fabs(BHpval - BHpvalMax)
            if lowDelta < highDelta and lowDelta < bestDelta:
                bestDelta = lowDelta
                bestii = ii
            if highDelta < lowDelta and highDelta < bestDelta:
                bestDelta = highDelta
                bestii = ii

        print '\nbest signal strength = %s'%lNorm[bestii]
        print 'BH p-vaue = %s'%lBumpHunterPValue[bestii]
        #return (lNorm[bestii], lBumpHunterPValue[bestii]) #signal peak strength, BH p-value
        norm = lNorm[bestii]
        bumpHunterPValueInitial = lBumpHunterPValue[bestii]
        
        #------------------------------------------
        #re-run with best parameters found
        #print '\n******************************************'
        #print 're-runnning with best parameters'
        #injectedQStarianFileName,  peakSignificance = injectQStar.injectQStar(inputFileName, inputHistName, mass, lNorm[bestii], lumi, str(nPar)+'.par.'+tag, False, False, True)#do NOT plot each step
        ##injectedQStarianFileName,  peakSignificance = injectQStar.injectQStar(inputFileName, inputHistName, mass, lNorm[bestii], lumi, str(nPar)+'.par.'+tag, False, True, False)#plot injected spectrum before moving on
        #bumpHunterPValue = runSearchPhase(nPar, lumi, stag, tag, injectedQStarianFileName, 'mjj.QCD_QStar')
        #print '\ndone re-running'
        #------------------------------------------

    return norm, bumpHunterPValueInitial, bumpFound


#******************************************
if __name__ == '__main__':

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 7:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters (%s/%s) \
            \nHOW TO: time python -u findBarelyDetectableQStar.py mass lumi nPar tag smooth debug\n'\
            %(len(sys.argv),7))

    #------------------------------------------
    #get input parameters and run
    mass = sys.argv[1].strip()
    lumi = sys.argv[2].strip()
    nPar = sys.argv[3].strip()
    tag = sys.argv[4].strip()
    smooth = sys.argv[5].strip()
    debug = sys.argv[6].strip()

    out = findBarelyDetectableQStar(mass, lumi, nPar, tag, smooth, debug)
    
    #------------------------------------------
    print '\n******************************************'
    print '\ndone %s'%(list(out)) #n steps, n signal events, BH p-value

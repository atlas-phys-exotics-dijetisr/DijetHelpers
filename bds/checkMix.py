#!/usr/bin/env python

#******************************************
#import stuff
import ROOT, math, os, sys
import plotUtils
import plotSearchPhase

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def checkMix(inputFileName, inputHistDir, lumi, nPar, pgg, pgq, pqg, pqq, tag):
    
    #------------------------------------------
    #input parameters
    print '\n******************************************'
    print 'checking JES mjj distribution'
    print '  input file:     %s'%inputFileName
    print '  input hist dir: %s'%inputHistDir
    print '  lumi:           %s'%lumi
    print '  nPar:           %s'%nPar
    print '  gg %%:           %s'%pgg
    print '  gq %%:           %s'%pgq
    print '  qg %%:           %s'%pqg
    print '  qq %%:           %s'%pqq
    print '  tag:            %s'%tag

    #input parameters
    slumi = ('%.1f'%float(lumi.replace('p','.'))).replace('.','p')
    stag = slumi+'.ifb.'+nPar+'.par.'+pgg.replace('.','p')+'.pgg.'+pgq.replace('.','p')+'.pgq.'+pqg.replace('.','p')+'.pqg.'+pqq.replace('.','p')+'.pqq'

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    #check input files
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file')

    #open input file
    f = ROOT.TFile.Open(inputFileName,'READ')
    if not f:
        raise SystemExit('\n***ERROR*** couldn\'t open input file')

    #get histograms
    #mgg
    mggHistName = 'mgg_Scaled_QCDDiJet_'+str(lumi)+'fb'
    mgg = f.GetDirectory(inputHistDir).Get(mggHistName)
    if not mgg:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram: %s'%mggHistName)

    #mgq
    mgqHistName = 'mgq_Scaled_QCDDiJet_'+str(lumi)+'fb'
    mgq = f.GetDirectory(inputHistDir).Get(mgqHistName)
    if not mgq:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram: %s'%mgqHistName)

    #mqg
    mqgHistName = 'mqg_Scaled_QCDDiJet_'+str(lumi)+'fb'
    mqg = f.GetDirectory(inputHistDir).Get(mqgHistName)
    if not mqg:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram: %s'%mqgHistName)

    #mqq
    mqqHistName = 'mqq_Scaled_QCDDiJet_'+str(lumi)+'fb'
    mqq = f.GetDirectory(inputHistDir).Get(mqqHistName)
    if not mqq:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram: %s'%mqqHistName)

    #mjj
    mjjHistName = 'mjj_Scaled_QCDDiJet_'+str(lumi)+'fb'
    mjj = f.GetDirectory(inputHistDir).Get(mjjHistName)
    if not mjj:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram: %s'%mjjHistName)
    
    #------------------------------------------
    #TEST
    #raise SystemExit('\n***EXIT*** exit')
    #------------------------------------------

    #------------------------------------------
    #scale and mix
    mgg.Scale(float(pgg))
    mgq.Scale(float(pgq))
    mqg.Scale(float(pqg))
    mqq.Scale(float(pqq))

    tot = mgg.Clone()
    tot.Add(mgq)
    tot.Add(mqg)
    tot.Add(mqq)
    tot.SetName('mjj_mixed')    
    tot.SetTitle('mjj_mixed')

    #------------------------------------------
    #get data-like histogram
    totDL = plotUtils.getDataLikeHist( plotUtils.getEffectiveEntriesHistogram(tot, 'totEffEnt'), tot, 'mjj_mixed_DL', 1986)
    
    #------------------------------------------
    #save
    outputFileName = localdir+'/results/mixedmjj.'+stag+'.'+tag+'.root'
    outputFile = ROOT.TFile.Open(outputFileName, 'RECREATE')
    outputFile.cd()
    mjj.Write()
    tot.Write()
    totDL.Write()
    outputFile.Write()

    #------------------------------------------
    #plot
    if True:
        c1 = ROOT.TCanvas('c1', 'c1', 1050, 50, 800, 600)
        c1.Clear()
        c1.SetLogy(1)
        c1.SetLogx(0)

        mjj.SetMarkerColor(ROOT.kBlack)
        mjj.SetMarkerStyle(20)
        mjj.SetLineColor(ROOT.kBlack)

        mgg.SetMarkerColor(ROOT.kAzure+1)
        mgg.SetMarkerStyle(25)
        mgg.SetLineColor(ROOT.kAzure+1)

        mgq.SetMarkerColor(ROOT.kRed+1)
        mgq.SetMarkerStyle(26)
        mgq.SetLineColor(ROOT.kRed+1)

        mqg.SetMarkerColor(ROOT.kGreen+1)
        mqg.SetMarkerStyle(27)
        mqg.SetLineColor(ROOT.kGreen+1)

        mqq.SetMarkerColor(ROOT.kMagenta+1)
        mqq.SetMarkerStyle(30)
        mqq.SetLineColor(ROOT.kMagenta+1)

        tot.SetMarkerColor(ROOT.kOrange+1)
        tot.SetMarkerStyle(24)
        tot.SetLineColor(ROOT.kOrange+1)

        hs = ROOT.THStack('hs','hs')
        hs.Add(mjj)
        hs.Add(mgg)
        hs.Add(mgq)
        hs.Add(mqg)
        hs.Add(mqq)
        hs.Add(tot)

        hs.Draw('nostack')
        hs.GetXaxis().SetTitle('m_{12} [GeV]')
        hs.GetYaxis().SetTitle('events')

        #ATLAS
        aX = 0.65
        aY = 0.88
        a = ROOT.TLatex()
        a.SetNDC()
        a.SetTextFont(73)
        a.SetTextColor(1)
        a.SetTextSize(30)
        a.DrawLatex(aX,aY,'ATLAS')
		
        #internal
        p = ROOT.TLatex()
        p.SetNDC()
        p.SetTextFont(43)
        p.SetTextColor(1)
        p.SetTextSize(30)
        p.DrawLatex(aX+0.13,aY,'internal')
    
        #note
        n = ROOT.TLatex()
        n.SetNDC()
        n.SetTextFont(42)
        n.SetTextColor(1)
        n.SetTextSize(0.04)
        n.DrawLatex(aX,aY-0.08,'#intL dt = %s fb^{-1}'%(float(lumi.replace("p","."))))
        #n.DrawLatex(aX,aY-0.14,'')
        #n.DrawLatex(aX,aY-0.22,'')

        #legend
        lXmin = aX
        lXmax = aX+0.15
        lYmin = aY-0.34#-0.28
        lYmax = aY-0.14#-0.40
        l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
        l.SetFillStyle(0)
        l.SetFillColor(0)
        l.SetLineColor(0)
        l.SetTextFont(43)
        l.SetBorderSize(0)
        l.SetTextSize(15)
        l.AddEntry(mjj,'jj',"pl")
        l.AddEntry(mgg,'gg * %s'%pgg,"pl")
        l.AddEntry(mgq,'gq * %s'%pgq,"pl")
        l.AddEntry(mqg,'qg * %s'%pqg,"pl")
        l.AddEntry(mqq,'qq * %s'%pqq,"pl")
        l.AddEntry(tot,'tot',"pl")
        l.Draw()
        
        c1.Update()
        #c1.WaitPrimitive()
        c1.SaveAs('figures/mixedmjj.'+stag+'.'+tag+'.pdf')
        #del c1

    #------------------------------------------
    #run search phase
    bumpHunterPValueInitial, bumpHunterPValueFinal, bumpFound = runSearchPhase(int(nPar),
                                                                               lumi,
                                                                               stag,
                                                                               tag,
                                                                               outputFileName,
                                                                               '',
                                                                               'mjj_mixed_DL')
    searchPhaseOutputFile = 'results/searchPhase.'+stag+'.'+tag+'.root'

    notes = []
    notes.append('gg * %s'%pgg)
    notes.append('gq * %s'%pgq)
    notes.append('qg * %s'%pqg)
    notes.append('qq * %s'%pqq)
    plotSearchPhase.plotSearchPhase(searchPhaseOutputFile, notes)
    
    return bumpHunterPValueFinal

#******************************************
def runSearchPhase(nPar, lumi, stag, tag, inputFileName, inputHistDir, histName):

    #------------------------------------------
    #prepare config file
    localdir = os.path.dirname(os.path.realpath(__file__))
    configFileName    = localdir+'/data/searchPhase.config'
    newConfigFileName = localdir+'/configs/searchPhase.'+stag+'.'+tag+'.config'
    #print configFileName
    #print newConfigFileName
    searchPhaseOutputFileName = localdir+'/results/searchPhase.'+stag+'.'+tag+'.root'

    #starting parameters
    #3 par, 1/fb:   0.180377, 8.1554,  -5.25718
    #4 par, 1/fb:   4.28171, 10.814,   -2.72612,  0.577889
    #4 par, 30/fb: 59.28,    12.85,    -0.5871,   1.069
    #5 par, 1/fb: 0.426909,   9.45062, -5.49925, -0.759262, -0.240173

    if nPar == 3:
        pars = [0.180377, 8.1554, -5.25718]
    elif nPar == 5:
        pars = [0.426909, 9.45062, -5.49925, -0.759262, -0.240173]
    elif nPar == 6:
        pars = [0.756961, 21.0028, -5.18678, -1.25474, -0.326143, 19998.6]
    else:
        nPar = 4 #DEFAULT
        pars = [4.28171, 10.814, -2.72612, 0.577889]
        if float(lumi.replace('p','.')) > 10.0:
            pars = [59.28,    12.85,  -0.5871,  1.069]

    with open(configFileName,'r') as configFile:
        with open(newConfigFileName,'w') as newConfigFile:
            for line in configFile:
                newLine = line
                newLine = newLine.replace('dummyInputFileName', inputFileName)
                if len(inputHistDir)>0:
                    newLine = newLine.replace('#inputHistDir', 'inputHistDir\t\t'+inputHistDir)#NEW #TEST
                newLine = newLine.replace('dummyHistName', histName)
                newLine = newLine.replace('dummyOutputFileName', searchPhaseOutputFileName)

                #3 parameters
                if nPar == 3:
                    newLine = newLine.replace('dummyFuncCode', str(9))
                    newLine = newLine.replace('dummyNPar', str(3))
                    newLine = newLine.replace('dummyP1', str(pars[0]))
                    newLine = newLine.replace('dummyP2', str(pars[1]))
                    newLine = newLine.replace('dummyP3', str(pars[2]))

                #4 parameters
                elif nPar == 4:
                    newLine = newLine.replace('dummyFuncCode', str(4))
                    newLine = newLine.replace('dummyNPar', str(4))
                    newLine = newLine.replace('dummyP1', str(pars[0]))
                    newLine = newLine.replace('dummyP2', str(pars[1]))
                    newLine = newLine.replace('dummyP3', str(pars[2]))
                    newLine = newLine.replace('#parameter4', 'parameter4\t\t'+str(pars[3]))

                #5 parameters
                elif nPar == 5:
                    newLine = newLine.replace('dummyFuncCode', str(7))
                    newLine = newLine.replace('dummyNPar', str(5))
                    newLine = newLine.replace('dummyP1', str(pars[0]))
                    newLine = newLine.replace('dummyP2', str(pars[1]))
                    newLine = newLine.replace('dummyP3', str(pars[2]))
                    newLine = newLine.replace('#parameter4', 'parameter4\t\t'+str(pars[3]))
                    newLine = newLine.replace('#parameter5', 'parameter5\t\t'+str(pars[4]))

                #6 parameters
                elif nPar == 6:
                    newLine = newLine.replace('dummyFuncCode', str(8))
                    newLine = newLine.replace('dummyNPar', str(6))
                    newLine = newLine.replace('dummyP1', str(pars[0]))
                    newLine = newLine.replace('dummyP2', str(pars[1]))
                    newLine = newLine.replace('dummyP3', str(pars[2]))
                    newLine = newLine.replace('#parameter4', 'parameter4\t\t'+str(pars[3]))
                    newLine = newLine.replace('#parameter5', 'parameter5\t\t'+str(1.000))
                    newLine = newLine.replace('#parameter6', 'parameter6\t\t'+str(1.000))

                newConfigFile.write(newLine)

    #------------------------------------------
    #search phase
    print '\n******************************************'
    print 'runnning SearchPhase'
    os.system('SearchPhase --config %s --noDE'%newConfigFileName)
    
    #------------------------------------------
    #get BH p-value
    #open SearchPhase results file
    if not os.path.isfile(searchPhaseOutputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find SearchPhase output file')
    searchPhaseOutputFile = ROOT.TFile(searchPhaseOutputFileName)

    #get SearchPhase results
    #histograms
    basicData                  = searchPhaseOutputFile.Get("basicData")
    basicBackground            = searchPhaseOutputFile.Get("basicBkgFrom4ParamFit")
    residualHist               = searchPhaseOutputFile.Get("residualHist")
    #bumpHunterStatHistNullCase = searchPhaseOutputFile.Get("bumpHunterStatHistNullCase")
    #print 'basicBackground entries = %s'%basicBackground.GetEntries()

    #initial BH p-value
    bumpHunterStatOfFitToDataInitial = searchPhaseOutputFile.Get("bumpHunterStatOfFitToDataInitial")#NEW
    #bumpHunterStatValueInitial = bumpHunterStatOfFitToDataInitial[0]
    bumpHunterPValueInitial    = bumpHunterStatOfFitToDataInitial[1]
    bumpHunterPValueErrInitial = bumpHunterStatOfFitToDataInitial[2]

    #vector
    bumpHunterStatOfFitToData = searchPhaseOutputFile.Get("bumpHunterStatOfFitToData")
    if not bumpHunterStatOfFitToData:
        raise SystemExit('\n***ERROR*** couldn\'t find bumpHunterStatOfFitToData vector')
    
    bumpHunterStatValue = bumpHunterStatOfFitToData[0]
    bumpHunterPValue    = bumpHunterStatOfFitToData[1]
    bumpHunterPValueErr = bumpHunterStatOfFitToData[2]

    #vector
    bumpHunterPLowHigh = searchPhaseOutputFile.Get('bumpHunterPLowHigh')
    #bumpHunterStatValue = bumpHunterPLowHigh[0]
    bumpLowEdge         = bumpHunterPLowHigh[1]
    bumpHighEdge        = bumpHunterPLowHigh[2]

    #vector
    bumpFoundVector = searchPhaseOutputFile.Get("bumpFound")
    bumpFound = bool(bumpFoundVector[0])

    #print
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "initial BumpHunter p-value = %s +/- %s"%(bumpHunterPValueInitial, bumpHunterPValueErrInitial)
    print "final BumpHunter p-value =   %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)
    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas
    print 'bump found? %s'%bumpFound
    
    #------------------------------------------
    return bumpHunterPValueInitial, bumpHunterPValue, bumpFound

#******************************************
if __name__ == '__main__':

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 10:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters (%s/%s) \
            \nHOW TO: time python -u checkJES.py inputFileName lumi nPar pgg pgq pqg pqq tag'\
            %(len(sys.argv),10))

    #------------------------------------------
    #get input parameters and run
    inputFileName = sys.argv[1].strip()
    inputHistDir = sys.argv[2].strip()
    lumi = sys.argv[3].strip()
    nPar = sys.argv[4].strip()
    pgg = sys.argv[5].strip()
    pgq = sys.argv[6].strip()
    pqg = sys.argv[7].strip()
    pqq = sys.argv[8].strip()
    tag = sys.argv[9].strip()

    output = checkMix(inputFileName, inputHistDir, lumi, nPar, pgg, pgq, pqg, pqq, tag)
    
    #------------------------------------------
    print '\ndone: %s'%output

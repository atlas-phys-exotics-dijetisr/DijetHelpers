#!/usr/bin/env python

#******************************************
#import stuff
import ROOT
import math, os, sys
import injectGaussian
import runSearchPhase
import plotSearchPhase
#import plotFuncRatio

#******************************************
#set ATLAS style #MOVED inside the functions
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()


#******************************************
def findBarelyDetectableSignal(mass, width, lumi, nPar1, nPar2, tag):

    print '\n******************************************'
    print 'finding barely detectable signal and compare two fit functions'

    #------------------------------------------
    #input parameters
    nPar1 = int(nPar1)
    nPar2 = int(nPar2)
    slumi = ('%.1f'%float(lumi.replace('p','.'))).replace('.','p')
    stag = mass+'.GeV.'+str(width).replace('.','p')+'.width.'+slumi+'.ifb.'+str(nPar1)+'.par1.'+str(nPar2)+'.par2'
    stag1 = mass+'.GeV.'+str(width).replace('.','p')+'.width.'+slumi+'.ifb.'+str(nPar1)+'.par'
    stag2 = mass+'.GeV.'+str(width).replace('.','p')+'.width.'+slumi+'.ifb.'+str(nPar2)+'.par'

    print '\nparameters:'
    print '  mass:          %s'%mass
    print '  width:         %s'%width
    print '  lumi [ifb]:    %s'%lumi
    print '  n par. def.:   %s'%nPar1
    print '  n par. alt.:   %s'%nPar2
    print '  tag:           %s'%tag
    print '  string tag:    %s'%stag

    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get input QCD histogram
    #NOTE this file and histogarms are created using the makeInputDijetMassDistributionFromFit.py script
    inputFileName = '/atlas/data5/userdata/guescini/dijet/inputs/MC15a_20150623/dataLikeHists_v1/dataLikeHistograms.QCDDiJet.root'
    inputHistDir = 'Nominal'
    inputHistName = 'mjj_Smoooth_QCDDiJet_'+str(lumi)+'fb'

    print '\n  input file name: %s'%inputFileName
    print '  input hist dir: %s'%inputHistDir
    print '  input hist name: %s'%inputHistName

    #get QCD histogram
    f = ROOT.TFile.Open(inputFileName,'READ')
    if not f:
        raise SystemExit('\n***ERROR*** couldn\'t open input file')
    if len(inputHistDir) > 0:
        qcd = f.GetDirectory(inputHistDir).Get(inputHistName)
    else:
        qcd = f.Get(inputHistName)
    if not qcd:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram')
    
    #------------------------------------------
    #run search phase on QCD background only
    print '\n******************************************'
    print 'running search phase on QCD background only'

    #nPar1
    QCDtag1 = slumi+'.ifb.'+str(nPar1)+'.par'
    searchPhaseQCDOutputFileName1 = 'results/searchPhase.'+QCDtag1+'.QCD.'+tag+'.root'
    if os.path.isfile(searchPhaseQCDOutputFileName1):
        print '\n***WARNING*** QCD search phase results exist already (n par = %s)'%nPar1
    else:
        searchPhaseQCDOutputFileName1 = runSearchPhase.runSearchPhase(inputFileName, inputHistDir, inputHistName, lumi, nPar1, QCDtag1+'.QCD.'+tag)

    #nPar2
    QCDtag2 = slumi+'.ifb.'+str(nPar2)+'.par'
    searchPhaseQCDOutputFileName2 = 'results/searchPhase.'+QCDtag2+'.QCD.'+tag+'.root'
    if os.path.isfile(searchPhaseQCDOutputFileName2):
        print '\n***WARNING*** QCD search phase results exist already (n par = %s)'%nPar2
    else:
        searchPhaseQCDOutputFileName2 = runSearchPhase.runSearchPhase(inputFileName, inputHistDir, inputHistName, lumi, nPar2, QCDtag2+'.QCD.'+tag)

    #------------------------------------------
    #estimate starting signal peak normalization (using peak significance)
    #NOTE aim for ~5.0 peak significance (for 0.10 width)
    peakBin = qcd.FindBin(float(mass))
    #print '  peak bin = %s'%peakBin
    #print '  bkg entries = %s'%qcd.GetBinContent(peakBin)
    startSignificance = 5. #DEFAULT 5
    norm = int(startSignificance * math.sqrt(qcd.GetBinContent(peakBin)))
    
    #------------------------------------------
    #LOOP 1
    #loop until: 0.010 < BH p-value < 0.015
    BHpvalHighMin = 0.010 #0.010
    BHpvalHighMax = 0.015 #0.015
    print '\n\n******************************************'
    print 'tuning signal injection INCLUDING signal range: %.3f < BH p-value < %.3f'%(BHpvalHighMin, BHpvalHighMax)
    print '******************************************'
    normHigh, bumpHunterPValueInitialHigh, bumpFoundHigh, injectedGaussianFileName = tuneSignalInjection(inputFileName, inputHistDir, inputHistName,
                                                                                        mass, width, norm, lumi, nPar1, stag1, 'includeSig.'+tag,
                                                                                        BHpvalHighMin, BHpvalHighMax, 100,
                                                                                        1.2, 0.9, #1.5, 0.8
                                                                                        False)
    
    if bumpHunterPValueInitialHigh < BHpvalHighMin or bumpHunterPValueInitialHigh > BHpvalHighMax:
        print '\n\n***WARNING*** signal injection tuning did NOT work'
        print '***WARNING*** initial BH p-value = %.4f'%bumpHunterPValueInitialHigh
        print '***WARNING*** BH range = %.4f - %.4f'%(BHpvalHighMin, BHpvalHighMax)
        print '  mass:          %s'%mass
        print '  width:         %s'%width
        print '  lumi [ifb]:    %s'%lumi
        print '  n parameters:  %s'%nPar1
        print '  tag:           %s'%tag
        print '  string tag:    %s'%stag1
        print '  norm:          %s'%normHigh
        print '  bump found:    %s'%bumpFoundHigh
        
        #------------------------------------------
        #TEST
        #raise SystemExit('\n***EXIT*** exit')
        #------------------------------------------

    #run search with alternative fit function
    searchPhaseSignalIncludedOutputFile2 = runSearchPhase.runSearchPhase(injectedGaussianFileName, '', 'mjj.QCD_gauss', lumi, nPar2, stag2+'.includeSig.'+tag) #TEST

    #------------------------------------------
    #LOOP 2
    #loop until: 0.005 < BH p-value < 0.010
    BHpvalLowMin = 0.005 #0.005
    BHpvalLowMax = BHpvalHighMin #0.010
    norm = normHigh
    print '\n\n******************************************'
    print 'tuning signal injection EXCLUDING signal range: %.3f < BH p-value < %.3f'%(BHpvalLowMin, BHpvalLowMax)
    print '******************************************'
    normLow, bumpHunterPValueInitialLow, bumpFoundLow, injectedGaussianFileName = tuneSignalInjection(inputFileName, inputHistDir, inputHistName,
                                                                                    mass, width, norm, lumi, nPar1, stag1, 'excludeSig.'+tag,
                                                                                    BHpvalLowMin, BHpvalLowMax, 100,
                                                                                    1.05, 0.98, #1.1, 0.9
                                                                                    True)
    
    if bumpHunterPValueInitialLow < BHpvalLowMin or bumpHunterPValueInitialLow > BHpvalLowMax:
        print '\n\n***WARNING*** signal injection tuning did NOT work'
        print '***WARNING*** initial BH p-value = %.4f'%bumpHunterPValueInitialLow
        print '***WARNING*** BH range = %.4f - %.4f'%(BHpvalLowMin, BHpvalLowMax)
        print '  mass:          %s'%mass
        print '  width:         %s'%width
        print '  lumi [ifb]:    %s'%lumi
        print '  n parameters:  %s'%nPar1
        print '  tag:           %s'%tag
        print '  string tag:    %s'%stag1
        print '  norm:          %s'%normLow
        print '  bump found:    %s'%bumpFoundLow
        
        #------------------------------------------
        #TEST
        #raise SystemExit('\n***EXIT*** exit')
        #------------------------------------------

    #run search with alternative fit function
    searchPhaseSignalExcludedOutputFile2 = runSearchPhase.runSearchPhase(injectedGaussianFileName, '', 'mjj.QCD_gauss', lumi, nPar2, stag2+'.excludeSig.'+tag) #TEST

    #------------------------------------------
    #plot results
    
    #plot SearchPhase results - signal included
    if (bumpHunterPValueInitialHigh > BHpvalHighMin or bumpHunterPValueInitialHigh < BHpvalHighMax):
        notes = []
        notes.append('sig. mass = %.0f GeV'%float(mass))
        notes.append('sig. width = %.0f%%'%(float(width)*100.))
        notes.append('sig. norm = %s'%normHigh)
        searchPhaseSignalIncludedOutputFile1 = 'results/searchPhase.'+stag1+'.includeSig.'+tag+'.root'
        plotSearchPhase.plotGaussianSearchPhase(searchPhaseSignalIncludedOutputFile1, notes)
        #searchPhaseSignalIncludedOutputFile2 = 'results/searchPhase.'+stag2+'.includeSig.'+tag+'.root'
        plotSearchPhase.plotGaussianSearchPhase(searchPhaseSignalIncludedOutputFile2, notes)

    #plot SearchPhase results - signal excluded
    if (bumpHunterPValueInitialLow > BHpvalLowMin  or bumpHunterPValueInitialLow < BHpvalLowMax):
        notes = []
        notes.append('sig. mass = %.0f GeV'%float(mass))
        notes.append('sig. width = %.0f%%'%(float(width)*100.))
        notes.append('sig. norm = %s'%normLow)
        searchPhaseSignalExcludedOutputFile1 = 'results/searchPhase.'+stag1+'.excludeSig.'+tag+'.root'
        plotSearchPhase.plotGaussianSearchPhase(searchPhaseSignalExcludedOutputFile1, notes)
        #searchPhaseSignalExcludedOutputFile2 = 'results/searchPhase.'+stag2+'.excludeSig.'+tag+'.root'
        plotSearchPhase.plotGaussianSearchPhase(searchPhaseSignalExcludedOutputFile2, notes)
        
    #plot functions ratios
    if (bumpHunterPValueInitialHigh > BHpvalHighMin or bumpHunterPValueInitialHigh < BHpvalHighMax) and (bumpHunterPValueInitialLow > BHpvalLowMin  or bumpHunterPValueInitialLow  < BHpvalLowMax):
        plotFuncRatio.plotDoubleFuncRatioWilks(searchPhaseQCDOutputFile1,
                                                searchPhaseQCDOutputFile2,
                                                searchPhaseSignalIncludedOutputFile1,
                                                searchPhaseSignalIncludedOutputFile2,
                                                searchPhaseSignalExcludedOutputFile1,
                                                searchPhaseSignalExcludedOutputFile2,
                                                'gaussian',#'signal'
                                                mass,
                                                width,
                                                normHigh,
                                                normLow,
                                                lumi,
                                                nPar1,
                                                nPar2 ,
                                                tag)
    
    #------------------------------------------
    return (bumpHunterPValueInitialHigh, bumpHunterPValueInitialLow) #initial BH p-value signal included, initial BH p-value signal excluded


#******************************************
def tuneSignalInjection(inputFileName, inputHistDir, inputHistName,
                        mass, width, norm, lumi, nPar, stag, tag,
                        BHpvalMin=0.010, BHpvalMax=0.15, countMax=50,
                        increaseStep=1.6, decreaseStep=0.6, #1.75, 0.8 #1.5, 0.8 #1.6, 0.8
                        findBump=True):

    #------------------------------------------
    bumpHunterPValueInitial = 999.
    count = int(0)
    bumpFound = False
    
    #lists
    lBumpHunterPValue = []
    lNorm = []
    
    while (bumpHunterPValueInitial < BHpvalMin or bumpHunterPValueInitial > BHpvalMax) and count < countMax:

        print '\n\n******************************************'
        #print 'trial %s - signal peak normalization = %s'%(count, norm)
        print 'trial %s'%count
        print 'signal mass = %s'%mass
        print 'signal width = %s'%width
        print 'signal peak normalization = %s'%norm
        if findBump:
            print 'excluding signal region'
        else:
            print 'including signal region'
        print '******************************************'

        #------------------------------------------
        #inject signal
        injectedGaussianFileName, peakSignificance = injectGaussian.injectGaussian(inputFileName, inputHistDir, inputHistName, mass, width, norm, lumi, str(nPar)+'.par.'+tag, False, True)#do NOT plot each step
        #injectedGaussianFileName, peakSignificance = injectGaussian.injectGaussian(inputFileName, inputHistDir, inputHistName, mass, width, norm, lumi, str(nPar)+'.par.'+tag, True, True)#plot injected spectrum before moving on

        #------------------------------------------
        #run search phase
        searchPhaseOutputFileName, bumpHunterPValueInitial, bumpHunterPValueFinal, bumpFound, chi2ndf, logLValue = runSearchPhase.runSearchPhase(injectedGaussianFileName, '', 'mjj.QCD_gauss', lumi, nPar, stag+'.'+tag)
        print 'signal peak normalization = %s'%norm

        #------------------------------------------
        #tune signal normalization
        #NOTE based on ***INITIAL*** BH p-value
        lBumpHunterPValue.append(bumpHunterPValueInitial)
        lNorm.append(norm)
        oldNorm = norm

        print '\n------------------------------------------'
        print 'initial BH p-values         = %s'%lBumpHunterPValue
        print 'signal peak normalizations = %s'%lNorm
        print 'loops = %s'%len(lBumpHunterPValue)
        print '------------------------------------------\n'

        if bumpHunterPValueInitial > BHpvalMax:
            #norm = int( round(norm*increaseStep))#DEFAULT
            norm = norm*increaseStep#TEST            
            if norm == oldNorm:
                print '***WARNING*** number of signal events (signal peak normalization = %s) is equal to previous value; increasing by one'%norm
                #norm += 1#DEAFULT
                norm *= 1.01

        elif bumpHunterPValueInitial < BHpvalMin:
            #norm = int( round(norm*decreaseStep))#DEFAULT
            norm = norm*decreaseStep#TEST
            if norm == oldNorm:
                print '    ***WARNING*** number of signal events (signal peak normalization = %s) is equal to previous value; decrementing by one'%norm
                #norm -= 1
                norm *= 0.99
                if norm <= 0:
                    print '\n***WARNING*** too few events: signal peak normalization = %s'%norm
                    break

        elif (findBump and bumpFound) or (not findBump and not bumpFound):
            injectedGaussianFileName, peakSignificance = injectGaussian.injectGaussian(inputFileName, inputHistDir, inputHistName, mass, width, norm, lumi, str(nPar)+'.par.'+tag, True, True)#OK, now plot
            break

        count+=1

    #------------------------------------------
    #check if injection has converged to the desired BH p-value range
    if count >= countMax and (bumpHunterPValueInitial < BHpvalMin or bumpHunterPValueInitial > BHpvalMax):
        print '\n***WARNING*** reached maximum number of trials (%s)'%countMax

    #------------------------------------------
    return norm, bumpHunterPValueInitial, bumpFound, injectedGaussianFileName


#******************************************
if __name__ == '__main__':

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 7:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters (%s/%s) \
            \nHOW TO: time python -u findBarelyDetectableSignal.py mass width lumi nPar1 nPar2 tag\n'\
            %(len(sys.argv),7))

    #------------------------------------------
    #get input parameters and run
    mass = sys.argv[1].strip()
    width = sys.argv[2].strip()
    lumi = sys.argv[3].strip()
    nPar1 = sys.argv[4].strip()
    nPar2 = sys.argv[5].strip()
    tag = sys.argv[6].strip()

    out = findBarelyDetectableSignal(mass, width, lumi, nPar1, nPar2, tag)
    
    #------------------------------------------
    print '\n******************************************'
    print '\ndone %s'%(list(out)) #n steps, n signal events, BH p-value

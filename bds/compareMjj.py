#!/bin/python
# -*- coding: utf-8 -*-

#******************************************
#plot SearchPhase results
#EXAMPLE python plotSearchPhase.py path/to/file/searchPhaseResults.root

#******************************************
#import stuff
import ROOT
import re, sys, os, math, shutil, fileinput
import raffaello

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def compareMjj(inputFile1Name, inputFile2Name, hist1Name, hist2Name, label1, label2, lumi):

    print '\n******************************************\ncomparing mjj distributions'
    print '\nparameters:'
    print '  input file name 1: %s'%inputFile1Name
    print '  input file name 2: %s'%inputFile2Name
    print '  hist name 1:       %s'%hist1Name
    print '  hist name 2:       %s'%hist2Name
    print '  label 1:           %s'%label1
    print '  label 2:           %s'%label2
    print '  lumi:              %s'%lumi

    #------------------------------------------
    logx = False

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
    
    #------------------------------------------
    #open SearchPhase results file
    if not os.path.isfile(inputFile1Name):
        raise SystemExit('\n***ERROR*** couldn\'t find input file 1: %s'%inputFile1Name)
    f1 = ROOT.TFile(inputFile1Name)

    if not os.path.isfile(inputFile2Name):
        raise SystemExit('\n***ERROR*** couldn\'t find input file 2: %s'%inputFile2Name)
    f2 = ROOT.TFile(inputFile2Name)

    #------------------------------------------
    #set weights
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get histograms
    h1 = f1.GetDirectory('Nominal').Get(hist1Name)
    #h1 = f1.Get(hist1Name)
    if not h1:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram 1')

    h2 = f2.GetDirectory('Nominal').Get(hist2Name)
    #h2 = f2.Get(hist2Name)
    if not h2:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram 2')

    #------------------------------------------
    #canvas
    c = ROOT.TCanvas('c', 'c', 400, 50, 800, 600)

    #pads
    outpad = ROOT.TPad("extpad","extpad", 0., 0.,   1., 1.)
    pad1   = ROOT.TPad("pad1",  "pad1",   0., 0.33, 1., 1.)
    pad2   = ROOT.TPad("pad2",  "pad2",   0., 0.,   1., 0.33)

    #setup drawing options
    outpad.SetFillStyle(4000)#transparent

    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)

    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.3)
    pad2.SetBorderMode(0)

    pad1.Draw()
    pad2.Draw()
    outpad.Draw()

    #------------------------------------------
    #draw histograms
    pad1.cd()
    pad1.SetLogy(1)
    pad1.SetLogx(logx)

    h1.SetMarkerStyle(24)
    h1.SetMarkerColor(ROOT.kAzure+1)
    h1.SetLineColor(ROOT.kAzure+1)
    h1.SetFillStyle(0)
    h1.SetLineWidth(2)

    h2.SetMarkerStyle(25)
    h2.SetMarkerColor(ROOT.kGreen+1)
    h2.SetLineColor(ROOT.kGreen+1)
    h2.SetFillStyle(0)
    h2.SetLineWidth(2)

    hs = ROOT.THStack('hs','hs')
    hs.Add(h1)
    hs.Add(h2)
    hs.Draw('nostack')
    hs.GetYaxis().SetTitle("events/bin")
    hs.GetYaxis().SetTitleFont(43)
    hs.GetYaxis().SetTitleSize(20)
    hs.GetXaxis().SetTitle("m_{jj} [GeV]")
    hs.GetYaxis().SetLabelFont(43)
    hs.GetYaxis().SetLabelSize(20)
    c.Update()

    #------------------------------------------
    #draw ratio
    pad2.cd()
    
    pad2.SetLogy(0)
    pad2.SetLogx(logx)

    r=h1.Clone()
    r.Divide(h2)
    r.SetMarkerStyle(1)
    r.SetMarkerColor(ROOT.kBlack)
    r.SetLineColor(ROOT.kBlack)
    r.SetFillStyle(0)
    r.SetLineWidth(2)
    
    r.GetYaxis().SetTitle('ratio')
    r.GetYaxis().SetTitleFont(43)
    r.GetYaxis().SetTitleSize(20)
    r.GetYaxis().SetLabelFont(43)
    r.GetYaxis().SetLabelSize(20)

    r.GetXaxis().SetTitle('m_{jj} [GeV]')
    r.GetXaxis().SetTitleFont(43)
    r.GetXaxis().SetTitleSize(20)
    r.GetXaxis().SetLabelFont(43)
    r.GetXaxis().SetLabelSize(20)
    r.GetXaxis().SetTitleOffset(4.0)

    r.Draw('')
    c.Update()
    
    #------------------------------------------
    #draw labels and legend
    pad1.cd()

    #ATLAS
    aX = 0.65
    aY = 0.85
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(30)
    a.DrawLatex(aX,aY,'ATLAS')
		
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(30)
    p.DrawLatex(aX+0.13,aY,'internal')
    
    #note
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(42)
    n.SetTextColor(1)
    n.SetTextSize(0.04)
    if float(lumi) > 0.:
        n.DrawLatex(aX,aY-0.08,'#intL dt = %s fb^{-1}'%lumi)
    #n.DrawLatex(aX,aY-0.14,'TETS 1')
    #n.DrawLatex(aX,aY-0.18,'TEST 2')

    #legend
    lXmin = aX
    lXmax = aX+0.15
    lYmin = aY-0.14#-0.32
    lYmax = aY-0.26#-0.44
    l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
    l.SetFillStyle(0)
    l.SetFillColor(0)
    l.SetLineColor(0)
    l.SetTextFont(43)
    l.SetBorderSize(0)
    l.SetTextSize(15)
    l.AddEntry(h1,label1,'pl')
    l.AddEntry(h2,label2,'pl')
    l.Draw()
    
    c.Update()
    c.WaitPrimitive()#TEST
    #c.SaveAs(localdir+'/figures/test.compare.mjj.pdf')
    del c

    

#******************************************
if __name__ == "__main__":

    #check input parameters
    if len(sys.argv) != 8:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
            \\nplot search phase results including the bump identified by the BumpHunter \
             \nHOW TO: python -u compareMjj.py inputFile1Name inputFile2Name hist1Name hist2Name label1 label2 lumi'
            %(len(sys.argv), 8))

    #get input parameters
    inputFile1Name = sys.argv[1].strip()
    inputFile2Name = sys.argv[2].strip()
    hist1Name = sys.argv[3].strip()
    hist2Name = sys.argv[4].strip()
    label1 = sys.argv[5].strip()
    label2 = sys.argv[6].strip()
    lumi = sys.argv[7].strip()

    #plot
    compareMjj(inputFile1Name, inputFile2Name, hist1Name, hist2Name, label1, label2, lumi)
    print '\ndone'



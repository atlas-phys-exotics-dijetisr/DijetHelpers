#!/usr/bin/env python

#******************************************
#import stuff
from findBarelyDetectableSignal import findBarelyDetectableSignal

#******************************************
if __name__ == '__main__':

    print '\n******************************************'
    print 'looping over signal mass values and widths'

    #tag = 'smooth'#poisson, smooth
    tag = 'test'
    
    #input parameters for tests
    #smooth, luminosity, mass, width, parameters
    #combinations = [(True, '1', '4000', '0.20', '4'),
    #                (True, '1', '4000', '0.30', '3'),
    #                ]
    combinations = [(True, '1', '3500', '0.20', '3')]
    print '\n******************************************'
    print 'tests: %s'%combinations
    
    #loop over combinations
    for combination in combinations:

        print '\n******************************************'
        #print '  %s %s %s %s %s'%(combination[0], combination[1], combination[2], combination[3], combination[4])
        print 'smooth:       %s'%combination[0]
        print 'luminosity:   %s'%combination[1]
        print 'signal mass:  %s'%combination[2]
        print 'signal width: %s'%combination[3]
        print 'n parameters: %s'%combination[4]
        print 'tag:          %s'%tag        
        out = findBarelyDetectableSignal( str(combination[2]),
                                          str(combination[3]),
                                          str(combination[1]),
                                          str(combination[4]),
                                          tag,
                                          combination[0],
                                          True)
        print '\ndone %s'%(list(out)) #n steps, n signal events, BH p-value
        
    #------------------------------------------
    print '\n******************************************'
    print '\ndone looping over tests'

{
    
    double max_num_events = 10**10;  // make large to get full data set
    double num_events_quick_stats = 10**10; // make large to get full data set (set small if quick debugging)
    
    double Luminosity = 1;  //fb-1  #not properly done, so just leave at 1 !!!!!!!
    
    //#########################################################################################################
    //                           Can configure these, but should be fine
    //#########################################################################################################
    float mark_e = 0.37;  //0.374;  //0.55;  // //0.6;   // set to 0 if using 2012 bins, and the script will find the appropriate value....  otherwise a value mush be given
    float mark_p = 0.00001;  // script primarily focuses on efficiency, but slowly increases bins so as to ideally maximize purity
    double res_factor_initial = 1.0;  //1.00001;    //1.50001; //1.00001;  // initial resolution factor for each try to begin when finding bin size
    double res_factor_step = 0.01;    // 0.01;  // steps of res_factor between attempts for bin sizing
    float eff_percent_for_match = 1.0; // 0.98;
    int data = 13;   // Center of mass energy for mc sample
    //#########################################################################################################


    int do_binning = 1;
    binStop2012 = -100;

    
    std::vector<double> bin_left;
    std::vector<double> bin_size;   // This is anctually the bin error or half a bin size
    std::vector<double> bin_center;
    int nbins;
    
    
    
    mjj_min = floor(mjj_min+0.5);
    
   
    
    
    bin_left_max = 13000;  //8000;
    
    
    bin_left.push_back(mjj_min);
    
    //============================================================================
    
    
    
    if(do_binning ==1){
        int i=bin_left.size()-1;
        while(bin_left[i]<bin_left_max){
                
            
            
//            //cut 2:  yStar<0.6
//            // Minimizer is Linear / Robust (h=0.95)
//            // Chi2                      =  4.12835e-07
//            // NDf                       =           17
            double p0                        =    0.0488777;
            double p1                        = -2.92624e-05;
            double p2                        =  1.39116e-08;
            double p3                        = -3.69195e-12;
            double p4                        =  5.48313e-16;
            double p5                        = -4.24416e-20;
            double p6                        =  1.33082e-24;
            
            
//            //cut 4:  0.175<yStar<1.18
//            bin_left_max = 6300;
//            double p0                        =    0.0418468;
//            double p1                        = -1.12613e-05;
//            double p2                        =  2.49905e-09;
//            double p3                        = -2.55166e-13;
//            double p4                        =  9.46501e-18;
//            double p5                        = 0;
//            double p6                        =  0;

            
            double m = bin_left[i];
            if(bin_left[i]<8000){ double r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;}
	    
            double proposed_center = bin_left[i] + 0.5 * r * bin_left[i];
            
            m = proposed_center;
            if(bin_left[i]<8000){ r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;}
            while ( m - bin_left[i] - 0.5*r*m > 0.001 ){  //while ( m - 0.5*r*m > bin_left[i] ){
                m = m - 0.000001;
                if(bin_left[i]<8000){ r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;}
//                cout << m << "  ...  " << m - bin_left[i] << endl;
            }
            
            while ( m - bin_left[i] - 0.5*r*m < 0.001 ){  //while ( m - 0.5*r*m > bin_left[i] ){
                m = m + 0.000001;
                if(bin_left[i]<8000){ r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;}
                //                cout << m << "  ...  " << m - bin_left[i] << endl;
            }
            
            
            while ( m - bin_left[i] - 0.5*r*m > 0.0001 ){  //while ( m - 0.5*r*m > bin_left[i] ){
                m = m - 0.00000001;
                if(bin_left[i]<8000){ r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;}
                //                cout << m << "  ...  " << m - bin_left[i] << endl;
            }
            
            while ( m - bin_left[i] - 0.5*r*m < 0.0001 ){  //while ( m - 0.5*r*m > bin_left[i] ){
                m = m + 0.00000001;
                if(bin_left[i]<8000){ r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;}
                //                cout << m << "  ...  " << m - bin_left[i] << endl;
            }
            
            
            proposed_edge = floor(m + 0.5*r*m +0.5);

            
//
//            
//            //-----------------------
//            double m = bin_left[i];
//            double r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;
//            double proposed_center = bin_left[i] + r * bin_left[i];
//            
//            m = proposed_center;
//            r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;
//            while ( m - bin_left[i] - r*m > 0.001 ){  //while ( m - 0.5*r*m > bin_left[i] ){
//                m = m - 0.000001;
//                r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;
//                //                cout << m << "  ...  " << m - bin_left[i] << endl;
//            }
//            
//            while ( m - bin_left[i] - r*m < 0.001 ){  //while ( m - 0.5*r*m > bin_left[i] ){
//                m = m + 0.000001;
//                r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;
//                //                cout << m << "  ...  " << m - bin_left[i] << endl;
//            }
//            
//            
//            while ( m - bin_left[i] - r*m > 0.0001 ){  //while ( m - 0.5*r*m > bin_left[i] ){
//                m = m - 0.00000001;
//                r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;
//                //                cout << m << "  ...  " << m - bin_left[i] << endl;
//            }
//            
//            while ( m - bin_left[i] - r*m < 0.0001 ){  //while ( m - 0.5*r*m > bin_left[i] ){
//                m = m + 0.00000001;
//                r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;
//                //                cout << m << "  ...  " << m - bin_left[i] << endl;
//            }
//            
//            
//            proposed_edge = floor(m + r*m +0.5);
//            //---------------------
            
            
            
            
            
            
            if (p0 > 0){      // || bin_left[i]<=1000){
                    bin_left.push_back(proposed_edge);
                    i++;
                                        cout<< "Bin Edge = " << bin_left[i] << endl;
            }
                
            

        }
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    std::vector<double> bin_left_ext;
    bin_left_ext.push_back(0);

    
    
    

    // Find bin centers and sizes for plotting
    nbins = bin_left.size()-1;
    for(int i=0;i<nbins;i++){
        bin_size.push_back((bin_left[i+1]-bin_left[i])/2);
        bin_center.push_back((bin_left[i]+bin_left[i+1])/2);
        bin_left_ext.push_back(bin_left[i]);
    }
    
    
    
    
    
    
    
        //Print Res Bin centers
	cout << "******************** RESOLUTION BIN CENTERS **********************" << endl;
        for(int i=0;i<res_bin_center.size();i++){
    //        res_bin_center[i] = res_bin_center[i];
            if(i==0){
                cout << "Resolution_Bin_Centers = [ " << res_bin_center[i] << ", ";
                continue;
            }
            if(i==res_bin_center.size()-1){
                cout << res_bin_center[i] << " ]" << endl;
                continue;
            }
            cout<<res_bin_center[i] << ", ";
        }
    
    
        //Print resolutions
	cout <<	"******************** RESOLUTION VALUES **********************" << endl;
        for(int i=0;i<resolution_adj.size();i++){
            //        resolution_adj[i] = resolution_adj[i];
            if(i==0){
                cout << "Resolution_Values = [ " << resolution_adj[i] << ", ";
                continue;
            }
            if(i==resolution_adj.size()-1){
                cout << resolution_adj[i] << " ]" << endl;
                continue;
            }
            cout<<resolution_adj[i] << ", ";
        }
    

    //Print Bins
	cout <<	"******************************************************************" << endl;
	cout <<	"******************** ANALYSIS BINNING ****************************" << endl;
	cout << "******************************************************************" << endl;
        for(int i=0;i<bin_left.size();i++){
//            bin_left[i] = floor(bin_left[i]+0.5);
            if(i==0){
                cout << "Binning = [ " << bin_left[i] << ", ";
                continue;
            }
            if(i==bin_left.size()-1){
                cout << bin_left[i] << " ]" << endl;
                continue;
            }
            cout<<bin_left[i] << ", ";
        }
	cout << "******************************************************************" << endl;
	cout << "******************************************************************" << endl;
	cout << "******************************************************************" << endl;    
    
}

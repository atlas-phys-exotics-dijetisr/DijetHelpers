{
TH1::SetDefaultSumw2(kTRUE);
TH2::SetDefaultSumw2(kTRUE);
gStyle->SetOptFit();
gROOT->ProcessLine(".L binningScripts/AnalyzeBinningVariables.C");
gROOT->ProcessLine("AnalyzeBinningVariables t (\"cut_2/binningTree_mc15a.root\",0)");

gROOT->ProcessLine(".x binningScripts/find_resolution.c");
gROOT->ProcessLine(".x binningScripts/find_binning.c");
int file_marker = 2;
gROOT->ProcessLine(".x binningScripts/make_plots.c");
}

//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed May 20 03:31:24 2015 by ROOT version 5.34/24
// from TTree tree/a simple Tree with two binning variables
// found on file: J3.root
//////////////////////////////////////////////////////////

#ifndef AnalyzeBinningVariables_h
#define AnalyzeBinningVariables_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class AnalyzeBinningVariables {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Float_t         varRecon;
   Float_t         varTruth;
   Float_t         weight;

   // List of branches
   TBranch        *b_varRecon;   //!
   TBranch        *b_varTruth;   //!
   TBranch        *b_weight;   //!

   AnalyzeBinningVariables(TTree *tree=0);
   virtual ~AnalyzeBinningVariables();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef AnalyzeBinningVariables_cxx
AnalyzeBinningVariables::AnalyzeBinningVariables(TString filename, TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(filename);
      if (!f || !f->IsOpen()) {
         f = new TFile(filename);
      }
      f->GetObject("tree",tree);

   }
   Init(tree);
}

AnalyzeBinningVariables::~AnalyzeBinningVariables()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t AnalyzeBinningVariables::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t AnalyzeBinningVariables::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void AnalyzeBinningVariables::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("varRecon", &varRecon, &b_varRecon);
   fChain->SetBranchAddress("varTruth", &varTruth, &b_varTruth);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   Notify();
}

Bool_t AnalyzeBinningVariables::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void AnalyzeBinningVariables::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t AnalyzeBinningVariables::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef AnalyzeBinningVariables_cxx

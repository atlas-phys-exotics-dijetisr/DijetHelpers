{
    
    std::vector<double> purity;
    std::vector<double> efficiency;
    std::vector<double> acceptance;
    std::vector<double> purity_error;
    std::vector<double> efficiency_error;
    std::vector<double> binError;
    
    
    
    
    
    
    int nbins_ext = bin_left_ext.size() - 1;
    //----Fill histograms for final plots(mjj and mjj v. mjjT)
    TH1D* h_mjj_recon = new TH1D("h_mjj_recon","mjjrecon",nbins,&bin_left[0]);
    TH1D* h_mjj_truth = new TH1D("h_mjj_truth","mjjtruth",nbins,&bin_left[0]);
    TH2D* h_TR = new TH2D("h_TR","Recon mjj v. Truth mjj",nbins,&bin_left[0],nbins,&bin_left[0]);
    
    TH1D* h = new TH1D("h","h",100,-6,6);
    
    TH1D* h_mjj_recon_print = new TH1D("background","background",nbins,&bin_left[0]);
    
    for(int ientry = 0; ientry < nentries; ientry++){
        if(ientry < 0) break;
        if(ientry>max_num_events) break;
        
        t.GetEntry(ientry);
        
        double mjj = t.varRecon;
        double mjjTruth = t.varTruth;
        double weight = t.weight;
        
        h_mjj_recon->Fill(mjj,weight);
        h_mjj_truth->Fill(mjjTruth,weight);
        h_TR->Fill(mjjTruth,mjj,weight);
        
        h_mjj_recon_print->Fill(mjj,weight);
        
    }
    
    //------ Calculated purity and efficiency values ----------
    double p_err = 0;
    double e_err = 0;
    for(int j=0; j<nbins; j++){
        if(h_mjj_recon->GetBinContent(j)==0 || h_mjj_truth->GetBinContent(j)==0){
            purity_error.push_back(0);
            efficiency_error.push_back(0);
            purity.push_back(0);
            efficiency.push_back(0);
            continue;
        }

        float p = h_TR->GetBinContent(j,j) / h_mjj_recon->GetBinContent(j);
        float e = h_TR->GetBinContent(j,j) / h_mjj_truth->GetBinContent(j);
        
        float Nr = h_mjj_recon->GetBinContent(j);
        float Nt = h_mjj_truth->GetBinContent(j);
        float Ntr = h_TR->GetBinContent(j,j);
        p_err = 1/Nr*sqrt(p*Nr*(1-p));
        e_err = 1/Nt*sqrt(e*Nt*(1-e));
        

        purity_error.push_back(0);  //p_err);
        efficiency_error.push_back(0);  //e_err);
        
        purity.push_back(p);
        efficiency.push_back(e);
//        cout << e << endl;
        
//        cout << "Nr = " << Nr << "    Nt = " << Nt << "     Ntr = " << Ntr << endl;
        
        binError.push_back(1/sqrt(h_mjj_recon->GetBinContent(j)));
    }
    
    //----Define TGraphs for purity, efficiency, acceptance, and resolution plots------
    
    
    
    //-----  RESOLUTION  -------------
    TGraphErrors *resolution_meanValues = new TGraphErrors( res_nbins, &res_bin_center[0], &res_mean[0], &res_bin_size[0], &res_mean_error[0] );
    resolution_meanValues->GetXaxis()->SetTitle("mjj [GeV]");
    resolution_meanValues->GetYaxis()->SetTitle("Resolution (mean of mjjRecon/mjjTruth)");
    resolution_meanValues->SetFillColor(1);
    resolution_meanValues->SetLineColor(4);
    resolution_meanValues->SetLineWidth(2);
    resolution_meanValues->SetMarkerColor(2);
    resolution_meanValues->SetMarkerStyle(20);
    resolution_meanValues->SetTitle("mean ( mjjRecon/mjjTruth )");
    resolution_meanValues->GetYaxis()->SetTitleOffset(1.3);
    resolution_meanValues->GetYaxis().SetRangeUser(0.9,1.1);
    
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    resolution_meanValues->Draw();
    C1->Print(Form("bin_plots/%i_meanResFits.pdf",file_marker));
    
    
    TGraphErrors *resolution_std = new TGraphErrors( res_nbins, &res_bin_center[0], &resolution_adj[0], &res_bin_size[0], &res_sig_error[0] );
    resolution_std->GetXaxis()->SetTitle("mjj [GeV]");
    resolution_std->GetYaxis()->SetTitle("Resolution (sig of mjjRecon/mjjTruth)");
    resolution_std->SetFillColor(1);
    resolution_std->SetLineColor(4);
    resolution_std->SetLineWidth(2);
    resolution_std->SetMarkerColor(2);
    resolution_std->SetMarkerStyle(20);
    resolution_std->SetTitle("Resolution (std. dev. of mjjRecon/mjjTruth)");
    resolution_std->GetYaxis()->SetTitleOffset(1.3);
    resolution_std->GetYaxis().SetRangeUser(0,0.06);
    
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    resolution_std->Draw();
    C1->Print(Form("bin_plots/%i_resolution.pdf",file_marker));
    C1->Print(Form("bin_plots/%i_resolution.root",file_marker));
    
    
    
    
    
    

    std::vector<double> res_Bins;
    int n = bin_center.size();
    for(int i=0;i<n;i++){
        res_Bins.push_back((bin_left[i+1]-bin_left[i])/bin_center[i]);
    }
    TGraphErrors *resolution_Bins = new TGraphErrors( nbins, &bin_center[0], &res_Bins[0], &bin_size[0], &purity_error[0] );
    resolution_Bins->GetYaxis().SetRangeUser(0,0.06);
    C1->Print(Form("bin_plots/%i_binResolution.pdf",file_marker));
    
    
    
    
    //SIGNAL WIDTHS CALUCLATED AS DESCRIBED IN THE BINNING APPENDIX OF EPS NOTE
    int A = 6;
//    float B_left[A] = {2882.1686724243636, 3843.785885946632, 4845.38910148928, 5334.989033210906};
    float sigs[A] = {128.76106759459296, 128.58513020329403, 139.9282133802887, 178.19048542242578, 176.16844235750233, 204.305454496138};
    float B_center[A] = {1895.7293189807676, 2396.0980946932623, 2891.721784633893, 3853.764902998061, 4847.948309994072, 5330.385014049905};
    float res_signal[A] = {0,0,0,0,0,0};
    float Z[A] = {0,0,0,0,0,0};
    for(int i=0;i<A;i++){
        res_signal[i] = 2*sigs[i]/B_center[i];
    }
    
    


    TGraphErrors *signal_plot = new TGraphErrors( A, B_center, res_signal,Z,Z );
            signal_plot->SetFillColor(1);
            signal_plot->SetLineColor(4);
            signal_plot->SetLineWidth(2);
            signal_plot->SetMarkerColor(3);
            signal_plot->SetMarkerStyle(20);
    
    
    
    
    TMultiGraph * mg = new TMultiGraph("mg","mg");
    //mg->Add(resolution_plot1);
    mg->Add(resolution_std);
    mg->Add(resolution_Bins);
        mg->Add(signal_plot);
    mg->SetTitle("Resolutions: Detector, Bins, Signals");
//    mg->GetYaxis().SetRangeUser(0,0.06);
    
    
    
    TLegend *legendInclusive = new TLegend(0.7,0.7,0.9,0.9);
    legendInclusive->AddEntry(resolution_std,"Detector","p");
    legendInclusive->AddEntry(resolution_Bins,"Binning","p");
    legendInclusive->AddEntry(signal_plot,"QStar","p");
                              
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    mg->Draw("ap");
    legendInclusive->Draw("same");
    C1->Print(Form("bin_plots/%i_resolutionInclusive.pdf",file_marker));
    
    
    
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    TGraphErrors *binSize = new TGraphErrors( nbins, &bin_center[0], &bin_size[0], &bin_size[0], &purity_error[0] );
    
    binSize->SetTitle("Bin Width Along Mass Spectrum");
    binSize->GetYaxis()->SetTitle("Bin Width [GeV]");
    binSize->GetXaxis()->SetTitle("mjj [GeV]");
    binSize->Draw();
    C1->Print(Form("bin_plots/%i_binSize.pdf",file_marker));
    
    std::vector<double> rBdivR;
    
        for(int i=0;i<bin_center.size();i++){
            m=bin_center[i];
            if(m<8055){r = p0 + p1*m + p2*m*m + p3*m*m*m + p4*m*m*m*m + p5*m*m*m*m*m + p6*m*m*m*m*m*m;}
            double some_value = res_Bins[i]/r;
            rBdivR.push_back(some_value);
        }
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    TGraphErrors *binResDivRes = new TGraphErrors( nbins, &bin_center[0], &rBdivR[0], &bin_size[0], &purity_error[0] );
    binResDivRes->SetTitle("Bin Resoltuion and Detector Resoltuion Agreement");
    binResDivRes->GetYaxis()->SetTitle("Bin Resoltuion / Detector Resoltuion");
    binResDivRes->GetXaxis()->SetTitle("mjj [GeV]");
//    binResDivRes->GetYaxis()->SetRangeUser(0.99.1.01);
    binResDivRes->Draw();
    cout << "E" << endl;
    C1->Print(Form("bin_plots/%i_binResDivRes.pdf",file_marker));
    
    
    TGraphErrors *purity_plot = new TGraphErrors( nbins, &bin_center[0], &purity[0], &bin_size[0], &purity_error[0] );
    purity_plot->GetXaxis()->SetTitle("mjj_bin_center [GeV]");
    purity_plot->GetYaxis()->SetTitle("Purity (mjj_truth_recon / mjj_recon)");
    purity_plot->SetFillColor(1);
    purity_plot->SetLineColor(4);
    purity_plot->SetLineWidth(2);
    purity_plot->SetMarkerColor(2);
    purity_plot->SetMarkerStyle(20);
    purity_plot->GetYaxis().SetRangeUser(0.0,0.6);
    purity_plot->SetTitle("purity v. mjj_bin_center");
    
    purity_plot->RemovePoint(0);
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    purity_plot->Draw();
    C1->Print(Form("bin_plots/%i_binPurity.pdf",file_marker));
    
    
    
    cout << nbins << endl;
//    cout << bin_center.size() << endl;
//    cout << efficiency.size() << endl;
//    cout << bin_size.size() << endl;
//    cout << efficiency_error.size() << endl;
//    cout << efficiency[nbins] << endl;
    
    
    TGraphErrors *efficiency_plot = new TGraphErrors( nbins, &bin_center[0], &efficiency[0], &bin_size[0], &efficiency_error[0] );
    efficiency_plot->GetXaxis()->SetTitle("mjj_bin_center [GeV]");
    efficiency_plot->GetYaxis()->SetTitle("Efficiency (mjj_truth_recon / mjj_truth)");
    efficiency_plot->SetFillColor(1);
    efficiency_plot->SetLineColor(4);
    efficiency_plot->SetLineWidth(2);
    efficiency_plot->SetMarkerColor(2);
    efficiency_plot->SetMarkerStyle(20);
    efficiency_plot->GetYaxis().SetRangeUser(0.0,0.6);
    efficiency_plot->SetTitle("efficiency v. mjj_bin_center");
    
    efficiency_plot->RemovePoint(0);
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    efficiency_plot->Draw();
    C1->Print(Form("bin_plots/%i_binEfficiency.pdf",file_marker));
    
    
    
    TGraph *binError_plot = new TGraph( nbins, &bin_center[0], &binError[0] );
    binError_plot->GetXaxis()->SetTitle("mjj_bin_center [GeV]");
    binError_plot->GetYaxis()->SetTitle("Error ( 1/sqrt(N) )");
    binError_plot->SetFillColor(1);
    binError_plot->SetLineColor(4);
    binError_plot->SetLineWidth(2);
    binError_plot->SetMarkerColor(2);
    binError_plot->SetMarkerStyle(20);
    binError_plot->SetTitle("1/sqrt(N) v. mjj_bin_center using recon mjj");
    
    
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    binError_plot->Draw();
    C1->Print(Form("bin_plots/%i_binErrorsqrtN.pdf",file_marker));
    

    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    C1.SetLogy();
    C1.SetLogx();
    h_mjj_recon_print->GetXaxis()->SetTitle("Mjj recon [GeV]");
    h_mjj_recon_print->SetTitle("Mjj with proposed binning");
    h_mjj_recon_print->Draw();
    C1->Print(Form("bin_plots/%i_mjjSpectrumLogLog.pdf",file_marker));
    
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    C1.SetLogy();
    C1.SetLogx();
    h_mjj_recon->GetXaxis()->SetTitle("Mjj recon [GeV]");
    h_mjj_recon->SetTitle("Mjj with proposed binning");
    h_mjj_recon->Draw();
    C1->Print(Form("bin_plots/%i_mjjRecon_LogLog.pdf",file_marker));
    
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    C1.SetLogy();
    C1.SetLogx();
    h_mjj_truth->GetXaxis()->SetTitle("Mjj truth [GeV]");
    h_mjj_truth->SetTitle("Mjj with proposed binning");
    h_mjj_truth->Draw();
    C1->Print(Form("bin_plots/%i_mjjTruth_LogLog.pdf",file_marker));
    
    
    
    
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    C1.SetLogy();
    h_mjj_recon->GetXaxis()->SetTitle("Mjj recon [GeV]");
    h_mjj_recon->SetTitle("Mjj with proposed binning");
    h_mjj_recon->Draw();
    C1->Print(Form("bin_plots/%i_mjjRecon.pdf",file_marker));
    
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    C1.SetLogy();
    h_mjj_truth->GetXaxis()->SetTitle("Mjj truth [GeV]");
    h_mjj_truth->SetTitle("Mjj with proposed binning");
    h_mjj_truth->Draw();
    C1->Print(Form("bin_plots/%i_mjjTruth.pdf",file_marker));
    
    
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    C1.SetLogy();
    h_mjj_truth->GetXaxis()->SetTitle("Mjj [GeV]");
    h_mjj_truth->SetTitle("Mjj with proposed binning");
    h_mjj_truth->Draw();
//    h_mjj_truth->SetLineColor(Color_t color = 4);
    h_mjj_recon->Draw("same");
    C1->Print(Form("bin_plots/%i_mjjBoth.pdf",file_marker));
    
    
    
    
  std::cout << "HELLO" << std::endl;    
    
    
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    h_TR->GetXaxis()->SetTitle("mjjTruth");
    h_TR->GetYaxis()->SetTitle("mjjRecon");
    h_TR->SetMinimum(1e-05);
    h_TR->GetYaxis()->SetTitleOffset(1.3);
    h_TR->SetStats(0);
    C1.SetLogz();
    h_TR->Draw("colz");
    C1->Print(Form("bin_plots/%i_mjjRecon_mjjTruth.pdf",file_marker));
    
    
    
    
    TCanvas *C1 = new TCanvas("C1", "C1",280,22,700,502);
    C1->SetFillColor(0);
    for(int i=0;i<28+1;i++){
        hResArray[i]->SetTitle("");
        hResArray[i]->GetXaxis()->SetTitle("mjj_{Recon} / mjj_{Truth}");
        hResArray[i]->GetYaxis()->SetTitle("events");
        hResArray[i]->Draw();
        C1->Print(Form("bin_plots/%i_resolutionBin_%i.pdf",file_marker,i));
    }

    
    
    cout << "HELLO" << endl;


    cout << "**********************************************************" << endl;
    cout << "******************** ANALYSIS BINNING ********************" << endl;
    cout << "**********************************************************" << endl;
        for(int i=0;i<bin_left.size();i++){
            if(i==0){
                cout << "Binning = [ " << bin_left[i] << ", ";
                continue;
            }
            if(i==bin_left.size()-1){
                cout << bin_left[i] << " ]" << endl;
                continue;
            }
            cout<<bin_left[i] << ", ";
        }
	cout << "**********************************************************" << endl;
        cout << "**********************************************************" << endl;
        cout << "**********************************************************" << endl;

}

#!/bin/bash

rm binningScripts/runBinning.c
cp ./Ref/runBinningPrefix$1.c binningScripts/runBinning.c
echo "gROOT->ProcessLine(\".x binningScripts/find_resolution.c\");" >> binningScripts/runBinning.c
echo "gROOT->ProcessLine(\".x binningScripts/find_binning.c\");" >> binningScripts/runBinning.c
if [ $2 = "makePlots" ]; then
	echo "int file_marker = $1;" >> binningScripts/runBinning.c
	echo "gROOT->ProcessLine(\".x binningScripts/make_plots.c\");" >> binningScripts/runBinning.c
fi
echo "}" >> binningScripts/runBinning.c

root -l -b -q binningScripts/runBinning.c




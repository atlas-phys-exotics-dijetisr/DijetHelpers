#!/usr/bin/env python

#******************************************
#make histogram of EW k-factors

#******************************************
#import stuff
import sys, os, ROOT, array
import numpy as np

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def getEWKFactors(inputTextFile, tag):

    print '\n******************************************\ngetting EW k-factors form input text file'
    print '\nparameters:'
    print '  input text file name: %s'%inputTextFileName
    print '  tag:                  %s'%tag

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))

    #------------------------------------------
    #read input text files and get info
    if not os.path.isfile(inputTextFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input text file: %s'%inputTextFileName)
    ft = open(inputTextFileName, "r")
    rows = []
    for line in ft:
        if '#' in line:
            continue
        rows.append(line.strip().split(' '))
    columns = zip(*rows)
    mjjlow = list(columns[0])
    mjjhigh = list(columns[1])
    ewk = list(columns[2])
    ewk = [float(k) for k in ewk]

    #------------------------------------------
    #make histogram
    binedges = mjjlow
    binedges.append(mjjhigh[-1])
    binedges = [float(binedge) for binedge in binedges]
    abinedges = array.array('f',binedges)
    hewk = ROOT.TH1D('EWkfactors', 'EWkfactors', len(binedges)-1, abinedges)
    for ii in xrange(len(ewk)):
        hewk.SetBinContent(ii, ewk[ii])
        hewk.SetBinError(ii, 0.)

    #------------------------------------------
    #save EW k-factors
    if tag == 'ok' or tag == 'note' or tag == '':
        outputFileName = localdir+'/results/EW.kfactors.root'
    else:
        outputFileName = localdir+'/results/EW.kfactors.'+tag+'.root'
    outputFile = ROOT.TFile.Open(outputFileName, 'RECREATE')
    outputFile.cd()
    hewk.Write()

    #------------------------------------------
    #plot
    
    c = ROOT.TCanvas('c', 'c', 400, 50, 800, 600)
    c.cd()
    c.SetLogy(0)
    c.SetLogx(1)
    c.SetGrid(1,1)
    
    hewk.SetMarkerStyle(0)
    hewk.SetMarkerColor(ROOT.kBlack)
    hewk.SetLineColor(ROOT.kBlack)
    hewk.SetFillStyle(0)
    hewk.SetLineWidth(2)
    
    hewk.GetYaxis().SetTitle('EW k-factors')
    hewk.GetYaxis().SetTitleFont(43)
    hewk.GetYaxis().SetTitleSize(20)
    hewk.GetYaxis().SetLabelFont(43)
    hewk.GetYaxis().SetLabelSize(20)
    
    hewk.GetXaxis().SetTitle('m_{jj} [GeV]')
    hewk.GetXaxis().SetTitleFont(43)
    hewk.GetXaxis().SetTitleSize(20)
    hewk.GetXaxis().SetLabelFont(43)
    hewk.GetXaxis().SetLabelSize(20)
    hewk.GetYaxis().SetMoreLogLabels(1)
    
    hewk.Draw('][E')
    hewk.SetAxisRange(0.9,1.1,'Y')
    c.Update()
    
    #draw labels and legend
    #ATLAS
    aX = 0.65
    aY = 0.85
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(30)
    a.DrawLatex(aX,aY,'ATLAS')

    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(30)
    p.DrawLatex(aX+0.13,aY,'internal')
    
    #note
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(42)
    n.SetTextColor(1)
    n.SetTextSize(0.04)
    n.DrawLatex(aX,aY-0.06,'#sqrt{s}=13 TeV')
    #n.DrawLatex(aX,aY-0.14,'TEST')
    
    #legend
    lXmin = aX
    lXmax = aX+0.15
    lYmin = aY-0.15
    lYmax = aY-0.10
    l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
    l.SetFillStyle(0)
    l.SetFillColor(0)
    l.SetLineColor(0)
    l.SetTextFont(43)
    l.SetBorderSize(0)
    l.SetTextSize(15)
    l.AddEntry(hewk,'EW k-factors','pl')
    #l.Draw()
    
    c.Update()
    c.WaitPrimitive()
    if tag == 'ok' or tag == 'note' or tag == '':
        c.SaveAs(localdir+'/figures/EW.kfactors.pdf')
    else:
        c.SaveAs(localdir+'/figures/EW.kfactors.'+tag+'.pdf')
    del c

    outputFile.Close()
    
#******************************************
if __name__ == "__main__":

    #check input parameters
    if len(sys.argv) != 3:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
             \nHOW TO: python -u getEWKFactors.py inputTextFileName tag'
            %(len(sys.argv), 3))#9

    inputTextFileName = sys.argv[1].strip()
    #inputTextFileName = 'inputs/DijetATLAS_13TeV_R0.4_MJJ_KEW'
    tag = sys.argv[2].strip()
    
    #plot
    getEWKFactors(inputTextFileName, tag)
    print '\ndone'

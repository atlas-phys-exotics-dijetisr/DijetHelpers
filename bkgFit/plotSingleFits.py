#!/usr/bin/env python

####################################
# plotSingleFits.py
#
# For plotting TFitResults from singleFit.py onto the fitted data
#
####################################

import os, sys, time, argparse, copy, glob
from array import array
from math import sqrt, log, isnan, isinf

#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--plotChi2", dest='plotChi2', action='store_true', default=False, help="Draw the chi2 result as well")
parser.add_argument("--noNLLPlot", dest='noNLLPlot', action='store_true', default=False, help="Do not draw the NLL fit")
parser.add_argument("--plotSignal", dest='plotSignal', action='store_true', default=False, help="Search for scaled signal files and also plot them")
parser.add_argument("--toyError", dest='toyError', action='store_true', default=False, help="Use Fit Function Error derived from Toys")
parser.add_argument("--funcError", dest='funcError', action='store_true', default=False, help="Use Fit Function Error derived from Covariance Matrix")
parser.add_argument("--massRanges", dest='massRanges', default=-1,
      type=int, nargs='+', help="List of mass ranges for fitting and generating purposes")
parser.add_argument("--file", dest='file', default="input/input_DataLikePythiaDijet.root",
      help="Input file name")
parser.add_argument("--histName", dest='histName', default="mjj_DataLikePythiaDiJet",
      help="string for histName to use")
parser.add_argument("--outDir", dest='outDir', default="plots",
      help="Name of output directory for plots")
parser.add_argument("--comEnergy", dest='comEnergy', default=13,
         type=int, help="Center of Mass Energy")
parser.add_argument("--systematic", dest='systematic', default="",
         help="Systematic directory to look for histograms")
parser.add_argument("--fitVariation", dest='fitVariation', default="",
         help="A variation string at the end of the name of the TFitResult if we varied a fit property")
parser.add_argument("--fitsToUse", dest='fitsToUse', default="",
         help="Use only these fit functions if available.  If empty then use all available fit functions")

args = parser.parse_args()

import ROOT
sys.path.insert(0, '../scripts/')
import AtlasStyle, DijetFuncs
#import plotUtils

def main():


  ## Setup Area ##
  AtlasStyle.SetAtlasStyle()
  ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 2000") #Ignore TCanvas::Print info

  args.outDir += '/'+args.systematic
  if not os.path.exists(args.outDir):
    os.makedirs(args.outDir)
  if not os.path.exists(args.outDir+"/mjjPlots"):
    os.makedirs(args.outDir+"/mjjPlots")
#  gStyle.SetOptStat(0)
  canv1 = ROOT.TCanvas()

  outName = os.path.basename(args.file)[:-5] #remove .root

  f_saveBinInfo = True
  if(f_saveBinInfo):
    if not os.path.exists(args.outDir+"/BinResults"):
      os.makedirs(args.outDir+"/BinResults")

  minMjj = maxMjj = -1
  binList = []

  ## Load global variables for fits ##
  DijetFuncs.initializeGlobals()
  DijetFuncs.comEnergy = args.comEnergy*1000.


  #### Get the Proper Fit Information for each Fit available ####
  #Items are fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy
  fitFiles = glob.glob(args.file[:-5]+'/*')
  print fitFiles

  bannedFitList = []#"BTH", "NEW1", "NEW2", "NEW3", "NEW4", "NEW5", "NEW6", "NEW7", "NEW8"]
  for bannedFit in bannedFitList:
    fitFiles = [fitFile for fitFile in fitFiles if not bannedFit in fitFile]

  ##choose fit order.  First will be drawn last!
  reverseFitOrder = ["CDF", "STD", "ADD", "BTH"]
  for iOrdered, orderedFit in enumerate(reverseFitOrder):
    for iFit, fitFile in enumerate(fitFiles):
      if orderedFit in fitFile:
        fitFiles.insert( 0, fitFiles.pop( iFit ) )

        continue

  fList = DijetFuncs.getFits( fitFiles )
  print "Fit Types are", fList["fNames"]

  #### Get the input histograms ####
  # Add data histograms that contain the args.histName string
  dataHists = []
  inFile = ROOT.TFile.Open(args.file, "READ")


  ## Get Systematic Dir ##
  if len(args.systematic) > 0:
    args.systematic = args.systematic.rstrip('/')
    thisDir = inFile.Get(args.systematic)
    if not thisDir:
      print "ERROR, couldn't find systematic directory ", args.systematic, " in ", args.file
      exit(0)
  else:
    thisDir = inFile
  for histKey in thisDir.GetListOfKeys():
    if args.histName in histKey.GetName():
      dataHists.append(histKey.ReadObj())
      dataHists[-1].SetDirectory(0) #Detach from inFile
  inFile.Close()

  for iHist in range(len(dataHists)):
    if "Scaled" in dataHists[iHist].GetName():
      print "Resetting Errors of Scaled Distribution"
      for iBin in range(1, dataHists[iHist].GetNbinsX()+1):
        dataHists[iHist].SetBinError(iBin, sqrt(dataHists[iHist].GetBinContent(iBin)) )


  #### Get Scaled Signal Histograms ####
  scaledSignalHists = []
  if args.plotSignal and 'With' in args.file.split('.')[1]:
    oldSampleName = args.file.split('.')[1]
    newSampleName = oldSampleName.split('With')[1]
    inFile = ROOT.TFile.Open( args.file.replace(oldSampleName, newSampleName) , "READ")
    if not inFile:
      print "Warning, could not find Signal Only File"
    else:
      ## Get Systematic Dir ##
      if len(args.systematic) > 0:
        args.systematic = args.systematic.rstrip('/')
        thisDir = inFile.Get(args.systematic)
        if not thisDir:
          print "ERROR, couldn't find systematic directory ", args.systematic, " in ", args.file
          exit(0)
      else:
        thisDir = inFile


      for thisHist in dataHists:
        scaledSignalHists.append(thisDir.Get( thisHist.GetName().replace(oldSampleName,newSampleName).replace('DataLike','Scaled') ))
      print "Found the following Scaled Signal Hists: "
      print scaledSignalHists
  #Make sure it's correct size if not done
  if not scaledSignalHists:
    scaledSignalHists = [None] * len(dataHists)


  #### Get binning if the user requested a specific range, from the dataHist ####
  binHist = []
  if args.massRanges is not -1:
    minMjj = []
    maxMjj = []
    for iRange in range(len(args.massRanges)/2):
      minMjj.append( args.massRanges[2*iRange] )
      maxMjj.append( args.massRanges[2*iRange+1] )

    binHist = dataHists[0].Clone("BinHist")
    for iBin in range(1, binHist.GetNbinsX()+1):
      binHist.SetBinContent(0)
      binHist.SetBinError(0)
      for iMass in range(len(minMjj)):
        if binHist.GetXaxis.GetBinLowEdge(iBin) > minMjj[iMass] and binHist.GetXaxis.GetBinUpEdge(iBin) < maxMjj[iMass]:
          binList.append(iBin)
          continue
    minMjj = binHist.GetXaxis().GetBinLowEdge( binList[0] )
    maxMjj = binHist.GetXaxis().GetBinUpEdge( binList[-1] )

  ############### Load fitResults into memory ############
  fitResults = [ [] for i in range(len(dataHists)) ] #[hist][fit]
  chi2Results = [ [] for i in range(len(dataHists)) ] #[hist][fit]
  if args.toyError:
    toyRMSError = [ [] for i in range(len(dataHists)) ] #[hist][fit]

  ##### First get all TFitResults ######
  #-- For each fit
  for iFit, thisFit in enumerate(fList["fNames"]):
    print "Opening Fit File ", args.file[:-5]+"/"+thisFit+".root"
    fitFile = ROOT.TFile.Open(args.file[:-5]+"/"+thisFit+".root","READ")

    ## Switch to systematics dir ##
    if len(args.systematic) > 0:
      args.systematic = args.systematic.rstrip('/')
      thisDir = fitFile.Get(args.systematic)
      if not thisDir:
        print "ERROR, couldn't find systematic directory ", args.systematic, " in ", args.file[:-5]+'/'+thisFit+'.root'
        exit(1)
      args.systematic+='/'


    ## If not previously specified, get the binning used from the first hist ##
    if minMjj is -1:

      binHist = fitFile.Get(args.systematic+"BinHist")
      binHist.SetDirectory(0)
      for iBin in range(1, binHist.GetNbinsX()+1):
        if binHist.GetBinContent(iBin) > 0:
          binList.append(iBin)
          binHist.SetBinContent(iBin, 0.) #Set to zero to allow plotting with this histogram
      minMjj = binHist.GetXaxis().GetBinLowEdge( binList[0] )
      maxMjj = binHist.GetXaxis().GetBinUpEdge( binList[-1] )

      lumiBinHists = []
      lumiBinLists = []
      for thisKey in thisDir.GetListOfKeys():
        if "BinHist" in thisKey.GetName() and 'fb' in thisKey.GetName():
          lumiBinHists.append( thisDir.Get( thisKey.GetName() ) )
          lumiBinHists[-1].SetDirectory(0)
          lumiBinLists.append([])
          for iBin in range(1, lumiBinHists[-1].GetNbinsX()+1):
            if lumiBinHists[-1].GetBinContent(iBin) > 0:
              lumiBinLists[-1].append(iBin)

      outFile = ROOT.TFile(args.outDir+"/"+outName+".root", "UPDATE")
      binHist.Write("", ROOT.TObject.kOverwrite)
      for thisLumiBinHist in lumiBinHists:
        thisLumiBinHist.Write("", ROOT.TObject.kOverwrite)
      outFile.Close()


    #--- For each data file
    for iData, thisData in enumerate( dataHists ):

      thisFitResult =  fitFile.Get( args.systematic+"FitResultLogL_"+thisFit+"_"+thisData.GetName()+args.fitVariation )
      if thisFitResult:
        fitResults[iData].append( copy.copy(thisFitResult) )
      else:
        print "Couldn't find any TFitFunction's FitResultLogL_"+thisFit+"_"+thisData.GetName()+args.fitVariation, ", Skipping it"
        fitResults[iData].append( None )

      if( args.plotChi2 ):
        thisChi2Result =  fitFile.Get( args.systematic+"FitResultChi2_"+thisFit+"_"+thisData.GetName()+args.fitVariation )
        if thisChi2Result:
          chi2Results[iData].append( copy.copy(thisChi2Result) )
        else:
          print "Couldn't find any TFitFunction's FitResultChi2_"+thisFit+"_"+thisData.GetName()+args.fitVariation, ", Skipping it"
          chi2Results[iData].append( None )

      ## Get toy fits if needed for error ##
      if args.toyError:
        toyRMSHist = fitFile.Get( args.systematic+'toyFitRMS_'+thisFit+'_'+thisData.GetName()+args.fitVariation )
        if toyRMSHist:
          toyRMSHist.SetDirectory(0)
          toyRMSError[iData].append( toyRMSHist )
        else:
          toyRMSError[iData].append( None )

    fitFile.Close()

  #### Loop over and draw all input files and fits #####
  for iData, thisData in enumerate( dataHists ):
    print "Drawing histogram", thisData.GetName()

    ### choose binning to use ###
    thisBinList = binList
    if ('fb' in thisData.GetName().split('_')[-1] ):
      for iLumiBinHist, thisLumiBinHist in enumerate(lumiBinHists):
        if thisData.GetName().split('_')[-1] in thisLumiBinHist.GetName():
          thisBinList = lumiBinLists[ iLumiBinHist  ]



    ########### Plot all fits for one histogram ##########
    canv1.Clear()
    canv1.cd()
    canv1.SetLogx()
    pad1 = ROOT.TPad("pad1","pad1",0,0.3,1,1)
    pad2 = ROOT.TPad("pad2","pad2",0,0.01,1,0.3)
    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    pad1.SetBottomMargin(0)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(.3)
    pad1.SetLogy()
    pad1.SetLogx()
    pad2.SetLogx()


    ##### Create TF1s from TFitResults #####
    fitFuncs = []
    chi2Funcs = []
    for iFit, thisFitRes in enumerate(fitResults[iData]):
      if thisFitRes == None:
        fitFuncs.append( None )
      else:
        fitFuncs.append( ROOT.TF1( fList["fNames"][iFit], fList["fEqs"][iFit], minMjj, maxMjj) )
        for iParam in range( len(fList["fParams"][iFit]) ):
          fitFuncs[iFit].SetParameter(iParam, thisFitRes.Parameter(iParam))

      if args.plotChi2:
        if chi2Results[iData][iFit] == None:
          chi2Funcs.append( None )
        else:
          chi2Funcs.append( ROOT.TF1( fList["fNames"][iFit], fList["fEqs"][iFit], minMjj, maxMjj) )
          for iParam in range( len(fList["fParams"][iFit]) ):
            chi2Funcs[iFit].SetParameter(iParam, chi2Results[iData][iFit].Parameter(iParam))

    ## Set up general plot configurations for top and bottom ##
    dataHists[iData].GetXaxis().SetRange(binList[0], binList[-1]) #Set xaxis
    dataHists[iData].GetXaxis().SetTitle("Mass [GeV]");
    dataHists[iData].GetYaxis().SetTitle("Events");
    dataHists[iData].GetYaxis().SetLabelSize(.055)
    dataHists[iData].GetYaxis().SetTitleSize(.05)
    dataHists[iData].SetLineColor(ROOT.kBlack)

    ratioHist = dataHists[iData].Clone() #save dataHists[iData] before making changes

    ##################### Draw Top Plot ########################

    ##### Configure top plot #####
    dataHists[iData].SetMaximum(dataHists[iData].GetMaximum()*2)
    #dataHists[iData].SetMinimum(.000001) #just above 0 to remove bottom y-axis tick-mark
    dataHists[iData].SetMinimum(.11) #just above 0 to remove bottom y-axis tick-mark


    ## Setup Legend ##
    leg = ROOT.TLegend(.63,.6,.9,.9,"")
    leg.SetFillStyle(0)
    leg.SetTextSize(0.04)

    ##### Draw data #####
    dataHists[iData].Draw()
    #!!leg.AddEntry(dataHists[iData], ' '.join(dataHists[iData].GetName().split('_')[2:]).replace('With','+'), "el")
    leg.AddEntry(dataHists[iData], ' '.join(dataHists[iData].GetName().split('_')[2:]).replace('With','+').replace('fb','fb'), "el")

    if scaledSignalHists[iData]:
      scaledSignalHists[iData].SetMarkerColor(ROOT.kRed)
      scaledSignalHists[iData].SetMarkerStyle(4)
      scaledSignalHists[iData].SetLineColor(ROOT.kWhite)
      scaledSignalHists[iData].Draw("same")
      leg.AddEntry(scaledSignalHists[iData], scaledSignalHists[iData].GetName().split('_')[2], "p")

    if(f_saveBinInfo):
      binContentFile = open(args.outDir+'/BinResults/'+outName+'_'+dataHists[iData].GetName()+'.txt', 'w+')
    ### Create and Draw a proper histogram from the TF1s ####
    topFitHists = []
    for iFit, thisFit in enumerate(fitFuncs):
      if not args.noNLLPlot:
        topFitHists.append( binHist.Clone("topFit_"+dataHists[iData].GetName()+"_"+fList["fNames"][iFit]) )
        for iBin in range(1, topFitHists[-1].GetNbinsX()+1):
          topFitHists[-1].SetBinContent(iBin, 0)
          topFitHists[-1].SetBinError(iBin, 0)

        ## Set bin content ##
        if thisFit == None:
        #if fitResults[iData][iFit] == None: #if it doesn't exist
          leg.AddEntry(topFitHists[-1], fList["fDisplayNames"][iFit]+" (-1)", "l")
        else:
          if(f_saveBinInfo):
            print >> binContentFile, "Fit ", fList["fNames"][iFit]
          #errorBandUp = topFitHists[-1].Clone("errorBandUp")
          #errorBandDn = topFitHists[-1].Clone("errorBandDn")
          for iBin in range(thisBinList[0], thisBinList[-1]):
            xStart = topFitHists[-1].GetXaxis().GetBinLowEdge(iBin)
            xEnd = topFitHists[-1].GetXaxis().GetBinUpEdge(iBin)
            funcContent = thisFit.Integral( xStart, xEnd )
            if isnan(funcContent):
              print "Setting function contents for bin", iBin, "(", xStart, "to", xEnd, "GeV) to zero!"
              funcContent = 0.
            topFitHists[-1].SetBinContent(iBin, funcContent)

            if args.toyError:
              if toyRMSError[iData][iFit]:
                topFitHists[-1].SetBinError( iBin, toyRMSError[iData][iFit].GetBinContent(iBin) )
              else:
                topFitHists[-1].SetBinError(iBin, 0.0000001)

            elif args.funcError:
              funcError = thisFit.IntegralError( xStart, xEnd, fitResults[iData][iFit].GetParams(), fitResults[iData][iFit].GetCovarianceMatrix().GetMatrixArray() )
              #funcError = 0.00001
              topFitHists[-1].SetBinError(iBin, funcError)

            else:
              topFitHists[-1].SetBinError(iBin, 0.0000001)

            #errorBandUp.SetBinContent(iBin, topFitHists[-1].GetBinContent(iBin) + topFitHists[-1].GetBinError(iBin) )
            #errorBandDn.SetBinContent(iBin, topFitHists[-1].GetBinContent(iBin) - topFitHists[-1].GetBinError(iBin) )



            if(f_saveBinInfo):
              print >> binContentFile, iBin,  dataHists[iData].GetXaxis().GetBinLowEdge(iBin), dataHists[iData].GetXaxis().GetBinUpEdge(iBin), dataHists[iData].GetBinContent(iBin), topFitHists[-1].GetBinContent(iBin)


          ##Draw error band over fit line ##
          topFitHists[-1].SetLineColor(fList["fColors"][iFit])
          topFitHists[-1].SetMarkerSize(0)
          topFitHists[-1].DrawCopy("hist same")
          topFitHists[-1].SetFillColor(fList["fColors"][iFit])
          topFitHists[-1].SetFillStyle(3001)
          topFitHists[-1].Draw("e2same")

          #topFitHists[-1].Draw("hist L same")
          leg.AddEntry(topFitHists[-1], fList["fDisplayNames"][iFit]+" ("+str(fitResults[iData][iFit].Status())+")", "l")

      if args.plotChi2:
        thisChi2Fit = chi2Funcs[iFit]
        topFitHists.append( binHist.Clone("topFitChi2_"+dataHists[iData].GetName()+"_"+fList["fNames"][iFit]) )
        for iBin in range(1, topFitHists[-1].GetNbinsX()+1):
          topFitHists[-1].SetBinContent(iBin, 0)
          topFitHists[-1].SetBinError(iBin, 0)

        ## Set bin content ##
        if thisChi2Fit == None:
          leg.AddEntry(topFitHists[-1], fList["fDisplayNames"][iFit]+" #chi^{2} (-1)", "p")
        else:
          for iBin in range(thisBinList[0], thisBinList[-1]):
            xStart = topFitHists[-1].GetXaxis().GetBinLowEdge(iBin)
            xEnd = topFitHists[-1].GetXaxis().GetBinUpEdge(iBin)
            funcContent = thisChi2Fit.Integral(xStart, xEnd)
            if isnan(funcContent):
              print "Setting function contents for bin", iBin, "(", xStart, "to", xEnd, "GeV) to zero!"
              funcContent = 0.
            funcError = thisFit.IntegralError( xStart, xEnd, chi2Results[iData][iFit].GetParams(), chi2Results[iData][iFit].GetCovarianceMatrix().GetMatrixArray() )
            topFitHists[-1].SetBinContent(iBin, funcContent)
            topFitHists[-1].SetBinError(iBin, funcError)


          ##Draw error band over fit line ##
          topFitHists[-1].SetLineColor(fList["fColors"][iFit]-2)
          topFitHists[-1].SetMarkerSize(0)
          topFitHists[-1].DrawCopy("hist same")
          topFitHists[-1].SetFillColor(fList["fColors"][iFit]-2)
          topFitHists[-1].SetFillStyle(3001)
          topFitHists[-1].Draw("e2same")
          leg.AddEntry(topFitHists[-1], fList["fDisplayNames"][iFit]+" #chi^{2} ("+str(fitResults[iData][iFit].Status())+")", "l")

    leg.Draw("same")

    ## ATLAS Labeling ##
    AtlasStyle.ATLAS_LABEL(0.43,0.85)
#    AtlasStyle.myText(0.51,0.85,1,"#scale[1]{Internal}");
    AtlasStyle.myText(0.43,0.8,1,"#scale[0.9]{#sqrt{s} = "+str(args.comEnergy)+" TeV}");


    ### Draw a Line defining fit regions ###
    xDividers = []
    regionLines = []
    for iBin in range(1,len(thisBinList)):
      if( not thisBinList[iBin] == thisBinList[iBin-1]+1 ):
        xDividers.append( dataHists[iData].GetXaxis().GetBinUpEdge(thisBinList[iBin-1]))
        xDividers.append( dataHists[iData].GetXaxis().GetBinLowEdge(thisBinList[iBin]))
    for xDivide in xDividers:
      regionLines.append(ROOT.TLine( xDivide, -4, xDivide, dataHists[iData].GetMaximum() ))
      regionLines[-1].SetLineStyle(7)
      regionLines[-1].SetLineColor(ROOT.kBlack)
      regionLines[-1].Draw()

    ################## Add Ratio Plot ######################
    pad2.cd()

    ## Configure a Dotted Line at 0 ##
    oneLine = ROOT.TF1("zl1","0", minMjj, maxMjj )
    oneLine.SetTitle("")
    oneLine.SetLineWidth(1)
    oneLine.SetLineStyle(7)
    oneLine.SetLineColor(ROOT.kBlack)

    oneLine.GetXaxis().SetLabelSize(.1)
    oneLine.GetXaxis().SetLabelOffset(.015)
    oneLine.GetXaxis().SetTitleSize(.1)
    oneLine.GetXaxis().SetTitle("Mass [GeV]");
    oneLine.GetXaxis().SetMoreLogLabels()

    oneLine.GetYaxis().SetLabelSize(.11)
    oneLine.GetYaxis().SetLabelOffset(.01)
    oneLine.GetYaxis().SetTitleSize(.13)
    oneLine.GetYaxis().SetTitleOffset(0.3)
    oneLine.GetYaxis().SetTitle("Significance")
    oneLine.GetYaxis().SetNdivisions(7)
    oneLine.GetYaxis().SetRangeUser(-3.7,3.7)

    oneLine.Draw()
    for regionLine in regionLines:
      regionLine.Draw()

    ####### Calulate significance plot for each fit #############
    botHists = [] # [Fit]
    for iFit, thisFit in enumerate(fitFuncs):
      if not args.noNLLPlot:
        botHists.append( binHist.Clone("botSig_"+dataHists[iData].GetName()+"_"+fList["fNames"][iFit]) )
        #botHists.append( plotUtils.getMassHist("botSig_"+dataHists[iData].GetName()+"_"+fList["fNames"][iFit], str(args.comEnergy)+"TeV") )
        if thisFit == None:
          continue

        ## Set significance for each bin ##
        for iBin in range(thisBinList[0], thisBinList[-1]):

          ## Get Effective Entries ##
          thisBinCont = ratioHist.GetBinContent(iBin) #Here ratioHist is a clone of dataHist
          thisBinErr = ratioHist.GetBinError(iBin)
          thisNeff = 0. if thisBinErr == 0. else (thisBinCont/thisBinErr)**2
          thisweightEff = 1. if thisNeff == 0. else thisBinCont/thisNeff # = thisBinErr**2/thisBinCont

#          print "Difference is ", thisBinCont, " vs ", thisNeff
          thisData = thisNeff
          thisBkg = thisFit.Integral(botHists[-1].GetXaxis().GetBinLowEdge(iBin),botHists[-1].GetXaxis().GetBinUpEdge(iBin))/thisweightEff
          if isnan(thisBkg): #Check in case function is too small to integrate
            print "Setting function contents for bin", iBin, "(", botHists[-1].GetXaxis().GetBinLowEdge(iBin), "to",botHists[-1].GetXaxis().GetBinUpEdge(iBin), "GeV) to zero!"
            thisBkg = 0.
          botHists[-1].SetBinError(iBin, 0.)

          #Caterina's method for significance, where pVal = GetPValPoisson(thisData, thisBkg) and zVal = GetZVal(pVal, thisData>thisBkg)##
          #if not fitResults[iData][iFit] == None and thisData > 0.9: # > 0.
          if not fitResults[iData][iFit] == None and iBin >= ratioHist.FindFirstBinAbove(0) and iBin <= ratioHist.FindLastBinAbove(0):
            if thisData > thisBkg:
              pVal = 1.-ROOT.Math.inc_gamma_c(thisData,thisBkg)
              zVal = ROOT.Math.normal_quantile(1-pVal,1);
            else:
              pVal = ROOT.Math.inc_gamma_c(thisData+1,thisBkg)
              zVal = ROOT.Math.normal_quantile(pVal,1)

            if pVal > 0.5:
              botHists[-1].SetBinContent(iBin, 0.)
            elif pVal == 0.:
              botHists[-1].SetBinContent(iBin, 100.)
            else:
              botHists[-1].SetBinContent(iBin, zVal)
          else:
            botHists[-1].SetBinContent(iBin, 0.)

        botHists[-1].SetLineColor(fList["fColors"][iFit])
        botHists[-1].Draw("HIST same")

      ### For Chi2 ###
      if args.plotChi2:
        thisChi2Fit = chi2Funcs[iFit]
        botHists.append( binHist.Clone("botSigChi2_"+dataHists[iData].GetName()+"_"+fList["fNames"][iFit]) )
        if thisChi2Fit == None:
          continue

        ## Set significance for each bin ##
        for iBin in range(thisBinList[0], thisBinList[-1]+1):

          ## Get Effective Entries ##
          thisBinCont = ratioHist.GetBinContent(iBin)
          thisBinErr = ratioHist.GetBinError(iBin)
          thisNeff = 0. if thisBinErr == 0. else (thisBinCont/thisBinErr)**2
          thisweightEff = 1. if thisNeff == 0. else thisBinCont/thisNeff # = thisBinErr**2/thisBinCont

          #print "Difference is ", thisBinCont, " vs ", thisNeff
          thisData = thisNeff
          thisBkg = thisChi2Fit.Integral(botHists[-1].GetXaxis().GetBinLowEdge(iBin),botHists[-1].GetXaxis().GetBinUpEdge(iBin))/thisweightEff
          if isnan(thisBkg): #Check in case function is too small to integrate
            print "Setting function contents for bin", iBin, "(", botHists[-1].GetXaxis().GetBinLowEdge(iBin), "to",botHists[-1].GetXaxis().GetBinUpEdge(iBin), "GeV) to zero!"
            thisBkg = 0.
          botHists[-1].SetBinError(iBin, 0.)

          #Caterina's method for significance, where pVal = GetPValPoisson(thisData, thisBkg) and zVal = GetZVal(pVal, thisData>thisBkg)##
          if not chi2Results[iData][iFit] == None and thisData > 0.5: # > 0.
            if thisData > thisBkg:
              pVal = 1.-ROOT.Math.inc_gamma_c(thisData,thisBkg)
              zVal = ROOT.Math.normal_quantile(1-pVal,1);
            else:
              pVal = ROOT.Math.inc_gamma_c(thisData+1,thisBkg)
              zVal = ROOT.Math.normal_quantile(pVal,1)

            if pVal > 0.5:
              botHists[-1].SetBinContent(iBin, 0.)
            elif pVal == 0.:
              botHists[-1].SetBinContent(iBin, 100.)
            else:
              botHists[-1].SetBinContent(iBin, zVal)
          else:
            botHists[-1].SetBinContent(iBin, 0.)

        botHists[-1].SetLineColor(fList["fColors"][iFit]-2)
        botHists[-1].Draw("HIST same")


    ## Save Plots to File ##
    outStr = args.outDir+"/mjjPlots/mjjPlot_"+outName+"_"+dataHists[iData].GetName()+args.fitVariation+".png"
    canv1.Print(outStr)
    outStr = args.outDir+"/mjjPlots/mjjPlot_"+outName+"_"+dataHists[iData].GetName()+args.fitVariation+".eps"
    canv1.Print(outStr)
    outFile = ROOT.TFile(args.outDir+"/"+outName+".root", "UPDATE")
    canv1.SetName("mjjPlot_"+outName+"_"+dataHists[iData].GetName()+args.fitVariation)
    canv1.Write("", ROOT.TObject.kOverwrite)
    outFile.Close()
    canv1.Clear()

    if(f_saveBinInfo):
      binContentFile.close()


if __name__ == "__main__":
   main()

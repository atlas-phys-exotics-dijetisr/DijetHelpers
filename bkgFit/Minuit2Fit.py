#!/usr/bin/env python

############################################################
#
# Minuit2Fit.py
#
# Code for the Minuit2 fit.
#
############################################################
import os, sys, time
from array import array
import copy
from math import sqrt, log, isnan, isinf
from ROOT import *

import DijetFuncs

############################################################
# Fit a function and retry the fit with kicked parameters
# if it fails.  Try up to 6 times.
############################################################
def FitWithRetries(maxAttempts, thisFitter, thisFunctor, aParams, fileOutName, sysDir, fitResultAppend):

  startParams = copy.copy(aParams)

  if(DijetFuncs.f_chi2):
    print "##################################Fit Using chi2 ##################################################"
    fitType = "Chi2"
  else:
    print "##################################Fit Using negative log likelihood ################################"
    fitType = "LogL"

  print "aParams are ", aParams

  ### Find ndof ###
  ndof = 0
  lastBin = min(DijetFuncs.binList[-1], DijetFuncs.fitHist.FindLastBinAbove(0.99) )
  firstBin = max(DijetFuncs.binList[0], DijetFuncs.fitHist.FindFirstBinAbove(0.99) )
  for iBin in DijetFuncs.binList:
    if iBin < firstBin or iBin > lastBin:
      continue
    else:
      ndof += 1

  thisFitter.FitFCN(thisFunctor, aParams, ndof, DijetFuncs.f_chi2 ) #give it data size to properly set NDF
#  thisFitter.CalculateHesseErrors()

  ##### If we get a bad fit, kick the parameters and retry, for up to maxAttempts retries #####
  numAttempts=1;
  while (numAttempts < maxAttempts and (thisFitter.Result().Status() > 1 or thisFitter.Result().MinFcnValue() > 10000.)):
    print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx Redoing Fit, Attempt #", numAttempts, " Status was ", thisFitter.Result().Status(), "xxxxxxxxxxxxxxxxxx"
    aParams[0] = startParams[0]*1.3*numAttempts
    for iParam in range(1, len(startParams)):
      aParams[iParam] = startParams[iParam]+ ((-1**(iParam+numAttempts))*.002*startParams[iParam])
    print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx aParams are ", aParams, " xxxxxxxxxxxxxxxxx"
    thisFitter.FitFCN(thisFunctor, aParams, ndof, DijetFuncs.f_chi2)
#    thisFitter.CalculateHesseErrors()
    numAttempts+=1

  #### Save results ####
  if fileOutName:
    fitResult = TFitResult(thisFitter.Result())
    fitResult.SetName("FitResult"+fitType+fitResultAppend)

    fileOut = TFile.Open(fileOutName,"UPDATE")
    fileOut.cd(sysDir)
    fitResult.Write(fitResult.GetName(), TObject.kWriteDelete)
    fileOut.Close()

  ##Ensure best fit values are currently set
  SetFunctionParams(thisFitter)

  ## set parameters back to original fit params ##
  for iParam in range(0, len(startParams)):
    aParams[iParam] = startParams[iParam]


############################################################
# Perform a chi2 fit and use the best fit parameters for
# a NLL fit.
############################################################
def Chi2LogFit(thisFitter, thisFunctor, aParams, fileOutName, sysDir, fitResultAppend):

   DijetFuncs.f_chi2 = True
   saveTol = thisFitter.Config().MinimizerOptions().Tolerance()
   thisFitter.Config().MinimizerOptions().SetTolerance(1) #minimize tolerance to reach EDM, 1 = 1e-3
   thisFitter.Config().MinimizerOptions().SetErrorDef(1.)
   FitWithRetries(5, thisFitter, thisFunctor, aParams, fileOutName, sysDir, fitResultAppend)
   DijetFuncs.f_chi2 = False
   thisFitter.Config().MinimizerOptions().SetTolerance(saveTol) #minimize tolerance to reach EDM, 1 = 1e-3
   thisFitter.Config().MinimizerOptions().SetErrorDef(0.5)
   for iParam in range(thisFitter.Result().NPar()):
      aParams[iParam] = thisFitter.Result().Parameter(iParam)
   tmpParams = copy.copy(aParams)
   FitWithRetries(5, thisFitter, thisFunctor, aParams, fileOutName, sysDir, fitResultAppend)

   ## if failed then keep original values ##
   if thisFitter.Result().Status() > 2:
     aParams = tmpParams



############################################################
# Fit class used by Minuit2
############################################################
class groupFNC(TPyMultiGenFunction):

  def __init__(self):
    TPyMultiGenFunction.__init__(self, self)

  def NDim(self):
    return len(DijetFuncs.fParams)

  def DoEval(self, args ):

    f_verbose = False
    fitHist = DijetFuncs.fitHist
    returnVal = 0.
    if(f_verbose): print "Parameters are ",

    ## Set Parameters given ##
    for iParam in range(len(DijetFuncs.fParams)):
      if(f_verbose):  print args[iParam], " ",
      DijetFuncs.thisFunction.SetParameter(iParam, args[iParam])
      ## if PyRoot gives you a nan parameter ##
      if (isnan(args[iParam])):
        print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ERROR: Parameter ", iParam, " is nan!"
        return sys.float_info.max
    if(f_verbose):  print " "

    ## Find empty bins at start or end of data.  Use 0.99 in case of scaled MC ##
    lastBin = min(DijetFuncs.binList[-1], fitHist.FindLastBinAbove(0.99) )
    firstBin = max(DijetFuncs.binList[0], fitHist.FindFirstBinAbove(0.99) )
    ## Calculate fit value for each bin ##
    for iBin in DijetFuncs.binList:

      ## Completely ignore empty bins before or after data ##
      # This does not ignore empty bins between data! #
      if iBin < firstBin or iBin > lastBin:
        continue

      ## Get effective entries ##
      binCont = fitHist.GetBinContent(iBin)
      binErr = fitHist.GetBinError(iBin)

      if "Scaled" in fitHist.GetName():
        binErr = sqrt(binCont)

      Neff = 0. if binErr == 0. else (binCont/binErr)**2
      weightEff = 1. if Neff == 0. else binCont/Neff


      ## We first try to evaluate, because if the function is too small it will be unable to Integrate or Eval it ##
      if(f_verbose): print "trying ", Neff, weightEff
      try:
        functionCont = DijetFuncs.thisFunction.Integral( fitHist.GetXaxis().GetBinLowEdge(iBin),fitHist.GetXaxis().GetBinUpEdge(iBin) )
        if isnan(functionCont) or isinf(functionCont):
          functionCont = 0.
          ## Calculate integral by hand using 20 points for each bin ##
          binWidth = fitHist.GetXaxis().GetBinUpEdge(iBin)-fitHist.GetXaxis().GetBinLowEdge(iBin)
          for iStep in range(0,20):
            functionCont += DijetFuncs.thisFunction.Eval(fitHist.GetXaxis().GetBinLowEdge(iBin)+(iStep/binWidth) )*binWidth/20.
            #functionCont += EvalFunction(fitHist.GetXaxis().GetBinLowEdge(iBin)+(iStep/binWidth) )*binWidth/100.
          print "Using exceptional function content ", functionCont
        if isnan(functionCont) or isinf(functionCont):
          raise Exception('Integral is nan')

        ## Get chi2 between Neff and the integral ##
        if (DijetFuncs.f_chi2):
          ### If number of events is less than 1, ignore for chi2 ###
          if Neff > 0.99:
            returnVal += ((( functionCont/weightEff)-Neff)**2)/Neff
            if(f_verbose):
              print "Adding ", (((functionCont/weightEff)-Neff)**2)/Neff
              print "Integ", DijetFuncs.thisFunction.Integral(fitHist.GetXaxis().GetBinLowEdge(iBin),fitHist.GetXaxis().GetBinUpEdge(iBin))
              print "bins are", fitHist.GetXaxis().GetBinLowEdge(iBin), fitHist.GetXaxis().GetBinUpEdge(iBin)
              print "val is ", DijetFuncs.thisFunction.Eval(fitHist.GetXaxis().GetBinLowEdge(iBin)), DijetFuncs.thisFunction.Eval(fitHist.GetXaxis().GetBinUpEdge(iBin) )
        ## Or get NLL between Neff and the integral ##
        else:
          thisLogVal = (TMath.PoissonI(Neff, functionCont/weightEff) ) #Prob of getting Neff if functionCont/weightEff
          if(f_verbose):  print "thisLogVal is ", thisLogVal

          ## If it is impossible, then it is because Neff >> functionCont/weightEff. ##
          ## We will instead get probability of the function given data, but multiply this by an weighting factor ##
          if(thisLogVal == 0):
            # Kate's method #
            #thisVal = Neff*log( functionCont/weightEff )-(functionCont/weightEff)-TMath::LnGamma(Neff+1.)
            ## Reverse NLL method ##
            returnVal += -log( TMath.PoissonI(functionCont/weightEff, Neff) ) #*100.
          else:
            returnVal += -log(thisLogVal)
    ## Debug information if an exception is thrown ##
      except:
        print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ERROR: Fit Calculation for bin ", iBin
        print "     Neff/weightEff: ", Neff, "/", weightEff
        print "     Function Content: ", functionCont
        print("     Parameters:")
        for iParam in range(len(DijetFuncs.fParams)):
          print('       '+str(args[iParam]))
        print ""
        return sys.float_info.max

    if(f_verbose):  print "returnVal is ", returnVal
    return returnVal


############################################################
# Get a TTree for Pvalue calculation by creating datalike
# histograms from the fit function and fitting them
############################################################
def GetPVal(thisFitter, thisFunctor, aParams, dataHist, thisFileName, sysDir, treeName, numPseudo):
  frozenFunction = DijetFuncs.thisFunction.Clone()
  logLValue = array('f',(0.,))

  toyTrees = TTree(treeName, treeName)
  toyTrees.Branch('logLValue', logLValue, 'logLValue/F')

  toyFitHists = []

  # List for calculating uncertain on fit
  # Use RMS of fits
  RMSList = [] #[bin][toy]

  # Add original fit
  SetFunctionParams(thisFitter)
  for iBin in range(1, dataHist.GetNbinsX()+1):
    RMSList.append([])
    RMSList[iBin-1].append( GetFunctionContent(iBin) )

  for iPseudo in range(numPseudo):
    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Doing PseudoFit", iPseudo, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

    PoissonFluctuate(frozenFunction, dataHist)
    for iParam in range(len(DijetFuncs.fParams)):
      print frozenFunction.GetParameters()[iParam],
    print aParams
    FitWithRetries(5, thisFitter, thisFunctor, aParams, "", "", "")

    ###Only keep good fits ###
    print "Result is ", thisFitter.Result().Status()
    if  thisFitter.Result().Status() < 2 and not isnan(thisFitter.Result().MinFcnValue()) and not isinf(thisFitter.Result().MinFcnValue()):
      logLValue[0] = thisFitter.Result().MinFcnValue()
      print "Adding logLValue", logLValue
      toyTrees.Fill()

      ## Add toy fit to RMSHist
      SetFunctionParams(thisFitter)
      for iBin in range(1, dataHist.GetNbinsX()+1):
        RMSList[iBin-1].append( GetFunctionContent(iBin) )

  ## Calculate the RMS of the toy fits+nominal for each bin
  RMSHist = dataHist.Clone('toyFitRMS_'+'_'.join(treeName.split('_')[1:]))
  for iBin in range(1, RMSHist.GetNbinsX()+1):
    toyVals = RMSList[iBin-1]
    binMean = sum(toyVals)/len(toyVals)
    for iToy, toyVal in enumerate(toyVals):
      try:
        toyVals[iToy] = (toyVal - binMean)**2
      except:
        print "Bad toy/mean values: ", toyVal, binMean
    toyRMS = sqrt( sum(toyVals)/len(toyVals) )

    RMSHist.SetBinContent(iBin, toyRMS)

  fileOut = TFile.Open(thisFileName,"UPDATE")
  fileOut.cd(sysDir)
  toyTrees.Write(toyTrees.GetName(), TObject.kOverwrite)
  RMSHist.Write( RMSHist.GetName(), TObject.kOverwrite)
  fileOut.Close()

############################################################
# Dode for calculating the poisson data like
# histograms, works if an event weighting is used.
############################################################
def PoissonFluctuate(frozenFunction, dataHist):
  gRandom = TRandom3(0)
  for iBin in DijetFuncs.binList:

    #This makes sure we're fitting the same range as data
    if iBin < dataHist.FindFirstBinAbove(0.99) or iBin > dataHist.FindLastBinAbove(0.10):
      DijetFuncs.fitHist.SetBinContent(iBin, 0.)
      DijetFuncs.fitHist.SetBinError(iBin, 0.)

    else:
      #### If data hist exists, Get data's weight to give data-like errors ####
      if dataHist.GetBinContent(iBin) > 0:
         dataWeight = dataHist.GetBinError(iBin)**2/dataHist.GetBinContent(iBin)
      else:
         dataWeight = 1.

      binCont = frozenFunction.Integral(DijetFuncs.fitHist.GetXaxis().GetBinLowEdge(iBin),DijetFuncs.fitHist.GetXaxis().GetBinUpEdge(iBin))
      binCont /= dataWeight
      if(binCont < 1E9):
         binContP = gRandom.Poisson(binCont)
      else:
         binContP = gRandom.PoissonD(binCont)

      binErrP = sqrt(binContP)*dataWeight
      binContP = binContP*dataWeight

      DijetFuncs.fitHist.SetBinContent(iBin, binContP)
      DijetFuncs.fitHist.SetBinError(iBin, binErrP)

#  dataHist.SetMarkerColor(kRed)
#  dataHist.SetLineColor(kRed)
#  DijetFuncs.fitHist.SetMarkerColor(kBlue)
#  DijetFuncs.fitHist.SetLineColor(kBlue)
#  dataHist.Draw()
#  DijetFuncs.fitHist.Draw("same")
#  c1.SetLogy()
#  time.sleep(10)

def SetFunctionParams(thisFitter):
  for iParam in range(len(DijetFuncs.fParams)):
    DijetFuncs.thisFunction.SetParameter( iParam, thisFitter.Result().Parameter(iParam) )

def GetFunctionContent(iBin):
  val = DijetFuncs.thisFunction.Integral(DijetFuncs.fitHist.GetXaxis().GetBinLowEdge(iBin),DijetFuncs.fitHist.GetXaxis().GetBinUpEdge(iBin))
  if isnan(val):
    print "Error ignored by Minuit2.py.  Setting Function Integral to 0"
    val = 0

  return val


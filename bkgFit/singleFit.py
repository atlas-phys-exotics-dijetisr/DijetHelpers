#!/usr/bin/env python

############################################################
#
# singleFit.py
#
# Code for running a single fit.
#
############################################################
import os, sys, time, argparse, copy
from array import array
from math import sqrt, log, isnan, isinf

#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--f_chi2", dest='f_chi2', action='store_true', default=False, help="For regular TH1F Fit: Calculate chi2 manually")
parser.add_argument("--f_chi2Log", dest='f_chi2Log', action='store_true', default=False, help="Do an initial fit with chi2, then final fit with log likelihood")
parser.add_argument("--minMjj", dest='minMjj', default=2000,
         type=int, help="Minimum Mjj mass for fitting and generating")
parser.add_argument("--maxMjj", dest='maxMjj', default=13000,
         type=int, help="Maximum Mjj mass for fitting and generating")
parser.add_argument("--massRanges", dest='massRanges', default=-1,
      type=int, nargs='+', help="List of mass ranges for fitting and generating purposes")
parser.add_argument("--excludeBins", dest='excludeBins', default=-1,
      type=int, nargs='+', help="List of bin ranges to exclude")
parser.add_argument("--file", dest='file', default="input/trees/mjj_combined_nEff0.root",
         help="Input file that includes histograms to fit")
parser.add_argument("--histName", dest='histName', default="h_mjj_1fb",
         help="input histogram name")
parser.add_argument("--fitToUse", dest='fitToUse', default="BTH",
         help="Single fit name to use")
parser.add_argument("--numPseudo", dest='numPseudo', default=0,
         type=int, help="Number of Psuedo-data toys to throw when calculating a P-value")
parser.add_argument("--comString", dest='comString', default='13TeV',
         help="Center of Mass Energy")
parser.add_argument("--tolerance", dest='tolerance', default=0.1, #0.1
         type=float, help="Tolerance of the fit")
parser.add_argument("--fitVariation", dest='fitVariation', default="",
         help="A fitVariation string to append to the TFitResult name")
parser.add_argument("--systematic", dest='systematic', default="",
         help="Systematic directory to look for histograms")
parser.add_argument("--outNameAppend", dest='outNameAppend', default="",
         help="Expert User Option for creating seperate output files")

args = parser.parse_args()


from ROOT import *
import DijetFuncs
from Minuit2Fit import FitWithRetries, Chi2LogFit, groupFNC, GetPVal, PoissonFluctuate
sys.path.insert(0, '../scripts/')
import plotUtils

def main():

  ## Setup area ##
  DijetFuncs.initializeGlobals()
  DijetFuncs.f_chi2 = args.f_chi2
  if not os.path.exists(args.file[:-5]):
    os.mkdir(args.file[:-5])

  ### Determine mass ranges to fit ###
  if args.massRanges is not -1:
    if len(args.massRanges)%2 is not 0:
      print "Error, --massRanges needs an even number of arguments"
      exit(1)

    minMjj = []
    maxMjj = []
    for iRange in range(len(args.massRanges)/2):
      minMjj.append( args.massRanges[2*iRange] )
      maxMjj.append( args.massRanges[2*iRange+1] )
  else:
    minMjj = [ args.minMjj ]
    maxMjj = [ args.maxMjj ]

  ## Get closest mjj range that corresponds to official binning ##
  #standardMassBins = plotUtils.getMassBins( "Coarse"+str(args.comString)+"TeV" )
  #binHist = plotUtils.getMassHist( "BinHist", "Coarse"+str(args.comString)+"TeV" ) #binning histogram to be saved in output
  standardMassBins = plotUtils.getMassBins( args.comString )
  binHist = plotUtils.getMassHist( "BinHist", args.comString ) #binning histogram to be saved in output
  for iMass in range(len(minMjj)):
    minMjj[iMass] =  ([ i for i in standardMassBins if i >= minMjj[iMass]]) [0]
    maxMjj[iMass] =  ([ i for i in standardMassBins if i <= maxMjj[iMass]]) [-1]

  ## Get corresponding bins to use ##
  DijetFuncs.binList = []
  for iMass in range(len(minMjj)):
    thisMinBin = binHist.FindBin( minMjj[iMass] )
    thisMaxBin = binHist.FindBin( maxMjj[iMass] )
    for bin in range( thisMinBin, thisMaxBin ):
      DijetFuncs.binList.append(bin)


  ## Exclude bins ##
  if args.excludeBins is not -1:
    if len(args.excludeBins)%2 is not 0:
      print "Error, --excludeBins needs an even number of arguments"
      exit(1)
    for iExcl in range(0, len(args.excludeBins), 2): #For each exclusion range pair
      for bin in range( args.excludeBins[iExcl], args.excludeBins[iExcl+1]+1): #Each bin in this exclusion range
        DijetFuncs.binList = [y for y in DijetFuncs.binList if y != bin]

  ## Set BinHist ##
  for bin in DijetFuncs.binList:
    binHist.SetBinContent(bin, 1.)


  print "Final mass ranges are ",  minMjj, maxMjj
  print "Final bins to use are", DijetFuncs.binList


  ### Choose which function to fit ###
  fList = DijetFuncs.getFit( args.fitToUse )
  DijetFuncs.fParams = fList["fParams"][0]

  ### Get names for saving output ###
  fileOutName = args.file[:-5]+"/"+fList["fNames"][0]+args.outNameAppend+".root"
  fitResultAppend = "_"+fList["fNames"][0]+"_"+args.histName+args.fitVariation
  PvalTreeName = "LogLToys"+fitResultAppend

  ### Save binning ###
  fileOut = TFile.Open(fileOutName,"UPDATE")
  if not len(args.systematic) == 0:
    fileOut.mkdir(args.systematic)
    fileOut.cd(args.systematic)
  binHist.Write("BinHist", TObject.kWriteDelete)
  if ( 'fb' in args.histName.split('_')[-1] ):
    binHist.SetName("BinHist_"+args.histName.split('_')[-1]);
    binHist.SetTitle("BinHist_"+args.histName.split('_')[-1]);
    binHist.Write("BinHist_"+args.histName.split('_')[-1], TObject.kWriteDelete)
  fileOut.Close()


  ### Get input data file ###
  file = TFile.Open(args.file,"READ")
  if not file:
    print "ERROR, could not find file ", args.file
    exit(1)

  ## Find histogram ##
  if len(args.systematic) == 0: #Hist is in top directory
    rawHist = copy.copy(file.Get(args.histName))
  else: #Hist is in systematic dir
    sysDir = file.Get(args.systematic)
    if not sysDir:
      print "ERROR, could not find systematic directory ", args.systematic, " within file ", args.file
      exit(1)
    rawHist = copy.copy(sysDir.Get(args.histName))

  file.Close()
  if not rawHist:
    print "ERROR, could not find histogram", args.histName, " within file - dir ", args.file, "-", args.systematic
    exit(1)

  if "Scaled" in rawHist.GetName():
    print "Resetting Errors of Scaled Distribution"
    for iBin in range(1, rawHist.GetNbinsX()+1):
      rawHist.SetBinError(iBin, sqrt(rawHist.GetBinContent(iBin)) )

  ### Confirm correct mass bins ###
  inputMassBins = []
  ## Must round to .01 decimal place for equality
  for iBin in range(1, rawHist.GetNbinsX()+1):
    inputMassBins.append( round(rawHist.GetXaxis().GetBinLowEdge(iBin), 2) )
  inputMassBins.append( round(rawHist.GetXaxis().GetBinUpEdge(iBin), 2) )

  if not set(inputMassBins) == set(standardMassBins):
    print "ERROR, bins of ", args.histName, " in ", args.file, "are not identical to the ", args.comString, "TeV binning in plotUtils.py!"
    print "Standard Bins: ", standardMassBins
    print "Your Bins: ", inputMassBins
    return 1

  print "xxxxxxxxxxxxxxxxxx Fitting ", fList["fDisplayNames"][0], " with com energy ", fList["fEnergy"][0], " xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

  ############# Fit and get fit attributes for the function ########################
  DijetFuncs.thisFunction = TF1(fList["fNames"][0], fList["fEqs"][0], minMjj[0], maxMjj[-1])

  ##Set up Minuit2 Fitter ##
  #Status codes: 0 is ok, 1  cov made pos def, 2 Hesse is invalid, 3 Edm is above max, 4 reach call limit, 5 any other failure
  thisFitter = Fit.Fitter()
  thisFitter.Config().MinimizerOptions().SetMinimizerType("Minuit2")
  #thisFitter.Config().MinimizerOptions().SetMinimizerAlgorithm("Simplex")
  thisFitter.Config().MinimizerOptions().SetMinimizerAlgorithm("Minimize")
  thisFitter.Config().MinimizerOptions().SetMaxIterations(1000000)
  thisFitter.Config().MinimizerOptions().SetMaxFunctionCalls(100000)
  thisFitter.Config().MinimizerOptions().SetTolerance(args.tolerance) #minimize tolerance to reach EDM, 1 = 1e-3
  #SetStrategy: Minuit must decide weather to be safe (waste a few function calls to know where it is) or fast (attempt to get requested results with fewest possilbe calls).  Value 0 is least safe.  Value 2 is most safe
  thisFitter.Config().MinimizerOptions().SetStrategy(1)
  #SetErrorDef: Parameter error is the cahnge in parameter value requred to change the function Value by Up.  Normally this is 1 for chi2 and 0.5 for negative log likelihood.  [2 sigma chi2 would thus be 4 as Chi2(x+n*sigma) = Chi2(x) +n*n]
  if(DijetFuncs.f_chi2):
    thisFitter.Config().MinimizerOptions().SetErrorDef(1)
  else:
    thisFitter.Config().MinimizerOptions().SetErrorDef(0.5)
  thisFitter.Config().MinimizerOptions().SetPrintLevel(2)

  ## Setup our Fitting Function ##
  thisFunctor = groupFNC()
  aParams = array( 'd', DijetFuncs.fParams)
  aSteps = array( 'd', fList["fSteps"][0])

  ## Setup fit parameters and constrain first parameter ##
  thisFitter.Config().SetParamsSettings( len(DijetFuncs.fParams), aParams, aSteps)
  for iParam in range(len(DijetFuncs.fParams)):
    DijetFuncs.thisFunction.SetParameter(iParam, DijetFuncs.fParams[iParam])
    if(fList["fMin"][0][iParam] != -1):
      thisFitter.Config().ParSettings(iParam).SetLowerLimit(fList["fMin"][0][iParam])
    if(fList["fMax"][0][iParam] != -1):
      thisFitter.Config().ParSettings(iParam).SetUpperLimit(fList["fMax"][0][iParam])



  #### Fit to raw hist and get best fit parameters ####
  DijetFuncs.fitHist = rawHist.Clone()
  if(DijetFuncs.f_chi2):
    FitWithRetries(5, thisFitter, thisFunctor, aParams, fileOutName, args.systematic, fitResultAppend)
  elif(args.f_chi2Log):
    ## Save chi2 fit as well ##
    Chi2LogFit(thisFitter,thisFunctor, aParams, fileOutName, args.systematic, fitResultAppend)
  else:
    FitWithRetries(5, thisFitter, thisFunctor, aParams, fileOutName, args.systematic, fitResultAppend)

  #Set aParams to be NLL's best fit values
  for iParam in range(len(DijetFuncs.fParams)):
    aParams[iParam] = thisFitter.Result().Parameter(iParam)

  ###### Calculate p-val with pseudo-experiments #####
  if not DijetFuncs.f_chi2 and args.numPseudo > 0:
    GetPVal(thisFitter, thisFunctor, aParams, rawHist, fileOutName, args.systematic, PvalTreeName, args.numPseudo)

  #### Get Chi2 of NLL best fit #####
  if not DijetFuncs.f_chi2:
    DijetFuncs.f_chi2 = True
    fitResultAppend = "FromNLL"+fitResultAppend
    ## Fix parameters, we don't want to refit ##
    for iParam in range(len(DijetFuncs.fParams)):
      thisFitter.Config().ParSettings(iParam).Fix()
    FitWithRetries(5, thisFitter, thisFunctor, aParams, fileOutName, args.systematic, fitResultAppend)



if __name__ == "__main__":
  main()
  print "Finished main()"

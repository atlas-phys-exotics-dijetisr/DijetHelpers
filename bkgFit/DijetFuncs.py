####################################################
#
# DijetFuncs.py
#
# Fit tools and parameter definitions for each fit that are accessible
# at any point of the fitting procedure.
#
# fMin and fMax are the boundaries for fit parameters.
# Set to -1 if unbounded is desired
#
#
####################################################

import os
import ROOT
from array import array

##################################################
# Universal Fit Tools
# These must be universally accesible due to
# way PyRoot Minuit2 Fitter worked
##################################################
def initializeGlobals():
  global fParams, fitHist, thisFunction, binList, f_chi2, fEnergy #mjjList_14TeV, mjjList_8TeV, fEnergy
  fitHist = ""
  thisFunction = ""
  binList = []
  f_chi2 = False
  fParams = ""
  fEnergy = 13000.


##################################################
# Given a single fit, return a list of
# all attributes for this fits
##################################################
def getFit( fitName ):
  fList = dict((key,[]) for key in ["fNames", "fDisplayNames", "fParams", "fMin", "fMax", "fSteps", "fEqs", "fColors", "fEnergy"])
  conversion = {0:"fNames", 1:"fDisplayNames", 2:"fParams", 3:"fMin", 4:"fMax", 5:"fSteps", 6:"fEqs", 7:"fColors", 8:"fEnergy"}

  fitDict = {
    'CDF': fAdd_CDF,
    'STD': fAdd_STD,
    'ADD': fAdd_ADD,
    'BTH': fAdd_BTH,
    'NEW1': fAdd_New1,
    'NEW2': fAdd_New2,
    'NEW3': fAdd_New3,
    'NEW4': fAdd_New4,
    'NEW5': fAdd_New5,
    'NEW6': fAdd_New6,
    'NEW7': fAdd_New7,
    'NEW8': fAdd_New8
  }

  if fitName not in fitDict:
    print "ERROR, DijetFuncs.getFit() could not find fit named ", fitName
    return None

  fitInfo = fitDict[ fitName ]()


  #fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy = fitInfo
  for iF, thisFitInfo in enumerate(fitInfo):
    fList[conversion[iF]].append( thisFitInfo )

  return fList



##################################################
# Given a list of fit files, return a list of
# all attributes for these fits
##################################################
def getFits( fitFileNames ):
  fList = dict((key,[]) for key in ["fNames", "fDisplayNames", "fParams", "fMin", "fMax", "fSteps", "fEqs", "fColors", "fEnergy"])
#  conversion = {0:"fNames", 1:"fDisplayNames", 2:"fParams", 3:"fMin", 4:"fMax", 5:"fSteps", 6:"fEqs", 7:"fColors", 8:"fEnergy"}

  fitInfos = []
  for file in fitFileNames:
    fitName = os.path.basename(file)
    fitName = fitName.replace('.root','')

    singleFitList = getFit( fitName )
    if singleFitList:
      for fitIndex in singleFitList:
        fList[fitIndex].append(singleFitList[fitIndex][0])

  return fList

#  if any("CDF" in file for file in fitFileNames):
#    fitInfos.append(fAdd_CDF())
#  if any("STD" in file for file in fitFileNames):
#    fitInfos.append(fAdd_STD())
#  if any("ADD" in file for file in fitFileNames):
#    fitInfos.append(fAdd_ADD())
#  if any("BTH" in file for file in fitFileNames):
#    fitInfos.append(fAdd_BTH())
#
#  #fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy = fitInfo
#  for fitInfo in fitInfos:
#    for iF, thisFitInfo in enumerate(fitInfo):
#      fList[conversion[iF]].append(thisFitInfo)
#  return fList


##################################################
# Attributes for 3 Parameter function
##################################################
def fAdd_CDF():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "CDF"
  #fParams = [0.2, 10., -5.] #For 13 TeV MC (initial)
  #fParams = [0.43, 10.59, -4.48] #For 13 TeV MC Pohweg+Pythia8
  fParams = [0.002, 18, -4.4] #For PeriodC Data
  fMin = [-1.,-1., -1.]
  fMax = [-1.,-1., -1.]
  fSteps = [1., 1., 0.1]
  fEqs = "[0]*((1-"+xVal+")^[1])*("+xVal+"^[2])"
  fDisplayNames = "3 Param"
  fColors = ROOT.kRed
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy


##################################################
# Attributes for 4 Parameter function
##################################################
def fAdd_STD():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "STD"

  #fParams = [0.00033, 14.82, -4.74, 0.00] #For 13 TeV MC (initial)
  fParams = [0.0003, 16, -5.5, -0.2] #For Period C Data

  fMin = [-1., -1., -1., -1.]
  fMax = [-1., -1., -1., -1.]
  fSteps = [.1, .1, .1, 0.1]
  fEqs = "[0]*((1-"+xVal+")^[1])*("+xVal+"^([2]+[3]*log("+xVal+")))"
  fDisplayNames = "4 Param"
  #fDisplayNames = "Standard Dijet Function"
  fColors = ROOT.kBlue
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

##################################################
# Attributes for 5 Parameter function
##################################################
def fAdd_ADD():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "ADD"
  #fParams = [0.00120286, 9.34, 7.65652, -5.52659, -0.0546846] #For 14TeV MC (initial)
  fParams = [1, 9.34, -1.65, 2.5, 1.25] #For 14TeV MC (initial)
  #fParams = [0.001, 18.8, -6.2, -1.0, -0.17] #PeriodC data

  fMin = [-1., -1., -1., -1., -1.]
  fMax = [-1., -1., -1., -1., -1.]
  fSteps = [.1, .1, .1, 0.1, 0.1]
  fEqs = "[0]*((1-(x/"+str(fEnergy)+"))^([1]))*((x/"+str(fEnergy)+")^([2]+[3]*log("+xVal+")+[4]*(log("+xVal+")^2)))"
  fDisplayNames = "Adding ln(x)^{2} term"
  fDisplayNames = "5 Param"
  fColors = ROOT.kViolet
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy
##################################################
# Attributes for 6 Parameter function
##################################################
def fAdd_BTH():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "BTH"
  fParams = [1.47, 6.5, -1.2, 2.5, 0.55, 11168.] #For 14TeV MC (initial)

  fSteps = [.01, .1, .1, 0.1, 0.1, 10.]
  fMin = [-1., -1., -1., -1., -1., 5000.]
  fMax = [-1., -1., -1., -1., -1., 20000.]
  fEqs = "[0]*((1-(x/[5]))^[1])*((x/[5])^([2]+[3]*log(x/[5])+[4]*(log(x/[5])^2)))"
  fDisplayNames = "6 Param"
  fColors = ROOT.kGreen
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy



##################################################
# Attributes for 2nd 4 Parameter function
##################################################
def fAdd_4P2():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "4P2"

  fParams = [0.2, 10., 0.00, -5.] #For 14 TeV MC (initial)

  fMin = [-1., -1., -1., -1.]
  fMax = [-1., -1., -1., -1.]
  fSteps = [.1, .1, .1, .1]
  fEqs = "[0]*((1-"+xVal+")^([1]+[2]*log("+xVal+")))*("+xVal+"^([3]))"
  fDisplayNames = "4 Param 2"
  #fDisplayNames = "Standard Dijet Function"
  fColors = ROOT.kOrange+4
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

#2  fEqs = "[0]*((1-"+xVal+")^[1])*(e^([2]*"+xVal+"^(2)))"
# Awful
#def fAdd_New1():
#  xVal = "(x/"+str(fEnergy)+")"
#  fNames = "NEW1"
#  fEqs = "[0]*((1-"+xVal+")^[1])*(exp([3]*"+xVal+"^(2)))"
#  fParams = [0.000064, 7.4, -5.1, 0.] #For 14 TeV MC (initial)
#  #fParams = [1., 1., 0] #For 14 TeV MC (initial)
#  fMin = [-1., -1., -1., -1.]
#  fMax = [-1., -1., -1., -1.]
#  fSteps = [.1, .1, .1, .1]
#  fDisplayNames = "NEW1"
#  fColors = ROOT.kRed
#  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

#3  fEqs = "[0]*((1-"+xVal+")^[1])*("+xVal+"^([2]*"+xVal+"))"
def fAdd_New1():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "NEW1"
  fEqs = "[0]*((1-"+xVal+")^[1])*("+xVal+"^([2]*"+xVal+"))"
  fParams = [16000000, 17, 26] #For 14 TeV MC (initial)
  fMin = [-1., -1., -1.]
  fMax = [-1., -1., -1.]
  fSteps = [.1, .1, .1]
  fDisplayNames = "Variation 1"
  fColors = ROOT.kPink+6
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

#5  fEqs = "[0]*((1-"+xVal+")^[1])*((1+"+xVal+")^([2]*"+xVal+"))"
def fAdd_New2():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "NEW2"
  fEqs = "[0]*((1-"+xVal+")^[1])*((1+"+xVal+")^([2]*"+xVal+"))"
  fParams = [2400000, 69, 140] #For 14 TeV MC (initial)
  fMin = [-1., -1., -1.]
  fMax = [-1., -1., -1.]
  fSteps = [.1, .1, .1]
  fDisplayNames = "Variation 2"
  fColors = ROOT.kBlack
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

#6  fEqs = "[0]*((1-"+xVal+")^[1])*((1+"+xVal+")^([2]*log("+xVal+")))"
def fAdd_New3():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "NEW3"
  fEqs = "[0]*((1-"+xVal+")^[1])*((1+"+xVal+")^([2]*log("+xVal+")))"
  fParams = [3300000, 20, 29] #For 14 TeV MC (initial)
  fMin = [-1., -1., -1.]
  fMax = [-1., -1., -1.]
  fSteps = [.1, .1, .1]
  fDisplayNames = "Variation 3"
  fColors = ROOT.kOrange
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

#7  fEqs = "[0]/("+xVal+")*((1-"+xVal+")^([1]-[2]*log("+xVal+")))"
def fAdd_New4():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "NEW4"
  fEqs = "[0]/("+xVal+")*((1-"+xVal+")^([1]-[2]*log("+xVal+")))"
  fParams = [325855, 11, 21] #For 14 TeV MC (initial)
  fMin = [-1., -1., -1.]
  fMax = [-1., -1., -1.]
  fSteps = [.1, .1, .1]
  fDisplayNames = "Variation 4"
  fColors = ROOT.kAzure+10
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

#8  fEqs = "[0]/("+xVal+")^2*((1-"+xVal+")^([1]-[2]*log("+xVal+")))"
def fAdd_New5():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "NEW5"
  fEqs = "[0]/("+xVal+")^2*((1-"+xVal+")^([1]-[2]*log("+xVal+")))"
  fParams = [9800, 10, 16] #For 14 TeV MC (initial)
  fMin = [-1., -1., -1.]
  fMax = [-1., -1., -1.]
  fSteps = [.1, .1, .1]
  fDisplayNames = "Variation 5"
  fColors = ROOT.kCyan
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

def fAdd_New6():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "NEW6"
  fEqs = "[0]*("+xVal+")^[1]*(1-("+xVal+"^[2]))^[3]"
  fParams = [0.001, -5.9, 2.2, 22.6] #For 14 TeV MC (initial)

  #fParams = [0.2, 6.1, 0.01, 0.01] #For 14 TeV MC (initial)
  fMin = [0., -1., -1., -1.]
  fMax = [-1., -1., -1., -1.]
  fSteps = [.1, .1, .1, .1]
  fDisplayNames = "Variation 6"
  fColors = ROOT.kGreen
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

#  fEqs = "[0]*("+xVal+")^[1]*(1-exp([2]*("+xVal+"-1)))"
def fAdd_New7():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "NEW7"
  fEqs = "[0]*("+xVal+")^[1]*(1-exp([2]*("+xVal+"-1)))"
  fParams = [0.35, -6.32, 0.00065] #For 14 TeV MC (initial)

  fMin = [-1., -1., -1.]
  fMax = [-1., -1., -1.]
  fSteps = [.1, .1, .1]
  fDisplayNames = "Variation 7"
  fColors = ROOT.kGreen
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

#  fEqs = "[0]*("+xVal+")^[1]*(1-exp([2]*("+xVal+"-1)^2))"
def fAdd_New8():
  xVal = "(x/"+str(fEnergy)+")"
  fNames = "NEW8"
  fEqs = "[0]*("+xVal+")^[1]*(1-exp([2]*("+xVal+"-1)^2))"
  fParams = [-.00001, -4.6, 8.4] #For 14 TeV MC (initial)

  fMin = [-1., -1., -1.]
  fMax = [-1., -1., -1.]
  fSteps = [.1, .1, .1]
  fDisplayNames = "Variation 8"
  fColors = ROOT.kRed
  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

#Extra from 2  fEqs = "[0]*((1-"+xVal+")^[1])*(e^([2]*"+xVal+"^(2)))"
#def fAdd_New6():
#  xVal = "(x/"+str(fEnergy)+")"
#  fNames = "NEW6"
#  fEqs = "[0]*((1-"+xVal+")^[1])*("+xVal+"^[2])*(exp([3]*"+xVal+"^(2)))"
#  fParams = [0.000064, 7.4, -5.1, 0.] #For 14 TeV MC (initial)
#  #fParams = [1., 1., 0] #For 14 TeV MC (initial)
#  fMin = [-1., -1., -1., -1.]
#  fMax = [-1., -1., -1., -1.]
#  fSteps = [.1, .1, .1, .1]
#  fDisplayNames = "Variation 6"
#  fColors = ROOT.kViolet
#  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

#Ex1  fEqs = "[0]*(("+xVal+")^[1])*((1-"+xVal+")^([2]-[3]*log("+xVal+")))"
#def fAdd_New7():
## This is degenerate with 4 parameter fit function
#  xVal = "(x/"+str(fEnergy)+")"
#  fNames = "NEW7"
#  fEqs = "[0]*(("+xVal+")^[1])*((1-"+xVal+")^([2]-[3]*log("+xVal+")))"
#  fParams = [0.15, -5.2, 8.8, -0.01] #For 14 TeV MC (initial)
#  fMin = [-1., -1., -1., -1.]
#  fMax = [-1., -1., -1., -1.]
#  fSteps = [.1, .1, .1, 0.1]
#  fDisplayNames = "NEW7"
#  fColors = ROOT.kBlack
#  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy

#Ex2  fEqs = "[0]*((1-"+xVal+")^[1])*(1+"+xVal+")^[2]*("+xVal+"^([3]+[4]*log("+xVal+")))"
# This is degenerate with 4 parameter fit function
#def fAdd_New8():
#  xVal = "(x/"+str(fEnergy)+")"
#  fNames = "NEW8"
#  fEqs = "[0]*((1-"+xVal+")^[1])*(1+"+xVal+")^[2]*("+xVal+"^([3]+[4]*log("+xVal+")))"
#  fParams = [0.16, 8.4, -0.87, -5.2, -0.02] #For 14 TeV MC (initial)
#  fMin = [-1., -1., -1., -1., -1.]
#  fMax = [-1., -1., -1., -1., -1.]
#  fSteps = [.1, .1, .1, 0.1, .1]
#  fDisplayNames = "NEW8"
#  fColors = ROOT.kGreen
#  return fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy


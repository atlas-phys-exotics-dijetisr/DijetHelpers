################## Private code, don't attempt to use ################

import ROOT
import glob
import copy


files = glob.glob("plots/input_DataLike_v*/input_DataLike_v*.root")

ts_CDFx = []
ts_CDFy = []
ts_ADDx = []
ts_ADDy = []
ts_BTHx = []
ts_BTHy = []
for file in files:
  thisFile = ROOT.TFile.Open(file, "READ")
  thisCanv = thisFile.Get("testStatistic")
  print thisCanv.GetName()
  thisGraphCDF = thisCanv.GetPrimitive("Graph_CDF")
  ts_CDFx.append( thisGraphCDF.GetX() )
  ts_CDFy.append( thisGraphCDF.GetY() )
  thisGraphADD = thisCanv.GetPrimitive("Graph_ADD")
  ts_ADDx.append( thisGraphADD.GetX() )
  ts_ADDy.append( thisGraphADD.GetY() )
  thisGraphBTH = thisCanv.GetPrimitive("Graph_BTH")
  ts_BTHx.append( thisGraphBTH.GetX() )
  ts_BTHy.append( thisGraphBTH.GetY() )
  thisFile.Close()

tsavg_CDFy = []
for iL in range(0, 9):
  tsavg_CDFy.append(0.)
  for iF in range(len(files)):
    tsavg_CDFy[-1] += ts_CDFy[iF][iL]
  tsavg_CDFy[-1] = tsavg_CDFy[-1]/len(files)

tsavg_ADDy = []
for iL in range(0, 9):
  tsavg_ADDy.append(0.)
  for iF in range(len(files)):
    tsavg_ADDy[-1] += ts_ADDy[iF][iL]
  tsavg_ADDy[-1] = tsavg_ADDy[-1]/len(files)

tsavg_BTHy = []
for iL in range(0, 9):
  tsavg_BTHy.append(0.)
  for iF in range(len(files)):
    tsavg_BTHy[iL] += ts_BTHy[iF][iL]
  tsavg_BTHy[iL] = tsavg_BTHy[iL]/len(files)

lumi = [0.1, 0.5, 1., 3., 5, 7, 10, 15, 30]

thisFile = ROOT.TFile.Open(files[0], "READ")
thisCanv = thisFile.Get("testStatistic")
fGraph_CDF = copy.copy(thisCanv.GetPrimitive("Graph_CDF"))
for iL in range(0,9):
  fGraph_CDF.SetPoint(iL, lumi[iL], tsavg_CDFy[iL])

fGraph_ADD = copy.copy(thisCanv.GetPrimitive("Graph_ADD"))
for iL in range(0,9):
  fGraph_ADD.SetPoint(iL, lumi[iL], tsavg_ADDy[iL])
fGraph_BTH = copy.copy(thisCanv.GetPrimitive("Graph_BTH"))
for iL in range(0,9):
  fGraph_BTH.SetPoint(iL, lumi[iL], tsavg_BTHy[iL])

canv1 = ROOT.TCanvas()
leg1 = ROOT.TLegend(.15,.2,.45,.35,"")
leg1.SetFillStyle(0)

minVal = min( min(tsavg_CDFy), min(tsavg_ADDy), min(tsavg_BTHy) )
maxVal = max( max(tsavg_CDFy), max(tsavg_ADDy), max(tsavg_BTHy) )
minVal = minVal - abs(.15*minVal) - 1
maxVal = maxVal + abs(.15*maxVal) + 1



oneLine = ROOT.TF1("zl1","0", .0001, 35)
oneLine.SetLineWidth(1)#0.5
oneLine.SetLineStyle(7)
oneLine.SetLineColor(ROOT.kBlack)
oneLine.SetMaximum(maxVal)
oneLine.SetMinimum(minVal)
oneLine.SetTitle("Test Statistic [2*(NLL_{0} - NLL_{1}) ];Luminosity (fb^{-1});Test Statistic (NLL_{Nom} - NLL_{Test})")
oneLine.Draw()
topLine = ROOT.TF1("zl1","2.5", .0001, 35 )
topLine.SetLineWidth(1)#0.5
topLine.SetLineStyle(3)
topLine.SetLineColor(ROOT.kBlack)
botLine = ROOT.TF1("zl1","-1.5", .0001, 35 )
botLine.SetLineWidth(1)#0.5
botLine.SetLineStyle(3)
botLine.SetLineColor(ROOT.kBlack)
topLine.Draw("same")
botLine.Draw("same")



fGraph_CDF.Draw("same")
leg1.AddEntry( fGraph_CDF, "3 Parameter", "p")
fGraph_ADD.Draw("same")
leg1.AddEntry( fGraph_ADD, "5 Parameter", "p")
fGraph_BTH.Draw("same")
leg1.AddEntry( fGraph_BTH, "6 Parameter", "p")
leg1.Draw()
### ATLAS labeling ##
#AtlasStyle.ATLAS_LABEL(0.20,0.85)
#AtlasStyle.myText(0.32,0.85,1,"#scale[1]{Internal}");
#AtlasStyle.myText(0.20,0.8,1,"#scale[0.9]{#sqrt{s} = 13 TeV}");

canv1.Print("plots/mergedTestStatistic.png")
outF = ROOT.TFile.Open("plots/merged.root", "RECREATE")
canv1.Write()
outF.Close()

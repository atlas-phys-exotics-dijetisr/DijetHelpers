#!/bin/python

from ROOT import *
import subprocess
import os
foutTmp = TFile.Open("AllHistograms.root","RECREATE")
minMjjForChi = 10


inputDirectory_QBH = "RawInputs/angular_QBH/"
#inputDirectory_QBH = "RawInputs/angular_BlackMax/"
out = subprocess.check_output("ls "+inputDirectory_QBH+"|grep nominal | grep root", shell=True)
fileList_QBH = out.split()

for fileName in fileList_QBH :

    try :
      massValue=fileName.split("Mth0")[1].strip(".root")
    except :
      massValue="10000"

    fin = TFile.Open(inputDirectory_QBH+fileName, "READ")

    #"/Users/urania277/Work/ExoticDijetsRun2/DijetRun2_EPS/20150624_AngularCodeHarmonization/histograms_QBH_nominal/"
    fout_QBH = TFile.Open("limit_rawInputs_StandardSelection_LUMI1p0fb_SQRTS13TeV_QBH"+massValue+".root","RECREATE")
    ChiMassTotalHistograms_QBH = {}
    #find out which histogram names we need (to overcome pyROOT's memory management w/multiple files)

    for key in fin.GetListOfKeys() :

        tokens = key.GetName().split("_")

        #find the histogram named 'Scaled_chi_for_mjj_lowMass_highMass_datasetName'
        if "chi" in tokens and "mjj" in tokens and "NPV" not in tokens and "mu" not in tokens and "average" not in tokens and "HLT" not in tokens and "L1" not in tokens and "run" not in tokens and "lbn" not in tokens:
        #['Scaled', 'chi', 'for', 'mjj', '4900', '5400', 'QBH15', '13TeV', '00267638', 'physics', 'Main']

            chiHistoName = key.GetName()
            chiHisto = fin.Get(key.GetName())
            #print chiHistoName, "Number of entries:", chiHisto.GetEntries()
            foutTmp.cd()
            chiHisto.Write()
            chiMassLow = tokens[4]
            chiMassHigh = tokens[5]
            #lower mass cut for chi (we don't want biased ones)
            if chiMassLow == "underflow" or int(chiMassLow) < minMjjForChi :
                continue

            try :
                ChiMassTotalHistograms_QBH[(chiMassLow, chiMassHigh)].Add(chiHisto)
            except :
                ChiMassTotalHistograms_QBH[(chiMassLow, chiMassHigh)] = chiHisto.Clone()


    #write everything out as chi
    for key, totalHistoData in ChiMassTotalHistograms_QBH.iteritems() :
        (chiMassLow, chiMassHigh) = key
        #change name and add to output file
        fout_QBH.cd()
        totalHistoData.Write("Scaled_chi_for_mjj_"+chiMassLow+"_"+chiMassHigh)
        totalHistoData.SetName("Scaled_chi_for_mjj_"+chiMassLow+"_"+chiMassHigh)
        totalHistoData.SetTitle("Scaled_chi_for_mjj_"+chiMassLow+"_"+chiMassHigh)

    #clone an existing histogram
    fMatteo = TFile.Open("matteoFile.root")
    histoDummy = fMatteo.Get("Data_lnchiS1800E2000_nominal")
    histoDummy.Reset("ICE")

    for key, totalHistoData in ChiMassTotalHistograms_QBH.iteritems() :
        (chiMassLow, chiMassHigh) = key
        totalHistoDataLnChi = histoDummy.Clone()
        #change name and add to output file
        fout_QBH.cd()
        for ibin in xrange(0, totalHistoData.GetNbinsX()+1) :
            #print "chi bin", totalHistoDataLnChi.GetBinLowEdge(ibin), totalHistoDataLnChi.GetBinLowEdge(ibin+1)
            #print "ln chi bin", log(totalHistoData.GetBinLowEdge(ibin)), log(totalHistoData.GetBinLowEdge(ibin+1))
          totalHistoDataLnChi.SetBinContent(ibin, totalHistoData.GetBinContent(ibin))
          totalHistoDataLnChi.SetBinError(ibin, totalHistoData.GetBinError(ibin))

        totalHistoDataLnChi.SetName("signalQBH"+massValue+"_lnchiS"+chiMassLow+"E"+chiMassHigh+"_nominal")
        totalHistoDataLnChi.SetTitle("signalQBH"+massValue+"_lnchiS"+chiMassLow+"E"+chiMassHigh+"_nominal")
        totalHistoDataLnChi.Write("signalQBH"+massValue+"_lnchiS"+chiMassLow+"E"+chiMassHigh+"_nominal")

    fout_QBH.Close()

#!/bin/python

from ROOT import *

# infile = TFile.Open("Inputs/limit_rawInputs_StandardSelection_LUMI1p0fb_SQRTS13TeV_DryRunData.root","OPEN")
# outfile = TFile.Open("Outputs/limit_rawInputs_StandardSelection_LUMI1p0fb_SQRTS13TeV_DryRunData_merged.root","RECREATE")
infile = TFile.Open("limit_rawInputs_StandardSelection_LUMI0p70fb_SQRTS13TeV_DataPeriodC2toC4.root","OPEN")
outfile = TFile.Open("limit_rawInputs_StandardSelection_LUMI0p70fb_SQRTS13TeV_DataPeriodC2toC4_merged.root","RECREATE")

DictionaryPlots = {}

for key in infile.GetListOfKeys() :
    histo=infile.Get(key.GetName())
    histoKey = histo.GetName().split("_")[0]

    try :
        DictionaryPlots[histoKey].append(histo)
    except :
        DictionaryPlots[histoKey] = [histo]

for key, Plots in DictionaryPlots.iteritems() :

    print "merging:", key
    if "Data" in key : continue

    chi_for_mjj_2500_to_3400_histo = None
    chi_for_mjj_3400_to_10000_histo = None

    #find the one that we need to add to: 2.5 to 3.4
    #and find the other one that we need to add to: 3.4 to infinity
    for plot in Plots :
        if "chi_for_mjj_2500" in plot.GetName() :
            chi_for_mjj_2500_to_3400_histo = plot
            print "found", plot.GetName()
        if "chi_for_mjj_3400" in plot.GetName() :
            chi_for_mjj_3400_to_10000_histo = plot
            print "found", plot.GetName()

    #now loop and add
    for plot in Plots :
        if "chi_for_mjj_2500" in plot.GetName() : continue
        if "chi_for_mjj_3400" in plot.GetName() : continue

        tokens = plot.GetName().split("_")
        print tokens
        lowMjjEdge = tokens[4]
        print lowMjjEdge

        if int(lowMjjEdge) < 3400 :
            chi_for_mjj_2500_to_3400_histo.Add(plot)
            print "Entries to 2500 to 3400, before adding:", chi_for_mjj_2500_to_3400_histo.GetEntries()
            print "Entries of plot", plot.GetName(), ":", plot.GetEntries()
            print "Added entries to 2500 to 3400, total entries now:", chi_for_mjj_2500_to_3400_histo.GetEntries()
        else :
            chi_for_mjj_3400_to_10000_histo.Add(plot)
            print "Entries to 3400 to 10000, before adding:", chi_for_mjj_3400_to_10000_histo.GetEntries()
            print "Entries of plot", plot.GetName(), ":", plot.GetEntries()
            print "Added entries to 3400 to 10000, total entries now:", chi_for_mjj_3400_to_10000_histo.GetEntries()

    print "number of entries 2500 to 3400:", chi_for_mjj_2500_to_3400_histo.GetEntries()
    print  "number of entries 3400 to 10000:", chi_for_mjj_3400_to_10000_histo.GetEntries()

    outfile.cd()
    chi_for_mjj_2500_to_3400_histo.SetName(key+"_chi_for_mjj_2500_to_3400")
    chi_for_mjj_2500_to_3400_histo.Write(key+"_chi_for_mjj_2500_to_3400")
    chi_for_mjj_3400_to_10000_histo.SetName(key+"_chi_for_mjj_3400_to_10000")
    chi_for_mjj_3400_to_10000_histo.Write(key+"_chi_for_mjj_3400_to_10000")


outfile.ls()
#The data: count number of events per bin and per run
python mergeRawHistogramOutputs_Data.py > NumberOfEvents.log && cat NumberOfEvents.log | sort > NumberOfEventsPerRun.log
#The MC, nominal+JES
python mergeRawHistogramOutputs_QCD.py
python mergeRawHistogramOutputs_QCD_JESVariations.py
#QBH signal
python mergeRawHistogramOutputs_QBH.py
python mergeRawHistogramOutputs_QBH_JESVariations.py
#Wprime signal
python mergeRawHistogramOutputs_Wprime.py
python mergeRawHistogramOutputs_Wprime_JESVariations.py
#QStar signal
python mergeRawHistogramOutputs_QStar.py
python mergeRawHistogramOutputs_QStar_JESVariations.py
#CI signal
python mergeRawHistogramOutputs_CI.py
python mergeRawHistogramOutputs_CI_JESVariations.py

#merge everything
hadd limit_inputs_standardSelection_LUMI1p0fb_SQRTS13TeV_DataPeriod3p57FB.root limit_rawInputs_StandardSelection_LUMI*.root

#hadd limit_inputs_standardSelection_LUMI1p0fb_SQRTS13TeV_MCOnly.root limit_rawInputs_StandardSelection_LUMI*.root

rm limit_rawInputs_StandardSelection_LUMI*.root
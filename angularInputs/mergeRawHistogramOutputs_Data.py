#!/bin/python

from ROOT import *
import subprocess
import os
foutTmp = TFile.Open("AllHistograms.root","RECREATE")
minMjjForChi = 10


PeriodCombinations = [
                      #    ("C2", 775.558+4586.361+6734.014),
                      #    ("C3", 9970.45733+13610.3267),
                      #    ("C4", 19723.44+16593.77),
                      #    ("C5", 7863),
                      #    ("C2C3", 775.558+4586.361+6734.014+9970.45733+13610.3267),
                      #    ("C2C3C4", 775.558+4586.361+6734.014+9970.45733+13610.3267+19723.44+16593.77),
                      #    ("C2C3C4C5", 775.558+4586.361+6734.014+9970.45733+13610.3267+19723.44+16593.77+7863)
                      #    ("D3D4D5D6", 80000)
                      #    ("D3D4D5D6_Hopeful", 80000)
                      #    ("D3D4D5D6_Conservative", 72000)
                      #    ("50ns", 25000)
                      #    ("CDE", 520000)
                      #    ("CDEF", 1037000)
                      #    ("CDEFG", 1400000)
                      #    ("2FB", 2010000)
                      #    ("2p7FB", 2700000)
                      #    ("3p27FB", 3270000)
                      #    ("WBCID3p27FB", 3270000)
                      #    ("3p4FB", 3400000)
                      #    ("3p5FB_inclIBLOff", 3500000)
                      #    ("3p34FB", 3340000)
                          ("3p57FB", 3570000)

]

#make a loop on the various combinations: single runs, and multiple runs
for periodCombination in PeriodCombinations :

    periodCombinationName = periodCombination[0]
    periodCombinationLumi = str(round(periodCombination[1]/1000.,0))
    print periodCombinationLumi
    inputDirectory_data = "RawInputs/angular_Data/"+periodCombinationName+"/"

    #"/Users/urania277/Work/ExoticDijetsRun2/DijetRun2_EPS/20150624_AngularCodeHarmonization/histograms_data_nominal/"
    fout_data = TFile.Open("limit_rawInputs_StandardSelection_LUMI0p"+periodCombinationLumi+"fb_SQRTS13TeV_DataPeriod"+periodCombinationName+".root","RECREATE")
    ChiMassTotalHistograms_data = {}
    out = subprocess.check_output("ls "+inputDirectory_data+"|grep root", shell=True)
    fileList_data = out.split()
    #find out which histogram names we need (to overcome pyROOT's memory management w/multiple files)
    for fileName in fileList_data :
        #print fileName

        fin = TFile.Open(inputDirectory_data+fileName, "READ")

        for key in fin.GetListOfKeys() :

            tokens = key.GetName().split("_")

            #find the histogram named 'Scaled_chi_for_mjj_lowMass_highMass_datasetName'
            if "chi" in tokens[1] and "HLT" not in tokens and "L1" not in tokens:
                #['Scaled', 'chi', 'for', 'mjj', '4900', '5400', 'data15', '13TeV', '00267638', 'physics', 'Main']

                chiHistoName = key.GetName()
                chiHisto = fin.Get(key.GetName())
                print chiHistoName, "Number of entries:", chiHisto.GetEntries()
                foutTmp.cd()
                chiHisto.Write()
                chiMassLow = tokens[4]
                chiMassHigh = tokens[5]
                runNo = tokens[8]
                #lower mass cut for chi (we don't want biased ones)
                if chiMassLow == "underflow" or int(chiMassLow) < minMjjForChi :
                    continue

                try :
                    ChiMassTotalHistograms_data[(chiMassLow, chiMassHigh)].Add(chiHisto)
                except :
                    ChiMassTotalHistograms_data[(chiMassLow, chiMassHigh)] = chiHisto.Clone()


    #write everything out as chi
    for key, totalHistoData in ChiMassTotalHistograms_data.iteritems() :
        (chiMassLow, chiMassHigh) = key
        #change name and add to output file
        fout_data.cd()
        totalHistoData.Write("Scaled_chi_for_mjj_"+chiMassLow+"_"+chiMassHigh)
        totalHistoData.SetName("Scaled_chi_for_mjj_"+chiMassLow+"_"+chiMassHigh)
        totalHistoData.SetTitle("Scaled_chi_for_mjj_"+chiMassLow+"_"+chiMassHigh)

    #clone an existing histogram
    fMatteo = TFile.Open("matteoFile.root")
    histoDummy = fMatteo.Get("Data_lnchiS1800E2000_nominal")
    histoDummy.Reset("ICE")

    for key, totalHistoData in ChiMassTotalHistograms_data.iteritems() :
        (chiMassLow, chiMassHigh) = key
        totalHistoDataLnChi = histoDummy.Clone()
        #change name and add to output file
        fout_data.cd()
        for ibin in xrange(0, totalHistoData.GetNbinsX()+1) :
            #print "chi bin", totalHistoDataLnChi.GetBinLowEdge(ibin), totalHistoDataLnChi.GetBinLowEdge(ibin+1)
            #print "ln chi bin", log(totalHistoData.GetBinLowEdge(ibin)), log(totalHistoData.GetBinLowEdge(ibin+1))
          totalHistoDataLnChi.SetBinContent(ibin, totalHistoData.GetBinContent(ibin))
          totalHistoDataLnChi.SetBinError(ibin, totalHistoData.GetBinError(ibin))
        totalHistoDataLnChi.SetName("Data_lnchiS"+chiMassLow+"E"+chiMassHigh+"_nominal")
        totalHistoDataLnChi.SetTitle("Data_lnchiS"+chiMassLow+"E"+chiMassHigh+"_nominal")
        totalHistoDataLnChi.Write("Data_lnchiS"+chiMassLow+"E"+chiMassHigh+"_nominal")

    fout_data.Close()

#!/bin/python

from ROOT import *
import subprocess
import os

#To test a scenario at a time
# scenario = "JES1"
# NP = "NP1"
# upOrDown = "down"
# tree = "outTreeJET_GroupedNP_1__1"

for scenario in ["JES1"] :

    for tree, NP, NPFileName in [("outTreeJET_GroupedNP_1__1", "NP1", "JES1"), ("outTreeJET_GroupedNP_2__1", "NP2", "JES2"), ("outTreeJET_GroupedNP_3__1", "NP3", "JES3")] :

        for upOrDown in ["up", "down"] :

            #JESStudies_JES1_outTreeJET_GroupedNP_1__1down
            inputDirectory_bkg = "RawInputs/angular_QCD/"
            #"/Users/urania277/Work/ExoticDijetsRun2/DijetRun2_EPS/20150624_AngularCodeHarmonization/histograms_bkg_nominal/"
            fout_bkg = TFile.Open("limit_rawInputs_StandardSelection_LUMI1p0fb_SQRTS13TeV_"+scenario+"_"+NP+"_"+upOrDown+".root","RECREATE")
            foutTmp = TFile.Open("AllHistograms.root","RECREATE")

            #fileName = inputDirectory_bkg+"standardPlotsMC.mc15_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.root"

            ChiMassBinsJZ ={}
            ChiMassTotalHistograms_bkg = {}
            thebuffer = []
            MjjJZ =[]


            minMjjForChi = 10
            #### Do the background first

            out = subprocess.check_output("ls "+inputDirectory_bkg+" | grep root | grep "+upOrDown+"| grep "+NPFileName, shell=True)
            print out
            fileList_bkg = out.split()
            #find out which histogram names we need (to overcome pyROOT's memory management w/multiple files)
            for fileName in fileList_bkg :

                if "JZ0W" in fileName : continue
                if "JZ1W" in fileName : continue
                if "JZ2W" in fileName : continue
                if "JZ3W" in fileName : continue

                fin = TFile.Open(inputDirectory_bkg+fileName, "READ")
                if "AllHistograms" in fileName : continue

                for key in fin.GetListOfKeys() :

                    tokens = key.GetName().split("_")
                    if "TObject" in tokens : continue
                    #find the histogram named 'Scaled_chi_for_mjj_lowMass_highMass_datasetName'
                    if "chi" in tokens[1] and "HLT" not in tokens and "L1" not in tokens:
                    #['Scaled', 'chi', 'for', 'mjj', '2800', '3100', 'mc15', '13TeV', '361027', 'Pythia8EvtGen', 'A14NNPDF23LO', 'jetjet', 'JZ7W']
                        chiHistoName = key.GetName()
                        chiHisto = fin.Get(key.GetName())
                        foutTmp.cd()
                        chiHisto.Write()
                        
                        if "chi" not in tokens :
                            pass
                        #    JZ = tokens[len(tokens)-1]
                        #    MjjJZ.append(chiHisto.Clone())
                    
                        else :
                            
                            chiMassLow = tokens[4]
                            chiMassHigh = tokens[5]
                            JZ = tokens[len(tokens)-1]
                            #lower mass cut for chi (we don't want biased ones)
                            if chiMassLow == "underflow" or int(chiMassLow) < minMjjForChi :
                                continue

                            #fill dictionary for merging
                            try :
                                ChiMassBinsJZ[(chiMassLow,chiMassHigh,JZ)].append((fileName, chiHistoName))

                            except :
                                ChiMassBinsJZ[(chiMassLow,chiMassHigh,JZ)] = []
                                ChiMassBinsJZ[(chiMassLow,chiMassHigh,JZ)].append((fileName, chiHistoName))

                            #fill dictionary for final histograms (a little wasteful but who cares if it improves readability)
                            ChiMassTotalHistograms_bkg[(chiMassLow, chiMassHigh)] = chiHisto.Clone()
                            ChiMassTotalHistograms_bkg[(chiMassLow, chiMassHigh)].Reset("ICE")

                fin.Close()

            #Dictionary at this point has entries like:
            # ('1600', '1800', 'JZ7W'):
            # [(,Scaled_chi_for_mjj_1600_1800_mc15_13TeV_361027_Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W)]
            for BinsAndSlice, Histograms in ChiMassBinsJZ.iteritems() :

                chiMassLow = BinsAndSlice[0]
                chiMassHigh = BinsAndSlice[1]
                JZ = BinsAndSlice[2]

                #merge (left for later)
                #for histogram in Histograms :
                #...

                fileName = Histograms[0][0]
                histoName = Histograms[0][1]
                finTmp = TFile.Open(inputDirectory_bkg+"/"+fileName)
                histogram = finTmp.Get(histoName)

                #add this (N vs chi) histogram to the total histogram for this MC
                #find the right histogram first
                totalHistoBkg = ChiMassTotalHistograms_bkg[(chiMassLow, chiMassHigh)]
                totalHistoBkg.Add(histogram)
                finTmp.Close()

            #write everything out
            for key, totalHistoBkg in ChiMassTotalHistograms_bkg.iteritems() :

                #clone an existing histogram
                fMatteo = TFile.Open("matteoFile.root")
                histoDummy = fMatteo.Get("Data_lnchiS1800E2000_nominal")
                histoDummy.Reset("ICE")
                totalHistoBkgLnChi = histoDummy.Clone()

                (chiMassLow, chiMassHigh) = key
                #change name and add to output file
                fout_bkg.cd()

                totalHistoBkg.SetTitle("Scaled_chi_for_mjj_"+chiMassLow+"_"+chiMassHigh)
                totalHistoBkg.SetName("Scaled_chi_for_mjj_"+chiMassLow+"_"+chiMassHigh)
                totalHistoBkg.Write("Scaled_chi_for_mjj_"+chiMassLow+"_"+chiMassHigh)

                for ibin in xrange(0, totalHistoBkg.GetNbinsX()+1) :
                    #print "chi bin", totalHistoDataLnChi.GetBinLowEdge(ibin), totalHistoDataLnChi.GetBinLowEdge(ibin+1)
                    #print "ln chi bin", log(totalHistoData.GetBinLowEdge(ibin)), log(totalHistoData.GetBinLowEdge(ibin+1))
                  totalHistoBkgLnChi.SetBinContent(ibin, totalHistoBkg.GetBinContent(ibin))
                  totalHistoBkgLnChi.SetBinError(ibin, totalHistoBkg.GetBinError(ibin))

                #bkg_lnchiS8000E10000_JET_GroupedNP_3__1up
                suffix = tree.strip("outTree")+upOrDown
                totalHistoBkgLnChi.SetName("bkg_lnchiS"+chiMassLow+"E"+chiMassHigh+"_"+suffix)
                totalHistoBkgLnChi.SetTitle("bkg_lnchiS"+chiMassLow+"E"+chiMassHigh+"_"+suffix)
                totalHistoBkgLnChi.Write("bkg_lnchiS"+chiMassLow+"E"+chiMassHigh+"_"+suffix)

            fout_bkg.Close()



#!/bin/bash

#pretty raw script to get AMI info, like cross section and filter efficiency. 
# to run pyAMI, you need to have a valid grid proxy, and set up ami
# this is done by the script but there are no checks done to avoid redoing it every time the script is run.
#
# the current version works, for the 8 TeV CI samples, but needs to be tailored towards whatever is desired (especially the combination of interference modes, Lambdas etc used in the channel numbering scheme)
#
#one could imagine parsing the pyAMI output for complaints too (a good candidate is "pyAMI exception: This logicalDatasetName was not found in AMI")

voms-proxy-init -voms atlas
setupATLAS
localSetupPyAMI

echo "Done setting up, running AMI"

let channelNumber=203208

for Lambda in {7,10} ; 
do 
    for mode in {plus,minus} ; do 
	for Jslice in {2..7} ; do
	    echo $channelNumber >> xsecFile.txt
	    ami show dataset info mc12_8TeV.${channelNumber}.Pythia8_AU2CT10_CI_${Lambda}TeV_${mode}LL_JZ${Jslice}.merge.NTUP_COMMON.e2941_a220_a205_r4540_p1575 | grep "crossSection\|FiltEff" >> xsecFile.txt
	    ((channelNumber++))
	done
    done 
done


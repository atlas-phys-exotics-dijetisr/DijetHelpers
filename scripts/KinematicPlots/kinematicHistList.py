#this generates an array of number, has a syntax that is equivalent to histogram making if endpoint is set to False
#http://docs.scipy.org/doc/numpy/reference/generated/numpy.linspace.html#numpy.linspace
from numpy import linspace  
from plotUtils import getChiBins, getMassBins

##Various bin arrays - may be moved to plotUtils.py if useful for more functions
MjjBinsForChi = [600, 800, 1200, 1600, 2000, 2600, 3200, 8000, 10000]

##the following could be achieved with a class too...
KinematicPlotsDic = {
  
  ###
  
  #final histogram naming convention: xAxisVariable_vs_yAxisVariable_noCuts/preSelection/massSelection/angularSelection  

  #example histogram, for reference
  #"mjj_vs_yStar_massSelection" : {

    #"histogramName" : "mjj_vs_yStar_massSelection", #this must be the same as the dictionary key, will get formed as we go
    #"activeBranches" : ["yStar", "mjj"], #list of branches that have to be activated to make this plot
    #"xVariableName" : "yStar", #variable that needs to be plotted on the x axis and will be in the name of the histogram
    #"xVariableFormula" : "abs(entry.yStar)", #branch name / branch combination that needs to be plotted on the x axis, including 'entry' as well
    #"xAxisBins" : linspace(0, 2, 100, False).tolist(), #python list containing low bin edges for x axis
    #"xAxisTitle" : "y*", #name and units for x axis
    #"yVariableName" : "mjj", #variable (branch name) that needs to be plotted on the y axis
    #"yVariableFormula" : "entry.mjj", #variable (branch name) that needs to be plotted on the y axis, including 'entry' as wel
    #"yAxisBins" : MjjBinsForChi, #python list containing low bin edges for y axis
    #"yAxisTitle" : "m_{jj} [GeV]", #name and units for y axis
  
  #},   

  #"mjj_vs_yStar" : {

    #"histogramName" : "mjj_vs_yStar", 
    #"activeBranches" : ["yStar", "mjj"], 
    #"xVariableName" : "yStar", 
    #"xVariableFormula" : "abs(entry.yStar)", 
    #"xAxisBins" : linspace(0, 2, 100, False).tolist(), 
    #"xAxisTitle" : "y*", 
    #"yVariableName" : "mjj", 
    #"yVariableFormula" : "entry.mjj", 
    #"yAxisBins" : MjjBinsForChi, 
    #"yAxisTitle" : "m_{jj} [GeV]", 
  
  #},   

  "mjj_vs_chi" : {

    "histogramName" : "mjj_vs_chi", 
    "activeBranches" : ["yStar", "mjj"],
    "xVariableName" : "chi",
    "xVariableFormula" : "exp(abs(2*entry.yStar))",
    "xAxisBins" : getChiBins(), 
    "xAxisTitle" : "#chi", 
    "yVariableName" : "mjj", 
    "yVariableFormula" : "entry.mjj", 
    "yAxisBins" : MjjBinsForChi, 
    "yAxisTitle" : "m_{jj} [GeV]", 
  
  },   

  "mjj_vs_etaLead" : {

    "histogramName" : "mjj_vs_etaLead", 
    "activeBranches" : ["mjj", "jet_eta"],
    "xVariableName" : "etaLead",
    "xVariableFormula" : "entry.jet_eta[0]",
    "xAxisBins" : linspace(-3, 3, 100, False).tolist(), 
    "xAxisTitle" : "#eta_{lead}", 
    "yVariableName" : "mjj", 
    "yVariableFormula" : "entry.mjj", 
    "yAxisBins" : getMassBins(),
    "yAxisTitle" : "m_{jj} [GeV]", 
  
  }   


}
#!/usr/bin/env python
###############################################
#  makeInputFiles.py                          #
#                                             #
#  purpose: makes histogram files that can    #
#           be used to run the statistical    #
#           machinery                         #
#                                             #
#           obtaining data-like statistics    #
#           histograms from the simulation    #
#           is not done here.  That is done   #
#           in ____.py                        #
#                                             #
#  output:                                    #
#        file with data histogram             #
#        file with background histogram       #
#        one file per signal                  #
#                                             #
###############################################

#from plotUtils import *
from ROOT import *
from array import array
import math
import os


def main( base, user, lumi, version ):

  print("\n")  
  print("Making an input file")
  print("User      : " + user )
  print("Lumi (fb) : " + str(lumi) )


  # find the files in this directory which match the strings user and sample
  allFiles = os.listdir( base )
  files = [ ]
  for file in allFiles:
      if not user   in file: continue
      files.append( base + "/" + file )
    
  if len(files) == 0:
    print("No files found for user - sample = " + user + " - " + sample)
    return
  
  # make directory for input files
  outputDir = "normHists_"+str(version)
  if not os.path.exists(outputDir):
    os.makedirs(outputDir)

  #lumiStr = str(lumi).replace(".","-")
  #tagWithLumi = tag + "_" + lumiStr + "fb"

  folder = "highPtJets"
  for file in files:
    print("Opening file: " + file)
    inFile = TFile.Open(file,"READ")
    if not inFile.cd(folder): 
      print(" Tree not found!")
      continue

    # need number of events from cutflow
    cutflowFileName = file.replace("hist","cutflow")
    cutflowFile = TFile.Open(cutflowFileName,"READ")
    cutflow = cutflowFile.Get("cutflow")
    events = cutflow.GetBinContent(1)
    print(" Cutflow Entries : " + str(events))
    cutflowFile.Close()


    dividedName = cutflowFileName.split(".")
    named = dividedName[5]

    sliceNum = (dividedName[4]).split("_")[3]

    print named, sliceNum
    outFile = TFile.Open(outputDir + "/scaled_" + named + "_"+sliceNum+ ".root","RECREATE")
    direc = inFile.Get(folder)
    listOfkeys = direc.GetListOfKeys()
    outlist = []
    for h in listOfkeys:
      hi= direc.Get(h.GetTitle())
      hi.Scale(1./events)
      outlist.append(hi)

#    inFile.Close()
    outFile.cd()
    for h in outlist:
      h.Write()
    outFile.Close()
    inFile.Close()
if __name__ == "__main__":
   base="gridOutput/hists/"
   user="mfrate"
# date on grid jobs - used to make the output directory
   version="2" 
# base is output directory of downloadAndMerge.py
# user is username of grid jobs
# 3rd input is the string to be used to search for the sample
# 4th name used for output (input) files
# 5th lumi (fb)
   main( base, user, 1, version )

   print "Finished makeNormFiles.py"


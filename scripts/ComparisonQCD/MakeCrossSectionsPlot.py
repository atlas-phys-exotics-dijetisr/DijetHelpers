#!/bin/python

##Helper functions

#this function will simply divide bin content by bin width
def makeCrossSectionHistogram (histo, normalization) :
  print histo
  xsecHisto = histo.Clone()
  histo.Print("all")
  for ibin in xrange(1, xsecHisto.GetNbinsX()) :
    binWidth = xsecHisto.GetBinLowEdge(ibin+1) - xsecHisto.GetBinLowEdge(ibin)
    #print xsecHisto.GetBinLowEdge(ibin+1)
    #print xsecHisto.GetBinLowEdge(ibin)
    #print "binwidth", binWidth
    xsecHisto.SetBinContent(ibin, histo.GetBinContent(ibin)/(binWidth*normalization))
    xsecHisto.SetBinError(ibin, histo.GetBinError(ibin)/(binWidth*normalization))

  return xsecHisto

import ROOT as ro
# import from parent directory
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import AtlasStyle
AtlasStyle.SetAtlasStyle()

# no idea why this is needed...? I did setup the release
filePythia15 = ro.TFile.Open("MC15/dataLikeHistograms.QCDDiJet.v10.root")
filePowheg15 = ro.TFile.Open("MC15/powheg_dijet_mass.root")
filePythia12 = ro.TFile.Open("DC14PythiaHerwigFrancesco/week21Histograms.0p01.ifb.pythia.01.root")
fileHerwig12 = ro.TFile.Open("DC14PythiaHerwigFrancesco/week21Histograms.0p01.ifb.herwig.01.root")

comparisonDic = {
  
  ##############
  ###MC12 Pythia
  #"Pythia 8 TeV, CT10 PDF, AU2 tune" : { "histogram" : ro.TFile.Open("8TeV/JZXW_totalhists_new.root").Get("combined_20"),#normalized to 20 inv fb
	      #"xsechistogram" : None, 
	      #"normalization" : 20, 
	      #"color" : ro.kRed,
	      #"marker" : ro.kCircle     
		#},
		
		      
  ##############
  ###MC15 Pythia
  #"Pythia 13 TeV, NNPDF2.3LO, A14 tune" : {"histogram" : ro.TFile.Open("MC15/dataLikeHistograms.QCDDiJet.v10.narrowBinning.root").Get("Nominal/mjj_Scaled_QCDDiJet_1fb"), 
  "Pythia 13 TeV, NNPDF2.3LO, A14 tune" : {"histogram" : filePythia15.Get("Nominal/mjj_Scaled_QCDDiJet_1fb"),
		    "xsechistogram" : None, 
		    "normalization" : 1, 
		    "color" : ro.kBlue,
            "marker" : ro.kOpenCircle,
            "order" : 3
		    },
  
  ##############
  ###MC15 Powheg
  "Powheg 13 TeV, NNPDF2.3LO, A14 tune" : {"histogram" : filePowheg15.Get("mjjDijetforTotal"),
		    "xsechistogram" : None, 
		    "normalization" : 0.000001,  # 1e-6 nb vs fb for cross section
		    "color" : ro.kGreen-4,
            "marker" : ro.kOpenTriangleDown,
            "order" : 4
		    },
  
  ##############
  ###DC14 Pythia
  #"Pythia 13 TeV, CT10 PDF, AU2 tune" : {"histogram" : ro.TFile.Open("DC14/input_PythiaDiJet_1fb.root").Get("mjj_PythiaDiJet_1fb"),#normalized to 1 inv fb 
		    #"xsechistogram" : None, 
		    #"normalization" : 1, 
		    #"color" : ro.kBlue,
		    #"marker" : ro.kFullCircle
		      #},#old narrow binning
 
  "Pythia 13 TeV, CT10 PDF, AU2 tune" : {"histogram" : filePythia12.Get("default/mjj_scaled"),
		    "xsechistogram" : None, 
		    "normalization" : 0.01, 
		    "color" : ro.kBlue,
            "marker" : ro.kFullCircle,
            "order" : 1
		    },

  ##############
  "Herwig 13 TeV, CTEQ6L1, EE4 tune" : {"histogram" : fileHerwig12.Get("default/mjj_scaled"),
		    "xsechistogram" : None, 
		    "normalization" : 0.01, 
		    "color" : ro.kRed,
		    "marker" : ro.kFullSquare, 
            "order" : 2
		    }

  }

c=ro.TCanvas("PythiaHerwigPowhegDijetComparison")

firstDrawn = True
#label=ro.TPaveText(0.2,0.2,0.45,0.45)
#label.Draw()

leg=ro.TLegend(0.18,0.2,0.55,0.37)
leg.SetHeader("Dijet Simulation Configurations")
leg.SetFillColor(0)
leg.SetLineColor(0)
leg.SetShadowColor(0)
leg.SetTextFont(42)

for i in range(1,5):
  for MCSample, histogramDic in comparisonDic.iteritems() :
    if histogramDic["order"] != i: continue
    print("Considering " + MCSample)
    #pick the histogram
    histogramDic["xsechistogram"] = makeCrossSectionHistogram(histogramDic["histogram"], histogramDic["normalization"])
    #format the histogram
    histogramDic["xsechistogram"].SetMarkerStyle(histogramDic["marker"])
    histogramDic["xsechistogram"].SetMarkerColor(histogramDic["color"])
    histogramDic["xsechistogram"].SetLineColor(histogramDic["color"])
    leg.AddEntry(histogramDic["xsechistogram"], MCSample, "P")
    if firstDrawn : 
      histogramDic["xsechistogram"].Draw()
      histogramDic["xsechistogram"].GetXaxis().SetRangeUser(1060, 9000)
      histogramDic["xsechistogram"].GetXaxis().SetTitleOffset(1.4)
      histogramDic["xsechistogram"].GetYaxis().SetTitleOffset(1.4)
      histogramDic["xsechistogram"].GetXaxis().SetTitle("m_{12} [GeV]")
      histogramDic["xsechistogram"].GetYaxis().SetTitle("Events/bin width, 1 fb^{-1}")
      firstDrawn = False
      leg.Draw("same")
    else : histogramDic["xsechistogram"].Draw("same")

AtlasStyle.ATLAS_LABEL(0.18,0.4)
AtlasStyle.myText(0.30, 0.4, ro.kBlack, "Internal")

c.SetLogy()
#c.SaveAs("PythiaHerwigDijetComparison.png")
#c.SaveAs("PythiaHerwigDijetComparison.pdf")
c.Print("PythiaHerwigDijetComparison.png","")
c.Print("PythiaHerwigDijetComparison.eps","")
c.Print("PythiaHerwigDijetComparison.pdf","")


#!/bin/python

from ROOT import *
from math import *
import array

import prettyPalette
import atlasStyleMacro

gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

# Initialize the xAOD infrastructure: 
if(not xAOD.Init().isSuccess()): print "Failed xAOD.Init()"

files = ["user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36045749/user.cyoung.6035908.EXT0._000001.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36045749/user.cyoung.6035908.EXT0._000002.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36045749/user.cyoung.6035908.EXT0._000003.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36046484/user.cyoung.6035908.EXT0._000004.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36046484/user.cyoung.6035908.EXT0._000006.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36046484/user.cyoung.6035908.EXT0._000009.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36046484/user.cyoung.6035908.EXT0._000019.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36047840/user.cyoung.6035908.EXT0._000005.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36047840/user.cyoung.6035908.EXT0._000008.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36047840/user.cyoung.6035908.EXT0._000010.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36047840/user.cyoung.6035908.EXT0._000011.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36047840/user.cyoung.6035908.EXT0._000012.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36047840/user.cyoung.6035908.EXT0._000013.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36047840/user.cyoung.6035908.EXT0._000015.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36047840/user.cyoung.6035908.EXT0._000016.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36054936/user.cyoung.6035908.EXT0._000014.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36054936/user.cyoung.6035908.EXT0._000017.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36054936/user.cyoung.6035908.EXT0._000018.DAOD_SUSY4.test.pool.root",
         "user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36054936/user.cyoung.6035908.EXT0._000014.DAOD_SUSY4.test.pool.root" ]

bunchIds = { 52: [974],
             53: [974],
             54: [974],
             55: [974],
             56: [974],
             57: [974],
             58: [974],
             59: [974],
             60: [974],
             61: [974],
             62: [974],
             63: [974],
             64: [974],
             65: [974],
             66: [974],
             67: [974],
             68: [974],
             69: [974],
             70: [974],
             71: [974],
             72: [974],
             73: [974],
             74: [974],
             75: [974],
             76: [974],
             77: [974],
             78: [974],
             79: [974],
             80: [974],
             81: [974],
             82: [974],
             83: [974],
             84: [974],
             85: [974],
             86: [974],
             87: [974],
             88: [974],
             89: [974],
             90: [974],
             91: [974],
             92: [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,974],
}
             
for i in range(93,93):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,974]
for i in range(94,95):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974]
    
for i in range(96,96):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974]
    
for i in range(97,98):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124]
    
for i in range(99,99):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124]
    
for i in range(100,100):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124,1270]
    
for i in range(101,101):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124,1270]
    
for i in range(102,103):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124,1270,1828]
    
for i in range(104,106):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124,1270,1828]
    
for i in range(107,107):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124,1270,1828,1868,1870,1872,1874,1876,1878,1880,1882,1884,1886,1888,1890,1892,1894,1896,1898,1900,1902,1904,1906,1908,1910,1912,1914,1916,1918,1920,1922,1924,1926,1928,1930,1932,1934,1936,1938,1948,1950,1952,1954,1956,1958,1960,1962,1964,1966,1968,1970,1972,1974,1976,1978,1980,1982,1984,1986,1988,1990,1992,1994,1996,1998,2000,2002,2004,2006,2008,2010,2012,2014,2016,2018]
    
for i in range(108,109):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124,1270,1828,1868,1870,1872,1874,1876,1878,1880,1882,1884,1886,1888,1890,1892,1894,1896,1898,1900,1902,1904,1906,1908,1910,1912,1914,1916,1918,1920,1922,1924,1926,1928,1930,1932,1934,1936,1938,1948,1950,1952,1954,1956,1958,1960,1962,1964,1966,1968,1970,1972,1974,1976,1978,1980,1982,1984,1986,1988,1990,1992,1994,1996,1998,2000,2002,2004,2006,2008,2010,2012,2014,2016,2018]
    
for i in range(110,110):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124,1270,1828,1868,1870,1872,1874,1876,1878,1880,1882,1884,1886,1888,1890,1892,1894,1896,1898,1900,1902,1904,1906,1908,1910,1912,1914,1916,1918,1920,1922,1924,1926,1928,1930,1932,1934,1936,1938,1948,1950,1952,1954,1956,1958,1960,1962,1964,1966,1968,1970,1972,1974,1976,1978,1980,1982,1984,1986,1988,1990,1992,1994,1996,1998,2000,2002,2004,2006,2008,2010,2012,2014,2016,2018]
    
for i in range(111,111):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124,1270,1828,1868,1870,1872,1874,1876,1878,1880,1882,1884,1886,1888,1890,1892,1894,1896,1898,1900,1902,1904,1906,1908,1910,1912,1914,1916,1918,1920,1922,1924,1926,1928,1930,1932,1934,1936,1938,1948,1950,1952,1954,1956,1958,1960,1962,1964,1966,1968,1970,1972,1974,1976,1978,1980,1982,1984,1986,1988,1990,1992,1994,1996,1998,2000,2002,2004,2006,2008,2010,2012,2014,2016,2018]
    
for i in range(112,112):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124,1270,1828,1868,1870,1872,1874,1876,1878,1880,1882,1884,1886,1888,1890,1892,1894,1896,1898,1900,1902,1904,1906,1908,1910,1912,1914,1916,1918,1920,1922,1924,1926,1928,1930,1932,1934,1936,1938,1948,1950,1952,1954,1956,1958,1960,1962,1964,1966,1968,1970,1972,1974,1976,1978,1980,1982,1984,1986,1988,1990,1992,1994,1996,1998,2000,2002,2004,2006,2008,2010,2012,2014,2016,2018]
    
for i in range(113,114):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124,1270,1828,1868,1870,1872,1874,1876,1878,1880,1882,1884,1886,1888,1890,1892,1894,1896,1898,1900,1902,1904,1906,1908,1910,1912,1914,1916,1918,1920,1922,1924,1926,1928,1930,1932,1934,1936,1938,1948,1950,1952,1954,1956,1958,1960,1962,1964,1966,1968,1970,1972,1974,1976,1978,1980,1982,1984,1986,1988,1990,1992,1994,1996,1998,2000,2002,2004,2006,2008,2010,2012,2014,2016,2018,2762,2764,2766,2768,2770,2772,2774,2776,2778,2780,2782,2784,2786,2788,2790,2792,2794,2796,2798,2800,2802,2804,2806,2808,2810,2812,2814,2816,2818,2820,2830,2832,2842,2844,2846,2848,2850,2852,2854,2856,2858,2860,2862,2864,2866,2868,2870,2872,2874,2876,2878,2880,2882,2884,2886,2888,2890,2892,2894,2896,2898,2900]
    
for i in range(115,498):
    bunchIds[i] = [80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150, 160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,224,226,228,230,270,272,274,276,278,280,282,284,286,288,290,292,294,296,298,300,302,304,306,308,310,312,314,316,318,320,322,324,326,328,330,332,334,336,338,340,350,352,354,356,358,360,362,364,366,368,370,372,374,376,378,380,382,384,386,388,390,392,394,396,398,400,402,404,406,408,410,412,414,416,418,420,974,976,978,980,982,984,986,988,990,992,994,996,998,1000,1002,1004,1006,1008,1010,1012,1014,1016,1018,1020,1022,1024,1026,1028,1030,1032,1034,1036,1038,1040,1042,1044,1054,1056,1058,1060,1062,1064,1066,1068,1070,1072,1074,1076,1078,1080,1082,1084,1086,1088,1090,1092,1094,1096,1098,1100,1102,1104,1106,1108,1110,1112,1114,1116,1118,1120,1122,1124,1270,1828,1868,1870,1872,1874,1876,1878,1880,1882,1884,1886,1888,1890,1892,1894,1896,1898,1900,1902,1904,1906,1908,1910,1912,1914,1916,1918,1920,1922,1924,1926,1928,1930,1932,1934,1936,1938,1948,1950,1952,1954,1956,1958,1960,1962,1964,1966,1968,1970,1972,1974,1976,1978,1980,1982,1984,1986,1988,1990,1992,1994,1996,1998,2000,2002,2004,2006,2008,2010,2012,2014,2016,2018,2762,2764,2766,2768,2770,2772,2774,2776,2778,2780,2782,2784,2786,2788,2790,2792,2794,2796,2798,2800,2802,2804,2806,2808,2810,2812,2814,2816,2818,2820,2830,2832,2842,2844,2846,2848,2850,2852,2854,2856,2858,2860,2862,2864,2866,2868,2870,2872,2874,2876,2878,2880,2882,2884,2886,2888,2890,2892,2894,2896,2898,2900,2952,2954,2956,2958,2960,2962,2964,2966,2968,2970,2972,2974,2976,2978,2980,2982,2984,2986,2988,2990,2992,2994,2996,2998,3000,3002,3004,3006,3008,3010,3020,3022,3032,3034,3036,3038,3040,3042,3044,3046,3048,3050,3052,3054,3056,3058,3060,3062,3064,3066,3068,3070,3072,3074,3076,3078,3080,3082,3084,3086,3088,3090]

firstInTrain = {}

for lumiBlock in bunchIds.keys():
    firsts = []
    last = 0
    bcids = bunchIds[lumiBlock]
    for i in range(0,len(bcids)):
        thisbcid = bcids[i]
        if (thisbcid-last)>2:
            firsts.append(thisbcid)
        last = thisbcid
    firstInTrain[lumiBlock] = firsts
    # print 'lb: ' + str(lumiBlock) + ' heads: ' +  str(firsts).strip('[]')


    

fout = TFile.Open("output.root","RECREATE")
CutFlowJetPt = TH1D( "CutFlowJetPt" , "" , 100 , 0 , 3000);
hBCID = TH1D( "BCID" , "" , 3000 , 0 , 3000 )
hBCID_1TeV = TH1D( "BCID_1TeV" , "" , 3000 , 0 , 3000 )

hET8x8VsOffline = TH2D( "ET8x8VsOffline" , "" , 100 , 0 , 1500 , 100 , 0 , 1500 )
hET8x8VsOfflineLeadingBunch = TH2D( "ET8x8VsOfflineLeadingBunch" , "" , 100 , 0 , 1500 , 100 , 0 , 1500 )
hET8x8VsOfflineTrailingBunch = TH2D( "ET8x8VsOfflineTrailingBunch" , "" , 100 , 0 , 1500 , 100 , 0 , 1500 )

hOfflineJetPtLeadingBunch = TH1D( "hOfflineJetPtLeadingBunch" , "" , 100 , 0 , 1500 )
hOfflineJetPtTrailingBunch = TH1D( "hOfflineJetPtTrailingBunch" , "" , 100 , 0 , 1500 )
hOfflineJetPtInSTLeadingBunch = TH1D( "hOfflineJetPtInSTLeadingBunch" , "" , 100 , 0 , 1500 )
hOfflineJetPtInSTTrailingBunch = TH1D( "hOfflineJetPtInSTTrailingBunch" , "" , 100 , 0 , 1500 )

for filename in files:

    file = TFile.Open(filename)
    tree = xAOD.MakeTransientTree( file , "CollectionTree" )
    print str(tree.GetEntries())

    # fin = TFile.Open("user.cyoung.DAOD_PhotTrig.data15_13TeV.00271516.f611_m1463.22July15.v3_EXT0.36054936/user.cyoung.6035908.EXT0._000018.DAOD_SUSY4.test.pool.root")

    #tree = TChain("CollectionTree")

# deltaRJetROI_vs_pTJetAll = TH2D( "deltaRJetROI_vs_pTJetAll" , "" , 100 , 410 , 1500, 100, 0, 1 );
# deltaRJetROI_vs_pTJet1 = TH2D( "deltaRJetROI_vs_pTJet1" , "" , 100 , 410 , 1500, 100, 0, 1 );
# deltaRJetROI_vs_pTJet2 = TH2D( "deltaRJetROI_vs_pTJet2" , "" , 100 , 410 , 1500, 100, 0, 1 );
# ROIPt_vs_pTJet1 = TH2D( "ROIPt_vs_pTJet1" , "" , 100 , 0 , 1500, 100 , 410 , 1500 );
# ROIPt_vs_pTJet2 = TH2D( "ROIPt_vs_pTJet2" , "" , 100 , 0 , 1500, 100 , 410 , 1500 );

# variableList = {}
# branchTypes = {}
# branchList = []
# for thisBranch in tree.GetListOfBranches():
#     branchName = thisBranch.GetName()
#     branchType = thisBranch.GetListOfLeaves().At(0).GetTypeName()
#     print branchName
#     print branchType
#     if "vector<float>" in branchType:
#       variableList[branchName] = std.vector('float')()
#     # elif "Int_t" in branchType:
#     #   variableList[branchName] = array.array('i', [0])
#     # elif "EventInfoAux.bcid" in branchName:
#     elif "UInt_t" in branchType:
#       variableList[branchName] = array.array('I', [0])
#     else:
#       print "Type of ", branchName, "(", branchType, ") Not recognized!!"
#       continue
#     branchList.append(branchName)
#     branchTypes[branchList[-1]] = branchType
#     tree.SetBranchStatus(branchList[-1], 1)
#     tree.SetBranchAddress(branchList[-1], variableList[branchList[-1]])


    nEvents = tree.GetEntries()
    count = 0
    while tree.GetEntry(count) and count<nEvents:

        count = count + 1
        if count%500==0:
            print count
            if count>1000:
                break
    
        eventInfo = tree.EventInfo
        bcid = eventInfo.bcid()
        lbn = eventInfo.lumiBlock()
        if bcid in (firstInTrain[lbn]):
            isFirstBunch = True
        else:
            isFirstBunch = False

        njets = tree.AntiKt4EMTopoJets.size()
        if njets<2:
            continue

        # find highest pt jets

        # #loop on the jets
        firstJet_tlv = TLorentzVector()
        secondJet_tlv = TLorentzVector()
        thirdJet_tlv = TLorentzVector()
        fourthJet_tlv = TLorentzVector()
        fifthJet_tlv = TLorentzVector()
        # njets = variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"].size()

        jet_tlv = TLorentzVector()
        for ijet in xrange(0,njets) :
            jet_tlv.SetPtEtaPhiM(tree.AntiKt4EMTopoJets.at(ijet).pt()/1000.,
                                 tree.AntiKt4EMTopoJets.at(ijet).eta(),
                                 tree.AntiKt4EMTopoJets.at(ijet).phi(),
                                 tree.AntiKt4EMTopoJets.at(ijet).m()/1000.)
            if fabs(jet_tlv.Eta())>2.8:
                continue
            if jet_tlv.Pt()>firstJet_tlv.Pt():
                firstJet_tlv = jet_tlv.Clone()
            elif jet_tlv.Pt()>secondJet_tlv.Pt():
                secondJet_tlv = jet_tlv.Clone()
            elif jet_tlv.Pt()>thirdJet_tlv.Pt():
                thirdJet_tlv = jet_tlv.Clone()
            elif jet_tlv.Pt()>fourthJet_tlv.Pt():
                fourthJet_tlv = jet_tlv.Clone()
            elif jet_tlv.Pt()>fifthJet_tlv.Pt():
                fifthJet_tlv = jet_tlv.Clone()

    #select three jet events triggered by j360
    # if variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"].size()<2: continue #make sure there are jets
    # if variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][0] < 410000  : continue
    # #basic sanity cut because there is junk
    # if variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][0] > 7000000 : continue
    # # CutFlowJetPt.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][0]/1000.)
    # firstJetMatch = False
    # secondJetMatch = False
    # thirdJetMatch = False

        matching_ROI_tlv = TLorentzVector()
        matching_ROI_dr = 999
        for iRoi in xrange(0,tree.LVL1JetRoIs.size()):
            ROI_tlv = TLorentzVector()
            ROI_tlv.SetPtEtaPhiE(tree.LVL1JetRoIs.at(iRoi).et8x8()/1000.,
                                 tree.LVL1JetRoIs.at(iRoi).eta(),
                                 tree.LVL1JetRoIs.at(iRoi).phi(),
                                 0)
            if ROI_tlv.Pt()<75:
                continue
        
            thisDeltaR = firstJet_tlv.DeltaR(ROI_tlv)
            # print str(thisDeltaR)
            if thisDeltaR<matching_ROI_dr:
                matching_ROI_dr = thisDeltaR
                matching_ROI_tlv = ROI_tlv.Clone()
          
        #check BCID
        bcid = eventInfo.bcid()
        hBCID.Fill(bcid)
        if firstJet_tlv.Pt()>1000.:
            hBCID_1TeV.Fill(bcid)


        if isFirstBunch:
            hOfflineJetPtLeadingBunch.Fill(firstJet_tlv.Pt())
        else:
            hOfflineJetPtTrailingBunch.Fill(firstJet_tlv.Pt())
            
        # print str(matching_ROI_dr)
        if matching_ROI_dr<0.3:
            hET8x8VsOffline.Fill(firstJet_tlv.Pt(),matching_ROI_tlv.Pt())
            if isFirstBunch:
                hET8x8VsOfflineLeadingBunch.Fill(firstJet_tlv.Pt(),matching_ROI_tlv.Pt())
                if matching_ROI_tlv.Pt()>=1023:
                    hOfflineJetPtInSTLeadingBunch.Fill(firstJet_tlv.Pt())
            else:
                hET8x8VsOfflineTrailingBunch.Fill(firstJet_tlv.Pt(),matching_ROI_tlv.Pt())
                if matching_ROI_tlv.Pt()>=1023:
                    hOfflineJetPtInSTTrailingBunch.Fill(firstJet_tlv.Pt())

            
    # print "jet pts: " + str(firstJet_tlv.Pt()) + "|" + str(secondJet_tlv.Pt())+"|" + str(thirdJet_tlv.Pt())+"|" + str(fourthJet_tlv.Pt()) + "|" + str(fifthJet_tlv.Pt())
          
    # #loop on the ROI
    # deltaRMin=999
    # bestRoi_tlv = TLorentzVector()
    # iBestRoi = None
    # iBestRoiMatch1 = None
    # iBestRoiMatch2 = None
    # iBestRoiMatch3 = None
    # iBestRoiMatch4 = None
    # iBestRoiMatch5 = None
    # for iRoi in xrange(0,variableList["LVL1JetRoIsAuxDyn.et8x8"].size()):
    #   ROI_tlv = TLorentzVector()
    #   ROI_tlv.SetPtEtaPhiE(variableList["LVL1JetRoIsAuxDyn.et8x8"][iRoi]/1000.,
    #                        variableList["LVL1JetRoIsAuxDyn.eta"][iRoi],
    #                        variableList["LVL1JetRoIsAuxDyn.phi"][iRoi], 0)
    #   if ROI_tlv.Pt()<75:
    #       continue
        
    #   thisDeltaR = thirdJet_tlv.DeltaR(ROI_tlv)
    #   if thirdJet_tlv.Pt()>0 and thisDeltaR < deltaRMin:
    #     deltaRMin = thisDeltaR
    #     iBestRoiMatch3 = iRoi
    #     iBestRoi = iRoi
    #     bestRoi_tlv = ROI_tlv.Clone()
    #   thisDeltaR = fourthJet_tlv.DeltaR(ROI_tlv)
    #   if fourthJet_tlv.Pt()>0 and thisDeltaR < deltaRMin:
    #     deltaRMin = thisDeltaR
    #     iBestRoiMatch4 = iRoi
    #     iBestRoi = iRoi
    #     bestRoi_tlv = ROI_tlv.Clone()
    #   thisDeltaR = fifthJet_tlv.DeltaR(ROI_tlv)
    #   if fifthJet_tlv.Pt()>0 and thisDeltaR < deltaRMin:
    #     deltaRMin = thisDeltaR
    #     iBestRoiMatch5 = iRoi
    #     iBestRoi = iRoi
    #     bestRoi_tlv = ROI_tlv.Clone()
    # #end loop on the ROI
    # # print " best ROI match: " + str(iBestRoiMatch3) + "|" + str(iBestRoiMatch4) + "|" + str(iBestRoiMatch5)
    # # print " best ROI pt: " + str(bestRoi_tlv.Pt()) + " drmin: " + str(deltaRMin) + " dr1: " + str(firstJet_tlv.DeltaR(bestRoi_tlv))

# end loop over files

fout.cd()
c=TCanvas()

# deltaRJetROI_vs_pTJet1.Draw("COLZ")
# deltaRJetROI_vs_pTJet1.Write()
# deltaRJetROI_vs_pTJet1.GetYaxis().SetTitle("#DeltaR to nearest 6x6 ROI")
# deltaRJetROI_vs_pTJet1.GetXaxis().SetTitle("Offline leading jet pT [GeV]")
# c.SaveAs("deltaRJetROI_vs_pTJet1.png")
#
# deltaRJetROI_vs_pTJet2.Draw("COLZ")
# deltaRJetROI_vs_pTJet2.Write()
# deltaRJetROI_vs_pTJet2.GetYaxis().SetTitle("#DeltaR to nearest 6x6 ROI")
# deltaRJetROI_vs_pTJet2.GetXaxis().SetTitle("Offline subleading jet pT [GeV]")
# c.SaveAs("deltaRJetROI_vs_pTJet2.png")
#
# deltaRJetROI_vs_pTJetAll.Draw("COLZ")
# deltaRJetROI_vs_pTJetAll.Write()
# deltaRJetROI_vs_pTJetAll.GetYaxis().SetTitle("#DeltaR to nearest 6x6 ROI")
# deltaRJetROI_vs_pTJetAll.GetXaxis().SetTitle("Offline jet pT [GeV]")
# c.SaveAs("deltaRJetROI_vs_pTJetAll.png")
#
# ROIPt_vs_pTJet1.Draw("COLZ")
# ROIPt_vs_pTJet1.Write()
# ROIPt_vs_pTJet1.GetYaxis().SetTitle("6x6 ROI et [GeV]")
# ROIPt_vs_pTJet1.GetXaxis().SetTitle("Offline jet pT [GeV]")
# c.SaveAs("ROIPt_vs_pTJet1.png")
#
# ROIPt_vs_pTJet2.Draw("COLZ")
# ROIPt_vs_pTJet2.Write()
# ROIPt_vs_pTJet2.GetYaxis().SetTitle("6x6 ROI et [GeV]")
# ROIPt_vs_pTJet2.GetXaxis().SetTitle("Offline jet pT [GeV]")
# c.SaveAs("ROIPt_vs_pTJet2.png")

CutFlowJetPt.Draw("COLZ")
CutFlowJetPt.Write()
CutFlowJetPt.GetXaxis().SetTitle("Offline jet pT [GeV]")
CutFlowJetPt.GetYaxis().SetTitle("Number of offline jets")
c.SaveAs("CutFlowJetPt.png")

hBCID.Write()
hBCID_1TeV.Write()
hBCID.Draw()
c.SaveAs("hBCID.pdf")
hBCID_1TeV.Draw()
c.SaveAs("hBCID_1TeV.pdf")

hET8x8VsOffline.Write()
hET8x8VsOffline.Draw("colz")
c.SaveAs("hET8x8VsOffline.pdf")

hET8x8VsOfflineLeadingBunch.Write()
hET8x8VsOfflineLeadingBunch.Draw("colz")
c.SaveAs("hET8x8VsOfflineLeadingBunch.pdf")

hET8x8VsOfflineTrailingBunch.Write()
hET8x8VsOfflineTrailingBunch.Draw("colz")
c.SaveAs("hET8x8VsOfflineTrailingBunch.pdf")

hOfflineJetPtLeadingBunch.Write()
hOfflineJetPtLeadingBunch.Draw()
c.SaveAs("hOfflineJetPtLeadingBunch.pdf")

hOfflineJetPtTrailingBunch.Write()
hOfflineJetPtTrailingBunch.Draw()
c.SaveAs("hOfflineJetPtTrailingBunch.pdf")

hOfflineJetPtInSTLeadingBunch.Write()
hOfflineJetPtInSTLeadingBunch.Draw()
c.SaveAs("hOfflineJetPtInSTLeadingBunch.pdf")

hOfflineJetPtInSTTrailingBunch.Write()
hOfflineJetPtInSTTrailingBunch.Draw()
c.SaveAs("hOfflineJetPtInSTTrailingBunch.pdf")


fout.Close()

        #LVL1JetRoIsAuxDyn.et6x6
#     nTriggerJets = 0
#     nOfflineJets = 0
#     for ijet in range(variableList["jet_calib_pt"].size()):
#         if variableList["jet_calib_pt"][ijet]/1000. > offline_jet.Pt():
#             nOfflineJets = nOfflineJets + 1
#             offline_jet.SetPtEtaPhiM( variableList["jet_calib_pt"][ijet]/1000. ,
#                                       variableList["jet_calib_eta"][ijet],
#                                       variableList["jet_calib_phi"][ijet],
#                                       variableList["jet_calib_m"][ijet]/1000. )
#
#     for ijet in range(variableList["trigger_jet_calib_pt"].size()):
#         if variableList["trigger_jet_calib_pt"][ijet]/1000. > trigger_jet.Pt() and fabs(offline_jet.DeltaPhi(trigger_jet))>2:
#             nTriggerJets = nTriggerJets + 1
#             trigger_jet.SetPtEtaPhiM( variableList["trigger_jet_calib_pt"][ijet]/1000. ,
#                                       variableList["trigger_jet_calib_eta"][ijet],
#                                       variableList["trigger_jet_calib_phi"][ijet],
#                                       variableList["trigger_jet_calib_m"][ijet]/1000. )
#     if offline_jet.Pt()<410:
#         continue
#     if trigger_jet.Pt()<410:
#         continue
#
#     filled = filled + 1
#     asymmetry = (offline_jet.Pt()-trigger_jet.Pt())/(offline_jet.Pt()+trigger_jet.Pt())
#     print asymmetry
#     print offline_jet.Pt()
#     # if filled>10:
#     #     break
#
#     hoff.Fill(offline_jet.Pt(),asymmetry)
#     htrig.Fill(trigger_jet.Pt(),asymmetry)
#
# c = TCanvas()
# hoff.Draw("colz")
# c.Print("output_off.pdf")
# c.Print("output_off.C")
# htrig.Draw("colz")
# c.Print("output_trig.pdf")
# c.Print("output_trig.C")

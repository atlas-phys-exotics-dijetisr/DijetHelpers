from ROOT import *
# import atlasStyleMacro

f = TFile.Open("output.root")

hLeading = f.Get("hOfflineJetPtLeadingBunch").Clone("hLeading")
hTrailing = f.Get("hOfflineJetPtTrailingBunch").Clone("hTrailing")

hLeading.Rebin(4)
hTrailing.Rebin(4)

hLeading.SetLineWidth(2)
hTrailing.SetLineWidth(2)
# hLeading.SetFillColor(kBlack)
hTrailing.SetFillColor(kRed)
hTrailing.SetLineColor(kRed)
hTrailing.SetMarkerColor(kRed)

canvas = TCanvas("c","",800,800)
pad1 = TPad("pad1","",0.00,0.3,1,1.0)

pad1.SetBottomMargin(0)
pad1.SetLeftMargin(0.15)
pad1.Draw()
pad1.cd()
pad1.SetLogy()
hTrailing.GetYaxis().SetTitle("Fraction of Distribution")
hTrailing.GetYaxis().SetTitleOffset(1.2)
hTrailing.DrawNormalized("he")
hLeading.DrawNormalized("same E")
# hLeading.GetYaxis().SetLabelSize(0.)
hTrailing.GetYaxis().SetLabelSize(0.);
axis = TGaxis(0, 20, -5, 220, 20,220,510,"");
axis.SetLabelFont(43);
axis.SetLabelSize(15);
axis.Draw();

legend = TLegend(0.6,0.7,0.9,0.9)
legend.AddEntry(hLeading,"first bunch")
legend.AddEntry(hTrailing,"trailing bunches")
legend.Draw()

text = TLatex()
#text.SetTextSize(0.05)
text.DrawLatex(900,0.03,"ATLAS Internal")
text.DrawLatex(900,0.015,"Run 271516")

canvas.cd()

pad2 = TPad("pad2","",0.00,0.0,1,0.3)
pad2.SetTopMargin(0)
pad2.SetBottomMargin(0.25)
pad2.SetLeftMargin(0.15)
pad2.Draw()
pad2.cd()

ratio = hLeading.Clone("ratio")
ratio.Scale(hTrailing.Integral()/hLeading.Integral())
ratio.SetLineColor(kBlack)
ratio.SetMinimum(0.)
ratio.SetMaximum(3)
ratio.Sumw2()
ratio.GetXaxis().SetLabelSize(15)
ratio.GetXaxis().SetTitle("Leading Jet p_{T} [GeV]")
ratio.GetXaxis().SetTitleSize(20);
ratio.SetStats(0)
ratio.Divide(hTrailing)
ratio.SetMarkerStyle(21)
ratio.Draw("ep")

ratio.SetTitle("");
ratio.GetYaxis().SetTitle("#frac{first bunch}{trailing bunches}");
ratio.GetYaxis().SetNdivisions(505);
ratio.GetYaxis().SetTitleSize(20);
ratio.GetYaxis().SetTitleFont(43);
ratio.GetYaxis().SetTitleOffset(1.55);
ratio.GetYaxis().SetLabelFont(43);
ratio.GetYaxis().SetLabelSize(15);

ratio.GetXaxis().SetTitleSize(20);
ratio.GetXaxis().SetTitleFont(43);
ratio.GetXaxis().SetTitleOffset(4.);
ratio.GetXaxis().SetLabelFont(43);
ratio.GetXaxis().SetLabelSize(15);

ratio.Fit("pol0","","",200,500)
fitLow = ratio.GetFunction("pol0").Clone()
# ratio.Fit("pol0","","",600,800)
# fitHigh = ratio.GetFunction("pol0").Clone()
fitLow.SetLineColor(kBlue)
fitLow.SetLineStyle(kSolid)
fitLow.Draw("same")
extrapHigh = fitLow.Clone("extrapHigh")
extrapHigh.SetRange(500,1500)
extrapHigh.SetLineStyle(kDashed)
extrapHigh.Draw("same")
# fitHigh.SetLineColor(kRed)
# fitHigh.SetLineStyle(kDashed)
# fitHigh.Draw("same")
pad2.Draw()

pad1.cd()

canvas.Print("ratio.pdf")
outfile = TFile("outfile.root","RECREATE")
outfile.cd()
ratio.SetDirectory(outfile)
canvas.Write()


#!/usr/bin/env python
import ROOT
import AtlasStyle


def makeTH1D(varName, sampleName, xnbins, xmin, xmax, xlabel="", ylabel=""):
  name = varName + "_" + sampleName
  title = varName
  hist = ROOT.TH1D( name, title, xnbins, xmin, xmax )
  hist.GetXaxis().SetTitle(xlabel)
  hist.GetYaxis().SetTitle(ylabel)
  hist.Sumw2()
  #hist.SetDirectory(0)
  return hist

def makeTH2D(varName, sampleName, xnbins, xmin, xmax, ynbins, ymin, ymax, xlabel="", ylabel=""):
  name = varName + "_" + sampleName
  title = varName
  hist = ROOT.TH2D( name, title, xnbins, xmin, xmax, ynbins, ymin, ymax )
  hist.GetXaxis().SetTitle(xlabel)
  hist.GetYaxis().SetTitle(ylabel)
  hist.Sumw2()
  #hist.SetDirectory(0)
  return hist

def makeTH2Dbinned(name, title, xnbins, xmin, xmax, ynbins, ybins, xlabel, ylabel):
  hist = ROOT.TH2D( name, title, xnbins, xmin, xmax, ynbins, ybins )
  hist.GetXaxis().SetTitle(xlabel)
  hist.GetYaxis().SetTitle(ylabel)
  hist.Sumw2()
  #hist.SetDirectory(0)
  return hist

def makeCanvas(logx, logy, logz):
  c1 = ROOT.TCanvas("c1","c1",1000,1000)
  if logx: c1.SetLogx()
  if logy: c1.SetLogy()
  if logz: c1.SetLogz()
  c1.SetMargin(0.17,0.15,0.15,0.08)
  return c1

def makeCanvasResiduals():
  c1 = ROOT.TCanvas("c1","c1", 1000, 1000)
  Pad1 = ROOT.TPad("pad1","pad1",0,0.33,1,1)
  Pad2 = ROOT.TPad("pad2","pad2",0,0,1,0.33)
  Pad1.SetBottomMargin(0.0001)
  Pad1.SetBorderMode(0)
  Pad1.SetRightMargin(0.15)
  Pad1.SetLeftMargin(0.17)
  Pad1.SetLogy()
  Pad1.SetLogz()
  Pad2.SetTopMargin(0.01)
  Pad2.SetBottomMargin(0.5)
  Pad2.SetBorderMode(0)
  Pad2.SetRightMargin(0.15)
  Pad2.SetLeftMargin(0.17)
  zeroLine = ROOT.TF1("zl0", "0", -50000, 50000 )
  zeroLine.SetTitle("")
  zeroLine.SetLineWidth(1)
  zeroLine.SetLineStyle(7)
  zeroLine.SetLineColor(ROOT.kBlack)
  return c1, Pad1, Pad2, zeroLine

  
AtlasStyle.SetAtlasStyle()
def EperLength(caloLayerName, isData, inDir, outDir, inFile, v):

  c1 = makeCanvas(False, True, True)

  layerList = [0,1,2,3,12,13,14]
  layerNames = ["EM Barrel PreSampler", "EM Barrel Layer 1", "EM Barrel Layer 2","EM Barrel Layer 3","Tile Barrel Layer 0", "Tile Barrel Layer 1", "Tile Barrel Layer 2"]
  layerLengths = [11., 90.627802691,337.219730944,42.152466368,321.283783784,878.175675675,385.54054054]
  interactionLengths = [0.036184211, 0.311184211, 1.157894737, 0.144736842, 1.5, 4.1, 1.8]
  alllayerNames =  ["PreSamplerB", "EMB1", "EMB2","EMB3","PreSamplerE", "EME1", "EME2","EME3","HEC0", "HEC1", "HEC2", "HEC3", "TileBar0", "TileBar1", "TileBar2", "TileGap1", "TileGap2", "TileGap3", "TileExt0", "TileExt1", "TileExt2", "FCAL0", "FCAL1", "FCAL2"]
  if not caloLayerName:
    layer = alllayerNames.index(caloLayerName)
  else: layer = caloLayerName
  if v: print "LAYER", layer


  f = ROOT.TFile.Open(inDir+inFile, "READ")
  if args.c: print "In File:", inFile
  keyList = []
  for key in f.GetListOfKeys():
    if "energyLayer" in key.GetName(): keyList.append(f.Get(key.GetName()))

  for hist in keyList:
    if "mc15" in inFile:
      etaMin = hist.GetName().split("_")[2]
      etaMin = etaMin.replace("p",".")
      etaMax = hist.GetName().split("_")[3]
      etaMax = etaMax.replace("p",".")
    else:
      etaMin = hist.GetName().split("_")[3]
      etaMin = etaMin.replace("p",".")
      etaMax = hist.GetName().split("_")[4]
      etaMax = etaMax.replace("p",".")
#energyLayersLength_Layerseta_2p1_2p8

    for i in range(7):
      hist.GetXaxis().SetBinLabel(i+1, layerNames[i])
    hist.GetXaxis().SetLabelSize(0.05)

    hist.GetXaxis().SetBinLabel(8, "N_{Segment}")
    hist.GetXaxis().SetBinLabel(9, "E[GeV]")
    if not isData:
      hist.GetXaxis().SetBinLabel(10, "E_{truth}[GeV]")
      hist.GetXaxis().SetBinLabel(11, "E_{Reconstructed}/E_{truth}")

    if etaMin != -1:
      title = "max energy deposit in" + str (caloLayerName) + "for"+ str(etaMin) + " < #eta < " + str(etaMax)

    if caloLayerName == "None":
      title = "No cuts"

    hist.SetTitle(title)
    hist.GetYaxis().SetTitle("E_{layer}/(E_{calo}L_{layer}) [mm^{-1}]")
  

    avg = hist.ProfileX("_px")

    avg.SetMarkerStyle(8)
    avg.SetMarkerSize(1.5)
    avg.SetDirectory(0)
    ROOT.gStyle.SetPalette(1)
    ROOT.gStyle.SetOptStat(0)

    c1.cd()

    AtlasStyle.ATLAS_LABEL(0.20,0.88)
    AtlasStyle.myText(0.20,0.82,"#sqrt{s}=13 TeV, Period C2-C4")

    hist.Draw("colz")
    avg.Draw("same")
    if v: print "EtaMin, EtaMax", etaMin, etaMax
    saveName = "EperLvsLayer_"+str(caloLayerName)+"_" + etaMin+"eta"+etaMax+"_"+str(isData)
    saveName = saveName.replace('.','p')
    saveName = saveName.replace('False', 'MC')
    saveName = saveName.replace('True', 'Data')
    saveName+=".png"
    saveName = outDir+saveName
    c1.SaveAs(saveName)
    print "making:", saveName


def makeResidual(inDir, outDir, caloLayerName, inMCFile, inDataFile, v ):
  layerNames = ["EM Barrel PreSampler", "EM Barrel Layer 1", "EM Barrel Layer 2","EM Barrel Layer 3","Tile Barrel Layer 0", "Tile Barrel Layer 1", "Tile Barrel Layer 2"]


  f_mc = ROOT.TFile.Open(inDir+inMCFile, "READ")
  if "Angular" in inDataFile: cuts = "Angular"
  if "Resonance" in inDataFile: cuts = "Resonance"
  if "Minimal" in inDataFile: cuts = "Minimal"

  if v: print "MC file:", inMCFile, "with selection:", cuts

  mcHists={}
  for key in f_mc.GetListOfKeys():
    if "energyLayer" in key.GetName():
      mc_tot = f_mc.Get(key.GetName()).Clone(key.GetName()+"mc_new")
      mc_tot.SetDirectory(0)
      if "0_0p8" in key.GetName():
        mcHists["0_0p8"] = mc_tot
      elif "0p8_1p2" in key.GetName():
        mcHists["0p8_1p2"] = mc_tot
      elif "1p2_1p5" in key.GetName():
        mcHists["1p2_1p5"] = mc_tot
      elif "1p5_1p8" in key.GetName():
        mcHists["1p5_1p8"] = mc_tot
      elif "1p8_2p1" in key.GetName():
        mcHists["1p8_2p1"] = mc_tot
      elif "2p1_2p8" in key.GetName():
        mcHists["2p1_2p8"] = mc_tot
      elif "2p8_3p1" in key.GetName():
        mcHists["2p8_3p1"] = mc_tot
      elif "3p1_4p9" in key.GetName():
        mcHists["3p1_4p9"] = mc_tot



  f_mc.Close()

  f_data = ROOT.TFile.Open(inDir+inDataFile, "READ")
  if "Angular" in inDataFile: cuts = "Angular"
  if "Resonance" in inDataFile: cuts = "Resonance"
  if "Minimal" in inDataFile: cuts = "Minimal"

  if v: print "Data file:",inDataFile, "with selection:", cuts

  dataHists = {}
  for key in f_data.GetListOfKeys():
    if "energyLayer" in key.GetName():
      data_tot = f_data.Get(key.GetName()).Clone(key.GetName()+"data_new")
      data_tot.SetDirectory(0)
      if "0_0p8" in key.GetName():
        dataHists["0_0p8"] = data_tot
      elif "0p8_1p2" in key.GetName():
        dataHists["0p8_1p2"] = data_tot
      elif "1p2_1p5" in key.GetName():
        dataHists["1p2_1p5"] = data_tot
      elif "1p5_1p8" in key.GetName():
        dataHists["1p5_1p8"] = data_tot
      elif "1p8_2p1" in key.GetName():
        dataHists["1p8_2p1"] = data_tot
      elif "2p1_2p8" in key.GetName():
        dataHists["2p1_2p8"] = data_tot
      elif "2p8_3p1" in key.GetName():
        dataHists["2p8_3p1"] = data_tot
      elif "3p1_4p9" in key.GetName():
        dataHists["3p1_4p9"] = data_tot


  f_data.Close()
 
  

  for histKey in dataHists.keys():
    c1, pad1, pad2, zeroline = makeCanvasResiduals()
    pad1.Draw()
    AtlasStyle.ATLAS_LABEL(0.20,0.92, 0.04)
    AtlasStyle.myText(0.20,0.87,"#sqrt{s}=13 TeV, Period C2-C4 ", 0.04)


    pad2.Draw()
    pad1.cd()

    etaMin = histKey.split("_")[0].replace("p",".")
    etaMax = histKey.split("_")[1].replace("p",".")

    if v: print "EtaMin, EtaMax", etaMin, etaMax
    data_tot = dataHists[histKey]
    mc_tot = mcHists[histKey]


    if mc_tot.Integral() == 0:
      scaleFactor = 1.
    else:
      scaleFactor = data_tot.Integral() / mc_tot.Integral()
    mc_tot.Scale(scaleFactor)
    mc_prof = mc_tot.ProfileX("_px")
    data_prof = data_tot.ProfileX("_dpx")
    ROOT.gStyle.SetPalette(1)
    mc_prof.SetMarkerStyle(24)
    mc_prof.SetMarkerSize(1.5)
    mc_prof.SetDirectory(0)  
    ROOT.gStyle.SetOptStat(0)

    mc_tot.GetXaxis().SetLabelSize(0.)
    mc_tot.GetXaxis().SetTitleSize(0.)
    mc_tot.GetYaxis().SetRange(2,299)
    mc_tot.SetMinimum(1.00001)
    
    mc_tot.GetYaxis().SetTitleOffset(1)
    mc_tot.GetYaxis().SetTitleSize(0.07)
    mc_tot.GetYaxis().SetLabelSize(0.07)
    mc_tot.GetZaxis().SetLabelSize(0.07)
    mc_tot.SetStats(0)
    mc_tot.Draw("colz")  
    mc_prof.Draw("same")
  
    data_prof.SetLineColor(1)
    data_prof.SetMarkerStyle(21)
    data_prof.SetMarkerSize(1.5)
    data_prof.Draw("same")
    leg = ROOT.TLegend(.2, .65, .4, .8)
    leg.SetTextFont(62)
    leg.SetFillStyle(0)
    leg.AddEntry(mc_prof, "MC")
    leg.AddEntry(data_prof, "data")
    leg.SetEntrySeparation(0.001)
    leg.Draw("same")


    pad2.SetTopMargin(0)
    pad2.SetFillColor(0)
    pad2.SetFillStyle(0)
    pad2.cd()
    results = makeTH1D("results", "", 11, 0, 11, "")

    for bin in range(10):
      B = mc_prof.GetBinContent(bin)
      Berr = mc_prof.GetBinError(bin)
      D = data_prof.GetBinContent(bin)
    
      if B != 0: # Protect against divide-by-zero
        frac = (D-B)/B
      # fracErr = math.fabs(Berr/B)
        results.SetBinContent(bin,frac)
        results.SetBinError(bin,0)

    results.SetBinContent(10,0)
    results.SetBinContent(11,0)

    for j in range(7):
      results.GetXaxis().SetBinLabel(j+1, layerNames[j])

    results.GetXaxis().SetBinLabel(8, "N_{segments}")
    results.GetXaxis().SetBinLabel(9, "E [GeV]")
    results.GetXaxis().SetBinLabel(10, "E_{reco} [GeV]")
    results.GetXaxis().SetBinLabel(11, "E_{reco}/E_{truth} [GeV]")
    results.GetYaxis().SetNdivisions(5)
    results.SetMarkerStyle(8)
    results.SetMarkerSize(1.8)
    results.GetYaxis().SetLabelSize(0.12)
    results.GetXaxis().SetLabelSize(0.15)
    results.GetYaxis().SetRangeUser(results.GetMinimum()-(results.GetMaximum()-results.GetMinimum())/2., results.GetMaximum()+(results.GetMaximum()-results.GetMinimum())/2.-0.0001)

    results.Draw("p")
    zeroline.Draw("same")
    c1.cd()


    saveName = "residuals_"+str(caloLayerName)+"_" + etaMin+"eta"+etaMax+ cuts
    saveName = saveName.replace('.','p')
    saveName+=".pdf"
    saveName = outDir+saveName
    c1.Print(saveName,"pdf")
    print "making:", saveName

def allLayers(inDir, inFile, outDir, isData, v):

  c1 = makeCanvas(False, False, True)
  layerHists = []

  elayernames =  ["PreSamplerB", "EMB1", "EMB2","EMB3","PreSamplerE", "EME1", "EME2","EME3","HEC0", "HEC1", "HEC2", "HEC3", "TileBar0", "TileBar1", "TileBar2", "TileGap1", "TileGap2", "TileGap3", "TileExt0", "TileExt1", "TileExt2", "FCAL0", "FCAL1", "FCAL2"]


  f = ROOT.TFile.Open(inDir+inFile, "READ")
  if v: print "input file:", inFile
  for key in f.GetListOfKeys():
    if key.GetName().split("_")[1] in elayernames:
      newHist = f.Get(key.GetName()).Clone("new_"+key.GetName().split("_")[1])
      newHist.SetDirectory(0)
      layerHists.append(newHist)

  f.Close()

  alllayersHist = makeTH2D("AllLayers", " ", layerHists[0].GetNbinsX(), 0, 1,24,0,24, "Fraction of Jet E")
  for hist in range(24):
    for bin in range(layerHists[hist].GetNbinsX()):
      alllayersHist.SetBinContent(bin, hist+1, layerHists[hist].GetBinContent(bin))
    
    histname = layerHists[hist].GetName()
    alllayersHist.GetYaxis().SetBinLabel(hist+1,histname.split("_")[1])


  alllayersHist.SetDirectory(0)
  ROOT.gStyle.SetOptStat(0)
  ROOT.gStyle.SetPalette(1)
  c1.cd()
  alllayersHist.Draw("colz")
  if isData: 
    c1.SaveAs(outDir+"allLayers_data.pdf")
    print "making:", outDir+"allLayers_data.pdf"
  else: c1.SaveAs(outDir+"allLayers_MC.pdf")
    print "making:", outDir+"allLayers_MC.pdf"



if __name__ == "__main__":
    main()










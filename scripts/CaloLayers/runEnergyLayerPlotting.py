from plotEnergyLayers import *
import os
import argparse
import AtlasStyle
import ROOT

parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-v", dest='v', action='store_true', default=False, help="Verbose mode for debugging")
parser.add_argument("--inDirTag", dest="inDirTag", default="", help="input histogram directory name contains this tag")
parser.add_argument("--outDir", dest="outDir", default="",help="output plot directory")
parser.add_argument("--plotTypes", dest"plotTypes", default="all", help="all, compare, single, allLayers. compare: MC with average data overlaid, single: MC and data separately, allLayers: 2D distribution of energy deposit vs calorimeter later for MC and data")
args = parser.parse_args()


AtlasStyle.SetAtlasStyle()

alllayerNames =  ["PreSamplerB", "EMB1", "EMB2","EMB3","PreSamplerE", "EME1", "EME2","EME3","HEC0", "HEC1", "HEC2", "HEC3", "TileBar0", "TileBar1", "TileBar2", "TileGap1", "TileGap2", "TileGap3", "TileExt0", "TileExt1", "TileExt2", "FCAL0", "FCAL1", "FCAL2"]    

for direc in [d for d in os.listdir("./histograms") if args.inDirTag in d]:
    if args.v: print "Directory:",direc

    if [layer for layer in alllayerNames if layer in direc.split("_")]:
        caloLayerName = layer
    else:
        caloLayerName = "None"

    mergedDataFiles = [f for f in os.listdir("./histograms/"+direc) if ("mergedFiles.root" in f and "data" in f)]
    mergedMCFiles = [f for f in os.listdir("./histograms/"+direc) if ("mergedFiles.root" in f and "MC15" in f)]
    mergedShowerFiles = [f for f in os.listdir("./histograms/"+direc) if ("mergedFiles.root" in f and "shower" in f)]
    #outDir = "./plots/DijetConfig1_PeriodC2C3C4_noEM_v02/"
    if args.outDir: outDir = "./plots/"+args.outDir+"/"
    else: outDir = "./plots/"+direc+"/"
    if args.v: print "Merged Data, MC files:", mergedDataFiles, mergedMCFiles
    for fi, f in enumerate(mergedDataFiles):
        dirc = "./histograms/"+direc+"/"
        tags = f.split("_")[1]
        if args.plotType == "single" or args.plotType == "all":
            if args.v: print("Making Energy/Length vs layer plots")
            if args.v: print("MC")
            EperLength(caloLayerName, False, dirc, outDir, mergedMCFiles[fi], args.v)
            if args.v: print("DATA")
            EperLength(caloLayerName, True,  dirc, outDir, f, args.v)
        if args.plotType == "compare" or args.plotType == "all":
            if args.v: print("RESIDUAL")
            makeResidual(dirc, outDir, caloLayerName, mergedMCFiles[fi], f, args.v)
        if args.plotType == "allLayers" or args.plotType == "all":
            if args.v: print("2D plots")
            if args.v: print("MC")
            allLayers(dirc, mergedMCFiles[fi], outDir, False, args.v)
            if args.v: print("Data")
            allLayers(dirc, f, outDir, True, args.v)
    
    

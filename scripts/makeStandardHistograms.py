#!/usr/bin/env python

import os, sys, glob, copy, subprocess
import time
import argparse
import plotUtils
import AtlasStyle
import ROOT
from math import sqrt, log, isnan, isinf
import multiprocessing as mp

##################################################
##################################################
# THIS script is used to loop over a set of files
# and call the standard histogram filling script
# after all the files are done, plots can be made
# creating and filling the histograms is optional
##################################################
##################################################

#
#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='b', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("-v", dest='v', action='store_true', default=False, help="Verbose mode for debugging")
#parser.add_argument("--powheg", dest='powheg', action='store_true', default=False, help="If running on powheg")
parser.add_argument("--fillHists", dest='fillHists', action='store_true', default=False, help="Call plotNtuple to fill hists")
parser.add_argument("--pathToTrees", dest='pathToTrees', default="./gridOutput/tree",
                    help="Path to the input trees")
parser.add_argument("--tags", dest='tags', default="Pythia8+jetjet",
                    help="Plus (+) seperated list of tags. Input files if they contain all the tags")
parser.add_argument("--outputTag", dest='outputTag', default="newStudy",
                    help="Name of histograms to be created")

parser.add_argument("--studies", dest='studies', default="", help=" Comma seperated list of histogram configurations to include ")
parser.add_argument("--lumi", dest='lumi', type=float, default=1.0, help="Desired Luminosity")
parser.add_argument("--condor", dest='condor', action='store_true',default=False, help="Submit to Condor")

parser.add_argument("--skipTags", dest='skipTags', default="JZ0", help="Tags in files that you don't want to run over")
#------------------------------------------------------------------------------------------------------------------------
#  ARGUMENTS FOR PLOTNTUPLE.PY....  They are active in this script, but generally inputed for use in plotNtuple.py
#------------------------------------------------------------------------------------------------------------------------

#parser.add_argument("--verbose", dest='verbose', action='store_true', default=False, help="VERBOSE!!")
#parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
#------------------------------------------
parser.add_argument("--file", dest='file', default="", help="Input file name which contains a tree")
parser.add_argument("--tree", dest='tree', default="outTree", help="Name of tree")
parser.add_argument("--maxEvents", dest='maxEvents', default=-1, help="Max number of events in tree to process")
parser.add_argument("--outDir", dest='outDir', default="./histograms", help="Name of output directory for ROOT file containing histograms")
parser.add_argument("--powheg", dest='powheg', action='store_true', default=False, help="If running on powheg")
parser.add_argument("--outFileName", dest='outFileName', default="", help="output file name which contains filled histograms")

parser.add_argument("--applyRW", dest='RWFile', default="", help="File for applying reweighting")
parser.add_argument("--truthOnlyInput", dest='truthOnlyInput', action='store_true', default=False, help="Run on MC input which is truth level only")

## adding all branches and the assoicated parameters
parser.add_argument("--plotAll", dest='plotAll', action='store_true', default=False, help="Plot all tree entries.  This will not plot vector branches")
parser.add_argument("--plotAllVector", dest='plotAllVector', action='store_true', default=False, help="Plot all tree entries for branches of vectors")
parser.add_argument("--fillJetPlots", dest='fillJetPlots', action='store_true', default=False, help="Fill plots for individual jets (slow!)")
parser.add_argument("--nJetToPlot", dest='nJetToPlot', default=100, type=int, help="Number of jets to put in jet plots (-1 is all)")
parser.add_argument("--nBins", dest='nBins', default=100, type=int, help="Default number of bins to be used for 1D histograms added on the fly")
#------------------------------------------
## truning on and off specific plot sets
parser.add_argument("--basicInfo", dest='basicInfo', action='store_true', default=False, help="runNumber, lumiBlock, etc.")
parser.add_argument("--typicalVariables", dest='typicalVariables', action='store_true', default=False, help="mjj, yStar, yBoost, NPV, njets, etc. (all non-vectors!)")
parser.add_argument("--chiVariables", dest='chiVariables', action='store_true', default=False, help="chi variables")
parser.add_argument("--sensitivity", dest='sensitivity', action='store_true', default=False, help="mjj and chi sensitivity to a user inputed cut specified elsewhere")
parser.add_argument("--cleaning", dest='cleaning', action='store_true', default=False, help="plot cleaning variables")
parser.add_argument("--punchThrough", dest='punchThrough', action='store_true', default=False, help="fill punch through study")
parser.add_argument("--kinematics", dest='kinematics', action='store_true', default=False, help="fill kinematic variables")
parser.add_argument("--energyLayers", dest='energyLayers', action='store_true', default=False, help="energyLayers")
parser.add_argument("--truth", dest='truth', action='store_true', default=False, help="fill truth jet kinematic variables")
parser.add_argument("--wideJets", dest='wideJets', action='store_true', default=False, help="wide jet variables")

#parser.add_argument("--truthDetail", dest='truthDetail', action='store_true', default=False, help="fill detailed truth jet variables, i.e. flavTag Truth, etc.")
parser.add_argument("--flavTag", dest='flavTag', action='store_true', default=False, help="flavor tags for jets")
parser.add_argument("--jetConstituents", dest='jetConstituents', action='store_true', default=False, help="Include plots of jet constituents")
parser.add_argument("--jetCaloQuant", dest='jetCaloQuant', action='store_true', default=False, help="Include plots of calorimeter based quantities in jets")
parser.add_argument("--jetTrackQuant", dest='jetTrackQuant', action='store_true', default=False, help="Include plots of composite track quantities in jets")
#parser.add_argument("--", dest='', action='store_true', default=False, help="")
parser.add_argument("--jetTrackQuantAll", dest='jetTrackQuantAll', action='store_true', default=False, help="Include plots of ALL composite track quantities in jets")  #a few more plots than jetTrackQuant... do we need both?

parser.add_argument("--do_massPartonPlots", dest='do_massPartonPlots', action='store_true', default=False, help="Include plots of mjj split by incoming and outgoing parton")


#### CUT OPTIONS HERE FOR EASY CONFIGURABILITY #####

parser.add_argument("--resonanceCuts",        dest='resonanceCuts',    action='store_true', default=False,  help="apply resonance analysis cuts")
parser.add_argument("--angularCuts",        dest='angularCuts',    action='store_true', default=False,  help="apply angular analysis cuts")
parser.add_argument("--minimalCuts",        dest='minimalCuts',    action='store_true', default=False,  help="apply minimal analysis cuts")

####### EVENT LEVEL CUTS #####
parser.add_argument("--cut_yStarMin",   dest='cut_yStarMin',   type=float, default=-1, help="Minimum yStar cut")
parser.add_argument("--cut_yStarMax",   dest='cut_yStarMax',   type=float, default=-1, help="Maximum yStar cut")
parser.add_argument("--cut_yBoost",  dest='cut_yBoost',  type=float, default=-1,  help="yBoost cut")
parser.add_argument("--cut_NJet",    dest='cut_NJet',    type=float, default=-1,  help="N Jet cut")
parser.add_argument("--cut_LJetPt",  dest='cut_LJetPt',  type=float, default=-1,  help="Leading Jet pT [GeV]")
parser.add_argument("--cut_LJetPtMax",  dest='cut_LJetPtMax',  type=float, default=-1,  help="Max leading Jet pT [GeV]")
parser.add_argument("--cut_NLJetPt", dest='cut_NLJetPt', type=float, default=-1,  help="Next-to-Leading Jet pT [GeV]")
parser.add_argument("--cut_mjjMin",  dest='cut_mjjMin',  type=float, default=-1,  help="Minimum dijet mass [GeV]")
parser.add_argument("--cut_mjjMax",  dest='cut_mjjMax',  type=float, default=-1,  help="Maximum dijet mass [GeV]")
parser.add_argument("--cut_NPVMax",  dest='cut_NPVMax',  type=float, default=-1,  help="Maximum NPV")
parser.add_argument("--cut_AvgMuMin",  dest='cut_AvgMuMin',  type=float, default=0.1,  help="Minimum NPV - DEFAULT at 0.1 for now")
##############################
####### JET LEVEL CUTS - for plots of individual jet quantities #####
parser.add_argument("--cut_jetPtMin",  dest='cut_jetPtMin',    type=float, default=-1,  help="for jet plots: Minimum jet pT cut [GeV]")
parser.add_argument("--cut_jetPtMax",  dest='cut_jetPtMax',    type=float, default=-1,  help="for jet plots: Maximum jet pT cut [GeV]")
parser.add_argument("--cut_jetEtaMin",  dest='cut_jetEtaMin',  type=float, default=-1,  help="for jet plots: Minimum jet pT cut [GeV]")
parser.add_argument("--cut_jetEtaMax",  dest='cut_jetEtaMax',  type=float, default=-1,  help="for jet plots: Maximum jet pT cut [GeV]")
parser.add_argument("--cut_jetYMin",    dest='cut_jetYMin',    type=float, default=-1,  help="for jet plots: Minimum jet pT cut [GeV]")
parser.add_argument("--cut_jetYMax",    dest='cut_jetYMax',    type=float, default=-1,  help="for jet plots: Maximum jet pT cut [GeV]")
parser.add_argument("--cut_jetDetEtaMin",    dest='cut_jetDetEtaMin',    type=float, default=-1,  help="for jet plots: Minimum jet pT cut [GeV]")
parser.add_argument("--cut_jetDetEtaMax",    dest='cut_jetDetEtaMax',    type=float, default=-1,  help="for jet plots: Maximum jet pT cut [GeV]")
parser.add_argument("--cut_jetNumTrkPt500PVMin",    dest='cut_jetNumTrkPt500PVMin',    type=float, default=-1,  help="for jet plots: Min number of tracks in jet (pT>500MeV)")
parser.add_argument("--cut_jetMV2c20Min",    dest='cut_jetMV2c20Min',    type=float, default=-1,  help="for jet plots: Min MV2c20 cut")
parser.add_argument("--cut_jetEMin", dest='cut_jetEMin', type=float, default=-1, help="for jet plots: Minimum jet E cut [GeV]")
parser.add_argument("--cut_jetEMax", dest='cut_jetEMax', type=float, default=-1, help="for jet plots: Maximum jet E cut [GeV]")

parser.add_argument("--cut_caloLayerName", dest='cut_caloLayerName', default="None", help="energy layer with maximum energy deposit")


parser.add_argument("--cut_massDropMin",    dest='cut_massDropMin',    type=float, default=-1,  help="for jet plots: Minimum massDrop cut [GeV]")
parser.add_argument("--cut_massDropMax",    dest='cut_massDropMax',    type=float, default=-1,  help="for jet plots: Maximum massDrop cut [GeV]")
parser.add_argument("--cut_pTjjMin",   dest='cut_pTjjMin',   type=float, default=-1, help="Minimum pTjj cut")
parser.add_argument("--cut_pTjjMax",   dest='cut_pTjjMax',   type=float, default=-1, help="Maximum pTjj cut")
parser.add_argument("--cut_NJetMax",    dest='cut_NJetMax',    type=float, default=-1,  help="N Jet Max cut")
parser.add_argument("--cut_wideJetdR",    dest='cut_wideJetdR',    type=float, default=-1,  help="wide jet dR cut")

parser.add_argument("--noCleanEvent",          dest='cleanEvent',    action='store_true', default=False,  help="DO NOT apply LooseBad cleaning at the event level")
parser.add_argument("--cleanJet",          dest='cleanJet',    action='store_true', default=False,  help="apply LooseBad cleaning jet by jet")


parser.add_argument("--tileGap",        dest='tileGap',    action='store_true', default=False,  help="apply tileGap clean cut")
parser.add_argument("--lbn",            dest='lbn',    action='store_true', default=False,  help="specific cut for 265545/73 data")
parser.add_argument("--mbts",           dest='mbts',    action='store_true', default=False,  help="require MBTS Trigger")
parser.add_argument("--rd0filled",      dest='rd0filled',    action='store_true', default=False,  help="require RD0_FILLED Trigger")
parser.add_argument("--triggers",   dest='triggers',  default='', help='List of triggers to cut on')
parser.add_argument("--muRescale",    dest='muRescale',    type=float, default=1,  help="rescale mu (should only be done in data)")
parser.add_argument("--mcClean",          dest='mcClean',    action='store_true', default=False,  help="require leading jet to be truth matched - pile-up generation thing")
parser.add_argument("--truthMatch",          dest='truthMatch',    action='store_true', default=False,  help="require truth matched jets")
parser.add_argument("--puMatch",             dest='puMatch',    action='store_true', default=False,  help="require pu matched jets")
parser.add_argument("--truthB",          dest='truthB',    action='store_true', default=False,  help="require truth labeled b-jets")
parser.add_argument("--truthC",          dest='truthC',    action='store_true', default=False,  help="require truth labeled b-jets")
parser.add_argument("--truthL",          dest='truthL',    action='store_true', default=False,  help="require truth labeled b-jets")
##############################

parser.add_argument('--applyNLOCorrection', dest='applyNLOCorrection', action='store_true', default=False, help='apply k-factors for NLO corrections')
parser.add_argument('--applyEWCorrection', dest='applyEWCorrection', action='store_true', default=False, help='apply k-factors for EW corrections')
parser.add_argument('--correctLargeEtaJets', dest='correctLargeEtaJets', default = -1, help='Value by which to multiply pT of jets outside of eta 1.8')

#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------


args = parser.parse_args()


def main():

  AtlasStyle.SetAtlasStyle()


## CHANGED THIS TO RUN OVER ALL EVENTS 1x INSTEAD OF ONCE PER STUDY

  if len(args.studies) == 0:
    print "Error, no studies were requested"
    exit(1)
  args.studies = args.studies.split(',')

  args.studies.append("fillJetPlots")
  callPlotNtuple()

####################################################
##        FUNCTIONS TO CALL PLOTNTUPLE.PY         ##
####################################################

def callPlotNtuple():
  #------------------------------------------
  ## get the files in path which match tags ##
  ## get list of merged output files ##
  tags = args.tags.split('+')
  inputFiles = plotUtils.getMergedTreeFileList(args.pathToTrees, tags)

  ## make sure we have found some files ##
  if len(inputFiles) == 0:
    raise SystemExit('\n***EXIT*** no files found for tags: %s'%(tags))

  print '\nmatching input files:'
  for file in inputFiles:
    print ' %s'%file
  print("\n")

  #------------------------------------------
  # loop over files and call plotNtule.py for each
  pids = []
  logFiles = []
  NCORES = mp.cpu_count()
  if not args.condor:os.system("rm -f .cond_sub")
  print 'filling histograms using the %s cores of this computer'%NCORES
  logDir = args.outDir + "/" + "runlogs"
  if not os.path.exists(logDir):
    os.makedirs(logDir)

  outFileNamePrefix = args.outDir + "/" + args.outputTag #  = "." + sampleName +  ".root" added in callPlotNtuple
  for inputFile in inputFiles:
    if any(thisTag in inputFile for thisTag in args.skipTags.split('+')):
      continue
    while len(pids) >= NCORES:
      wait_completion(pids, logFiles)
    if args.v: print inputFile

    sampleName = os.path.basename(inputFile).split('.')
    if len(sampleName) < 6:
      print "Error, we expect input files to have more than 6 fields of information, where a field is speerated by '.'"
      print "For example: user.jdandoy.data15_comm.00265545.physics_MinBias.None.JetInputs_DataMay23_v0_20150527_tree.root"
      sampleName = "None"

    else:
      #sampleName = '.'.join( sampleName[3:6] )
      if sampleName[len(sampleName)-2].find("tree")>-1:     #---MERGE CHANGE
        sampleName = '.'.join( sampleName[2:5] )
      else:                                                 #---MERGE CHANGE
        tempSampleName = '.'.join( sampleName[2:5] )        #---MERGE CHANGE
        sampleName = '.'.join( [tempSampleName, str(sampleName[len(sampleName)-2])] )    #---MERGE CHANGE


    if args.v: print sampleName
    logFile = logDir + "/" + sampleName + "run"+args.outputTag+".log"

    sendStringBase  = 'python -u DijetHelpers/scripts/plotNtuple.py '
    sendStringBase += '--file '+inputFile+' '
    sendStringBase += '--outFileName '+outFileNamePrefix+'.'+sampleName+'.root '
    sendString = sendStringBase + getPlotNtupleFlags()

    print ("\n Starting job " + sendString + "\n")

    if not args.condor:
      res = submit_local_job(sendString, logFile)
      pids.append(res[0])
      logFiles.append(res[1])
    else:
      submit_condor_job(sendString,logFile)
  wait_all(pids, logFiles)
  if args.condor: os.system("condor_submit .cond_sub")

  for f in logFiles:
    f.close()

#----------------------------------------------------
def getPlotNtupleFlags():

    # These args are for makeStandardPlots only and should not be passed to plotNtuply
    removeArgs = ['pathToTrees', 'tags', 'studies', 'outputTag','condor','skipTags']

    passThroughCommands = ' '
    iArg = 1
    while iArg < len(sys.argv):
      if any( thisArg in sys.argv[iArg] for thisArg in removeArgs):
        if 'condor' in sys.argv[iArg]:iArg += 1 #only skip one since condor has no extra argument
        else:iArg += 2 #skip the next arguement as welli
        continue

      passThroughCommands += sys.argv[iArg]+' '
      iArg += 1

    for flag in args.studies:
      if flag not in str(args):
        print "ERROR, requested flag could not be found in plotNtuple.py ... exiting"
        exit(1)

      passThroughCommands += '--'+flag+' '


    return passThroughCommands



####################################################
##               Control parallel jobs            ##
##       taken from bkgFit/runSingleFit.py        ##
####################################################
def submit_local_job(exec_sequence, logfilename):
  #os.system("rm -f "+logfilename)
  output_f=open(logfilename, 'w')
  pid = subprocess.Popen(exec_sequence, shell=True, stderr=output_f, stdout=output_f)
  time.sleep(0.5)  #Wait to prevent opening / closing of several files

  return pid, output_f

def submit_condor_job(exec_sequence, logfilename):
  #os.system("rm -f "+logfilename)
  exec_cmd=os.path.abspath("./DijetHelpers/scripts/condor_python.sh")
  con_submit=open(".cond_sub","a")
  con_submit.write("Executable="+exec_cmd+"""
Universe = vanilla
Notification = Error
Output="""+logfilename+"\n")
  con_submit.write("Error="+logfilename.replace(".log",".err")+"\n")
  con_submit.write("Log="+logfilename.replace(".log",".clog")+"\n")
  con_submit.write("Arguments="+exec_sequence.replace("python",os.getcwd())+"\n")
  con_submit.write("Queue\n")
  return (0,0)
 


def wait_completion(pids, logFiles):
  print """Wait until the completion of all of the launched jobs"""
  while True:
    for pid in pids:
      if pid.poll() is not None:
        print "\nProcess", pid.pid, "has completed"
        logFiles.pop(pids.index(pid)).close()  #remove logfile from list and close it
        pids.remove(pid)

        return
    print ".",
    sys.stdout.flush()
    time.sleep(3) # wait before retrying

def wait_all(pids, logFiles):
  print """Wait until the completion of all launched jobs"""
  while len(pids)>0:
    wait_completion(pids, logFiles)
  print "All jobs finished!"
####################################################
##    END        Control parallel jobs            ##
####################################################

if __name__ == "__main__":
    main()










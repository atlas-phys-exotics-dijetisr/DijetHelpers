#!/usr/bin/env python

#******************************************
#makeDataLikeFromHistWithPatch.py

# Given a simple histogram, get the data-like version and then apply a patch to the truncated part of the spectrum.
# The patch requires a TFitResult to a 20ifb smooth spectrum.


#******************************************

import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--file", dest='file', default="", help="Filename that contains the histogram")
parser.add_argument("--histName", dest='histName', default="", help="Name of the histogram")
parser.add_argument("--newName", dest='newName', default="", help="New name of the histogram")
parser.add_argument("--inLumi", dest='inLumi', default=1.0, type=float, help="Luminosity of the input sample")
parser.add_argument("--outLumi", dest='outLumi', default=1.0, type=float, help="Lumoutosity of the output sample")
parser.add_argument("--seed", dest='seed', default=10, type=int, help="Seed for getDataLikeHist")
parser.add_argument("--systematic", dest='systematic', default="", help="Name of TDirectory to put histograms in")
parser.add_argument("--outDir", dest='outDir', default="", help="Output directory")
parser.add_argument("--plot", dest='plot', action='store_true', default=False, help="Plot results?")
parser.add_argument("--patch", dest='patch', default="", help="Location of directory containing fit file")
parser.add_argument("--patchFitName", dest='patchFitName', default="", help="Name of patch fit to be applied")
parser.add_argument("--comEnergy", dest='comEnergy', type = int, default="13", help="C of M energy in TeV, default = 13 TeV")
parser.add_argument("--thresholdMass", dest='thresholdMass', type = int, default="1100", help="Lower Invariant Mass Cut in GeV, default = 1100 GeV")
args = parser.parse_args()

#******************************************
#import stuff
import ROOT, math, os, sys
from array import array
from math import sqrt, log, isnan, isinf
from ROOT import TF1
sys.path.insert(0, 'DijetHelpers/scripts/')
sys.path.insert(0, 'DijetHelpers/bkgFit/')
import plotUtils, DijetFuncs

#******************************************
#set ATLAS style
import AtlasStyle
AtlasStyle.SetAtlasStyle()

#******************************************
#def makeDataLikeFromHist(file, histName, inLumi, outLumi, seed):
def makeDataLikeFromHist(args):

    print('\nmaking data-like histogram')

    #------------------------------------------
    #input parameters
    print '\ninput parameters:'
    argsdict = vars(args)
    for ii in xrange(len(argsdict)):
        print '  %s = %s'%(argsdict.keys()[ii], argsdict.values()[ii],)

    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    #open input file
    f = ROOT.TFile.Open(args.file,'READ')
    if not f:
        raise SystemExit('\n***ERROR*** input file not found: %s'%args.file)

    hist = f.Get(args.histName)
    if not hist:
        raise SystemExit('\n***ERROR*** input histogram not found: %s'%args.histName)
    hist.SetDirectory(0) #Detatch
    f.Close()

  
    #------------------------------------------
    #output directories
    localdir = os.path.dirname(os.path.realpath(__file__))
    print '\nlocal dir: %s'%localdir

    if len(args.outDir) > 0:
        #NOTE paths are relative to the current directory
        #NOTE unless the path starts with '/'
        outDir = args.outDir
    else:
        outDir = localdir+'/results/'

    print 'output dir: %s'%outDir
    if not os.path.exists(outDir):
        os.makedirs(outDir)

    #------------------------------------------
    #outDir = args.file.split('.')
    #outDir = outDir[:-1] #remove .root
    #outDir = '.'.join(outDir)+'/'
    #print 'output dir: %s'%outDir
    #if not os.path.exists(outDir): os.makedirs(outDir)

    #------------------------------------------
    #configure input histogram
    #hist.Scale(float(args.outLumi)/float(args.inLumi))
    newName = args.newName
    if len(newName) == 0:
        newName = os.path.basename(args.file) #get basename
        newName = os.path.splitext(newName)[0] #remove extension
    #print 'new name: %s'%newName
    
    #To remove unecessary decimal points from integer names
    if int(args.outLumi) == args.outLumi:
        args.outLumi = int(args.outLumi)
          
    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

  

    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------
      
    #------------------------------------------
    #get effective entries histogram before scaling
    effEntHist = plotUtils.getEffectiveEntriesHistogram(hist, hist.GetName()+'_effent')
    #effEntHist.SetName( 'mjj_EffectiveEntries_'+args.newName+'_'+str(args.outLumi).replace('.','p')+'fb' )
    effEntHist.SetName(hist.GetName()+'_effent')

    #scale histogram
    hist.Scale(float(args.outLumi)/float(args.inLumi))
    #hist.SetName( 'mjj_Scaled_'+args.newName+'_'+str(args.outLumi).replace('.','p')+'fb' )
    hist.SetName(hist.GetName())

    #get data-like histogram
    dataLikeHist = plotUtils.getDataLikeHist(effEntHist, hist, hist.GetName()+'_DataLike', args.seed)
    #dataLikeHist.SetName( 'mjj_DataLike_'+args.newName+'_'+str(args.outLumi).replace('.','p')+'fb' )
    dataLikeHist.SetName(hist.GetName()+'_DataLike')

    #get smooth histogram
    smoothHist = plotUtils.getSmoothHistogram(hist, hist.GetName()+'_smooth')
    #smoothHist.SetName( 'mjj_Smooth_'+args.newName+'_'+str(args.outLumi).replace('.','p')+'fb' )
    smoothHist.SetName(hist.GetName()+'_Smooth')
    
    ################################################
    #-----------------------------------------------
    #### Get patch like

    # Declare Empty TH1 for writing.
    patchedDataLikeHist = ROOT.TH1

    #Get patchFile
    if args.patch: 
        
        print "\n\n**************************************"
        print "Applying patch to data-like histograms\n"

        # Name of  file that contains TFitResult as an output to makeDataLike.py
        patchFileName =  args.patch+args.patchFitName+'.root'
        # Name of TFitResult of Patch
        #patchFuncName = 'FitResultChi2FromNLL_'+args.patchFitName+'_mjj_Smooth_'+args.histName.replace('_total_final','')+'_20fb'
        patchFuncName = 'FitResultChi2FromNLL_'+args.patchFitName+'_'+args.histName+'_Smooth'

        print "  Patch File: ", patchFileName
        print "  Patch Fit File:",patchFuncName

        # Find where to start patch
        firstBin = -1
        for ii in xrange(dataLikeHist.GetNbinsX()):
            if dataLikeHist.GetBinContent(ii)>0.:
                firstBin=ii
                break
        if firstBin > 0.:
            print '  data-like histogram starts at %s GeV'%dataLikeHist.GetBinLowEdge(firstBin)
        else:
            raise SystemExit('\n***ERROR*** data-like histogram has no entries')

        #Clone to get a histogram for patched data-like distribution with correct binning, give it an appropriate name.
        patchedDataLikeHist = dataLikeHist.Clone()
        patchedDataLikeHist.SetName(dataLikeHist.GetName().replace('DataLike','PatchedDataLike'))
        patchedDataLikeHist.SetTitle(dataLikeHist.GetName().replace('DataLike','PatchedDataLike'))

        patchFile = ROOT.TFile(patchFileName,'READ')
        if not patchFile:
          raise SystemExit('\n***ERROR*** couldn\'t open patch file: %s'%patchFileName)

        #..........................................
        #get patch hist

        ## Load global variables for fits ##
        DijetFuncs.initializeGlobals()
        DijetFuncs.comEnergy = args.comEnergy*1000

        #Get TFitResult For Patch
        fullPatchFuncResult = patchFile.Get(patchFuncName)

        #Get TF1 with from TFitResult
        fList = DijetFuncs.getFit( args.patchFitName )
        fullPatchFunc=ROOT.TF1( fList["fNames"][0], fList["fEqs"][0], hist.GetXaxis().GetBinLowEdge(1), hist.GetXaxis().GetBinUpEdge(hist.GetNbinsX() ) )
        print "  Patch Fit Params:"
        for iParam in range( len(fList["fParams"][0]) ):
            print "     Parameter",iParam, fullPatchFuncResult.Parameter(iParam)
            fullPatchFunc.SetParameter(iParam, fullPatchFuncResult.Parameter(iParam))
            
        if not fullPatchFunc:
          raise SystemExit('\n***ERROR*** couldn\'t open patch fit function: %s'%patchFuncName)
        #print 'Debug, do I have the full patchFunc', fullPatchFunc.GetName()
        
        # Get TH1 that contains the Patch Function
        fullPatchHist = dataLikeHist.Clone()
        for iBin in range(1, fullPatchHist.GetNbinsX()+1):
          fullPatchHist.SetBinContent(iBin, 0)
          fullPatchHist.SetBinError(iBin, 0)

        for iBin in range(1, fullPatchHist.GetNbinsX()+1):
          xStart = fullPatchHist.GetXaxis().GetBinLowEdge(iBin)
          xEnd = fullPatchHist.GetXaxis().GetBinUpEdge(iBin)
          funcContent = fullPatchFunc.Integral( xStart, xEnd )
          if isnan(funcContent):
            print "Setting function contents for bin", iBin, "(", xStart, "to", xEnd, "GeV) to zero!"
            funcContent = 0.
          fullPatchHist.SetBinContent(iBin, funcContent)

        # Now create the patched data-like hist by combining the hist containing the patch fit and the data-like histogram.

        #check the binninng of the patch and the nominal hist are the same
        #if hist.GetXaxis().GetXbins().GetArray() != fullPatchHist.GetXaxis().GetXbins().GetArray():
        if hist.GetNbinsX() != fullPatchHist.GetNbinsX() or hist.GetBinLowEdge(1) != fullPatchHist.GetBinLowEdge(1) or hist.GetBinLowEdge( hist.GetNbinsX() ) != fullPatchHist.GetBinLowEdge( fullPatchHist.GetNbinsX() ):
            raise SystemExit('\n***ERROR*** Scaled Hist and patch hist have different binning')
        #..........................................
        #scale patch to right luminosity
        #NOTE the patch are obtained from 20/fb fits
        patchScaleFactor = float(args.outLumi)/20.
        print '  patch scale factor = %s '%patchScaleFactor
        fullPatchHist.Scale(patchScaleFactor)

        #..........................................
        #apply patch
        patchHist = ROOT.TH1D(hist.GetName()+'_patch', hist.GetName()+'_patch', hist.GetXaxis().GetNbins(), hist.GetXaxis().GetXbins().GetArray())
        for ii in xrange( int( round(dataLikeHist.GetNbinsX()))):
          #if dataLikeHist.GetXaxis().GetBinUpEdge(ii) > args.thresholdMass:
            #print (fullPatchFunc.Eval(patchHist.GetBinCenter(ii) )/20.0), fullPatchHist.GetBinContent(ii),  smoothHist.GetBinContent(ii), hist.GetBinContent(ii) #Some debugging lines
          if ii >= firstBin:
            print "  Bin", ii, "Data-like exits, no patch in this bin and beyond."
            break
          if dataLikeHist.GetXaxis().GetBinUpEdge(ii) > args.thresholdMass and patchedDataLikeHist.GetBinContent(ii) == 0.:
            #bincontent
            bincontent = int( round(fullPatchHist.GetBinContent(ii)))
            #random number generator
            #NOTE the seed for each bin must be always the same
            binSeed = int( round( patchHist.GetBinCenter(ii) + args.seed*1e5))
            rand3 = ROOT.TRandom3(binSeed)
            #add Poisson noise
            bincontent = int( round( rand3.PoissonD(bincontent)))
                    
            #fill
            for jj in xrange(bincontent):
              patchHist.Fill(patchHist.GetBinCenter(ii))
              #patch data-like histogram           
            print "  Bin", ii, "Filled with", patchHist.GetBinContent(ii)
        patchedDataLikeHist.Add(patchHist)
        patchFile.Close();
        print "  End Patching"
        print "*******************************************"
        
    else:
      print 'Patch not applied'
      
    ###########################################################

    #------------------------------------------
    # open output file
    outFileName='dataLikeHistograms.v'+str(args.seed)+'.root'
    outF = ROOT.TFile.Open(outDir+'dataLikeHistograms.v'+str(args.seed)+'.root', 'UPDATE')
    #print 'output file name: %s'%outFileName
    #outF = ROOT.TFile.Open(outDir+'/'+outFileName, 'UPDATE')
    if len(args.systematic) > 0:
        outF.mkdir( args.systematic )
        outF.cd( args.systematic )

    # Write to file
    effEntHist.Write("", ROOT.TObject.kOverwrite)
    hist.Write("", ROOT.TObject.kOverwrite)
    dataLikeHist.Write("", ROOT.TObject.kOverwrite)
    smoothHist.Write("", ROOT.TObject.kOverwrite)
    if args.patch:
      patchedDataLikeHist.Write("", ROOT.TObject.kOverwrite)

   
    #------------------------------------------
    #plot

    if args.plot:
        
        outputFigDir = localdir+'/figures/'
        if not os.path.exists(outputFigDir):
            os.makedirs(outputFigDir)

        #canvas
        c1 = ROOT.TCanvas('c1', 'c1', 200, 50, 800, 600)
        c1.Clear()
        c1.SetLogy(1)
        c1.SetLogx(0)

        hist.SetMarkerColor(ROOT.kAzure+1)
        hist.SetMarkerStyle(25)
        hist.SetLineColor(ROOT.kAzure+1)

        effEntHist.SetMarkerColor(ROOT.kGreen+1)
        effEntHist.SetMarkerStyle(21)
        effEntHist.SetLineColor(ROOT.kGreen+1)

        dataLikeHist.SetMarkerColor(ROOT.kRed+1)
        dataLikeHist.SetMarkerStyle(25)
        dataLikeHist.SetLineColor(ROOT.kRed+1)

        hs = ROOT.THStack("hs","hs")
        hs.Add(effEntHist)
        hs.Add(hist)
        hs.Add(dataLikeHist)
        hs.Draw("nostack")
        hs.GetXaxis().SetTitle("m_{jj} [GeV]")
        hs.GetYaxis().SetTitle("entries")

        #legend
        lXmin = 0.65
        lXmax = 0.80
        lYmin = 0.65
        lYmax = 0.75
        l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
        l.SetFillStyle(0)
        l.SetFillColor(0)
        l.SetLineColor(0)
        l.SetTextFont(43)
        l.SetBorderSize(0)
        l.SetTextSize(15)
        l.AddEntry(effEntHist,"eff. ent. histogram","pl")
        l.AddEntry(hist,"input histogram","pl")
        l.AddEntry(dataLikeHist,"data-like histogram","pl")
        l.Draw()

        #ATLAS
        a = ROOT.TLatex()
        a.SetNDC()
        a.SetTextFont(73)
        a.SetTextColor(1)
        a.SetTextSize(30)
        a.DrawLatex(lXmin,lYmax+0.12,'ATLAS')

        #internal
        p = ROOT.TLatex()
        p.SetNDC()
        p.SetTextFont(43)
        p.SetTextColor(1)
        p.SetTextSize(30)
        p.DrawLatex(lXmin+0.13,lYmax+0.12,'internal')

        #note
        n = ROOT.TLatex()
        n.SetNDC()
        n.SetTextFont(42)
        n.SetTextColor(1)
        n.SetTextSize(0.04)
        n.DrawLatex(lXmin,lYmax+0.05,'#intL dt = %s fb^{-1}'%args.outLumi)

        c1.Update()
        #c1.SaveAs(outputFigDir+'/dataLikeHistogram.'+args.histName+'.'+str(args.outLumi)+'.ifb.pdf')
        c1.SaveAs(outputFigDir+'/dataLikeHistogram.'+args.histName+'.pdf')

        #------------------------------------------
        #effective luminosity of the sample

        #canvas
        c2 = ROOT.TCanvas('c2', 'c2', 1020, 50, 800, 600)
        c2.Clear()
        c2.SetLogy(1)
        c2.SetLogx(0)
        c2.SetGridy(1)
        c2.SetGrid(1)

        ratioHist = effEntHist.Clone()
        ratioHist.SetName('effectiveLuminosity')
        ratioHist.SetTitle('effectiveLuminosity')
        ratioHist.Divide(hist)
        ratioHist.Scale(float(args.inLumi)) #TEST #CHECK

        ratioHist.SetMarkerColor(1)
        ratioHist.SetLineColor(1)
        ratioHist.SetMarkerStyle(20)
        ratioHist.SetXTitle("m_{jj} [GeV]")
        ratioHist.SetYTitle("luminosity [fb^{-1}]")
        ratioHist.Draw("pe")

        #legend
        lXmin=0.20
        lXmax=0.35

        #ATLAS
        a = ROOT.TLatex()
        a.SetNDC()
        a.SetTextFont(73)
        a.SetTextColor(1)
        a.SetTextSize(30)
        a.DrawLatex(lXmin,lYmax+0.12,'ATLAS')

        #internal
        p = ROOT.TLatex()
        p.SetNDC()
        p.SetTextFont(43)
        p.SetTextColor(1)
        p.SetTextSize(30)
        p.DrawLatex(lXmin+0.13,lYmax+0.12,'internal')

        #note
        n = ROOT.TLatex()
        n.SetNDC()
        n.SetTextFont(42)
        n.SetTextColor(1)
        n.SetTextSize(0.04)
        n.DrawLatex(lXmin,lYmax+0.07,'MC effective luminosity')

        #stats
        print '\neffective luminosity at 1.0 TeV = %.3f fb^-1'%ratioHist.GetBinContent(ratioHist.FindBin(1e3))
        print   'effective luminosity at 1.5 TeV = %.3f fb^-1'%ratioHist.GetBinContent(ratioHist.FindBin(1.5e3))
        print   'effective luminosity at 2.0 TeV = %.3f fb^-1'%ratioHist.GetBinContent(ratioHist.FindBin(2e3))

        c2.Update()
        c2.WaitPrimitive()
        c1.SaveAs(outputFigDir+'/effectiveLuminosity.'+args.histName+'.pdf')

    #close the output file, at last
    outF.Close()


#******************************************
if __name__ == '__main__':

    makeDataLikeFromHist(args)
    print '\ndone'

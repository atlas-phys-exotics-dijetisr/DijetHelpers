#!/bin/bash
exec_dir=$1
Arguments="${@:2:$#}"
set -- "${@:0:0}" #bash voodoo to keep @ from being passed to setupAtlas
export HOME=$PWD
echo $@
source /usr/local/bin/setup/cvmfs_atlas.sh
export PYTHONPATH=$ROOTSYS/lib:$PYTHONPATH
cd ${exec_dir}
rcSetup 
echo $Arguments
python $Arguments

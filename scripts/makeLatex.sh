#!/bin/sh

#script to dump plots from a dir to a .tex document. One figure per slide. Caption given by plot file name. in the end, it runs pdflatex.

#it relies on a definitions file presDefinitions.tex, with a minimal set of latex definitions. This can grow!


plotDir=$1
myTitle="Week1 plots"
myAuthor="Dijet team"

if [ -z $1 ]
then 
    echo
    echo "Arguments missing."
    echo "Usage: $0 [plots-directory] [ slides-name.tex ] [footline author name]"
    echo "Run from inside DijetHelpers/scripts, for accessing definitions file"
    echo 'Only the plot directory argument is non-optional. Defaults: "newPresentation.tex" "Exotic dijets team"'
    echo
    return
fi

if [ -z $2 ]
then 
latexFile=newPresentation.tex
else
latexFile=$2
fi

if [ -z $3 ]
then 
myFootAuthor="Exotic dijets team"
else
myFootAuthor=$3
fi

#setup the .tex file

#defsPath=$(find ~/. -name presDefinitions.tex)
#dPath=${defsPath%\.*}
#echo "will include definitions from ${dPath}"
#for some reason not working: pdflatex attempts (and fails) to overwrite the definitions file -- hardwired for now; run from DijetHelpers/scripts


touch ${latexFile}
echo "\documentclass[xcolor=dvipsnames]{beamer} "> ${latexFile}
echo >> ${latexFile}
#echo "\include{${dPath}}">> ${latexFile}
echo "\include{presDefinitions}">> ${latexFile}
echo >> ${latexFile}
echo "\title{${myTitle}}">> ${latexFile}
echo >> ${latexFile}
echo "\author[${myFootAuthor}]{ \textbf{${myAuthor}} }">> ${latexFile}
echo "\date{\today}">> ${latexFile}
echo >> ${latexFile}
echo "\begin{document}">> ${latexFile}
echo >> ${latexFile}
echo "\thispagestyle{empty}">> ${latexFile}
echo "\maketitle">> ${latexFile}
echo >> ${latexFile}

for plot in $(ls ${plotDir}/*.pdf)
do 
    #remove extension
    plotName=${plot%\.pdf}

    #set up caption
    echo ${plotName} > plotNameFile
    #remove path and _ (latex fix)
    sed -i 's,'"${plotDir}/"',,' plotNameFile
    sed -i 's,_,-,g' plotNameFile
    caption=$(cat plotNameFile)
    echo $caption
#    caption=${plotName##*/}  #more elegant but keeps _

    #plug this into .tex file
    echo "
\begin{frame}
\begin{figure}
\includegraphics[width=0.75\textwidth]{${plotName}}
\caption{ ${caption} }
\end{figure}
\end{frame}" >>  ${latexFile}
done


echo "\end{document}" >> ${latexFile}

pdflatex ${latexFile}
#!/usr/bin/env python

###########################################################
# correctTreeXS.py                                        #
# A short script to correct weight branch of TTrees.      #
# For more information contact Jeff.Dandoy@cern.ch        #
###########################################################

import os, glob, array, argparse

#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--pathToTrees", dest='pathToTrees', default="./gridOutput/tree", help="Path to the input trees")
parser.add_argument("--fileName", dest='fileName', default="", help="Filename to run over")
parser.add_argument("--XSFile", dest='XSFile', default="../../DijetResonanceAlgo/data/XsAcc_13TeV.txt", help="XSFile for corrections")
parser.add_argument("--nominal", dest='nominal', action='store_true', default=False, help="Nominal only")

args = parser.parse_args()


from ROOT import *

def correctTreeXS():

  ## Gather Files
  if len(args.fileName) > 0:
    files = [args.fileName]
  else:
    files = glob.glob(args.pathToTrees+"/*.root")
  treeName = "outTree"
  XSFileName = args.XSFile

  for file in files:

    ## Calculate new XS value from text file
    fileNum = file.split('.')[3]
    newXS = 0
    with open(XSFileName,'r') as XSFile:
      for line in XSFile:
        if fileNum in line:
          line = line.split(' ')
          newXS = float(line[1])*float(line[2])
          break

    if newXS == 0:
      print "ERROR, could not find file number in ", XSFileName

    print "Setting ", file, " weight_xs to ", newXS

    ## Create a corrected TTree
    inFile = TFile.Open(file, "UPDATE")
    print "Saving to ", os.path.dirname(file)
    if len( os.path.dirname(file)) > 0:
      outFile = TFile.Open(os.path.dirname(file)+'/corrected_'+os.path.basename(file), "UPDATE")
    else:
      outFile = TFile.Open('corrected_'+os.path.basename(file), "UPDATE")
    for key in inFile.GetListOfKeys():
      if not key.GetName().startswith("outTree"):
        object = inFile.Get( key.GetName() )
        object.Write("", TObject.kOverwrite)
      elif not args.nominal or key.GetName() == "outTree":
        treeName = key.GetName()

        tree = inFile.Get( treeName )

        print "Reweighting file ", file
        newTree = tree.CloneTree(0)
        newTree.SetName(treeName)

        weight = array.array('f', [0])
        weight_xs = array.array('f', [0])
        mcEventWeight = array.array('f', [0])
        newTree.SetBranchAddress("weight", weight)
        newTree.SetBranchAddress("weight_xs", weight_xs)
        newTree.SetBranchAddress("mcEventWeight", mcEventWeight)
        tree.SetBranchAddress("mcEventWeight", mcEventWeight)

#        tree.SetBranchAddress("weight_xs", weight_xs) # !!

        i = 0
        while tree.GetEntry(i):
          i += 1
          weight_xs[0] = newXS
          weight[0] = mcEventWeight[0] * newXS
          newTree.Fill()

#          weight[0] = mcEventWeight[0] * newXS # !!
#          newTree.Fill() # !!

        newTree.Write("", TObject.kOverwrite)
    outFile.Close()
    inFile.Close()

if __name__ == "__main__":
  correctTreeXS()
  print "Finished correctTreeXS()"

#!/usr/bin/env python

#******************************************
#bjethistmaker.py
# Based on script from DijetHelpers package written by run II dijet team, and adapted by Lydia Beresford

#make scaled histograms (not just mjj) from input MC JZ*W trees

#NOTE just run the script without any parameter to get instruction

#EXAMPLE: python -u bjethistmaker.py ../../../gridOutput/trees/ Pythia8EvtGen_A14NNPDF23LO_jetjet v24_20150422 10 test.01
#******************************************

# TODO FIXME
# don't cut on emscale eta, use flag to say EM or LC and cut/fill accordingly
#******************************************
#import stuff
import ROOT, plotUtils, math, os, sys
from ROOT import *

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('../atlasstyle-00-03-05/AtlasStyle.C') #~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
# Set batch mode for root
ROOT.gROOT.SetBatch(True)

#******************************************
def singlexchecksdijethistmaker(inputFile, lumi, tag):

    print('\nmaking scaled histograms')

    #------------------------------------------
    #input parameters    
    print '\nparameters:'
    print '  inputFile:  %s'%inputFile
    print '  lumi [ifb]: %s'%lumi.replace("p",".")
    print '  tag:        %s'%tag

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))

    #------------------------------------------
    # set isMC flag
    if "data" in inputFile:
      isMC = False
      print "IS MC?"
      print isMC
    else:
      isMC = True
      print "IS MC?"
      print isMC    
  
    #------------------------------------------
    #check output file 
    outputDir = localdir+'/../results/'
    outputFileName = outputDir+'/ScaledHistograms.'+lumi+'.ifb.'+tag+'.root'
    if os.path.isfile(outputFileName):
        raise SystemExit('\n***WARNING*** output file exists already: '+outputFileName)

    #make directories for output files
    if not os.path.exists(outputDir):
        os.makedirs(outputDir)
        
    #create output file
    print '\noutput file: %s'%outputFileName
    outputFile = ROOT.TFile.Open(outputFileName,'RECREATE')

    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()
    ROOT.TH3.SetDefaultSumw2()
    ROOT.TH3.StatOverflows()

    #------------------------------------------
    # Setup bins for binning in eta
    coarseetaBins = [0.8,1.2,1.8,2.8]# plotUtils.getBins("coarseetaBins") # To match https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JetEtmissHighPtJets
    #------------------------------------------
    #declare histograms
    #then add them to dictionary of scaled histograms, their name is the key and the value is a list containing xaxis label and y axis label
    scaledHistograms = {} # dictionary of scaled histograms

    #------------------------------------------
    # Plots 

    h_mjj                               = plotUtils.get1DHist('mjj', '13TeV') # always plotted, mjj with mjj binning
    h_mjj_1PT                           = plotUtils.get1DHist('mjj_1PT', '13TeV') # mjj with 1 PT (punch-through) jet, where PT jet has >= 20 segs
    h_mjj_2PT                           = plotUtils.get1DHist('mjj_2PT', '13TeV') # mjj with 2 PT (punch-through) jets, where PT jet has >= 20 segs
    h_mjj_0PT                           = plotUtils.get1DHist('mjj_0PT', '13TeV') # mjj with 2 PT (punch-through) jets, where PT jet has >= 20 segs
    h_yStar                             = plotUtils.get1DHist('yStar', '20,0,2') 
    h_mjj_vs_SumNSegments               = plotUtils.get2DHist('mjj_vs_SumNSegments','13TeV','finenSegBins') 
    h_LeadJetpT_vs_NSegments            = plotUtils.get2DHist('LeadJetpT_vs_NSegments','modpTBins','finenSegBins') 
    h_SubLeadJetpT_vs_NSegments         = plotUtils.get2DHist('SubLeadJetpT_vs_NSegments','modpTBins','finenSegBins') 
    h_LeadJetEta_vs_NSegments           = plotUtils.get2DHist('LeadJetEta_vs_NSegments','Shauneta','finenSegBins') 
    h_SubLeadJetEta_vs_NSegments        = plotUtils.get2DHist('SubLeadJetEta_vs_NSegments','Shauneta','finenSegBins') 
    h_pTAsym_vs_SumNSegments            = plotUtils.get2DHist('pTAsym_vs_SumNSegments','asymBins','finenSegBins') 
    h_pTAsym_vs_LeadNSegments           = plotUtils.get2DHist('pTAsym_vs_LeadNSegments','asymBins','finenSegBins') 
    h_pTAsym_vs_SubLeadNSegments        = plotUtils.get2DHist('pTAsym_vs_SubLeadNSegments','asymBins','finenSegBins') 
   
   
    addHisttoDict(scaledHistograms,h_mjj,'m_{jj} [GeV]')
    addHisttoDict(scaledHistograms,h_mjj_0PT,'m_{jj} [GeV]')
    addHisttoDict(scaledHistograms,h_mjj_1PT,'m_{jj} [GeV]')
    addHisttoDict(scaledHistograms,h_mjj_2PT,'m_{jj} [GeV]')
    addHisttoDict(scaledHistograms,h_yStar,'yStar')
    addHisttoDict(scaledHistograms,h_mjj_vs_SumNSegments, 'm_{jj} [GeV]' ,'Sum of N{Segments} behind leading jets') 
    addHisttoDict(scaledHistograms,h_LeadJetpT_vs_NSegments, 'Lead jet p_{T} [GeV]' ,'N_{Segments} behind lead jet') 
    addHisttoDict(scaledHistograms,h_SubLeadJetpT_vs_NSegments, 'Sub-Lead jet p_{T}' ,'N_{Segments} behind sublead jet') 
    addHisttoDict(scaledHistograms,h_LeadJetEta_vs_NSegments, 'Lead jet \eta' ,'N_{Segments} behind lead jet') 
    addHisttoDict(scaledHistograms,h_SubLeadJetEta_vs_NSegments, 'Sub-Lead jet \eta' ,'N_{Segments} behind sublead jet') 
    addHisttoDict(scaledHistograms,h_pTAsym_vs_SumNSegments, 'm_{jj} [GeV]' ,'Sum of N_{Segments} behind leading jets') 
    addHisttoDict(scaledHistograms,h_pTAsym_vs_LeadNSegments, 'm_{jj} [GeV]' ,'N_{Segments} behind lead jet') 
    addHisttoDict(scaledHistograms,h_pTAsym_vs_SubLeadNSegments, 'm_{jj} [GeV]' ,'N_{Segments} behind sublead jet') 

    h_LeadJetEta            = plotUtils.get1DHist('LeadJetEta','Shauneta') 
    h_SubLeadJetEta         = plotUtils.get1DHist('SubLeadJetEta','Shauneta') 

    addHisttoDict(scaledHistograms,h_LeadJetEta, 'Lead jet \eta' ,'Lead jet') 
    addHisttoDict(scaledHistograms,h_SubLeadJetEta, 'Sub-Lead jet \eta' ,'Sublead jet') 

    # ---------------------
    # Angular plots

    h_chi                  = plotUtils.get1DHist('chi', 'ChiBins') # chi distribution
    h_chi_1PT              = plotUtils.get1DHist('chi_1PT', 'ChiBins') # chi distribution
    h_chi_2PT              = plotUtils.get1DHist('chi_2PT', 'ChiBins') # chi distribution
    h_chi_0PT              = plotUtils.get1DHist('chi_0PT', 'ChiBins') # chi distribution

    addHisttoDict(scaledHistograms,h_chi,'\chi')
    addHisttoDict(scaledHistograms,h_chi_1PT,'\chi')
    addHisttoDict(scaledHistograms,h_chi_2PT,'\chi')
    addHisttoDict(scaledHistograms,h_chi_0PT,'\chi')

    h_punch_type_segs                       = plotUtils.get1DHist('punch_type_segs', '4,0,4')
    h_NSegments                             = plotUtils.get1DHist('NSegments', 'finenSegBins')
    h_jet_pt_vs_NSegments                   = plotUtils.get2DHist('jet_pt_vs_NSegments','coarsepTBins','finenSegBins')
    h_jet_pt_vs_NSegmentsmodpTBins         = plotUtils.get2DHist('jet_pt_vs_NSegmentsmodpTBins','modpTBins','finenSegBins')
    h_jet_E_vs_NSegments                    = plotUtils.get2DHist('jet_E_vs_NSegments','coarsepTBins','finenSegBins')
    h_jet_E_vs_NSegmentsmodpTBins          = plotUtils.get2DHist('jet_E_vs_NSegmentsmodpTBins','modpTBins','finenSegBins')
    h_jet_eta_vs_NSegments                  = plotUtils.get2DHist('jet_eta_vs_NSegments','Shauneta','finenSegBins')
    h_jet_phi_vs_NSegments                  = plotUtils.get2DHist('jet_phi_vs_NSegments','coarsephiBins','finenSegBins')
    h_emscaleeta_vs_NSegments               = plotUtils.get2DHist('emscaleeta_vs_NSegments','Shauneta','finenSegBins')

    addHisttoDict(scaledHistograms,h_punch_type_segs, 'Punch type segments','entries') 
    addHisttoDict(scaledHistograms,h_NSegments,'N_{Segments}','jets')

    addHisttoDict(scaledHistograms,h_jet_pt_vs_NSegments,'jet p_{T} [GeV]','N_{Segments}') 
    addHisttoDict(scaledHistograms,h_jet_pt_vs_NSegmentsmodpTBins,'jet p_{T} [GeV]','N_{Segments}') 
    addHisttoDict(scaledHistograms,h_jet_E_vs_NSegments,'jet E [GeV]','N_{Segments}') 
    addHisttoDict(scaledHistograms,h_jet_E_vs_NSegmentsmodpTBins,'jet E [GeV]','N_{Segments}') 
    addHisttoDict(scaledHistograms,h_jet_eta_vs_NSegments,'jet \eta','N_{Segments}') 
    addHisttoDict(scaledHistograms,h_jet_phi_vs_NSegments, 'jet \phi' ,'N_{Segments}') 
    addHisttoDict(scaledHistograms,h_emscaleeta_vs_NSegments,'EMscale \eta','N_{Segments}') 

    # Coarse Eta Binned coarse pT x axis
    # Declare list of lists to hold eta binned histograms

    h_jet_pt_vs_NSegments_EtaBinned         = [ [] for i in range(0,len(coarseetaBins)-1) ]      
    h_jet_E_vs_NSegments_EtaBinned          = [ [] for i in range(0,len(coarseetaBins)-1) ]      
      
    for i in range (0,len(coarseetaBins)-1): # loop over eta bins -1 as want number of eta bins not array length 
      name       = "jet_pt_vs_NSegments_EtaBinned_eta_"+str(i)
      Ename      = "jet_E_vs_NSegments_EtaBinned_eta_"+str(i)

      h_jet_pt_vs_NSegments_EtaBinned[i]       = plotUtils.get2DHist(name,'coarsepTBins','finenSegBins')
      h_jet_E_vs_NSegments_EtaBinned[i]        = plotUtils.get2DHist(Ename,'coarsepTBins','finenSegBins')

      addHisttoDict(scaledHistograms,h_jet_pt_vs_NSegments_EtaBinned[i],'jet p_{T} [GeV]','N_{Segments}')
      addHisttoDict(scaledHistograms,h_jet_E_vs_NSegments_EtaBinned[i],'jet E [GeV]','N_{Segments}')
      
    # Coarse Eta Binned fine pT x axis
    # Declare list of lists to hold eta binned histograms

    h_jet_pt_vs_NSegmentsmodpTBins_EtaBinned         = [ [] for i in range(0,len(coarseetaBins)-1) ]      
    h_jet_E_vs_NSegmentsmodpTBins_EtaBinned          = [ [] for i in range(0,len(coarseetaBins)-1) ]      
      
    for i in range (0,len(coarseetaBins)-1): # loop over eta bins -1 as want number of eta bins not array length 
      name       = "jet_pt_vs_NSegmentsmodpTBins_EtaBinned_eta_"+str(i)
      Ename      = "jet_E_vs_NSegmentsmodpTBins_EtaBinned_eta_"+str(i)

      h_jet_pt_vs_NSegmentsmodpTBins_EtaBinned[i]       = plotUtils.get2DHist(name,'modpTBins','finenSegBins')
      h_jet_E_vs_NSegmentsmodpTBins_EtaBinned[i]        = plotUtils.get2DHist(Ename,'modpTBins','finenSegBins')

      addHisttoDict(scaledHistograms,h_jet_pt_vs_NSegmentsmodpTBins_EtaBinned[i],'jet p_{T} [GeV]','N_{Segments}')
      addHisttoDict(scaledHistograms,h_jet_E_vs_NSegmentsmodpTBins_EtaBinned[i],'jet E [GeV]','N_{Segments}')

    # ---------------------
    # Plots to obtain fraction of jets with > 20 segs as a function of pT (divide them in next plotting macro) 
    # filled for lead jets

    h_jet_pt                    = plotUtils.get1DHist('jet_pt', 'coarseGeVpTBins')
    h_jet_ptfinepTBins          = plotUtils.get1DHist('jet_ptfinepTBins', 'fineGeVpTBins')
    h_jet_pt_g20seg             = plotUtils.get1DHist('jet_pt_g20seg', 'coarseGeVpTBins')
    h_jet_ptfinepTBins_g20seg   = plotUtils.get1DHist('jet_ptfinepTBins_g20seg', 'fineGeVpTBins')

    # filled for all jets
    h_jet_pt_all                    = plotUtils.get1DHist('jet_pt_all', 'coarseGeVpTBins')
    h_jet_ptfinepTBins_all          = plotUtils.get1DHist('jet_ptfinepTBins_all', 'fineGeVpTBins')
    h_jet_pt_g20seg_all             = plotUtils.get1DHist('jet_pt_g20seg_all', 'coarseGeVpTBins')
    h_jet_ptfinepTBins_g20seg_all   = plotUtils.get1DHist('jet_ptfinepTBins_g20seg_all', 'fineGeVpTBins')

    addHisttoDict(scaledHistograms,h_jet_pt,'jet p_{T} [GeV]','jets') 
    addHisttoDict(scaledHistograms,h_jet_ptfinepTBins,'jet p_{T} [GeV]','jets') 
    addHisttoDict(scaledHistograms,h_jet_pt_g20seg,'jet p_{T} [GeV]','jets') 
    addHisttoDict(scaledHistograms,h_jet_ptfinepTBins_g20seg,'jet p_{T} [GeV]','jets') 
     
    addHisttoDict(scaledHistograms,h_jet_pt_all,'jet p_{T} [GeV]','jets') 
    addHisttoDict(scaledHistograms,h_jet_ptfinepTBins_all,'jet p_{T} [GeV]','jets') 
    addHisttoDict(scaledHistograms,h_jet_pt_g20seg_all,'jet p_{T} [GeV]','jets') 
    addHisttoDict(scaledHistograms,h_jet_ptfinepTBins_g20seg_all,'jet p_{T} [GeV]','jets')


    #------------------------------------------
    #open input file
    print '\ninput file: %s'%inputFile
    f = ROOT.TFile.Open(inputFile,'READ')
    t = f.Get('outTree')

    if not t:
        raise SystemExit("tree not found")

    if t.GetEntries() == 0:
        raise SystemExit("empty tree!")


    #------------------------------------------
    #get number of events from cutflow

    events = 0
        
    #find cutflow histogram
    keys = f.GetListOfKeys()
    for key in keys:
        if key.ReadObj().GetTitle() == 'cutflow':
            events = key.ReadObj().GetBinContent(1)
            print '  cutflow entries in bin 1 = %s'%events
            continue

    #------------------------------------------
    # define passedTriggers as a std vector of strings

    passedTriggers = ROOT.std.vector('string')()

    #------------------------------------------
    #read branches
        
    #turn off all branches
    t.SetBranchStatus('*',0)
    #turn on all necessary branches
    t.SetBranchStatus('mjj',1)
    t.SetBranchStatus('yStar',1)
    t.SetBranchStatus('weight',1)
    t.SetBranchStatus('jet_pt',1)
    t.SetBranchStatus('jet_E',1)            
    t.SetBranchStatus('jet_phi',1)         
    t.SetBranchStatus('jet_eta',1)          
    t.SetBranchStatus('jet_emScaleEta',1)      
    t.SetBranchStatus('jet_constitScaleEta',1) 
    t.SetBranchStatus('jet_GhostMuonSegmentCount',1)      
    t.SetBranchStatus('punch_type_segs',1) 
    t.SetBranchStatus('jet_clean_passLooseBad',1) 
    t.SetBranchStatus('jet_MV2c20',1)
    t.SetBranchStatus( "passedTriggers", 1)
    if isMC:
      t.SetBranchStatus('jet_truth_pt',1)      
      t.SetBranchStatus('jet_truth_eta',1)      
      t.SetBranchStatus('jet_truth_phi',1)      
      t.SetBranchStatus('jet_truth_E',1)      

    #------------------------------------------
    #fill histogram after applying cuts
    pass2jet           = 0
    passCleaning       = 0
    passpT             = 0
    passtrigger        = 0
    passmjj            = 0
    passYstar          = 0
    passEta            = 0
    sumOfW             = 0
       
    #stop = 0
    for entry in t:
      #print entry.weight
      #stop +=1
      #print stop
      #events = 1000
      #if stop>1000:
      #  print "STOP1000000"
      #  break

      #------------------------------------------
      # 2 jet cut
      if entry.jet_pt.size() < 2: continue
      pass2jet += 1

      # Skip event if any of the leading 3 jets ( 2 if there are only 2 ) has jet_clean_passLooseBad == 0
      if entry.jet_clean_passLooseBad[0] == 0: continue
      if entry.jet_clean_passLooseBad[1] == 0: continue
      if entry.jet_pt.size() >= 3:
        if entry.jet_clean_passLooseBad[2] == 0: continue
      passCleaning += 1

      #------------------------------------------
      #pT cut, pT leading >440 GeV  (No pT Sub leading > 50 GeV cut as done at NTuple Level)
      if (entry.jet_pt[0] <= 440): continue
      passpT += 1

      #------------------------------------------
      # Trigger cut (lowest unprescaled trigger), lead jet cut not sufficient as NTuple contains events that passed 3 and 4 jet trig too! Uses trigger OR.
      if not "HLT_j360" in entry.passedTriggers: continue
      passtrigger += 1

      useOldMinimalSelection = False
      if useOldMinimalSelection:
        #------------------------------------------
        # mjj cut
        if entry.mjj <= 1100: continue
        passmjj += 1

        #------------------------------------------
        #y* cut 
        if abs(entry.yStar) >= 1.7: continue
        passYstar += 1

      useNewMinimalSelection = True
      if useNewMinimalSelection:
        #------------------------------------------
        #Eta cut 
        if abs(entry.jet_eta[0]) > 2.8: continue
        if abs(entry.jet_eta[1]) > 2.8: continue
        passEta += 1

            
      #fill histograms
      #NOTE the entry weight contains the MC event weight, the x-section and the acceptance
      if isMC:
        weight = entry.weight * float(lumi.replace("p",".")) / events    
      else:
        weight = 1.0      
            
      # Calculate variables
      sumNSeg = entry.jet_GhostMuonSegmentCount[0]+entry.jet_GhostMuonSegmentCount[1]
      pTAsym  = (entry.jet_pt[0]-entry.jet_pt[1])/(entry.jet_pt[0]+entry.jet_pt[1])
      chi = math.exp(2*math.fabs(entry.yStar))


      # Fill hists
      h_mjj.Fill(entry.mjj/1000., weight) 
      if (entry.jet_GhostMuonSegmentCount[0]>=20 and entry.jet_GhostMuonSegmentCount[1] >=20): 
        h_mjj_2PT.Fill(entry.mjj/1000., weight) 
      else:
        if (entry.jet_GhostMuonSegmentCount[0]>=20 or entry.jet_GhostMuonSegmentCount[1] >=20): 
          h_mjj_1PT.Fill(entry.mjj/1000., weight) 
        else:
          h_mjj_0PT.Fill(entry.mjj/1000., weight) 

      h_yStar.Fill(entry.yStar, weight)  
      h_mjj_vs_SumNSegments.Fill(entry.mjj/1000.,sumNSeg, weight)
      h_LeadJetpT_vs_NSegments.Fill(entry.jet_pt[0]/1000.,entry.jet_GhostMuonSegmentCount[0], weight) 
      h_SubLeadJetpT_vs_NSegments.Fill(entry.jet_pt[1]/1000.,entry.jet_GhostMuonSegmentCount[1], weight) 
      h_LeadJetEta_vs_NSegments.Fill(entry.jet_eta[0],entry.jet_GhostMuonSegmentCount[0], weight) 
      h_SubLeadJetEta_vs_NSegments.Fill(entry.jet_eta[1],entry.jet_GhostMuonSegmentCount[1], weight) 
      h_pTAsym_vs_SumNSegments.Fill(pTAsym,sumNSeg, weight)
      h_pTAsym_vs_LeadNSegments.Fill(pTAsym,entry.jet_GhostMuonSegmentCount[0], weight)
      h_pTAsym_vs_SubLeadNSegments.Fill(pTAsym,entry.jet_GhostMuonSegmentCount[1], weight)
      h_punch_type_segs.Fill(entry.punch_type_segs, weight)

      h_LeadJetEta.Fill(entry.jet_eta[0], weight) 
      h_SubLeadJetEta.Fill(entry.jet_eta[1], weight)

      for i in range (0,2): # i.e. plot for leading jets only
        h_NSegments.Fill(entry.jet_GhostMuonSegmentCount[i], weight)
        h_jet_pt_vs_NSegments.Fill(entry.jet_pt[i]/1000.,entry.jet_GhostMuonSegmentCount[i], weight) 
        h_jet_pt_vs_NSegmentsmodpTBins.Fill(entry.jet_pt[i]/1000.,entry.jet_GhostMuonSegmentCount[i], weight) 
        h_jet_E_vs_NSegments.Fill(entry.jet_E[i]/1000.,entry.jet_GhostMuonSegmentCount[i], weight) 
        h_jet_E_vs_NSegmentsmodpTBins.Fill(entry.jet_E[i]/1000.,entry.jet_GhostMuonSegmentCount[i], weight) 
        h_jet_phi_vs_NSegments.Fill(entry.jet_phi[i],entry.jet_GhostMuonSegmentCount[i], weight) 
        h_jet_eta_vs_NSegments.Fill(entry.jet_eta[i],entry.jet_GhostMuonSegmentCount[i], weight) 
        h_emscaleeta_vs_NSegments.Fill(entry.jet_emScaleEta[i],entry.jet_GhostMuonSegmentCount[i], weight)

      # Coarse Eta Binned 
      for i in range (0,2): # i.e. plot for leading jets only
        for k in range (0,len(coarseetaBins)-1): # loop over eta bins, -1 off length as use i+1 below for edge of last bin
          #print "fabs must be >= "+str( coarseetaBins[i])+" and < "+str(coarseetaBins[i+1])
          if (math.fabs(entry.jet_emScaleEta[i]) >= coarseetaBins[k] and math.fabs(entry.jet_emScaleEta[i]) < coarseetaBins[k+1]): # requirement on detector eta # i.e. keep going if absolute value of detector eta falls in eta bin

            # coarse pT x axis plots
            h_jet_pt_vs_NSegments_EtaBinned[k].Fill(entry.jet_pt[i]/1000.,entry.jet_GhostMuonSegmentCount[i], weight) 
            h_jet_E_vs_NSegments_EtaBinned[k].Fill(entry.jet_E[i]/1000.,entry.jet_GhostMuonSegmentCount[i], weight) 
            # fine pT x axis plots
            h_jet_pt_vs_NSegmentsmodpTBins_EtaBinned[k].Fill(entry.jet_pt[i]/1000.,entry.jet_GhostMuonSegmentCount[i], weight) 
            h_jet_E_vs_NSegmentsmodpTBins_EtaBinned[k].Fill(entry.jet_E[i]/1000.,entry.jet_GhostMuonSegmentCount[i], weight) 

      # chi plots for highest mjj bin
      if entry.mjj > 3200:
        h_chi.Fill(chi, weight) 
        if (entry.jet_GhostMuonSegmentCount[0]>=20 and entry.jet_GhostMuonSegmentCount[1] >=20): 
          h_chi_2PT.Fill(chi, weight) 
        else:
          if (entry.jet_GhostMuonSegmentCount[0]>=20 or entry.jet_GhostMuonSegmentCount[1] >=20): 
            h_chi_1PT.Fill(chi, weight) 
          else:
            h_chi_0PT.Fill(chi, weight) 
      # Plots to obtain fraction of jets with >5 segs and > 20 segs as a function of pT (divide them in next plotting macro) 
      for i in range (0,2): # i.e. plot for leading jets only
        h_jet_pt.Fill(entry.jet_pt[i], weight) 
        h_jet_ptfinepTBins.Fill(entry.jet_pt[i], weight) 
        if entry.jet_GhostMuonSegmentCount[i] > 20:
          h_jet_pt_g20seg.Fill(entry.jet_pt[i], weight) 
          h_jet_ptfinepTBins_g20seg.Fill(entry.jet_pt[i], weight) 
      for i in range (0,entry.jet_pt.size()): # i.e. plot for ALL jets
        h_jet_pt_all.Fill(entry.jet_pt[i], weight) 
        h_jet_ptfinepTBins_all.Fill(entry.jet_pt[i], weight) 
        if entry.jet_GhostMuonSegmentCount[i] > 20:
          h_jet_pt_g20seg_all.Fill(entry.jet_pt[i], weight) 
          h_jet_ptfinepTBins_g20seg_all.Fill(entry.jet_pt[i], weight) 

      sumOfW += entry.weight

    #------------------------------------------
    print '  entries:        %s'%h_mjj.GetEntries()
    print '  initial events:   %s'%events
    print '  pass 2 jet cut:    %s'%pass2jet
    print '  pass 3 Lead jet cleaning:    %s'%passCleaning
    print '  pass pT cut:    %s'%passpT
    print '  pass trigger: (HLT_j360) %s'%passtrigger
    if useOldMinimalSelection:
      print '  pass yStar cut:        %s'%passYstar
      print '  pass mjj cut:    %s'%passmjj
    if useNewMinimalSelection:
      print '  pass eta cut:        %s'%passEta
    print '  sum of weights: %s'%sumOfW
    f.Close()

    print "Plotting"

    #------------------------------------------
    #canvas
    c1 = ROOT.TCanvas('c1', 'c1', 400, 50, 800, 600)
    
    #------------------------------------------
    #get scaled histograms
    #for scaledHist, xaxisLabel in zip(scaledHistograms, xaxisLabels):

    for scaledHist in scaledHistograms:
        
        #------------------------------------------
        #plot
        c1.Clear()
        c1.SetLogy(0)
        c1.SetLogx(0)

        scaledHist.SetMarkerColor(ROOT.kAzure+1)
        scaledHist.SetLineColor(ROOT.kAzure+1)
        scaledHist.Draw()
        scaledHist.GetXaxis().SetTitle(scaledHistograms[scaledHist][0])
        scaledHist.GetYaxis().SetTitle(scaledHistograms[scaledHist][1])
        scaledHist.Draw()

        #legend
        lXmin = 0.65
        lXmax = 0.80
        lYmin = 0.65
        lYmax = 0.75
        l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
        l.SetFillStyle(0)
        l.SetFillColor(0)
        l.SetLineColor(0)
        l.SetTextFont(43)
        l.SetBorderSize(0)
        l.SetTextSize(15)
        l.AddEntry(scaledHist,"QCD scaled","pl") 
        l.Draw()
        
        #ATLAS
        a = ROOT.TLatex()
        a.SetNDC()
        a.SetTextFont(73)
        a.SetTextColor(1)
        a.SetTextSize(30)
        a.DrawLatex(lXmin,lYmax+0.12,'ATLAS')
		
        #internal
        p = ROOT.TLatex()
        p.SetNDC()
        p.SetTextFont(43)
        p.SetTextColor(1)
        p.SetTextSize(30)
        p.DrawLatex(lXmin+0.13,lYmax+0.12,'internal')
    
        #note
        n = ROOT.TLatex()
        n.SetNDC()
        n.SetTextFont(42)
        n.SetTextColor(1)
        n.SetTextSize(0.04)
        n.DrawLatex(lXmin,lYmax+0.05,'#intL dt = %s fb^{-1}'%(float(lumi.replace("p","."))))
        
        c1.Update()
        
        #------------------------------------------
        #write output file
        outputFile.cd()
        scaledHist.Write(scaledHist.GetName(), ROOT.TObject.kOverwrite)

        #------------------------------------------
        #write output canvas to file
        #c1.Write("c_"+scaledHist.GetName(), ROOT.TObject.kOverwrite) 

    #close output file
    outputFile.Close()

#******************************************
def addHisttoDict(dict,name, xaxislabel, yaxislabel = 'entries'):
  dict[name] = [xaxislabel,yaxislabel]

#******************************************
if __name__ == '__main__':

    #------------------------------------------
    #HOW TO: bjethistmaker(path, sample, sampletag, lumi, tag)
    #'path' the is trees/ output directory of downloadAndMerge.py
    #'sample' is the string to be used to search for the sample
    #'sampletag' is the tag to be used to search for the sample
    #'lumi' is the output luminosity [ifb], EXAMPLE: '0p01' for 0.01 ifb
    #'tag' tag used for the output files

    #EXAMPLE
    #path='../../../gridOutput/trees'
    #makeMjjHist(path, 'Pythia8_AU2CT10_jetjet', 'v5.0.0', '1p0', 'test.01')

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 4:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters (%s/%s) \
            \nHOW TO: time python -u singlexchecksdijethistmaker.py path sample sampletag lumi tag\n\
            \'inputFile\' path to file and filename including .root\n\
            \'lumi\' is the output luminosity [ifb], EXAMPLE: \'0p01\' for 0.01 ifb\n\
            \'tag\' tag used for the output files\n\
EXAMPLE: python -u singlexchecksdijethistmaker.py ../../../gridOutput/trees/test.root 10 test.01'\
#EXAMPLE: time python -u singlexchecksdijethistmaker.py ../../../gridOutput/trees/ Pythia8_AU2CT10_jetjet v5.0.0 1p0 test.01'\
            %(len(sys.argv),4))

    #------------------------------------------
    #get input parameters and run
    inputFile = sys.argv[1].strip()
    lumi = sys.argv[2].strip()
    tag = sys.argv[3].strip()

    singlexchecksdijethistmaker(inputFile, lumi, tag)

    #------------------------------------------
    print '\ndone'

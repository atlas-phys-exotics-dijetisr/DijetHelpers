# Latest files: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ExoticDijets2015Ntuples#Week_1_jet_inputs_configuration

# tree+IBLOnOnly = 3p34 inv fb, tree+IBLOffAndOn = 3p57 invfb With Debug Stream & corresponding 25ns MC now duplicateless

# CurrentData 
python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/CurrentData/tree Main data 10 EOYE_tree xchecks

python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/CurrentData/IBLOnOnly Main data 10 EOYE_IBLOnOnly xchecks

python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/CurrentData/IBLOffAndOn Main data 10 EOYE_IBLOffAndOn xchecks

# MC15_20151017
python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/MC15_20151017/QCD/tree jetjet All 10 EOYE_XChecks_MC15_20151017 xchecks

#-------------------------------------------------------------------------------------------------------
# Period D and E 520/pb With Debug Stream & corresponding 25ns MC 
# Current_Data 
#python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/Current_Data/tree Main data 10 XChecksPeriodDE520invpb xchecks

# Full_25ns_20150813
#python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/Full_25ns_20150813/QCDPythia8 jetjet All 10 XChecksMC15aFull25ns xchecks

#-------------------------------------------------------------------------------
# LHCP files!!

# Period D (D3-D6 incl 50ns D5) 80.4/pb With Debug Stream & corresponding 25ns MC

# Data15_D_fGRL_20150826 

#python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/Data_fGRL_Hopeful_20150828/tree Main data 10 XChecksPeriodDFinalLHCPHopeful xchecks

# Full_25ns_20150813
#python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/Full_25ns_20150813/QCDPythia8 jetjet All 10 XChecksMC15aFull25ns xchecks


#-------------------------------------------------------------------------------
## Running on older files

# Period D (D3-D6 incl 50ns D5) 80.4/pb & corresponding 25ns MC

# Data15_D_fGRL_20150826
#python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/Data15_D_fGRL_20150826/tree Main data 10 XChecksPeriodDFinalLHCP xchecks

# Period D3 + Run 276416 final All Good GRL used & corresponding 25ns MC

# Data15_D34_fGRL_20150821
#python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/Data15_D34_fGRL_20150821/tree/tree Main data 10 XChecksD3PlusRun276416fGrl xchecks

# Full_25ns_20150813
#python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/Full_25ns_20150813/QCDPythia8 jetjet All 10 XChecksMC15aFull25ns xchecks

#Period C & Corresponding 50 ns MC

# Data15_C_fGRL_20150718 (Period C2+C3+C4, 72 inv pb)
#python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/Data15_C_fGRL_JetCalibCorrection_20150721/tree Main Nominal 10 XChecksPeriodC2C3C4 xchecks

# Full_20150710/MC15a
#python -u runsingle.py /afs/cern.ch/work/l/lberesfo/public/JetEtMiss/Samples/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/Full_20150710/MC15a/tree jetjet AllUp 10 XChecksMC15a xchecks


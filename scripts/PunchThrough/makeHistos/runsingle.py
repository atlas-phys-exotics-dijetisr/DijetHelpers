#!/usr/bin/env python

#******************************************
#runsingle.py
# Based on script from DijetHelpers package written by run II dijet team, and adapted by Lydia Beresford

#make scaled histograms (not just mjj) from input MC JZ*W trees

#NOTE just run the script without any parameter to get instruction

#EXAMPLE: python -u runsingle.py ../../../gridOutput/trees/ Pythia8EvtGen_A14NNPDF23LO_jetjet v24_20150422 10 test.01

#******************************************
# TODO FIXME
# don't cut on emscale eta, use flag to say EM or LC and cut/fill accordingly

#******************************************
#import stuff
import ROOT, plotUtils, math, os, sys, subprocess,time, sys, glob, datetime
from ROOT import *

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('../atlasstyle-00-03-05/AtlasStyle.C') #~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
# Set batch mode for root
ROOT.gROOT.SetBatch(True)

#******************************************
def runsingle(path, sample, sampletag, lumi, tag, plotMe):

    print('\nmaking scaled histograms')

    #------------------------------------------
    #check lumi syntax
    #NOTE points are used to separate fields in the name
    lumi = str(lumi)
    if '.' in lumi:
        raise SystemExit('\n***ERROR*** luminosity value cannot have points\n***ERROR*** EXAMPLE: use \'0p01\' for 0.01 ifb')

    #------------------------------------------
    #input parameters    
    print '\nparameters:'
    print '  path:       %s'%path
    print '  sample:     %s'%sample
    print '  sampletag:  %s'%sampletag
    print '  lumi [ifb]: %s'%lumi.replace("p",".")
    print '  tag:        %s'%tag

    #------------------------------------------
    #find the files in path which match sample and sample-tag
    inputFileList = os.listdir(path)
    inputFiles = []
    print '\ninput files:'
    for file in inputFileList:
        if not sample in file: continue
        if not sampletag in file: continue
        inputFiles.append(path+'/'+file)
        print '  '+path+'/'+file

    #------------------------------------------
    #make sure we have found some files
    if len(inputFiles) == 0:
        print '\nno files found for sample: %s and sample-tag: %s'%(sample, sampletag)
        return

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))

    #------------------------------------------
    #check output files
    outputDir = localdir+'/../results/'
    outputFileName = outputDir+'/ScaledHistograms.'+lumi+'.ifb.'+tag+'.root'
    if os.path.isfile(outputFileName):
        raise SystemExit('\n***WARNING*** output file exists already: '+outputFileName)

    pids = []
    logFiles = []
    NCORES = 8

    if not os.path.exists("logs/plotting/"):
      os.makedirs("logs/plotting/")

    for iFile, thisFile in enumerate(inputFiles):
      if not os.path.exists("logs/plotting/{0}/".format(tag)):#os.path.basename(thisFile)[:-5])):
        os.makedirs("logs/plotting/{0}/".format(tag))#os.path.basename(thisFile)[:-5]))

      ## Wait for job if cores are all used or the output file is in use ##
      while len(pids) >= NCORES:
        #while len(pids) >= NCORES or any(os.path.basename(thisFile)[:-5] in logFile.name and thisFit in logFile.name for logFile in logFiles):
        wait_completion(pids, logFiles)

      sendString = ''
      print plotMe
      if "bjet" in plotMe:
        sendString = 'python -u singlebjethistmaker.py %s %s %s_%i'%(thisFile, lumi,tag,iFile)
      elif "xchecks" in plotMe:
        print "Running xchecks"
        sendString = 'python -u singlexchecksdijethistmaker.py %s %s %s_%i'%(thisFile, lumi,tag,iFile)
      elif "dijet" in plotMe:
        print "Running dijet"
        sendString = 'python -u singledijethistmaker.py %s %s %s_%i'%(thisFile, lumi,tag,iFile)
      elif "resonance" in plotMe:
        print "Running resonance"
        sendString = 'python -u singleresonancedijethistmaker.py %s %s %s_%i'%(thisFile, lumi,tag,iFile)
      elif "angular" in plotMe:
        print "Running angular"
        sendString = 'python -u singleangulardijethistmaker.py %s %s %s_%i'%(thisFile, lumi,tag,iFile)
      else:
        print "Empty plotMe, please specify one word from the options available"

      logFile = "logs/plotting/%s/%s_%i.txt"%(tag,os.path.basename(thisFile)[:-5],iFile)
      print "Starting job", sendString
  
      res = submit_local_job(sendString, logFile)
      pids.append(res[0])
      logFiles.append(res[1])

    wait_all(pids, logFiles)

    for f in logFiles:
      f.close()


    ### Merge output files ###
    # Gather output files with this tag
    
    outputFiles = glob.glob('../results/ScaledHistograms.%s.ifb.%s_*.root'%(lumi,tag))
    outputHadFile = '../results/ScaledHistograms.%s.ifb.%s.root'%(lumi,tag)
    haddString = 'hadd '+outputHadFile+' '+' '.join(outputFiles) # Space then join adds elements of list and puts space between them 
    os.system(haddString)
  
def submit_local_job(exec_sequence, logfilename):
  #os.system("rm -f "+logfilename)
  output_f=open(logfilename, 'w')
  pid = subprocess.Popen(exec_sequence, shell=True, stderr=output_f, stdout=output_f)
  time.sleep(0.1)  #Wait to prevent opening / closing of several files

  return pid, output_f

def wait_completion(pids, logFiles):
  print """Wait until the completion of one of the launched jobs"""
  while True:
    for pid in pids:
      if pid.poll() is not None:
        print "\nProcess", pid.pid, "has completed"
        logFiles.pop(pids.index(pid)).close()  #remove logfile from list and close it
        pids.remove(pid)

        return
    print ".",
    sys.stdout.flush()
    time.sleep(3) # wait before retrying

def wait_all(pids, logFiles):
  print """Wait until the completion of all launched jobs"""
  while len(pids)>0:
    wait_completion(pids, logFiles)
  print "All jobs finished!"


#******************************************
if __name__ == '__main__':

    #------------------------------------------
    #HOW TO: runsingle(path, sample, sampletag, lumi, tag)
    #'path' the is trees/ output directory of downloadAndMerge.py
    #'sample' is the string to be used to search for the sample
    #'sampletag' is the tag to be used to search for the sample
    #'lumi' is the output luminosity [ifb], EXAMPLE: '0p01' for 0.01 ifb
    #'tag' tag used for the output files

    #EXAMPLE
    #path='../../../gridOutput/trees'
    #makeMjjHist(path, 'Pythia8_AU2CT10_jetjet', 'v5.0.0', '1p0', 'test.01')

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 7:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters (%s/%s) \
            \nHOW TO: time python -u runsingle.py path sample sampletag lumi tag\n\
            \'path\' is the trees/ output directory of downloadAndMerge.py\n\
            \'sample\' is the string to be used to search for the sample\n\
            \'sampletag\' is the tag to be used to search for the sample\n\
            \'lumi\' is the output luminosity [ifb], EXAMPLE: \'0p01\' for 0.01 ifb\n\
            \'tag\' tag used for the output files\n\
            \'plotMe\' string used to state which plotter you are running, options: bjet, xchecks\n\
EXAMPLE: python -u runsingle.py ../../../gridOutput/trees/ Pythia8EvtGen_A14NNPDF23LO_jetjet v24_20150422 10 test.01 xchecks'\
            %(len(sys.argv),7))

    #------------------------------------------
    #get input parameters and run
    path = sys.argv[1].strip()
    sample = sys.argv[2].strip()
    sampletag = sys.argv[3].strip()
    lumi = sys.argv[4].strip()
    tag = sys.argv[5].strip()
    plotMe = sys.argv[6].strip()

    print "Starting plotting at", datetime.datetime.now().time()
    runsingle(path, sample, sampletag, lumi, tag, plotMe)
    print "Finished plotting at", datetime.datetime.now().time()

    #------------------------------------------
    print '\ndone'

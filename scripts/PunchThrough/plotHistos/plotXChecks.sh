# MC followed by Data (as for overlay plots 1st plot (MC) is overlaid to integral of 2nd plot (Data))

# 2p01 inv fb With Debug Stream & corresponding 25ns MC 
python XChecksplots.py XChecks_MC15_20151017 XChecks_2p01invfb "MC15a" "Data" XChecksMC1525ns2p01invfb

#-------------------------------------------------------------------------------------------------------
# Period D and E F G 1/fb With Debug Stream & corresponding 25ns MC 
#python XChecksplots.py XChecks_MC15_20151017 XChecks_PeriodDEFGH_1p4invfb "MC15a" "Period D-H data" XChecksMC1525nsPeriodDEFGH1p4invfb

# Period D and E F G 1/fb With Debug Stream & corresponding 25ns MC 
#python XChecksplots.py XChecksMC15aFull25nsNewMinimalTeVAsymFix XChecksPeriodDEFG1invfbNewMinimalTeVAsymFix "MC15a" "Period D-G data" XChecksMC15a25nsPeriodDEFG1invfbLatest

# Period D and E 520/pb With Debug Stream & corresponding 25ns MC 
#python XChecksplots.py XChecksMC15aFull25ns XChecksPeriodDE520invpb "MC15a" "Period D+E data" XChecksMC15a25nsPeriodDE520invpb

# Period D and E 320/pb With Debug Stream & corresponding 25ns MC 
#python XChecksplots.py XChecksMC15aFull25ns XChecksPeriodDE320invpb "MC15a" "Period D+E data" XChecksMC15a25nsPeriodDE320invpb

#-------------------------------------------------------------------------------------------------------
# LHCP files!!

# Period D (D3-D6 incl 50ns D5) 80.4/pb & corresponding 25ns MC With Debug Stream
#python XChecksplots.py XChecksMC15aFull25ns XChecksPeriodDFinalLHCPHopeful "MC15a" "Period D data" XChecksMC15a25nsPeriodDFinalLHCPHopeful

#-------------------------------------------------------------------------------------------------------
## Running on older files

# Period D (D3-D6 incl 50ns D5) 80.4/pb & corresponding 25ns MC
#python XChecksplots.py XChecksMC15aFull25ns XChecksPeriodDFinalLHCP "MC15a" "Period D data" XChecksMC15a25nsPeriodDFinalLHCP

# Period D3 + Run 276416 final All Good GRL used
#python XChecksplots.py XChecksMC15aFull25ns XChecksD3PlusRun276416fGrl "MC15a" "D3 + Run 276416" XChecksMC15a25nsPeriodD3PlusRun276416fGrl

# Early Period D no GRL and corresponding 25 ns MC
#python XChecksplots.py XChecksMC15aFull25ns XChecksEarlyD12noGrl "MC15a" "Period D Data" XChecksMC15aPeriodD12

# Period C & Corresponding 50 ns MC
#python XChecksplots.py XChecksMC15a XChecksPeriodC2C3C4 "MC15a" "Period C Data" HCWPrelimXChecksMC15aPeriodC2C3C4

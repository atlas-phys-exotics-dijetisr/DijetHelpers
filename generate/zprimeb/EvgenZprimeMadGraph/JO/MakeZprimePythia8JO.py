#!/bin/python
import commands

VectorOf_M_Zprime = [ "350","400","450","500","550","600","650","700","750","800","850","900","950","1000","1050","1100","1150","1200","1250","1300","1350","1400","1450","1500","1550","1600","1650","1700","1750","1800","1850","1900","1950","2000","2100","2200","2300","2400","2500","2600","2700","2800","2900","3000","3200","3400","3600","3800","4000","4200","4400","4600","4800","5000","5200","5400","5600","6000"]
## above 2.7 TeV couplings to large (>2.5)
#,"2800","2900","3000","3200","3400","3600","3800","4000","4200"]

#ChannelNumbers = xrange(180700,180770)
#ChannelNumbers = xrange(180600,180670)
#ChannelNumbers = xrange(180800,180870)

ChannelNumbers = xrange(180900,180970) ## coupling 20%

M_Zprime_value = VectorOf_M_Zprime[0]
channelNumber = ChannelNumbers[0]

submissionfile = open('submitAllSubmissions.sh','w')

for M_Zprime_value, channelNumber in zip(VectorOf_M_Zprime, ChannelNumbers) :


  #let's write the JO
  outputJOFileName = "MC15."+str(channelNumber)+".MadGraphPythia8EvtGen_A14MSTW2008LO_Zprime_"+M_Zprime_value+"f1.py"
  inputLHEcheck = "Zprime_"+M_Zprime_value
  inputLHEfile = "group.phys-gener.madgraph."+str(channelNumber)+".Zprime_"+M_Zprime_value+".TXT.mc12_v1"
  outputJOFile = open(outputJOFileName,"w")

  outputJOFile.write("""evgenConfig.description = "Zprime using MadGraph/Pythia"
evgenConfig.keywords = ["Zprime"]
evgenConfig.inputfilecheck = '"""+inputLHEcheck+"""'
runArgs.inputGeneratorFile='"""+inputLHEfile+"""._00001.events'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
""")


  #include("MC12JobOptions/Pythia8_Photos.py")

  #now that we have the JO, let's write a script to run it
  jobscriptName = "MC15."+str(channelNumber)+".Pythia8EvtGen_A14MSTW2008LO_Zprime_"+M_Zprime_value+"f1_submit.sh"
  jobscript = open(jobscriptName,"w")
    
  jobscript.write("""
#!/bin/bash
# Simple submission script for MC15 validation

# for tracking when and where you run
hostname
date

# to make and use a temporary directory
tmpDir=`mktemp -d`
cd ${tmpDir}
echo 'we are in '${PWD}

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

#localSetupGcc gcc462_x86_64_slc6

# Setup ROOT and various libraries
source /afs/cern.ch/atlas/software/dist/AtlasSetup/scripts/asetup.sh 19.2.3.5

#copy everything we need into this temp directory
cp /afs/cern.ch/user/b/boveia/work/zprimeb/EvgenZprimeMadGraph/JO/"""+outputJOFileName+""" .
cp /afs/cern.ch/user/b/boveia/work/zprimeb/EvgenZprimeMadGraph/JO/"""+inputLHEfile+"""._00001.tar.gz """+inputLHEfile+"""._00001.events.tar.gz
tar zxvf """+inputLHEfile+"""._00001.events.tar.gz
echo 'Needed files copied:'
ls

#run the job transform
Generate_tf.py --ecmEnergy 13000 --runNumber """+str(channelNumber)+""" --firstEvent 0 --maxEvents 30000 --randomSeed 2354 --jobConfig """+outputJOFileName+""" --outputEVNTFile event.root 

#run the ntuplemaker
#Reco_tf.py --inputEVNTFile event.root --outputNTUP_TRUTHFile output.root 
source /afs/cern.ch/atlas/software/dist/AtlasSetup/scripts/asetup.sh 20.1.4.1,AtlasDerivation,gcc48,here
Reco_tf.py --inputEVNTFile event.root --outputDAODFile output.root --reductionConf TRUTH1

#copy the output
if [ ! -d  /afs/cern.ch/work/b/boveia/MCoutput/ ]; then
mkdir /afs/cern.ch/work/b/boveia/MCoutput/
fi

cp event.root   /afs/cern.ch/work/b/boveia/MCoutput/"""+outputJOFileName+"""_event.root
cp DAOD_TRUTH1.output.root /afs/cern.ch/work/b/boveia/MCoutput/DAOD_TRUTH1."""+outputJOFileName+""".root

""")

  #making it executable

  commands.getoutput("chmod 755 "+jobscriptName)
  
  #running it                                                                                    
  submissionfile.write("bsub -q 8nh "+jobscriptName+"\n")

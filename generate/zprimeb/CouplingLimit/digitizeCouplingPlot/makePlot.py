from ROOT import *
from array import array
from math import *

gROOT.LoadMacro("AtlasStyle.C")
SetAtlasStyle()

# actual lumi used for limits
actualLumi = 80
# luminosity to scale limits, in ipb
scaleLumi = actualLumi

def readDataFromFile(filename):
  xArr = []
  yArr = []
  with open(filename) as f:
    content = f.readlines()
    dataFound=False
    for line in content:
      if dataFound:
        tmpData=line.rstrip().rstrip(",").split(",")
        xArr.append(float(tmpData[0])/1000.)
        yArr.append(float(tmpData[1])/1000)
      if "Mass,coupling" in line:
        dataFound=True
    return xArr,yArr

def getTGraphFromFile(filename):
  if 'ichep' in filename:
    return getIchep()
  if 'hcp' in filename:
    return getHcp()
  if 'eps2015' in filename:
    return getEps2015()
  if 'lhcp2015' in filename:
    return getLHCP2015()
  xArr,yArr=readDataFromFile(filename)
  tmpX=array("d",xArr)
  tmpY=array("d",yArr)
  graph =TGraph(len(tmpX),tmpX,tmpY)
  graph.SetName(filename)
  return graph

def getIchep():
  ### Add ICHEP limit
  ichepLimitX=[350.0, 400.0, 450.0, 500.0, 550.0, 600.0, 650.0, 700.0, 750.0, 800.0, 850.0, 900.0, 950.0, 1000.0, 1050.0, 1100.0, 1150.0, 1200.0, 1250.0, 1300.0, 1350.0, 1400.0, 1450.0, 1500.0, 1550.0, 1600.0, 1650.0, 1700.0, 1750.0, 1800.0, 1850.0, 1900.0, 1950.0, 2000.0, 2100.0, 2200.0, 2300.0, 2400.0, 2500.0, 2600.0, 2700.0] 
  ichepLimitY=[1.83226460946, 1.42138407588, 1.66801648057, 1.69411596458, 1.28328786941, 1.04752415135, 0.960153807887, 0.931853558699, 0.707833005367, 0.539342349237, 0.547281503216, 0.488638912274, 0.458895798825, 0.468023716253, 0.61054151248, 0.680838968461, 0.694103864637, 0.628964667799, 0.51447495217, 0.535075264471, 0.598848277835, 0.696378667345, 0.797397542416, 0.916751399725, 1.04792821617, 1.0590602504, 1.12957508134, 1.19704973832, 1.31254117254, 1.37698444128, 1.40326540136, 1.35039850854, 1.17634902775, 1.27961656078, 1.64090298882, 1.81211412757, 1.83896580436, 1.72308127826, 2.1041661851, 2.54936880756, 2.68784437302]
  for i in range(0,len(ichepLimitX)):
    ichepLimitX[i] = ichepLimitX[i]/1000.
  tmpX=array("d",ichepLimitX)
  tmpY=array("d",ichepLimitY)
  graph =TGraph(len(tmpX),tmpX,tmpY)
  graph.SetName("ichep")
  return graph

def getHcp():
  ### Add HCP limit
  hcpLimitX=[1500.0, 1550.0, 1600.0, 1650.0, 1700.0, 1750.0, 1800.0, 1850.0, 1900.0, 1950.0, 2000.0, 2100.0, 2200.0, 2300.0, 2400.0, 2500.0, 2600.0, 2700.0] 
  hcpLimitY=[1.1197146499, 1.24427347277, 1.24185822227, 1.38813889474, 1.43385504252, 1.48648326924, 1.575744256, 1.53026023765, 1.46602002948, 1.38634062427, 1.31468104095, 1.51918096882, 1.76105838431, 2.04014938506, 2.3448166206, 2.57212509636, 2.9527187655, 3.14515783204]
  for i in range(0,len(hcpLimitX)):
    hcpLimitX[i] = hcpLimitX[i]/1000.
  tmpX=array("d",hcpLimitX)
  tmpY=array("d",hcpLimitY)
  graph =TGraph(len(tmpX),tmpX,tmpY)
  graph.SetName("hcp")
  return graph

def getLHCP2015():
  lhcp2015LimitX=[1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1900,1950,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3200,3400,3600,3800,4000,4200,4400,4600,4800,5000]
  lhcp2015LimitY=[1.46424860566,1.52810934184,1.71147229174,1.99574561215,2.02701051517,2.04067629976,2.33216069992,2.47533301953,2.78605319039,3.27480129883,3.31175761296,2.75864915005,2.82827591588,2.51588367434,3.49855871494,2.77033285109,3.72520920851,2.99474772683,3.74245549291,3.78538589632,3.7678369581,4.99473727232,4.94750077287,6.07004010771,6.8304022057,8.15805809392,7.48450167703,8.77825052346,11.931892747,10.7863296973,12.9308725551,13.049927663,15.4088945762,21.3795697214,23.219524661,31.3311737172]
  for i in range(0,len(lhcp2015LimitY)):
    lhcp2015LimitY[i] = lhcp2015LimitY[i]/sqrt(sqrt(scaleLumi/actualLumi))
  for i in range(0,len(lhcp2015LimitX)):
    lhcp2015LimitX[i] = lhcp2015LimitX[i]/1000.
  tmpX=array("d",lhcp2015LimitX)
  tmpY=array("d",lhcp2015LimitY)
  graph =TGraph(len(tmpX),tmpX,tmpY)
  graph.SetName("lhcp2015")
  return graph

def getEps2015():
  ### Add EPS2015 limit
  #### first run before JES uncertainty fix
  # eps2015LimitX=[1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1900,1950,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3200]
  #eps2015LimitY=[2.92120353016, 2.70318610612, 3.31426834346, 3.26547280952, 3.29335188073, 4.1523818724, 4.39963393131, 4.42129797697, 5.30187047227, 6.31644447724, 6.08846784714, 4.9267224766, 5.77885681014, 4.51576416386, 6.89907581032, 5.54674368979, 7.29125581442, 5.68387128603, 6.57059742018, 8.03994580528, 8.26659177451, 11.3205016062, 10.8067664042, 11.6464844416, 10.4986648532, 11.8336146386, 27.6710038323]

  # 6 july version
  #eps2015LimitX=[1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1900,1950,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3200]
  #eps2015LimitY=[7.36813567113,6.19020510005,5.1458954009,4.21806579163,3.84707600756,4.59947480352,4.57984209921,4.38541099473,5.23198956331,6.42237993505,6.73681760547,6.1067563689,7.47086395046,7.13821778633,10.4410197028,8.76166671617,11.7200712263,9.71758885932,9.22172048113,9.36445834265,8.73742888318,11.4649596725,11.3644761261,13.242625771,13.5103488489,15.7997968109,18.5949614905]
  
  # remove every other number from 6 july version
  # eps2015LimitX=[1200,1300,1400,1500,1600,1700,1800,1950,2100,2300,2500,2700,2900,3200]
  # eps2015LimitY=[7.36813567113,5.1458954009,3.84707600756,4.57984209921,5.23198956331,6.73681760547,7.47086395046,10.4410197028,11.7200712263,9.22172048113,8.73742888318,11.3644761261,13.5103488489,18.5949614905]

  # 16 july version, for approval
  # eps2015LimitX=[1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1900,1950,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3200]
  # eps2015LimitY=[6.49736253866,2.62081527381,3.57392840115,4.64487084849,4.67303731287,3.3683298884,2.50141318976,2.49579077444,3.37460107175,5.03825809467,5.67401460961,5.1801397113,5.72017680215,4.86607334519,4.70935160893,4.3130671924,6.80400192641,6.57069022798,7.8448492358,8.77535857066,7.22879423181,7.69234434396,8.19558079458,9.97073674542,10.218065673,11.3713155806,11.7618630503]

  # 19 july version, C4
  # eps2015LimitX=[1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1900,1950,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3200,3400]
  # eps2015LimitY=[1.42306993933,1.30622229848,1.71549608962,2.04857330141,2.36645044151,2.49349752985,1.79681388858,1.96573022298,2.82951690755,3.51453034473,3.2516083244,2.50417930903,2.52934536213,2.77580124763,3.2604521821,3.06702540623,4.59893071245,3.6235032428,3.355307496,3.37845647196,2.83587000579,4.07335275988,4.20194590928,4.75709347253,4.21663067496,4.88444616505,7.0943900191,9.83468422933]

  # 20 july version, C4 72/pb
  eps2015LimitX=[1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1900,1950,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3200,3400]
  eps2015LimitY=[1.4827236738,1.36068123867,1.7902461876,2.13771307439,2.45301459125,2.60063361765,1.87447333256,2.04241185963,2.94362731662,3.65613530172,3.38723169476,2.61225573681,2.62761391443,2.90853326069,3.39000166349,3.19680385679,4.78076169375,3.79962427857,3.48991219102,3.51835267717,2.94548022185,4.25539983634,4.36573828322,4.96952985623,4.43478093755,5.08580453538,7.35630373645,10.2097497015]  
  
  for i in range(0,len(eps2015LimitY)):
    eps2015LimitY[i] = eps2015LimitY[i]/sqrt(sqrt(scaleLumi/actualLumi))
  for i in range(0,len(eps2015LimitX)):
    eps2015LimitX[i] = eps2015LimitX[i]/1000.
  tmpX=array("d",eps2015LimitX)
  tmpY=array("d",eps2015LimitY)
  graph =TGraph(len(tmpX),tmpX,tmpY)
  graph.SetName("eps2015")
  return graph

 
def convertNameToLegend(inputName):
  convertDict={'atlas1.csv' : 'ATLAS 8 TeV fb^{-1}', 'cdf1.csv' : 'CDF 1.96 TeV, 1.1 fb^{-1}', 'cdf2.csv' : 'CDF Run I', 'cms1.csv' : 'CMS 8 TeV, 20 fb^{-1}', 'cms2.csv' : 'CMS 4 fb^{-1}', 'cms3.csv' : 'CMS 5 fb^{-1}', 'cms4.csv' : 'CMS 0.13 fb^{-1}', 'ua2.csv' : 'UA2', 'ichep': 'ATLAS 8 TeV, 20 fb^{-1}','hcp' : 'ATLAS 8 TeV 13 fb^{-1}' , 'eps2015':'ATLAS 13 TeV, '+str(scaleLumi/1000.)+' fb^{-1}'}
  return convertDict[inputName]

def convertNameToStyle(inputName):
  convertDict={'atlas1.csv' : 3, 'cdf1.csv' : 1, 'cdf2.csv' : 2, 'cms1.csv' : 1, 'cms2.csv' : 3, 'cms3.csv' : 2, 'cms4.csv' : 4, 'ua2.csv' : 1, 'ichep': 4, 'hcp' : 2, 'eps2015': 1}
  return convertDict[inputName]

def convertNameToColor(inputName):
  convertDict={'atlas1.csv' : 2, 'cdf1.csv' : 3, 'cdf2.csv' : 3, 'cms1.csv' : 4, 'cms2.csv' : 4, 'cms3.csv' : 4, 'cms4.csv' : 4, 'ua2.csv' : kCyan, 'ichep': 2, 'hcp' : 2 , 'eps2015': 2}
  convertDict={'atlas1.csv' : kRed, 'cdf1.csv' : kGreen+1, 'cdf2.csv' : kGreen+1, 'cms1.csv' : 4, 'cms2.csv' : 4, 'cms3.csv' : 4, 'cms4.csv' : 4, 'ua2.csv' : kCyan+1, 'ichep': kRed, 'hcp' : kRed , 'eps2015': kRed}
#  convertDict={'atlas1.csv' : kBlue, 'cdf1.csv' : kGreen, 'cdf2.csv' : kGreen, 'cms1.csv' : kTeal, 'cms2.csv' : kTeal, 'cms3.csv' : kTeal, 'cms4.csv' : kTeal, 'ua2.csv' : kCyan+1, 'ichep': kBlue, 'hcp' : kBlue }
  return convertDict[inputName]

#c1 = TCanvas()
c1 = TCanvas("c1","c1",1600,1200)
multiGraph=TMultiGraph()

l1=TLegend(0.6,0.2,0.9,0.45)
l1.SetNColumns(1)
fileList=['cms1.csv','ichep','eps2015'] # 'atlas1.csv',
# fileList=['ua2.csv','cms3.csv','cdf2.csv','cms1.csv','cdf1.csv','cms4.csv','ichep','cms2.csv','eps2015'] # 'atlas1.csv',
for iFile in fileList:
  g1=getTGraphFromFile(iFile)
  g1.SetLineColor(convertNameToColor(g1.GetName()))
  g1.SetLineStyle(convertNameToStyle(g1.GetName()))
  g1.SetLineWidth(2)
  multiGraph.Add(g1)
  legendName = convertNameToLegend(g1.GetName())
  l1.AddEntry(g1,legendName,"l")


multiGraph.SetTitle(";M_{Z^{'}_{B}}[TeV];g_{B}")
if scaleLumi<10:
  multiGraph.SetMaximum(2.7) # 2.7
else:
  multiGraph.SetMaximum(2.7) #12.6) # 2.7
multiGraph.Draw("al")
l1.SetLineColor(0)
l1.SetFillColor(0)
l1.SetShadowColor(0)
l1.Draw()

text = TLatex()
# text.SetTextFont(40)
text.SetTextAngle(90)
text.SetTextSize(0.03)
text.DrawLatex(3.1,0.4,"CMS 8 TeV 20 fb^{-1} from arXiv:1306.2629")

textATLAS = TLatex()
textATLAS.SetTextSize(0.04)
textATLAS.DrawLatex(2.13,1.05,"#font[72]{ATLAS }#bf{Internal}")

xaxis = multiGraph.GetXaxis()
xaxis.SetRangeUser(1,3)

c1.SaveAs("couplingLimit"+str(scaleLumi)+".pdf")
c1.SaveAs("couplingLimit"+str(scaleLumi)+".C")
#c1.SaveAs("couplingLimit.eps")

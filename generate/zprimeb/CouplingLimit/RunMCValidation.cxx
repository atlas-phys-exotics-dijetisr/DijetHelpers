//g++ -o MCValidation.exe RunMCValidation.cxx `$ROOTSYS/bin/root-config --cflags --glibs`

#include "MCValidation.h"
#include "TString.h"
#include "stdlib.h"
#include <iostream>
#include <sstream>
#include "Utilities.h"
#include <TEnv.h>
//#include <vector.h>

using namespace std;

int main (int argc, char *argv[]) {

  TEnv *settings = new TEnv();

  TString config_fn=argv[1];

  printf("Reading settings from: %s\n",config_fn.Data());
  printf("...\n\n");
  int status=settings->ReadFile(config_fn.Data(),EEnvLevel(0));
  if (status!=0) std::cout<<"Cannot read file"<<std::endl;

  std::vector<TString> FilesVector;
  std::vector<TString> OutputDirs;


  std::string MassesString = settings->GetValue("Masses","900:1000:1100:1200:1400:1600:1800:2000:2200:2400");
  std::string NumberMCSamplesString = settings->GetValue("NumberMCSamples","3"); //New flag
  std::string useSmearing = settings->GetValue("UseSmearing","False"); //New flag
  
  istringstream buffer(NumberMCSamplesString);
  int NumberMCSamples;
  buffer >> NumberMCSamples; 

  std::vector<int>  masses = Utilities::tokenizeBins(MassesString);
  
  for(int i=0;i<NumberMCSamples;i++)
    {
      TString Ntuple=TString::Format("NtupleFile%d",i);
      TString OutputHisto=TString::Format("OutputHistoFile%d",i);

      FilesVector.push_back(settings->GetValue(Ntuple,"/atlas/users/picazio/WprimeAnalisis/MC12_Wprime_Validation/MatrixCodes/Outputs/OutputNtuples_MC11_Pythia6/m%d.root"));
            
      OutputDirs.push_back(settings->GetValue(OutputHisto,"/atlas/users/picazio/WprimeAnalisis/MC12_Wprime_Validation/MatrixCodes/Outputs/OutputHistos_MC11_Pythia6"));
    }
  
  int initialChannelNumber = 158066;
  for (int p=0; p<FilesVector.size();p++){
    for (int i=0; i<masses.size();  ++i) { 
      
      TString filename = TString::Format(FilesVector[p],masses[i]);
      cout<<"filename"<<filename<<endl;
      TString mass = TString::Format("%d", masses[i]);
      
      TChain * eventTree = new TChain("truth");
      MCValidation * theMCValidation = new MCValidation(eventTree, mass, masses[i],OutputDirs[p],useSmearing=="True");  
      
      eventTree->Add(filename); 
      eventTree->Process(theMCValidation);
    }  
  }
}

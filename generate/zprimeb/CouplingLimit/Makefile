# W. H. Bell, G. Brandt, O. Kepka
#
# Standalone make file for building with Root.  To use it type
#
#   make
#
# There are also clean and veryclean targets
#
#   make veryclean
# 
# Adapted by C. Doglioni - 08/11/2010

PACKAGE=MC12ValidationFramework

ANALYSIS=RunMC12Validation

CC=g++ -Wall

#INCLUDES = Makefile.standalone

BASEDIR = .
SRCDIR  = $(BASEDIR)
INCDIR  = $(BASEDIR)
OBJDIR  = $(BASEDIR)/tmp
EXEDIR  = $(BASEDIR)/bin

TARGET  = $(EXEDIR)/$(PACKAGE)

SKIPLIST = RootDictionary.cxx 
CCLIST = $(filter-out $(SKIPLIST),$(patsubst $(SRCDIR)/%cxx, %cxx, $(wildcard $(SRCDIR)/*.cxx)))
CCLIST += RootDictionary.cxx
#CCLIST   += Root/RegularFormula.cxx

OBJECTS = $(patsubst %.cxx,%.o,$(CCLIST))

# # histograms
# HISTOBJ = $(patsubst %, %_hist.o, $(ANALYSIS)) 
# # remove *_hist.o which are passed to linker with a separate variable
# OBJECTS :=$(patsubst %_hist.o, , $(OBJECTS))
# #OBJECTS +=$(HISTOBJ)

vpath %.h   $(INCDIR) $(SRCDIR) 
vpath %.cxx $(SRCDIR) 
vpath %.o   $(OBJDIR)

DEBUG=-g -DDEBUG

INCFLAGS = $(DEBUG) -I$(INCDIR)
#-I$(INCMATHMOREDIR)
INCFLAGS += $(shell root-config --cflags) 

ROOTLIBS = $(shell root-config --libs) 

LIBS  = $(shell root-config --ldflags) -lTreePlayer $(DEBUG)
LIBS += $(ROOTLIBS) -ldl 
#LIBS += $(ROOTLIBS) -ldl  
# To link to the static library for the trigger helpers (compiled separately):

.PHONY: all
#$(TARGET): dirs $(HISTOBJ) $(OBJECTS) $(INCLUDES)
$(TARGET): dirs $(OBJECTS) $(INCLUDES)
	@echo $(CCLIST)
	@echo "**"
	@echo "** Linking Executable"
	@echo "**"
# 	$(CC) $(addprefix $(OBJDIR)/, $(notdir $(OBJECTS))) $(addprefix $(OBJDIR)/, $(notdir $(HISTOBJ))) $(LIBS) -o $(TARGET)
	$(CC) $(addprefix $(OBJDIR)/, $(notdir $(OBJECTS))) $(LIBS) -o $(TARGET)

dirs:
	@mkdir -p $(SRCDIR)
	@mkdir -p $(EXEDIR)
	@mkdir -p $(OBJDIR)
#	@rm -f $(SRCDIR)/*
#	cp $(BASEDIR)/*/*.h $(SRCDIR)
#	cp $(BASEDIR)/*/*.C $(SRCDIR)
#	cp $(BASEDIR)/*/*.cxx $(SRCDIR)
#	rename $(SRCDIR)/*.C $(SRCDIR)/*.cxx

clean:
	@rm -rf $(OBJDIR)
	@rm -f $(SRCDIR)/*~
	@rm -f $(SRCDIR)/.*.swp

veryclean: clean
	@rm -f $(SRCDIR)/RootDictionary.*
#	@rm -f $(SRCDIR)/*_hist.h; rm -f $(SRCDIR)/*_hist.cxx
	@rm -rf $(EXEDIR)

%.o: %.cxx  
	@echo "**"
	@echo "** Compiling C++ Source" 
	@echo "**"
	$(CC) -c $(INCFLAGS) $< -o $(OBJDIR)/$(notdir $@)

RootDictionary.cxx: MCValidation.h 
	@echo "**"
	@echo "** Creating Root dictionary"
	@echo "**"
	rootcint -f $(SRCDIR)/RootDictionary.cxx -c $<
#(SRCDIR)/EvtReader.h $(SRCDIR)/ChainWrapper.h $(SRCDIR)/LinkDef.h

# Hack to avoid need to re-run make file following code generation target.
RootDictionary.o: RootDictionary.cxx
	@echo "**"
	@echo "** Compiling C++ Source" 
	@echo "**"
	$(CC) -c $(INCFLAGS)  $(SRCDIR)/RootDictionary.cxx -o $(OBJDIR)/$(notdir $@)

###############################
# This includes the setting of random seeds and c.o.m. energy
include('PowhegControl/PowhegControl_Dijet_Common.py')
PowhegConfig.bornktmin = int(runArgs.runNumber)
PowhegConfig.pdf = 10800
PowhegConfig.fixspikes = True
PowhegConfig.withnegweights = 0 #1

ktmin = { 1:2 , 2:5 , 3:15 , 4:30 , 5:50 , 6:75 , 7:100 ,
        8:150 , 9:180 , 10:250 , 11:250 , 12:250 }
supp  = { 1:60 , 2:160 , 3:400 , 4:800 , 5:1300 , 6:1800 ,
        7:2500 , 8:3200 , 9:3900 , 10:4600 , 11:5300 ,
        12:5300 }

PowhegConfig.bornktmin = 15
PowhegConfig.bornsuppfact = 400

# This is a total guess...
#PowhegConfig.nEvents =  100000
PowhegConfig.nEvents =  10000
PowhegConfig.ncall2    = 20000

print PowhegConfig

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

##--------------------------------------------------------------
## EVGEN transform
##--------------------------------------------------------------
evgenConfig.description    = "POWHEG+Pythia8 dijet production with bornktmin = %i GeV, bornsuppfact = %i GeV, muR=muF=1 and AU2 CT10 tune: %i"%(PowhegConfig.bornktmin,PowhegConfig.bornsuppfact,runArgs.runNumber)
evgenConfig.keywords       = [ "QCD", "dijet", "jets" ]
evgenConfig.contact        = [ "james.robinson@cern.ch" ]
evgenConfig.generators    += [ "Pythia8" ]
evgenConfig.weighting      = 0      # to avoid failure with high weights - TODO: remove this if fix is implemented

##--------------------------------------------------------------
## Pythia8 showering with new, main31-style shower
##--------------------------------------------------------------
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
#include("MC12JobOptions/Pythia8_Powheg.py")
# -- New release (18.X.X)
include("MC12JobOptions/Pythia8_LHEF.py")
topAlg.Pythia8.Commands += [ "SpaceShower:pTmaxMatch = 2"
        , "TimeShower:pTmaxMatch = 2"
        ]
topAlg.Pythia8.UserHook = "Main31"

#########################################

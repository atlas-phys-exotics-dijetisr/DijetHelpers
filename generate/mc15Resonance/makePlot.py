#!/usr/bin/env python

from array import array
# Set up ROOT and RootCore:
import ROOT
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

# Initialize the xAOD infrastructure: 
ROOT.xAOD.Init()

IDS = [ 303892, 303893, 303894, 303895, 303896, 303897, 303898, 303899, 303900, 303901, 303902, 303903, 303904, 303905, 303906 ]

outFileName = "masses.root"
fOut = ROOT.TFile.Open(outFileName,"RECREATE")

color = ROOT.kBlue-10
count = 0
hists = [] 
maximum = 0
c0 = ROOT.TCanvas()

for id in IDS:

  # a mass plot - just hardcode it to be faster...sleepy
  bins = [96.0, 105, 116, 127, 138, 151, 164, 177, 192, 207, 222, 239, 256, 274, 293, 312, 333, 354, 376, 400, 424, 449, 475, 502, 530, 558, 587, 618, 650, 682, 716, 750, 787, 825, 865, 906, 949, 994, 1040, 1088, 1138, 1189, 1242, 1297, 1353, 1410, 1469, 1531, 1595, 1663, 1733, 1807, 1884, 1964, 2047, 2132, 2221, 2313, 2407, 2503, 2602, 2703, 2807, 2916, 3028, 3146, 3267, 3394, 3525, 3660, 3801, 3946, 4096, 4250, 4409, 4571, 4738, 4909, 5085, 5267, 5453, 5644, 5843, 6066, 6311, 6566, 6831, 7107, 7394, 7692, 8003, 8326, 8662, 9012 , 9376, 9755 ] #, 10149, 10559, 10985, 11429, 11890, 12370, 12870, 13390, 13930, 14493]
  name = "mass_"+str(id)
  hists.append( ROOT.TH1F( name, name, len(bins)-1, array('f', bins) ) )
  hists[count].Sumw2()
  hists[count].GetXaxis().SetTitle("m_{jj} from Leading Truth Jets [GeV]")
  hists[count].GetYaxis().SetTitle("Arbitrary Units")
  hists[count].SetTitle("q* Signals")
  hists[count].SetStats(0)
  hists[count].Rebin(5)
  hists[count].SetLineColor( color )
  hists[count].SetLineWidth( 2 )
  color = color+1

  # Set up the input files:
  fileName = "aod_"+str(id)+"/"
  treeName = "CollectionTree" # default when making transient tree anyway
  print("Open " + fileName)

  f = ROOT.TFile.Open(fileName)

  # Make the "transient tree":
  t = ROOT.xAOD.MakeTransientTree( f, treeName)

  # Print some information:
  print( "Number of input events: %s" % t.GetEntries() )
  for entry in xrange( t.GetEntries() ):
    t.GetEntry( entry )
#    print( "Processing run #%i, event #%i" % ( t.EventInfo.runNumber(), t.EventInfo.eventNumber() ) )
#    print( "Number of jet : %i" % len( t.AntiKt4TruthJets) )
    # loop over jet collection
#    for jet in t.AntiKt4TruthJets:
#      print( "  jet eta = %g, pT = %g" %  ( jet.eta(), jet.pt() ) )
    dijet = t.AntiKt4TruthJets[0].p4() + t.AntiKt4TruthJets[1].p4()
#    print( " mass %g " % dijet.M() )
    hists[count].Fill(dijet.M()/1000.)
  
  hists[count].Scale( 1.0 / hists[count].Integral() )
  if hists[count].GetMaximum() > maximum: maximum = hists[count].GetMaximum()
  if count == 0: hists[count].Draw()
  else: hists[count].Draw("same")
  c0.Update()
   
  fOut.cd()
  hists[count].Write(name)

  count = count + 1
  f.Close()


hists[0].SetMaximum(maximum*1.2)
hists[0].Draw("hist e")
for i in range(1,len(hists)):
  hists[i].Draw("hist e same")
c0.Print("masses.eps")
c0.Print("masses.png")


fOut.Write()
fOut.Close()




IDS="303892 303893 303894 303895 303896 303897 303898 303899 303900 303901 303902 303903 303904 303905 303906"
for t in `echo ${IDS}`; do
  echo ${t}
  INDIR="evgen_${t}"
  OUTDIR="aod_${t}"
  if [ ! -d $OUTDIR ]; then
    mkdir $OUTDIR
  else
    rm -rf $OUTDIR/*
  fi

  Reco_tf.py --inputEVNTFile ${INDIR}/OUT.EVNT.pool.root --outputDAODFile TRUTH.root --reductionConf TRUTH1
  
  mv DAOD_TRUTH1.TRUTH.root ${OUTDIR}

done

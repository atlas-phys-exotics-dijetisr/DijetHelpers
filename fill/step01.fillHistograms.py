#!/usr/bin/env python

#******************************************
#import stuff
import ROOT
import math, os, sys, argparse
import multiprocessing as mp
import plotUtils
from itertools import repeat
import time

#******************************************
#parse input arguments
parser = argparse.ArgumentParser(description='%prog [options]', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--pathToTrees', dest='pathToTrees', default='', help='path to the input trees')
parser.add_argument('--inputTags', dest='inputTags', nargs='+', type=str, default=['Pythia8', 'jetjet'], help='list of tags to identify the input files')
parser.add_argument('--outputTag', dest='outputTag', default='results', help='tag for output files')
parser.add_argument('--lumi', dest='lumi', type=float, default=1.0, help='desired luminosity in fb^-1')
parser.add_argument('--isMC', dest='isMC', action='store_true', default=False, help='is it MC?')
parser.add_argument('--isData', dest='isMC', action='store_false', default=False, help='is it data?')
parser.add_argument('-b', '--batch', dest='batch', action='store_true', default=False, help='batch mode for PyRoot')
parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', default=False, help='verbose mode for debugging')
parser.add_argument('--config', dest='configFileName', default='', help='config file')

#******************************************
#set ATLAS style
if os.path.isfile(os.path.expanduser('~/RootUtils/AtlasStyle.C')):
    ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
    ROOT.SetAtlasStyle()
    ROOT.set_color_env()
else:
    print '\n***WARNING*** couldn\'t find ATLAS Style'
    #import AtlasStyle
    #AtlasStyle.SetAtlasStyle()

#******************************************
def fillHistograms(args):

    print '\n******************************************'
    print 'fill histograms'

    #------------------------------------------
    #input parameters
    print '\ninput parameters:'
    argsdict = vars(args)
    for ii in xrange(len(argsdict)):
        print '  %s = %s'%(argsdict.keys()[ii], argsdict.values()[ii],)
    
    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get the files in path which match tags
    inputFileNames = plotUtils.getMergedTreeFileList(args.pathToTrees, args.inputTags)
    
    #make sure we have found some files
    if len(inputFileNames) == 0:
        raise SystemExit('\n***EXIT*** no files found for tags: %s'%' '.join(args.inputTags))
  
    print '\nmatching input files (%s):'%len(inputFileNames)
    for fileName in inputFileNames:
        print ' %s'%fileName

    #------------------------------------------
    #clean tmp directory
    print '\ncleaning tmp/ directory'
    print 'deleting file:'
    localdir = os.path.dirname(os.path.realpath(__file__))
    tmpdir=os.path.join(localdir,'tmp')
    for f in os.listdir(tmpdir):
        if os.path.isfile(os.path.join(tmpdir,f)) and 'histograms.' in f and '.root' in f:
            print '  %s'%os.path.join(tmpdir,f)
            os.remove(os.path.join(tmpdir,f))

    print 'remaining files:'
    for f in os.listdir(tmpdir):
        print '  %s'%os.path.join(tmpdir,f)
    
    #------------------------------------------
    #fill histograms using multiple processes
    ncpu=mp.cpu_count()
    print '\nfilling histograms using the %s cores of this computer\n'%ncpu

    #use as many processes as the number of cores
    pool = mp.Pool(processes=ncpu)

    pargs = zip( range(1, len( inputFileNames)+1),
                 repeat(len(inputFileNames)),
                 inputFileNames,
                 repeat(args.outputTag),
                 repeat(args.lumi),
                 repeat(args.isMC),
                 repeat(args.configFileName))
    
    results = pool.map(fillHistogramsProcess, pargs)
    #results = pool.map(fillHistogramsProcessTEST, pargs)#TEST
    pool.close()
    pool.join()
    print '\ngot results of %s/%s processes'%(len(results), len(inputFileNames))
    if len(results) != len(inputFileNames):
        print '  successful processes: %s'%' '.join(results)
        
    #------------------------------------------
    #add results together
    results = [os.path.join(tmpdir,f) for f in os.listdir(tmpdir) if os.path.isfile(os.path.join(tmpdir,f)) and 'histograms.' in f and '.root' in f]

    print '\nresults:'
    for result in results:
        print '  %s'%result

    print '\nhadd-ing results'
    #outputFileName = '.'.join(results[0].split('.')[0:-3]) + '.' + '.'.join(results[0].split('.')[-2:])#TEST
    if args.isMC:
        outputFileName = 'results/histograms.mc.'+args.outputTag+'.root'
    else:
        outputFileName = 'results/histograms.data.'+args.outputTag+'.root'
    
    shadd = 'hadd '+outputFileName+' '+' '.join(results)
    #print shadd
    os.system(shadd)
    
#******************************************
def fillHistogramsProcessTEST(pargs):

    #------------------------------------------
    #input parameters
    num, tot = pargs
    time.sleep(num)
    processTag = '[process %s/%s]'%(num, tot)
    print '%s starting'%processTag
    print '%s input parameters:\n%s'%(processTag, pargs)
    print '%s done'%processTag
    return str(num)

#******************************************
def fillHistogramsProcess(pargs):

    #------------------------------------------
    #input parameters
    num, tot, inputFileName, tag, lumi, isMC, configFileName = pargs
    #time.sleep(num)#TEST
    processTag = '[process %s/%s]'%(num, tot)
    print '%s starting fillHistograms'%processTag
    print '%s input parameters:\n%s'%(processTag, pargs,)

    #------------------------------------------
    #run compiled C++
    if isMC:
        commandLine = './fillHistograms --process "'+processTag+'" --file '+inputFileName+' --isMC --name '+tag+' --lumi '+str(lumi)
    else:
        commandLine = './fillHistograms --process "'+processTag+'" --file '+inputFileName+' --isData --name '+tag+' --lumi '+str(lumi)
    
    if configFileName != "":
        commandLine+=' --config '+configFileName
    
    print '%s %s'%(processTag, commandLine)
    os.system(commandLine)
                
    #------------------------------------------
    print '%s done'%processTag
    return str(num)

#******************************************
if __name__ == '__main__':
    args = parser.parse_args()
    fillHistograms(args)
    print '\n******************************************'
    print 'done'

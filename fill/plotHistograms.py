#!/usr/bin/env python

#******************************************
#plotBinnedHistograms: given two 2D input distributions, plot both of them separately in 2D, then plot their x profiles on the same canvas, together with their ratio
#plotBinnedProfileHistograms: given a 2D input histogram, plot it in 2D with its profile overlaid
#compareDistributions: given a series of input histograms (1D or 2D), plot the histograms (1D, flag --compareDistributions) or their profiles (2D, flag --compareProfiles) together on the same canvas, together with their ratio (WRT the first histogram)

#******************************************
#import stuff
import ROOT
import math, os, sys, argparse
import plotUtils
import time

#******************************************
#set ATLAS style
if os.path.isfile(os.path.expanduser('~/RootUtils/AtlasStyle.C')):
    ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
    ROOT.SetAtlasStyle()
    ROOT.set_color_env()
else:
    print '\n***WARNING*** couldn\'t find ATLAS Style'
    #import AtlasStyle
    #AtlasStyle.SetAtlasStyle()

#******************************************
#parse input arguments
parser = argparse.ArgumentParser(description='%prog [options]', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--plotBinned', dest='plotBinned', action='store_true', default=False, help='plot binned histograms')
parser.add_argument('--plotProfile', dest='plotProfile', action='store_true', default=False, help='plot binned profile histograms')
parser.add_argument('--compareProfiles', dest='compareProfiles', action='store_true', default=False, help='compare binned profile histograms')
parser.add_argument('--compareDistributions', dest='compareDistributions', action='store_true', default=False, help='compare distributions')

parser.add_argument('--file', dest='inputFileName', default='', help='input file name')
parser.add_argument('--hist1', dest='inputHistName1', default='', help='input histogram 1 name')
parser.add_argument('--hist2', dest='inputHistName2', default='', help='input histogram 2 name')
parser.add_argument('--hist3', dest='inputHistName3', default='', help='input histogram 3 name')
parser.add_argument('--label1', dest='label1', default='', help='input histogram 1 label')
parser.add_argument('--label2', dest='label2', default='', help='input histogram 2 label')
parser.add_argument('--label3', dest='label3', default='', help='input histogram 3 label')
parser.add_argument('--xlabel', dest='xlabel', default='', help='x axis label')
parser.add_argument('--ylabel', dest='ylabel', default='', help='y axis label')
parser.add_argument('--zlabel', dest='zlabel', default='', help='z axis label')
parser.add_argument('--logx', dest='logx', action='store_true', default=False, help='x axis log scale?')
parser.add_argument('--logy', dest='logy', action='store_true', default=False, help='y axis log scale?')
parser.add_argument('--logz', dest='logz', action='store_true', default=False, help='z axis log scale?')
parser.add_argument('--tag', dest='tag', default='results', help='tag for output files')
parser.add_argument('--lumi', dest='lumi', type=float, default=-1.0, help='desired luminosity in fb^-1')
parser.add_argument('--isMC', dest='isMC', action='store_true', default=False, help='is it MC?')
parser.add_argument('--isData', dest='isMC', action='store_false', default=False, help='is it data?')
parser.add_argument('--notes', dest='notes', nargs='+', type=str, default=[], help='list of notes to add to the plots')
parser.add_argument('--wait', dest='wait', action='store_true', default=False, help='wait?')
parser.add_argument('--save', dest='save', action='store_true', default=False, help='save plots?')

parser.add_argument('--files', dest='inputFileNames', nargs='+', type=str, default=[], help='input file names')
parser.add_argument('--labels', dest='labels', nargs='+', type=str, default=[], help='input file labels')
parser.add_argument('--hists', dest='inputHistNames', nargs='+', type=str, default=[], help='input histograms names')
parser.add_argument('--lumis', dest='lumis', nargs='+', type=str, default=[], help='input histograms luminosities')

parser.add_argument('-b', '--batch', dest='batch', action='store_true', default=False, help='batch mode for PyRoot')
parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', default=False, help='verbose mode for debugging')

#******************************************
def plotBinnedHistograms(args):

    print '\n******************************************'
    print 'plot binned histograms'

    #------------------------------------------
    #input parameters
    print '\ninput parameters:'
    argsdict = vars(args)
    for ii in xrange(len(argsdict)):
        print '  %s = %s'%(argsdict.keys()[ii], argsdict.values()[ii],)
    
    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #define useful variables
    lumi = float(args.lumi) #fb^-1

    #------------------------------------------
    #open input file
    if len(args.inputFileName) == 0:
        raise SystemExit('\n***ERROR*** no input file name')
    if not os.path.isfile(args.inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file')
    f = ROOT.TFile(args.inputFileName)

    #------------------------------------------
    #get input histograms
    if len(args.inputHistName1) == 0:
        raise SystemExit('\n***ERROR*** no input histogram1 name')
    h1 = f.Get(args.inputHistName1)
    if not h1.GetName():
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram: %s'%inputHistName1)
    
    if len(args.inputHistName2) == 0:
        raise SystemExit('\n***ERROR*** no input histogram2 name')
    h2 = f.Get(args.inputHistName2)
    if not h2.GetName():
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram: %s'%inputHistName2)

    #------------------------------------------
    #labels and legends

    #ATLAS
    ax = 0.65
    ay = 0.90
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(30)
		
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(30)
    
    #note
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(42)
    n.SetTextColor(1)
    n.SetTextSize(0.04)

    #legend
    l = ROOT.TLegend(ax, ay, ax+0.15, ay-0.12)
    l.SetFillStyle(0)
    l.SetFillColor(0)
    l.SetLineColor(0)
    l.SetTextFont(43)
    l.SetBorderSize(0)
    l.SetTextSize(15)
    
    #------------------------------------------
    #canvases

    #2D histograms
    c1 = ROOT.TCanvas('c1', 'c1', 0, 50, 800, 600)
    c1.cd()
    c1.SetLogx(0)
    c1.SetLogy(1)
    c1.SetLogz(1)
    c1.SetRightMargin(0.15)
    c1.Draw()

    #1D ratio
    c2 = ROOT.TCanvas('c2', 'c2', 800, 50, 800, 600)
    c2.cd()
    c2.SetRightMargin(0.15)
    c2.Draw()

    c2p  = ROOT.TPad("c2p",  "c2p",  0., 0.,   1., 1.)
    c2p1 = ROOT.TPad("c2p1", "c2p1", 0., 0.33, 1., 1.)
    c2p2 = ROOT.TPad("c2p2", "c2p2", 0., 0.,   1., 0.33)

    c2p.SetFillStyle(4000)#transparent
    
    c2p1.SetLogx(0)
    c2p1.SetLogy(0)
    c2p2.SetLogx(0)
    c2p2.SetLogy(0)

    c2p1.SetBottomMargin(0.)
    c2p1.SetBorderMode(0)
    c2p2.SetTopMargin(0.)
    c2p2.SetBottomMargin(0.3)
    c2p2.SetBorderMode(0)
    c2p1.Draw()
    c2p2.Draw()
    c2p.Draw()

    #------------------------------------------
    #plot 2D distributions
    print '\nplotting 2D histograms'
    for h in [h1, h2]:

        print '  %s'%h.GetName()
        
        c1.cd()
        
        h.GetYaxis().SetTitle(args.ylabel)
        h.GetYaxis().SetTitleFont(43)
        h.GetYaxis().SetTitleSize(20)
        h.GetYaxis().SetTitleOffset(1.5)
        h.GetYaxis().SetLabelFont(43)
        h.GetYaxis().SetLabelSize(20)
        #h.GetYaxis().SetRangeUser(0.5,h.GetMaximum()*10.)
    
        h.GetXaxis().SetTitle(args.xlabel)
        h.GetXaxis().SetTitleFont(43)
        h.GetXaxis().SetTitleSize(20)
        h.GetXaxis().SetTitleOffset(1.5)
        h.GetXaxis().SetLabelFont(43)
        h.GetXaxis().SetLabelSize(20)
        #h.GetXaxis().SetRange(firstBin,lastBin)

        h.GetZaxis().SetTitle(args.zlabel)
        h.GetZaxis().SetTitleFont(43)
        h.GetZaxis().SetTitleSize(20)
        h.GetZaxis().SetTitleOffset(1.5)
        h.GetZaxis().SetLabelFont(43)
        h.GetZaxis().SetLabelSize(20)
        #h.GetZaxis().SetRange(firstBin,lastBin)
                
        h.Draw("colz")

        #labels
        ax = 0.60
        ay = 0.90
        a.DrawLatex(ax,ay,'ATLAS')
        p.DrawLatex(ax+0.13,ay,'internal')
    
        #notes
        if float(lumi) > 0.:
            n.DrawLatex(ax,ay-0.04,'L_{int} = %s fb^{-1}'%lumi)
            for ii, note in enumerate(args.notes):
                n.DrawLatex(ax,ay-0.04-0.04*(ii+1),note)
        else:
            for ii, note in enumerate(args.notes):
                n.DrawLatex(ax,ay-0.04*(ii+1),note)

        c1.Update()
        if args.wait:
            c1.WaitPrimitive()
        if args.save:
            c1.SaveAs('figures/distribution.%s.%s.pdf'%(h.GetName(),args.tag))
    
    #------------------------------------------
    #plot profiles
    print '\nplotting profiles'

    #plot profiles
    c2p1.cd()

    pfx1 = h1.ProfileX()
    pfx2 = h2.ProfileX()
    
    #pfx1.GetYaxis().SetTitle("<"+args.ylabel+">")
    #pfx1.GetYaxis().SetTitleFont(43)
    #pfx1.GetYaxis().SetTitleSize(20)
    #pfx1.GetYaxis().SetTitleOffset(1.5)
    #pfx1.GetYaxis().SetLabelFont(43)
    #pfx1.GetYaxis().SetLabelSize(20)
    
    #pfx1.GetXaxis().SetTitle(args.xlabel)
    #pfx1.GetXaxis().SetTitleFont(43)
    #pfx1.GetXaxis().SetTitleSize(20)
    #pfx1.GetXaxis().SetTitleOffset(1.5)
    #pfx1.GetXaxis().SetLabelFont(43)
    #pfx1.GetXaxis().SetLabelSize(20)    

    pfx1.SetMarkerStyle(1)
    pfx1.SetMarkerColor(ROOT.kRed+1)
    pfx1.SetLineColor(ROOT.kRed+1)

    pfx2.SetMarkerStyle(1)
    pfx2.SetMarkerColor(ROOT.kBlue+1)
    pfx2.SetLineColor(ROOT.kBlue+1)
    
    #pfx1.Draw("")
    #pfx2.Draw("same")
    #c2p1.Update()

    hs2 = ROOT.THStack('hs2','hs2')
    hs2.Clear()
    hs2.Add(pfx1)
    hs2.Add(pfx2)
    hs2.Draw('nostack')# hist') #NOTE draw then set stuff
    
    if ' [' in args.ylabel:
        hs2ylabel = '<'+args.ylabel.replace(' [', '> [')
    else:
        hs2ylabel = '<'+args.ylabel+'>'
    hs2.GetYaxis().SetTitle(hs2ylabel)
    hs2.GetYaxis().SetTitleFont(43)
    hs2.GetYaxis().SetTitleSize(20)
    hs2.GetYaxis().SetTitleOffset(1.5)
    hs2.GetYaxis().SetLabelFont(43)
    hs2.GetYaxis().SetLabelSize(20)

    #plot ratio
    c2p2.cd()

    pfxr = pfx1.Clone()
    pfxr.Divide(pfx2)

    pfxr.GetYaxis().SetTitle("ratio")
    pfxr.GetYaxis().SetTitleFont(43)
    pfxr.GetYaxis().SetTitleSize(20)
    pfxr.GetYaxis().SetTitleOffset(1.5)
    pfxr.GetYaxis().SetLabelFont(43)
    pfxr.GetYaxis().SetLabelSize(20)

    pfxr.GetXaxis().SetTitle(args.xlabel)
    pfxr.GetXaxis().SetTitleFont(43)
    pfxr.GetXaxis().SetTitleSize(20)
    pfxr.GetXaxis().SetTitleOffset(3.0)
    pfxr.GetXaxis().SetLabelFont(43)
    pfxr.GetXaxis().SetLabelSize(20)

    pfxr.SetMarkerStyle(1)
    pfxr.SetMarkerColor(ROOT.kBlack)
    pfxr.SetLineColor(ROOT.kBlack)

    pfxr.Draw("")
    c2p2.Update()
    
    #labels and legends
    c2p.cd()
    
    #labels
    ax = 0.65
    ay = 0.90
    a.DrawLatex(ax,ay,'ATLAS')
    p.DrawLatex(ax+0.13,ay,'internal')
    
    #notes
    if float(lumi) > 0.:
        n.DrawLatex(ax,ay-0.04,'L_{int} = %s fb^{-1}'%lumi)
        for ii, note in enumerate(args.notes):
            n.DrawLatex(ax,ay-0.04-0.04*(ii+1),note)
    else:
        for ii, note in enumerate(args.notes):
            n.DrawLatex(ax,ay-0.04*(ii+1),note)

    #legend
    l.Clear()
    l.SetTextSize(20)
    l.AddEntry(pfx1,args.label1,'pl')
    l.AddEntry(pfx2,args.label2,'pl')
    l.SetX1(ax)
    l.SetY1(ay-0.04-0.04*(len(args.notes)+1))
    l.SetX2(ax+0.15)
    l.SetY2(ay-0.04-0.04*(len(args.notes)+1)-0.10)
    l.Draw()

    c2.Update()
    if args.wait:
        c2.WaitPrimitive()
    if args.save:
        c2.SaveAs('figures/compareProfiles.%s.%s.%s.pdf'%(pfx1.GetName(),pfx2.GetName(),args.tag))

#******************************************
def plotBinnedProfileHistograms(args):

    print '\n******************************************'
    print 'plot binned profile histograms'

    #------------------------------------------
    #input parameters
    print '\ninput parameters:'
    argsdict = vars(args)
    for ii in xrange(len(argsdict)):
        print '  %s = %s'%(argsdict.keys()[ii], argsdict.values()[ii],)
    
    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #define useful variables
    lumi = float(args.lumi) #fb^-1

    #------------------------------------------
    #open input file
    if len(args.inputFileName) == 0:
        raise SystemExit('\n***ERROR*** no input file name')
    if not os.path.isfile(args.inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file')
    f = ROOT.TFile(args.inputFileName)

    #------------------------------------------
    #get input histograms
    if len(args.inputHistName1) == 0:
        raise SystemExit('\n***ERROR*** no input histogram1 name')
    h = f.Get(args.inputHistName1)
    if not h.GetName():
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram: %s'%inputHistName1)
    
    #------------------------------------------
    #labels and legends

    #ATLAS
    ax = 0.65
    ay = 0.90
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(30)
		
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(30)
    
    #note
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(42)
    n.SetTextColor(1)
    n.SetTextSize(0.04)

    #legend
    l = ROOT.TLegend(ax, ay, ax+0.15, ay-0.12)
    l.SetFillStyle(0)
    l.SetFillColor(0)
    l.SetLineColor(0)
    l.SetTextFont(43)
    l.SetBorderSize(0)
    l.SetTextSize(15)
    
    #------------------------------------------
    #canvases

    #2D histograms
    c1 = ROOT.TCanvas('c1', 'c1', 0, 50, 800, 600)
    c1.cd()
    c1.SetLogx(0)
    c1.SetLogy(1)
    c1.SetLogz(1)
    c1.SetRightMargin(0.15)
    c1.Draw()

    #------------------------------------------
    #plot 2D distribution with profile
    print '\nplotting 2D histogram with profile'
        
    c1.cd()
        
    h.GetYaxis().SetTitle(args.ylabel)
    h.GetYaxis().SetTitleFont(43)
    h.GetYaxis().SetTitleSize(20)
    h.GetYaxis().SetTitleOffset(1.5)
    h.GetYaxis().SetLabelFont(43)
    h.GetYaxis().SetLabelSize(20)
    #h.GetYaxis().SetRangeUser(0.5,h.GetMaximum()*10.)
    
    h.GetXaxis().SetTitle(args.xlabel)
    h.GetXaxis().SetTitleFont(43)
    h.GetXaxis().SetTitleSize(20)
    h.GetXaxis().SetTitleOffset(1.5)
    h.GetXaxis().SetLabelFont(43)
    h.GetXaxis().SetLabelSize(20)
    #h.GetXaxis().SetRange(firstBin,lastBin)

    h.GetZaxis().SetTitle(args.zlabel)
    h.GetZaxis().SetTitleFont(43)
    h.GetZaxis().SetTitleSize(20)
    h.GetZaxis().SetTitleOffset(1.5)
    h.GetZaxis().SetLabelFont(43)
    h.GetZaxis().SetLabelSize(20)
    #h.GetZaxis().SetRange(firstBin,lastBin)
                
    h.Draw("colz")

    #profile
    pfx = h.ProfileX()
    pfx.SetMarkerStyle(20)
    pfx.SetMarkerColor(ROOT.kBlack)
    pfx.SetLineColor(ROOT.kBlack)
    pfx.Draw('same')
    
    #labels
    ax = 0.60
    ay = 0.90
    a.DrawLatex(ax,ay,'ATLAS')
    p.DrawLatex(ax+0.13,ay,'internal')
    
    #notes
    if float(lumi) > 0.:
        n.DrawLatex(ax,ay-0.04,'L_{int} = %s fb^{-1}'%lumi)
        for ii, note in enumerate(args.notes):
            n.DrawLatex(ax,ay-0.04-0.04*(ii+1),note)
    else:
        for ii, note in enumerate(args.notes):
            n.DrawLatex(ax,ay-0.04*(ii+1),note)
        
    c1.Update()
    if args.wait:
        c1.WaitPrimitive()
    if args.save:
        c1.SaveAs('figures/profile.%s.%s.pdf'%(h.GetName(),args.tag))
    
#******************************************
def compareDistributions(args):

    print '\n******************************************'
    print 'compare binned profile histograms'

    #------------------------------------------
    #input parameters
    print '\ninput parameters:'
    argsdict = vars(args)
    for ii in xrange(len(argsdict)):
        print '  %s = %s'%(argsdict.keys()[ii], argsdict.values()[ii],)
    
    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #define useful variables
    lumi = float(args.lumi) #fb^-1
    colors=[]
    colors.append(ROOT.kGreen+1)
    colors.append(ROOT.kCyan+1)
    colors.append(ROOT.kRed+1)
    colors.append(ROOT.kOrange+1)
    colors.append(ROOT.kBlue+1)

    #------------------------------------------
    #open input files
    fs=[]
    for inputFileName in args.inputFileNames:
        if len(inputFileName) == 0:
            raise SystemExit('\n***ERROR*** no input file name')
        if not os.path.isfile(inputFileName):
            raise SystemExit('\n***ERROR*** couldn\'t find input file')
        fs.append(ROOT.TFile(inputFileName))

    print '\nfiles:'
    for ii in range(len(fs)):
        print '  %s'%fs[ii].GetName()

    #------------------------------------------
    #get input histograms
    hs=[]
    if len(args.inputHistName1) == 0:
        raise SystemExit('\n***ERROR*** no input histogram1 name')
    for f in fs:
        hs.append(f.Get(args.inputHistName1))
        if not hs[-1].GetName():
            raise SystemExit('\n***ERROR*** couldn\'t find input histogram: %s in file: '%(inputHistName1,f.GetName()))

    print '\nhistograms:'
    for ii in range(len(hs)):
        print '  %s: %s entries'%(hs[ii].GetName(), hs[ii].GetEntries())
    
    #------------------------------------------
    #labels and legends

    #ATLAS
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(30)
		
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(30)
    
    #note
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(42)
    n.SetTextColor(1)
    n.SetTextSize(0.04)

    #legend
    l = ROOT.TLegend()#ax, ay, ax+0.15, ay-0.12)
    l.SetFillStyle(0)
    l.SetFillColor(0)
    l.SetLineColor(0)
    l.SetTextFont(43)
    l.SetBorderSize(0)
    l.SetTextSize(15)
    
    #------------------------------------------
    #canvas
    c1   = ROOT.TCanvas('c1', 'c1', 0, 50, 800, 600)
    c1p  = ROOT.TPad("c1p",  "c1p",  0., 0.,   1., 1.)
    c1p1 = ROOT.TPad("c1p1", "c1p1", 0., 0.33, 1., 1.)
    c1p2 = ROOT.TPad("c1p2", "c1p2", 0., 0.,   1., 0.33)

    c1p.SetFillStyle(4000)#transparent
    
    c1p1.SetLogx(args.logx)
    c1p1.SetLogy(args.logy)
    c1p2.SetLogx(args.logx)
    c1p2.SetLogy(0)

    c1p1.SetBottomMargin(0.)
    c1p1.SetBorderMode(0)
    c1p2.SetTopMargin(0.)
    c1p2.SetBottomMargin(0.3)
    c1p2.SetBorderMode(0)
    c1p1.Draw()
    c1p2.Draw()
    c1p.Draw()

    #------------------------------------------
    #plot profiles
    print '\nplotting profiles comparison'
    c1p1.cd()

    pfxs=[]

    if args.compareProfiles:
        #get profiles
        for ii in range(len(hs)):
            pfxs.append(hs[ii].ProfileX(hs[ii].GetName()+'_pfx_'+str(ii)))#NOTE give a different name to each profile
    else:
        #use original distributions and scale them to the integral of the first
        for ii in range(len(hs)):
            pfxs.append( hs[ii].Clone( hs[0].GetName()+'_'+str(ii)))#NOTE give a different name to each profile
            #scale histogram
            pfxs[ii].Scale(1./pfxs[ii].Integral(), 'width')

    for ii in range(len(pfxs)):
        pfxs[ii].SetMarkerStyle(20+(ii%len(colors)))
        pfxs[ii].SetMarkerColor(colors[ii%len(colors)])
        pfxs[ii].SetLineColor(colors[ii%len(colors)])

        if ii==0:
            pfxs[ii].GetYaxis().SetTitle(args.ylabel)
            pfxs[ii].GetYaxis().SetTitleFont(43)
            pfxs[ii].GetYaxis().SetTitleSize(20)
            pfxs[ii].GetYaxis().SetTitleOffset(1.5)
            pfxs[ii].GetYaxis().SetLabelFont(43)
            pfxs[ii].GetYaxis().SetLabelSize(20)
            #pfxs[ii].GetYaxis().SetRangeUser(0.5,pfxs[ii].GetMaximum()*10.)
            
            pfxs[ii].GetXaxis().SetTitle(args.xlabel)
            pfxs[ii].GetXaxis().SetTitleFont(43)
            pfxs[ii].GetXaxis().SetTitleSize(20)
            pfxs[ii].GetXaxis().SetTitleOffset(1.5)
            pfxs[ii].GetXaxis().SetLabelFont(43)
            pfxs[ii].GetXaxis().SetLabelSize(20)
            #pfxs[ii].GetXaxis().SetRange(firstBin,lastBin)

            pfxs[ii].Draw("")

        else:
            pfxs[ii].Draw("same")

    #------------------------------------------
    #plot ratios
    c1p2.cd()

    pfxrs = []

    #clone
    for ii in range(len(pfxs)):
        pfxrs.append(pfxs[ii].Clone(pfxs[ii].GetName()+'_ratio'))#.Divide(pfxs[0]))

    #divide
    for ii in range(len(pfxs)):
        pfxrs[ii].Divide(pfxs[0])

    for ii in range(len(pfxrs)):
        pfxrs[ii].SetMarkerStyle(20+(ii%len(colors)))
        pfxrs[ii].SetMarkerColor(colors[ii%len(colors)])
        pfxrs[ii].SetLineColor(colors[ii%len(colors)])

        if ii==0: continue #NOTE skip the MC/MC ratio plot
        
        if ii==1:
            pfxrs[ii].GetYaxis().SetTitle('ratio')
            pfxrs[ii].GetYaxis().SetTitleFont(43)
            pfxrs[ii].GetYaxis().SetTitleSize(20)
            pfxrs[ii].GetYaxis().SetTitleOffset(1.5)
            pfxrs[ii].GetYaxis().SetLabelFont(43)
            pfxrs[ii].GetYaxis().SetLabelSize(20)
            pfxrs[ii].GetYaxis().SetRangeUser(0.,2.)
            
            pfxrs[ii].GetXaxis().SetTitle(args.xlabel)
            pfxrs[ii].GetXaxis().SetTitleFont(43)
            pfxrs[ii].GetXaxis().SetTitleSize(20)
            pfxrs[ii].GetXaxis().SetTitleOffset(3.0)
            pfxrs[ii].GetXaxis().SetLabelFont(43)
            pfxrs[ii].GetXaxis().SetLabelSize(20)
            #pfxrs[ii].GetXaxis().SetRange(firstBin,lastBin)

            pfxrs[ii].Draw("")

        else:
            pfxrs[ii].Draw("same")

    c1p2.Update()


    #------------------------------------------
    #labels
    c1p.cd()
    ax = 0.60#0.50
    ay = 0.55#0.90
    a.DrawLatex(ax,ay,'ATLAS')
    p.DrawLatex(ax+0.13,ay,'internal')

    #notes
    for ii, note in enumerate(args.notes):
        n.DrawLatex(ax,ay-0.04*(ii+1),note)

    #legend
    l.Clear()
    l.SetTextSize(20)
    for ii in range(len(hs)):
        l.AddEntry(pfxs[ii],args.labels[ii]+', L_{int} = '+args.lumis[ii]+' fb^{-1}','pl')
    l.SetX1(ax)
    l.SetY1(ay - 0.04*(len(args.notes)+1))
    l.SetX2(ax+0.15)
    l.SetY2(ay - 0.04*(len(args.notes)+1) - 0.04*len(pfxs))
    l.Draw()

    c1.cd()
    c1.Update()
    if args.wait:
        c1.WaitPrimitive()
    if args.save:
        if args.compareProfiles:
            c1.SaveAs('figures/compareProfiles.%s.%s.pdf'%(args.inputHistName1,args.tag))
        else:
            c1.SaveAs('figures/compareDistributions.%s.%s.pdf'%(args.inputHistName1,args.tag))

#******************************************
if __name__ == '__main__':
    args = parser.parse_args()
    if args.plotBinned:
        plotBinnedHistograms(args)
    elif args.plotProfile:
        plotBinnedProfileHistograms(args)
    elif args.compareProfiles:
        compareDistributions(args)
    elif args.compareDistributions:
        compareDistributions(args)
                
    print '\n******************************************'
    print 'done'

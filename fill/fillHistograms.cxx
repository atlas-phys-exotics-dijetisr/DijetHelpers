#include "TROOT.h"
#include "TObject.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TH2.h"
#include "TStopwatch.h"
#include "TMath.h"
#include "TKey.h"
#include "TEnv.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdlib.h>

#include "TreeClass.h"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;
using std::ofstream;

//******************************************
int main(int argc, char *argv[]) {

  //******************************************
  TStopwatch time;
  time.Start();

  //******************************************
  //get parameters
  string process="";
  string configFileName="";
  string inputFileName="";
  string name="results";
  bool isMC=false;
  double lumi=1.;
  bool bool_help=false;
  int ip=1;

  //cout<<"looping over input praameters"<<endl;
  
  while (ip<argc) {

    //cout<<"  "<<ip<<"  "<<argv[ip]<<endl;
    
    if (string(argv[ip]).substr(0,2)=="--") {

      //------------------------------------------
      //help
      if (string(argv[ip])=="--help") {
	bool_help=true; break;
      }

      //------------------------------------------
      //process
      //NOTE this is a tag to identify the process when running in parallel
      else if (string(argv[ip])=="--process") {
	if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
	  process=argv[ip+1];
	  process+=" ";
	  ip+=2;
	} else {cout<<"\n"<<process<<"no process ID"<<endl; bool_help=true; break;}
      }
      
      //------------------------------------------
      //config file
      else if (string(argv[ip])=="--config") {
	if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
	  configFileName=argv[ip+1];
	  ip+=2;
	} else {cout<<"\n"<<process<<"no config file name given"<<endl; bool_help=true; break;}
      }
      
      //------------------------------------------
      //input file name
      else if (string(argv[ip])=="--file") {
	if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
	  inputFileName=argv[ip+1];
	  ip+=2;
	} else {cout<<"\n"<<process<<"no input file name given"<<endl; bool_help=true; break;}
      }
      
      //------------------------------------------
      //name
      else if (string(argv[ip])=="--name") {
	if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
	  name=argv[ip+1];
	  ip+=2;
	} else {cout<<"\n"<<process<<"no name given"<<endl; bool_help=true; break;}
      }
      
      //------------------------------------------
      //is it MC?
      else if (string(argv[ip])=="--isMC") {
	isMC=true;
	ip+=1;
      }

      //------------------------------------------
      //is it data?
      else if (string(argv[ip])=="--isData") {
	isMC=false;
	ip+=1;
      }
      
      //------------------------------------------
      //luminosity
      else if (string(argv[ip])=="--lumi") {
	if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
	  lumi=atof(argv[ip+1]);
	  ip+=2;
	} else {cout<<"\n"<<process<<"no luminosity value given"<<endl; bool_help=true; break;}
      }
      
      //------------------------------------------
      //unknown command
      else {
	cout<<"\n"<<process<<"fillHistograms: command '"<<string(argv[ip])<<"' unknown"<<endl;
	bool_help=true;
	if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") ip+=2;
	else ip+=1;
      }
      
    }//end if "--command"
    
    //------------------------------------------
    else { //if command does not start with "--"
      cout<<"\n"<<process<<"fillHistograms: command '"<<string(argv[ip])<<"' unknown"<<endl;
      bool_help=true;
      break;
    }//end if "--"
    
  }//end while loop
  
  //------------------------------------------
  //after reading the input parameters, call the functions
  if (bool_help || inputFileName=="") {
    cout<<"\n"<<process<<"fillHistograms"<<endl;
    cout<<process<<"HOW TO: fillHistograms --file <file name> --name <name>"<<endl;
    cout<<process<<"HELP:   fillHistograms --help"<<endl;
    return 1;
  }

  //******************************************
  //read settings from config file
  TEnv *settings=new TEnv();
  if (configFileName != "") {
    cout<<process<<"config file = "<<configFileName<<endl;
    int status=settings->ReadFile(configFileName.c_str(),EEnvLevel(0));
    if (status!=0) {
      cout<<process<<"couldn't read config file"<<endl;
      std::exit(1);
    }
  }
  
  double leadJetpTCut=settings->GetValue("leadJetpTCut",410.);
  cout<<process<<"leadJetpTCut = "<<leadJetpTCut<<endl;
  
  double yStarCut=settings->GetValue("yStarCut",0.6);
  cout<<process<<"yStarCut = "<<yStarCut<<endl;
  
  //******************************************
  //open input file
  cout<<process<<"input file name: "<<inputFileName<<endl;
  UInt_t openFileCounter=0;
  TFile *file=0;
  while(!file && openFileCounter<10){
    file=TFile::Open(inputFileName.c_str(),"READ");
    if(!file) sleep(2);
    openFileCounter++;
    //cout<<process<<"  reading try number "<<openFileCounter<<endl;
  }
  
  //******************************************
  //get input tree
  if(!file) {std::cerr<<process<<"couldn't open input file: "<<inputFileName<<std::endl; return 1;}
  TTree *tree;
  tree = dynamic_cast<TTree*>(file->Get("physics"));
  if(!tree) tree = dynamic_cast<TTree*>(file->Get("outTree"));
  if(!tree) {
    std::cerr<<"\n"<<process<<"file '"<<inputFileName<<"' doesn't contain TTree 'physics' or 'outTree'"<<std::endl;
    delete file;
    return 1;
  }

  //******************************************
  //instantiate TreeClass object
  TreeClass *tc = new TreeClass(tree, isMC);
  if (tc->fChain == 0) {
    std::cerr<<process<<"couldn't find chain"<<std::endl;
    return 1;
  }

  tc->setProcess(process);//set process ID
  tc->readWhatYouNeed(0);//read first entry already

  //******************************************
  //get DSID (MC) or run number (data)
  string snumber="";
  if (isMC) {
    snumber = std::to_string(tc->mcChannelNumber);
    cout<<process<<"DSID = "<<snumber<<endl;
  } else {
    snumber = std::to_string(tc->runNumber);
    cout<<process<<"run number = "<<snumber<<endl;
  }

  //******************************************
  //get number of MC events for normalization
  double totalSliceEvents=1.;
  if (isMC) {
    TIter next(file->GetListOfKeys());
    TKey *key;
    TString keyName;
    while ((key = (TKey*)next())) {
      keyName = key->GetName();
      //cout<<process<<"  "<<keyName<<endl;
      if (keyName.Contains("cutflow") && !keyName.Contains("weighted")) {
	totalSliceEvents = ((TH1D*) key->ReadObj())->GetBinContent(1);
      }
    }
    cout<<process<<"total slice events = "<<totalSliceEvents<<endl;
  }
  
  //******************************************
  //set defaults
  TH1::SetDefaultSumw2(kTRUE);
  TH1::StatOverflows(kTRUE);
  TH2::SetDefaultSumw2(kTRUE);
  TH2::StatOverflows(kTRUE);

  //******************************************
  //binning
  double mjjBins[] = { 946, 976, 1006, 1037, 1068, 1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 1573, 1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756, 7904, 8055, 8208, 8364, 8523, 8685, 8850, 9019, 9191, 9366, 9544, 9726, 9911, 10100, 10292, 10488, 10688, 10892, 11100, 11312, 11528, 11748, 11972, 12200, 12432, 12669, 12910, 13156 };
  UInt_t nMjjBins=sizeof(mjjBins)/sizeof(double)-1;

  double pTBins[] = { 15. ,20. ,25. ,35. ,45. ,55. ,70. ,85. ,100. ,116. ,134. ,152. ,172. ,194. ,216. ,240. ,264. ,290. ,318. ,346.,376.,408.,442.,478.,516.,556.,598.,642.,688.,736.,786.,838.,894.,952.,1012.,1076.,1162.,1310.,1530.,1992.,2500.,3200. };
  UInt_t npTBins=sizeof(pTBins)/sizeof(double)-1;

  //NOTE pT bins go from 0. to tightpTRange
  //UInt_t nTightpTBins=325; double tightpTRange=6500.; double tightpTBins[nTightpTBins+1];
  //for (UInt_t ii=0;ii<=nTightpTBins;ii++) {tightpTBins[ii]=tightpTRange*ii/nTightpTBins;}

  //NOTE eta bins go from -etaRange to +etaRange
  UInt_t nEtaBins=80; double etaRange=4.; double etaBins[nEtaBins+1];
  for (UInt_t ii=0;ii<=nEtaBins;ii++) {etaBins[ii]=etaRange*(2.*ii/nEtaBins-1.);}

  //NOTE eta bins go from -phiRange to +phiRange
  UInt_t nPhiBins=100; double phiRange=TMath::Pi(); double phiBins[nPhiBins+1];
  for (UInt_t ii=0;ii<=nPhiBins;ii++) {phiBins[ii]=phiRange*(2.*ii/nPhiBins-1.);}
  
  UInt_t nNumTrkBins=200; double numTrkRange=200.; double numTrkBins[nNumTrkBins+1];
  for (UInt_t ii=0;ii<=nNumTrkBins;ii++) {numTrkBins[ii]=numTrkRange*ii/nNumTrkBins;}

  UInt_t nTrackWidthBins=200; double trackWidthRange=1.; double trackWidthBins[nTrackWidthBins+1];
  for (UInt_t ii=0;ii<=nTrackWidthBins;ii++) {trackWidthBins[ii]=trackWidthRange*(2.*ii/nTrackWidthBins-1.);}

  UInt_t nRatioBins=10000; double ratioRange=1000.; double ratioBins[nRatioBins+1];
  for (UInt_t ii=0;ii<=nRatioBins;ii++) {ratioBins[ii]=ratioRange*ii/nRatioBins;}

  UInt_t nShortRatioBins=200; double shortRatioRange=1000.; double shortRatioBins[nShortRatioBins+1];
  for (UInt_t ii=0;ii<=nShortRatioBins;ii++) {shortRatioBins[ii]=shortRatioRange*ii/nShortRatioBins;}
  
  //******************************************
  //coarse binning
  double coarseEtaBins[]={-4.9,-3.1,-2.8,-2.1,-1.8,-1.5,-1.2,-0.8,0,0.8,1.2,1.5,1.8,2.1,2.8,3.1,4.9};
  UInt_t nCoarseEtaBins=sizeof(coarseEtaBins)/sizeof(double)-1;

  double coarsepTBins[]={50., 100., 152., 240., 408., 598., 1012., 2500.};
  UInt_t nCoarsepTBins=sizeof(coarsepTBins)/sizeof(double)-1;

  double coarseMjjBins[]={0.4e3, 0.6e3, 0.9e3, 1.2e3, 1.6e3, 1.8e3, 2e3, 2.25e3, 2.5e3, 2.8e3, 3.1e3, 3.4e3, 3.7e3, 4e3, 4.3e3, 4.6e3, 4.9e3, 5.4e3, 6.5e3, 8e3, 10e3, 13e3};
  UInt_t nCoarseMjjBins=sizeof(coarseMjjBins)/sizeof(double)-1;

  //******************************************
  //histograms

  //default histogras
  TH1D* h_mjj=new TH1D("mjj","mjj",nMjjBins,mjjBins);
  TH1D* h_pt=new TH1D("pt","pt",npTBins,pTBins);
  TH1D* h_eta=new TH1D("eta","eta",nEtaBins,etaBins);
  TH1D* h_phi=new TH1D("phi","phi",nPhiBins,phiBins);
  TH2D* h_eta_phi=new TH2D("eta_phi","eta_phi",nEtaBins,etaBins,nPhiBins,phiBins);
  TH2D* h_eta_pt=new TH2D("eta_pt","eta_pt",nEtaBins,etaBins,npTBins,pTBins);
  
  //binned histograms (TH2)
  TH2D* bh_eta_pt=new TH2D("eta_pt_binned","eta_pt_binned",nCoarseEtaBins,coarseEtaBins,npTBins,pTBins);
  
  //SumPt
  TH2D* bh_eta_SumPtTrkPt500PV=new TH2D("eta_SumPtTrkPt500PV_binned","eta_SumPtTrkPt500PV_binned",nCoarseEtaBins,coarseEtaBins,npTBins,pTBins);
  TH2D* bh_pt_SumPtTrkPt500PV=new TH2D("pt_SumPtTrkPt500PV_binned","pt_SumPtTrkPt500PV_binned",nCoarsepTBins,coarsepTBins,npTBins,pTBins);

  TH2D* bh_eta_ptOverSumPtTrkPt500PV=new TH2D("eta_ptOverSumPtTrkPt500PV_binned","eta_ptOverSumPtTrkPt500PV_binned",nCoarseEtaBins,coarseEtaBins,nRatioBins,ratioBins);
  TH2D* bh_pt_ptOverSumPtTrkPt500PV=new TH2D("pt_ptOverSumPtTrkPt500PV_binned","pt_ptOverSumPtTrkPt500PV_binned",nCoarsepTBins,coarsepTBins,nRatioBins,ratioBins);

  //NumTrk
  TH2D* bh_eta_NumTrkPt500PV=new TH2D("eta_NumTrkPt500PV_binned","eta_NumTrkPt500PV_binned",nCoarseEtaBins,coarseEtaBins,nNumTrkBins,numTrkBins);
  TH2D* bh_pt_NumTrkPt500PV=new TH2D("pt_NumTrkPt500PV_binned","pt_NumTrkPt500PV_binned",nCoarsepTBins,coarsepTBins,nNumTrkBins,numTrkBins);

  //TrackWidth
  TH2D* bh_eta_TrackWidthPt500PV=new TH2D("eta_TrackWidthPt500PV_binned","eta_TrackWidthPt500PV_binned",nCoarseEtaBins,coarseEtaBins,nTrackWidthBins,trackWidthBins);
  TH2D* bh_pt_TrackWidthPt500PV=new TH2D("pt_TrackWidthPt500PV_binned","pt_TrackWidthPt500PV_binned",nCoarsepTBins,coarsepTBins,nTrackWidthBins,trackWidthBins);
  
  //debug histograms
  TH1D* h_eta_j0=new TH1D("eta_j0","eta_j0",nEtaBins,etaBins);
  TH1D* h_eta_j1=new TH1D("eta_j1","eta_j1",nEtaBins,etaBins);

  TH1D* h_phi_j0=new TH1D("phi_j0","phi_j0",nPhiBins,phiBins);
  TH1D* h_phi_j1=new TH1D("phi_j1","phi_j1",nPhiBins,phiBins);

  TH2D* h_eta_phi_j0=new TH2D("eta_phi_j0","eta_phi_j0",nEtaBins,etaBins,nPhiBins,phiBins);
  TH2D* h_eta_phi_j1=new TH2D("eta_phi_j1","eta_phi_j1",nEtaBins,etaBins,nPhiBins,phiBins);

  TH2D* h_eta_pt_j0=new TH2D("eta_pt_j0","eta_pt_j0",nEtaBins,etaBins,npTBins,pTBins);
  TH2D* h_eta_pt_j1=new TH2D("eta_pt_j1","eta_pt_j1",nEtaBins,etaBins,npTBins,pTBins);

  TH1D* h_NumTrkPt500PV=new TH1D("NumTrkPt500PV","NumTrkPt500PV",nNumTrkBins,numTrkBins);
  TH1D* h_SumPtTrkPt500PV=new TH1D("SumPtTrkPt500PV","SumPtTrkPt500PV",npTBins,pTBins);
  TH1D* h_TrackWidthPt500PV=new TH1D("trackWidthPt500PV","trackWidthPt500PV",nTrackWidthBins,trackWidthBins);

  TH1D* h_NumTrkPt1000PV=new TH1D("NumTrkPt1000PV","NumTrkPt1000PV",nNumTrkBins,numTrkBins);
  TH1D* h_SumPtTrkPt1000PV=new TH1D("SumPtTrkPt1000PV","SumPtTrkPt1000PV",npTBins,pTBins);
  TH1D* h_TrackWidthPt1000PV=new TH1D("TrackWidthPt1000PV","TrackWidthPt1000PV",nTrackWidthBins,trackWidthBins);
  
  //******************************************
  //loop over tree entries
  Long64_t nentries=tc->fChain->GetEntriesFast();
  UInt_t allevents=0;
  UInt_t goodevents=0;
  double weight=1.;

  //------------------------------------------
  //loop
  for (Long64_t ii=0; ii<nentries; ii++) {

    //------------------------------------------
    //load entry
    //cout<<process<<"entry = "<<ii<<endl;
    tc->readWhatYouNeed(ii);//NOTE this is way faster than tc->GetEntry(ii)    
    allevents++;

    //------------------------------------------
    //event selection
    //number of jets
    if ((*tc->jet_pt).size() < 2)
      continue;

    //leading jet pT
    if ((*tc->jet_pt)[0] < leadJetpTCut)
      continue;

    //y*
    if (fabs(tc->yStar) > yStarCut)
      continue;
    
    goodevents++;
    
    //------------------------------------------
    //compute weight
    weight = 1.;
    if (isMC && totalSliceEvents > 0.) {
      weight = 1. * tc->weight * lumi / totalSliceEvents;
    }
    
    //cout<<process<<"  weight = "<<tc->weight<<endl;
    //cout<<process<<"  lumi = "<<lumi<<endl;
    //cout<<process<<"  totalSliceEvents = "<<totalSliceEvents<<endl;
    //cout<<process<<"  final weight = "<<weight<<endl;
    
    //------------------------------------------
    //fill histograms

    //default histograms
    h_mjj->Fill(tc->mjj, weight);
    h_pt->Fill((*tc->jet_pt)[0], weight);
    h_eta->Fill((*tc->jet_eta)[0], weight);
    h_eta->Fill((*tc->jet_eta)[1], weight);
    h_phi->Fill((*tc->jet_phi)[0], weight);
    h_phi->Fill((*tc->jet_phi)[1], weight);
    h_eta_phi->Fill((*tc->jet_eta)[0],(*tc->jet_phi)[0], weight);
    h_eta_phi->Fill((*tc->jet_eta)[1],(*tc->jet_phi)[1], weight);
    h_eta_pt->Fill((*tc->jet_eta)[0],(*tc->jet_pt)[0], weight);
    h_eta_pt->Fill((*tc->jet_eta)[1],(*tc->jet_pt)[1], weight);
    h_NumTrkPt500PV->Fill((*tc->jet_NumTrkPt500PV)[0], weight);
    h_SumPtTrkPt500PV->Fill((*tc->jet_SumPtTrkPt500PV)[0], weight);
    h_TrackWidthPt500PV->Fill((*tc->jet_TrackWidthPt500PV)[0], weight);
    h_NumTrkPt1000PV->Fill((*tc->jet_NumTrkPt1000PV)[0], weight);
    h_SumPtTrkPt1000PV->Fill((*tc->jet_SumPtTrkPt1000PV)[0], weight);
    h_TrackWidthPt1000PV->Fill((*tc->jet_TrackWidthPt1000PV)[0], weight);
    
    //binned histograms
    bh_eta_pt->Fill((*tc->jet_eta)[0],(*tc->jet_pt)[0], weight);
    bh_eta_pt->Fill((*tc->jet_eta)[1],(*tc->jet_pt)[1], weight);

    //SumPt
    bh_eta_SumPtTrkPt500PV->Fill((*tc->jet_eta)[0],(*tc->jet_SumPtTrkPt500PV)[0], weight);
    bh_eta_SumPtTrkPt500PV->Fill((*tc->jet_eta)[1],(*tc->jet_SumPtTrkPt500PV)[1], weight);
    bh_pt_SumPtTrkPt500PV->Fill((*tc->jet_pt)[0],(*tc->jet_SumPtTrkPt500PV)[0], weight);
    bh_pt_SumPtTrkPt500PV->Fill((*tc->jet_pt)[1],(*tc->jet_SumPtTrkPt500PV)[1], weight);

    bh_eta_ptOverSumPtTrkPt500PV->Fill((*tc->jet_eta)[0],(*tc->jet_pt)[0]/(*tc->jet_SumPtTrkPt500PV)[0], weight);
    bh_eta_ptOverSumPtTrkPt500PV->Fill((*tc->jet_eta)[1],(*tc->jet_pt)[1]/(*tc->jet_SumPtTrkPt500PV)[1], weight);
    bh_pt_ptOverSumPtTrkPt500PV->Fill((*tc->jet_pt)[0],(*tc->jet_pt)[0]/(*tc->jet_SumPtTrkPt500PV)[0], weight);
    bh_pt_ptOverSumPtTrkPt500PV->Fill((*tc->jet_pt)[1],(*tc->jet_pt)[1]/(*tc->jet_SumPtTrkPt500PV)[1], weight);

    //NumTrk
    bh_eta_NumTrkPt500PV->Fill((*tc->jet_eta)[0],(*tc->jet_NumTrkPt500PV)[0], weight);
    bh_eta_NumTrkPt500PV->Fill((*tc->jet_eta)[1],(*tc->jet_NumTrkPt500PV)[1], weight);
    bh_pt_NumTrkPt500PV->Fill((*tc->jet_pt)[0],(*tc->jet_NumTrkPt500PV)[0], weight);
    bh_pt_NumTrkPt500PV->Fill((*tc->jet_pt)[1],(*tc->jet_NumTrkPt500PV)[1], weight);

    //TrackWidth
    bh_eta_TrackWidthPt500PV->Fill((*tc->jet_eta)[0],(*tc->jet_TrackWidthPt500PV)[0], weight);
    bh_eta_TrackWidthPt500PV->Fill((*tc->jet_eta)[1],(*tc->jet_TrackWidthPt500PV)[1], weight);
    bh_pt_TrackWidthPt500PV->Fill((*tc->jet_pt)[0],(*tc->jet_TrackWidthPt500PV)[0], weight);
    bh_pt_TrackWidthPt500PV->Fill((*tc->jet_pt)[1],(*tc->jet_TrackWidthPt500PV)[1], weight);
    
    //debug histograms
    h_eta_j0->Fill((*tc->jet_eta)[0], weight);
    h_eta_j1->Fill((*tc->jet_eta)[1], weight);
    h_phi_j0->Fill((*tc->jet_phi)[0], weight);
    h_phi_j1->Fill((*tc->jet_phi)[1], weight);
    h_eta_phi_j0->Fill((*tc->jet_eta)[0],(*tc->jet_phi)[0], weight);
    h_eta_phi_j1->Fill((*tc->jet_eta)[1],(*tc->jet_phi)[1], weight);
    h_eta_pt_j0->Fill((*tc->jet_eta)[0],(*tc->jet_pt)[0], weight);
    h_eta_pt_j1->Fill((*tc->jet_eta)[1],(*tc->jet_pt)[1], weight);
  }

  //******************************************
  //save results to output file
  string outputFileName="";
  if (isMC) {
    outputFileName = "tmp/histograms.mc."+snumber+"."+name;
  } else {
    outputFileName = "tmp/histograms.data."+snumber+"."+name;
  }
  TFile *outputFile=new TFile((outputFileName+".root").c_str(),"recreate");
  
  h_mjj->Write();
  h_pt->Write();
  h_eta->Write();
  h_phi->Write();
  h_eta_phi->Write();
  h_eta_pt->Write();
  h_NumTrkPt500PV->Write();
  h_SumPtTrkPt500PV->Write();
  h_TrackWidthPt500PV->Write();
  h_NumTrkPt1000PV->Write();
  h_SumPtTrkPt1000PV->Write();
  h_TrackWidthPt1000PV->Write();

  bh_eta_pt->Write();
  bh_eta_SumPtTrkPt500PV->Write();
  bh_pt_SumPtTrkPt500PV->Write();
  bh_eta_ptOverSumPtTrkPt500PV->Write();
  bh_pt_ptOverSumPtTrkPt500PV->Write();

  bh_eta_NumTrkPt500PV->Write();
  bh_pt_NumTrkPt500PV->Write();

  bh_eta_TrackWidthPt500PV->Write();
  bh_pt_TrackWidthPt500PV->Write();
  
  h_eta_j0->Write();
  h_eta_j1->Write();
  h_phi_j0->Write();
  h_phi_j1->Write();
  h_eta_phi_j0->Write();
  h_eta_phi_j1->Write();
  h_eta_pt_j0->Write();
  h_eta_pt_j1->Write();
  
  outputFile->Write();
  outputFile->Close();
  delete outputFile;
  
  //******************************************
  delete tree;
  file->Close();
  delete file;

  //******************************************
  time.Stop();
  cout<<process<<"ran over "<<goodevents<<" good events out of "<<allevents<<" ("<<nentries<<") events in "<<time.CpuTime()<<" seconds"<<std::endl;
  return 0;
}

#!/usr/bin/env python

#******************************************
#plotBinnedHistograms: given two 2D input distributions, plot both of them separately in 2D, then plot their x profiles on the same canvas, together with their ratio
#plotBinnedProfileHistograms: given a 2D input histogram, plot it in 2D with its profile overlaid
#compareDistributions: given a series of input histograms (1D or 2D), plot the histograms (1D, flag --compareDistributions) or their profiles (2D, flag --compareProfiles) together on the same canvas, together with their ratio (WRT the first histogram)

#******************************************
#import stuff
import ROOT
import math, os, sys, argparse
import plotUtils
import time

#******************************************
#set ATLAS style
if os.path.isfile(os.path.expanduser('~/RootUtils/AtlasStyle.C')):
    ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
    ROOT.SetAtlasStyle()
    ROOT.set_color_env()
else:
    print '\n***WARNING*** couldn\'t find ATLAS Style'
    #import AtlasStyle
    #AtlasStyle.SetAtlasStyle()

#******************************************
#parse input arguments
parser = argparse.ArgumentParser(description='%prog [options]', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--dataFiles', dest='inputDataFileNames',nargs='+', type=str, default=[], help='input data file names')
parser.add_argument('--MCFiles', dest='inputMCFileNames', nargs='+', type=str, default=[], help='input MC file names')

parser.add_argument('--dataLabels', dest='dataLabels', nargs='+', type=str, default=[], help='input data files labels')
parser.add_argument('--MCLabels', dest='MCLabels', nargs='+', type=str, default=[], help='input MC files labels')

parser.add_argument('--dataLumis', dest='dataLumis', nargs='+', type=str, default=[], help='input data histograms luminosities')
parser.add_argument('--MCLumis', dest='MCLumis', nargs='+', type=str, default=[], help='input MC histograms luminosities')

parser.add_argument('--config', dest='configFileName', default='', help='config file')

parser.add_argument('--tag', dest='tag', default='results', help='tag for output files')
parser.add_argument('--notes', dest='notes', nargs='+', type=str, default=[], help='list of notes to add to the plots')
parser.add_argument('--lumi', dest='lumi', type=float, default=1.0, help='desired luminosity in fb^-1')
parser.add_argument('--wait', dest='wait', action='store_true', default=False, help='wait?')
parser.add_argument('--save', dest='save', action='store_true', default=False, help='save plots?')

parser.add_argument('-b', '--batch', dest='batch', action='store_true', default=False, help='batch mode for PyRoot')
parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', default=False, help='verbose mode for debugging')

#******************************************
def getAxesLabels(hist):
    #given a histogram name using the convention 'x_y_z_anything_else', retrieve axes labels
    #FIX return raw labels if no labal is available from the dictionary
    
    #define labels dictionary
    labelsDict = {}
    labelsDict['pt'] = 'p_T [GeV]'
    labelsDict['pT'] = 'p_T [GeV]'
    labelsDict['Pt'] = 'p_T [GeV]'
    labelsDict['eta'] = '#eta'
    labelsDict['phi'] = '#phi'
    labelsDict['mjj'] = 'm_{jj} [GeV]'
    labelsDict['SumPtTrkPt500PV'] = '#sump_{T}^{track} [GeV]'
    labelsDict['NumTrkPt500PV'] = 'n_{tracks}'
    labelsDict['TrackWidthPt500PV'] = 'track width'
    labelsDict['ptOverSumPtTrkPt500PV'] = 'p_{T} / #sump_{T}^{track}'
    #labelsDict[''] = ''
    
    #get raw labels
    histName = hist.GetName()
    rawLabels = histName.split('_')
    #print '\nraw labels (%s) = %s'%(str(type(rawLabels)), rawLabels)
    
    #get labels
    labels = []
    if 'TH1' in str( type(hist) ):
        #print 'this is a TH1, x axis label: %s'%', '.join(rawLabels)
        labels.append( labelsDict.get(rawLabels[0],'') )
    elif 'TH2' in str( type(hist) ):
        #print 'this is a TH2, axes label: %s'%', '.join(rawLabels)
        labels.append( labelsDict.get(rawLabels[0],'') )
        labels.append( labelsDict.get(rawLabels[1],'') )
    else:
        print '\n***WARNING*** \'%s\' format is not supported (%s)'%(type(hist), histName)

    #check labels
    for ii in range( len(labels) ):
        if len(labels[ii]) <= 0:
            print '\n***WARNING*** could not find label for \'%s\' in %s histogram, using label from histogram name instead: \'%s\''%(rawLabels[ii], histName, rawLabels[ii])
            labels[ii] = rawLabels[ii]
    
    return labels

#******************************************
def getAxesLog(hist):
    #given an histogram, return a flag to set axis/axes logarithmic
    
    #define logs dictionary
    logsDict = {}
    logsDict['pt'] = True
    logsDict['pT'] = True
    logsDict['Pt'] = True
    logsDict['eta'] = True
    logsDict['phi'] = False
    logsDict['mjj'] = True
    logsDict['SumPtTrkPt500PV'] = True
    logsDict['NumTrkPt500PV'] = False
    logsDict['TrackWidthPt500PV'] = False
    logsDict['ptOverSumPtTrkPt500PV'] = False
    
    #get raw logs
    histName = hist.GetName()
    rawLogs = histName.split('_')

    #get logs
    logs = []
    if 'TH1' in str( type(hist) ):
        logs.append( labelsDict.get(rawLogs[0],'') )
    elif 'TH2' in str( type(hist) ):
        logs.append( labelsDict.get(rawLogs[0],'') )
        logs.append( labelsDict.get(rawLogs[1],'') )
    else:
        print '\n***WARNING*** \'%s\' format is not supported (%s)'%(type(hist), histName)

    #check logs
    for ii in range( len(logs) ):
        if logs[ii] <= 0:
            print '\n***WARNING*** could not find log for \'%s\' in %s'%(rawLogs[ii], histName)
    
    return logs

#******************************************
def test(args):

    print '\n******************************************'
    print 'test'

    #------------------------------------------
    #input parameters
    print '\ninput parameters:'
    argsdict = vars(args)
    for ii in xrange(len(argsdict)):
        print '  %s = %s'%(argsdict.keys()[ii], argsdict.values()[ii],)
    
    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #define useful variables

    #combine data and MC info
    inputFileNames = args.inputDataFileNames + args.inputMCFileNames
    inputFileLabels = args.dataLabels + args.MCLabels
    inputFileIsMC = [False for ii in range( len(args.inputDataFileNames) )]
    inputFileIsMC += [True for ii in range( len(args.inputMCFileNames) )]
    print '\nare input files MC? %s'%inputFileIsMC

    #other
    lumi = float(args.lumi) #fb^-1
    colors = []
    colors.append(ROOT.kGreen+1)
    colors.append(ROOT.kCyan+1)
    colors.append(ROOT.kRed+1)
    colors.append(ROOT.kOrange+1)
    colors.append(ROOT.kBlue+1)

    #------------------------------------------
    #read settings from config file
    print
    settings = ROOT.TEnv()
    if (args.configFileName != ""):
        #print 'config file = %s'%args.configFileName
        status = settings.ReadFile(args.configFileName,ROOT.EEnvLevel(0))
        if status != 0:
            raise SystemExit('\n***ERROR*** couldn\'t read config file')
    else:
        raise SystemExit('\n***ERROR*** no config file')
    
    inputHistNames = settings.GetValue('inputHistNames','')
    inputHistNames = inputHistNames.split(' ')
    print 'input histogram names = %s'%', '.join(inputHistNames)
  
    #------------------------------------------
    #open input files
    fs = []
    for inputFileName in inputFileNames:
        if len(inputFileName) == 0:
            raise SystemExit('\n***ERROR*** no input file name')
        if not os.path.isfile(inputFileName):
            raise SystemExit('\n***ERROR*** couldn\'t find input file')
        fs.append(ROOT.TFile(inputFileName))

    print '\nfiles:'
    for ii in range(len(fs)):
        print '  %s'%fs[ii].GetName()
        
    #------------------------------------------
    #get input histograms
    print '\nhistograms:'
    
    #build a dictionary of hist label and histogram array (one per input file)
    histDict = {}

    #retrieve input histograms from each input file

    #loop over input histogram names
    for inputHistName in inputHistNames:

        hists = []
        
        #loop over input files (all of them: data and MC)
        for f in fs:
        
            if not f.Get(inputHistName).GetName():
                raise SystemExit('\n***ERROR*** couldn\'t find input histogram \'%s\' in file '%(inputHistName, f.GetName()))
            hists.append( f.Get(inputHistName) )

        print '  %s'%hists
        histDict[hists[0].GetName()] = hists

    print '\nhistograms dictionary:\n%s'%histDict

    #------------------------------------------
    #get histograms labels
    print
    for histKey in histDict.keys():
        labels = getAxesLabels(histDict[histKey][0] )
        print 'histogram %s has labels: %s'%(histKey, labels)

#******************************************
if __name__ == '__main__':
    args = parser.parse_args()
    test(args)
                
    print '\n******************************************'
    print 'done'
